import { transliterate as translit } from 'transliteration';

export function transliterate(string: string): string {
    return translit(string);
}
