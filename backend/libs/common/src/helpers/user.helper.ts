import { JwtPayload } from '@/auth/interfaces';
import { User } from '@prisma/postgres/client';

export function getUserName(user: User | JwtPayload): string {
    const last_name = user.last_name ? user.last_name : undefined;
    const first_name = user.first_name ? user.first_name : undefined;
    const middle_name = user.middle_name ? user.middle_name : undefined;
    const email = user.email;
    const result: string[] = [];
    if (!last_name) {
        return email;
    }
    if (last_name) {
        result.push(last_name);
    }
    if (first_name) {
        result.push(first_name.slice(0, 1) + '.');
    }
    if (middle_name) {
        result.push(middle_name.slice(0, 1) + '.');
    }
    return result.join(' ');
}
