import { JwtPayload } from '@/auth/interfaces';
import { ExecutionContext, createParamDecorator } from '@nestjs/common';

export const CurrentCompany = createParamDecorator(
    (key: keyof JwtPayload, ctx: ExecutionContext): JwtPayload | Partial<JwtPayload> => {
        const request = ctx.switchToHttp().getRequest();
        const ApiKey = request.headers['x-api-key'] as string;
        if (!ApiKey) {
            return null;
        }

        return request.current_company ? request.current_company : null;
    },
);
