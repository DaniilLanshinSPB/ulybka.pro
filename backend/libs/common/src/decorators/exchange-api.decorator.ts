import { ExecutionContext, SetMetadata } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

export const EXCHANGE_API = 'exchange';
export const ExchangeApi = () => SetMetadata(EXCHANGE_API, true);

export const isExchange = (ctx: ExecutionContext, reflector: Reflector) => {
    const isPublic = reflector.getAllAndOverride<boolean>(EXCHANGE_API, [ctx.getHandler(), ctx.getClass()]);
    return isPublic;
};
