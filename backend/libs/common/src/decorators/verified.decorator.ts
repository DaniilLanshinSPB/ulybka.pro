import { ExecutionContext, SetMetadata } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

export const NOT_VERIFIED_KEY = 'verified';
export const NotVerified = () => SetMetadata(NOT_VERIFIED_KEY, true);

export const isNotVerified = (ctx: ExecutionContext, reflector: Reflector) => {
    const isNotVerified = reflector.getAllAndOverride<boolean>(NOT_VERIFIED_KEY, [ctx.getHandler(), ctx.getClass()]);

    return isNotVerified;
};
