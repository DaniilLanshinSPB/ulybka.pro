import { CabinetPermissions, SystemPermissions } from '@/auth/interfaces';
import { SetMetadata } from '@nestjs/common';

export const PERMISSION_KEY = 'permissions';
export const Permissions = (...permissions: SystemPermissions[] | CabinetPermissions[]) =>
    SetMetadata(PERMISSION_KEY, permissions);
