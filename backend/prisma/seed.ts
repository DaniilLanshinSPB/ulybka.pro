import { LogType, PrismaClient, RoleNoticeType, UserNoticeType } from '@prisma/postgres/client';
import { permissionsDto } from '../src/auth/interfaces';

const prisma = new PrismaClient();

async function main() {
    generateCompanyTypes();
    generateRoles();
    generatePermissions();
    generateOrderStatuses();
    generateServiceCategory();
    regenerateFieldsTechnicalName();
    generateNoticeTypes();
}

const generateRoles = async () => {
    const entities = [
        {
            id: 1,
            slug: 'SuperAdmin',
            name: 'Root',
            is_default: true,
            company_type_id: null,
            is_root: true,
        },
        {
            id: 2,
            slug: 'Admin',
            name: 'Администратор',
            is_default: true,
            company_type_id: 1,
            is_root: false,
        },
        {
            id: 3,
            slug: 'MainTechnic',
            name: 'Главный техник',
            is_default: false,
            company_type_id: 1,
            is_root: false,
        },
        {
            id: 4,
            slug: 'Technic',
            name: 'Техник',
            is_default: false,
            company_type_id: 1,
            is_root: false,
        },
        {
            id: 5,
            slug: 'ClinicAdmin',
            name: 'Администратор клиники',
            company_type_id: 2,
            is_default: false,
            is_root: false,
        },
        {
            id: 6,
            slug: 'Doctor',
            name: 'Врач',
            company_type_id: 2,
            is_default: false,
            is_root: false,
        },
    ];

    const existing = await prisma.role.findMany({
        where: {
            id: {
                in: entities.map((entity) => entity.id),
            },
        },
    });
    const existingIds = existing.map((entity) => entity.id);
    const toCreate = entities.filter((entity) => !existingIds.includes(entity.id));

    if (toCreate.length > 0) {
        await prisma.role.createMany({
            data: toCreate,
        });
        console.log(`Created ${toCreate.length} new roles`);
    } else {
        console.log('All roles already exist');
    }
};

const generateOrderStatuses = async () => {
    const entities = [
        {
            id: 1,
            sort: 0,
            slug: 'new',
            name: 'Новый заказ',
        },
        {
            id: 2,
            sort: 10000,
            slug: 'finished',
            name: 'Завершен',
        },
    ];

    const existing = await prisma.orderStatus.findMany({
        where: {
            id: {
                in: entities.map((entity) => entity.id),
            },
        },
    });
    const existingIds = existing.map((entity) => entity.id);
    const toCreate = entities.filter((entity) => !existingIds.includes(entity.id));

    if (toCreate.length > 0) {
        await prisma.orderStatus.createMany({
            data: toCreate,
        });
        console.log(`Created ${toCreate.length} new statuses`);
    } else {
        console.log('All statuses already exist');
    }
};

const generateCompanyTypes = async () => {
    const entities = [
        {
            id: 1,
            title: 'Лаборатория',
        },
        {
            id: 2,
            title: 'Клиника',
        },
    ];

    const existing = await prisma.companyType.findMany({
        where: {
            id: {
                in: entities.map((entity) => entity.id),
            },
        },
    });
    const existingIds = existing.map((entity) => entity.id);
    const toCreate = entities.filter((entity) => !existingIds.includes(entity.id));

    if (toCreate.length > 0) {
        await prisma.companyType.createMany({
            data: toCreate,
        });
        console.log(`Created ${toCreate.length} new company types`);
    } else {
        console.log('All company types already exist');
    }
};

const generateServiceCategory = async () => {
    const entities = [
        {
            id: 1,
            title: 'Ортопедия',
        },
        {
            id: 2,
            title: 'Ортодонтия',
        },
    ];

    const existing = await prisma.serviceCategory.findMany({
        where: {
            id: {
                in: entities.map((entity) => entity.id),
            },
        },
    });
    const existingIds = existing.map((entity) => entity.id);
    const toCreate = entities.filter((entity) => !existingIds.includes(entity.id));

    if (toCreate.length > 0) {
        await prisma.serviceCategory.createMany({
            data: toCreate,
        });
        console.log(`Created ${toCreate.length} new service categories`);
    } else {
        console.log('All service categories already exist');
    }
};
const regenerateFieldsTechnicalName = async () => {
    const fields = await prisma.field.findMany();
    let count = 0;
    fields.forEach(async (field) => {
        if (!field.technical_name || field.technical_name === '') {
            await prisma.field.update({
                where: {
                    id: field.id,
                },
                data: {
                    technical_name: field.name,
                },
            });
            count++;
        }
    });
    console.log(`Technical name regenerated for ${count} fields`);
};

const generatePermissions = async () => {
    const entitiesData = permissionsDto;
    const existing = await prisma.permissions.findMany({
        where: {
            slug: {
                in: entitiesData.map((entity) => entity.slug),
            },
        },
    });
    const existingSlugs = existing.map((entity) => entity.slug);

    const toCreate = entitiesData.filter((entity) => !existingSlugs.includes(entity.slug));
    if (toCreate.length > 0) {
        await prisma.permissions.createMany({
            data: toCreate,
        });
        console.log(`Created ${toCreate.length} new permissions`);
    } else {
        console.log('All permissions already exist');
    }
};

const generateNoticeTypes = async () => {
    const entities = [
        {
            id: 1,
            slug: LogType.COMMENT,
            name: 'Комментарии',
            active: true,
        },
        {
            id: 2,
            slug: LogType.ORDER,
            name: 'Заказы',
            active: true,
        },
        {
            id: 3,
            slug: LogType.PATIENT,
            name: 'Пациенты',
            active: true,
        },
        {
            id: 4,
            slug: LogType.SYSTEM,
            name: 'Системные',
            active: true,
        },
        {
            id: 5,
            slug: LogType.TELEGRAM,
            name: 'Телеграм',
            active: true,
        },
    ];

    const existing = await prisma.noticeType.findMany({
        where: {
            id: {
                in: entities.map((entity) => entity.id),
            },
        },
    });

    const existingIds = existing.map((entity) => entity.id);
    const toCreate = entities.filter((entity) => !existingIds.includes(entity.id));

    if (toCreate.length > 0) {
        for (const data of toCreate) {
            await prisma.noticeType.create({ data });
        }
        console.log(`Created ${toCreate.length} new notice types`);
    } else {
        console.log('All notice types already exist');
    }

    const roleNoticeTypes: RoleNoticeType[] = [];
    const userNoticeTypes: UserNoticeType[] = [];

    const roles = await prisma.role.findMany();
    const users = await prisma.user.findMany();
    for (const role of roles) {
        for (const type of entities) {
            const existingRoleNoticeTypes = await prisma.roleNoticeType.findFirst({
                where: {
                    role_id: role.id,
                    notice_type_id: type.id,
                },
            });
            if (!existingRoleNoticeTypes) {
                roleNoticeTypes.push({
                    role_id: role.id,
                    notice_type_id: type.id,
                    active: false,
                });
            }
        }
    }
    const toCreateRoleNoticeTypes = roleNoticeTypes.length;
    if (toCreateRoleNoticeTypes) {
        await prisma.roleNoticeType.createMany({ data: roleNoticeTypes });
        console.log(`Created ${toCreateRoleNoticeTypes} new role notice types`);
    }

    for (const user of users) {
        for (const type of entities) {
            if (type.slug !== LogType.SYSTEM) {
                const existingRoleNoticeTypes = await prisma.userNoticeType.findFirst({
                    where: {
                        user_id: user.id,
                        notice_type_id: type.id,
                    },
                });
                if (!existingRoleNoticeTypes) {
                    userNoticeTypes.push({
                        user_id: user.id,
                        notice_type_id: type.id,
                        active: false,
                    });
                }
            }
        }
    }
    const toCreateUserNoticeTypes = userNoticeTypes.length;
    if (toCreateUserNoticeTypes) {
        await prisma.userNoticeType.createMany({ data: userNoticeTypes });
        console.log(`Created ${toCreateUserNoticeTypes} new user notice types`);
    }
};

main()
    .catch((e) => {
        console.error(e);
        process.exit(1);
    })
    .finally(async () => {
        await prisma.$disconnect();
    });
