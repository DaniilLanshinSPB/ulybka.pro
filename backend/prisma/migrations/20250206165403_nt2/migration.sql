/*
  Warnings:

  - You are about to alter the column `slug` on the `notices_types` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Enum(EnumId(5))`.

*/
-- AlterTable
ALTER TABLE `notices_types` MODIFY `slug` ENUM('SYSTEM', 'COMMENT', 'ORDER', 'PATIENT') NOT NULL;
