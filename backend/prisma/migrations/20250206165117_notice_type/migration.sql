/*
  Warnings:

  - The values [NOTICE] on the enum `notices_types_slug` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterTable
ALTER TABLE `logs` MODIFY `entity_type` ENUM('User', 'Status', 'Permissions', 'RolePermissions', 'Role', 'Company', 'Clinic', 'CompanyType', 'Directory', 'Patient', 'Order', 'OrderComment', 'Field', 'Service', 'CompanyService', 'ServiceField') NOT NULL,
    MODIFY `entity_id` VARCHAR(191) NOT NULL,
    MODIFY `type` ENUM('SYSTEM', 'COMMENT', 'ORDER', 'PATIENT') NOT NULL;

-- CreateTable
CREATE TABLE `notices_types` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `slug` ENUM('SYSTEM', 'COMMENT', 'ORDER', 'PATIENT') NOT NULL,
    `name` VARCHAR(191) NOT NULL,
    `active` BOOLEAN NOT NULL DEFAULT true,

    UNIQUE INDEX `notices_types_slug_key`(`slug`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
