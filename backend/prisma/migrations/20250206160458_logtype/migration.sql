/*
  Warnings:

  - Added the required column `type` to the `logs` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `logs` ADD COLUMN `type` ENUM('SYSTEM', 'NOTICE') NOT NULL;
