/*
  Warnings:

  - Added the required column `crypto` to the `webhooks` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `webhooks` ADD COLUMN `crypto` VARCHAR(191) NOT NULL;
