-- CreateTable
CREATE TABLE `role_notice_type` (
    `role_id` INTEGER NOT NULL,
    `notice_type_id` INTEGER NOT NULL,

    PRIMARY KEY (`role_id`, `notice_type_id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `user_notice_type` (
    `user_id` INTEGER NOT NULL,
    `notice_type_id` INTEGER NOT NULL,

    PRIMARY KEY (`user_id`, `notice_type_id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `notifications` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `type_id` INTEGER NOT NULL,
    `is_read` BOOLEAN NOT NULL,
    `message` TEXT NOT NULL DEFAULT '',
    `metadata` JSON NOT NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `role_notice_type` ADD CONSTRAINT `role_notice_type_role_id_fkey` FOREIGN KEY (`role_id`) REFERENCES `roles`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `role_notice_type` ADD CONSTRAINT `role_notice_type_notice_type_id_fkey` FOREIGN KEY (`notice_type_id`) REFERENCES `notices_types`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `user_notice_type` ADD CONSTRAINT `user_notice_type_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `user_notice_type` ADD CONSTRAINT `user_notice_type_notice_type_id_fkey` FOREIGN KEY (`notice_type_id`) REFERENCES `notices_types`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `notifications` ADD CONSTRAINT `notifications_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `notifications` ADD CONSTRAINT `notifications_type_id_fkey` FOREIGN KEY (`type_id`) REFERENCES `notices_types`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
