/*
  Warnings:

  - You are about to alter the column `slug` on the `notices_types` table. The data in that column could be lost. The data in that column will be cast from `Enum(EnumId(0))` to `VarChar(255)`.

*/
-- DropIndex
DROP INDEX `notices_types_slug_key` ON `notices_types`;

-- AlterTable
ALTER TABLE `logs` MODIFY `type` ENUM('SYSTEM', 'COMMENT', 'ORDER', 'PATIENT', 'TELEGRAM') NOT NULL;

-- AlterTable
ALTER TABLE `notices_types` MODIFY `slug` VARCHAR(255) NOT NULL;
