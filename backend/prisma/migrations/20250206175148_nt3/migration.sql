/*
  Warnings:

  - Added the required column `active` to the `user_notice_type` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `user_notice_type` ADD COLUMN `active` BOOLEAN NOT NULL;
