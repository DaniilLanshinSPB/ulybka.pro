/*
  Warnings:

  - You are about to alter the column `slug` on the `notices_types` table. The data in that column could be lost. The data in that column will be cast from `Enum(EnumId(0))` to `VarChar(191)`.

*/
-- AlterTable
ALTER TABLE `notices_types` MODIFY `slug` VARCHAR(191) NOT NULL;
