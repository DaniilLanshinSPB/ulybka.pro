import { ApiProperty } from '@nestjs/swagger';

export class UserFilterDto {
    @ApiProperty({ name: 'search', type: String })
    search: string;
    @ApiProperty({ name: 'is_active', type: Boolean })
    is_active: boolean;
    @ApiProperty({ name: 'email_verified', type: Boolean })
    email_verified: boolean;
    @ApiProperty({ name: 'role_id', type: [Number] })
    role_ids: number[];
    @ApiProperty({ name: 'company_id', type: Number })
    company_id: number;
}
