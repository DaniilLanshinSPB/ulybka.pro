import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MinLength } from 'class-validator';

export class ChangePasswordDto {
    @IsString()
    @MinLength(6)
    @IsNotEmpty()
    @ApiProperty({ description: 'Пароль пользователя' })
    password: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'Повтор пароля пользователя' })
    confirm_password: string;
}
