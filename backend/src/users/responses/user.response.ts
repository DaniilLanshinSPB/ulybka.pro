import { ApiProperty } from '@nestjs/swagger';
import { Company, Prisma, Role, User } from '@prisma/postgres/client';
import { Exclude } from 'class-transformer';

export class UserResponse implements User {
    @ApiProperty({ description: 'Идентификатор пользователя' })
    id: number;

    @ApiProperty({ description: 'The is_active status of the user' })
    is_active: boolean;

    @Exclude()
    role_id: number;

    @ApiProperty({ description: 'The role id of the user' })
    role: Role;

    @ApiProperty({ description: 'The email of the user' })
    email: string;

    @Exclude()
    password: string;

    @ApiProperty({ description: 'The avatar of the user' })
    avatar: string;

    @ApiProperty({ description: 'The first name of the user' })
    first_name: string;

    @ApiProperty({ description: 'The last name of the user' })
    last_name: string;

    @ApiProperty({ description: 'The middle name of the user' })
    middle_name: string;

    @Exclude()
    telegram_id: string | null;

    @Exclude()
    telegram_key: string;

    @Exclude()
    telegram_username: string;

    @ApiProperty({ description: 'The email verification status of the user' })
    email_verified: boolean;

    @Exclude()
    company_id: number;

    @ApiProperty({ description: 'Компания' })
    company: Company;

    @Exclude()
    @ApiProperty({ description: 'The last login date of the user' })
    last_login: Date;

    @Exclude()
    login_attempts: number;

    @Exclude()
    last_failed_login: Date;

    @Exclude()
    verified_code: string;

    @ApiProperty({ description: 'The metadata of the user' })
    metadata: Prisma.JsonValue;

    created_at: Date;

    @Exclude()
    updated_at: Date;

    deleted_at: Date;

    constructor(user: User) {
        Object.assign(this, user);
    }
}
