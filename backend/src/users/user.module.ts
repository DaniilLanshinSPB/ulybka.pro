import { MailModule } from '@/mail/mail.module';
import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';

import { LogsModule } from '@/logs/logs.module';
import { NoticeModule } from '@/notice/notice.module';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
    providers: [UserService],
    exports: [UserService],
    controllers: [UserController],

    imports: [MailModule, CacheModule.register(), LogsModule, NoticeModule],
})
export class UserModule {}
