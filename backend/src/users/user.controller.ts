import { CurrentUser, Permissions } from '@common/decorators';
import { getUserName } from '@common/helpers/user.helper';
import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiExcludeController, ApiOperation, ApiParam, ApiResponse } from '@nestjs/swagger';
import { EntityType, LogType } from '@prisma/postgres/client';

import { NotVerifiedGuard } from '@/auth/guards/not_verified.guard';
import { PermissionsGuard } from '@/auth/guards/permission.guard';
import { JwtPayload, SystemPermissions } from '@/auth/interfaces';
import { LogsService } from '@/logs/logs.service';
import { ChangePasswordDto } from './dto/change-password.dto';
import { CreateUserDto } from './dto/create.dto';
import { UserFilterDto } from './dto/filter.dto';
import { UpdateUserDto } from './dto/update.dto';
import { UserResponse } from './responses';
import { UserService } from './user.service';

@ApiExcludeController()
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(PermissionsGuard)
@UseGuards(NotVerifiedGuard)
@Controller('user')
export class UserController {
    constructor(protected readonly userService: UserService, protected readonly logsService: LogsService) {}

    @Post('all')
    @Permissions(SystemPermissions.SYSTEM_VIEW_USER)
    @ApiBody({ type: UserFilterDto })
    @ApiOperation({ summary: 'Получить всех пользователй' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о пользователях', type: [UserResponse] })
    async findAll(@CurrentUser() currentUser: JwtPayload, @Body() body: UserFilterDto) {
        const { company_id: filter_company_id, ...filter } = body;
        const company_id = currentUser.company
            ? Number(currentUser.company)
            : filter_company_id
            ? filter_company_id
            : null;

        const users = await this.userService.findMany(company_id, filter);
        return users.map((user) => new UserResponse(user));
    }

    @Get(':idOrEmail')
    @ApiParam({ name: 'idOrEmail', type: String })
    @Permissions(SystemPermissions.SYSTEM_VIEW_USER)
    @ApiOperation({ summary: 'Получить информацию о пользователе по ID или email' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о пользователе', type: UserResponse })
    async findOneUser(@CurrentUser() currentUser: JwtPayload, @Param('idOrEmail') idOrEmail: string | number) {
        const company_id = currentUser.company ? Number(currentUser.company) : null;
        const foundUser = await this.userService.findOne(idOrEmail, company_id);
        return new UserResponse(foundUser);
    }

    @Delete(':id')
    @ApiParam({ name: 'id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_DELETE_USER)
    @ApiOperation({ summary: 'Удалить пользователя по ID' })
    @ApiResponse({ status: 200, description: 'Пользователь успешно удален' })
    @ApiResponse({ status: 400, description: 'Пользователь не найден' })
    async deleteUser(@CurrentUser() currentUser: JwtPayload, @Param('id', ParseIntPipe) id: number) {
        const company_id = currentUser.company ? Number(currentUser.company) : null;
        const user = await this.userService.findOne(id, company_id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.User,
            entity_id: user.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} удалил пользователя ${user.email} для компании ${
                user.company.title
            }`,
            type: LogType.SYSTEM,
            metadata: user,
        });
        return this.userService.delete(id, company_id);
    }

    @Post()
    @ApiBody({ type: CreateUserDto })
    @Permissions(SystemPermissions.SYSTEM_CREATE_USER)
    @ApiOperation({ summary: 'Создание пользователя' })
    @ApiResponse({ status: 200, description: 'Пользователь успешно создан' })
    async createUser(@CurrentUser() currentUser: JwtPayload, @Body() dto: CreateUserDto) {
        const company_id = currentUser.company ? Number(currentUser.company) : null;
        const newUser = await this.userService.create(dto, company_id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.User,
            entity_id: newUser.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} добавил нового пользователя ${
                newUser.email
            } для компнии ${newUser.company.title}`,
            type: LogType.SYSTEM,
            metadata: newUser,
        });
        return new UserResponse(newUser);
    }

    @Patch(':id')
    @ApiParam({ name: 'id', type: Number })
    @ApiBody({ type: UpdateUserDto })
    @Permissions(SystemPermissions.SYSTEM_UPDATE_USER)
    @ApiOperation({ summary: 'Обновление пользователя' })
    @ApiResponse({ status: 200, description: 'Пользователь успешно обновлен' })
    async updateUser(
        @CurrentUser() currentUser: JwtPayload,
        @Param('id', ParseIntPipe) id: number,
        @Body() dto: UpdateUserDto,
    ) {
        const company_id = currentUser.company ? Number(currentUser.company) : null;
        const updatedUser = await this.userService.update(id, dto, company_id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.User,
            entity_id: updatedUser.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} добавил нового пользователя ${
                updatedUser.email
            } для компнии ${updatedUser.company.title}`,
            type: LogType.SYSTEM,
            metadata: updatedUser,
        });
        return new UserResponse(updatedUser);
    }

    @Patch(':id/verify')
    @ApiParam({ name: 'id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_UPDATE_USER)
    @ApiOperation({ summary: 'Верификация пользователя' })
    @ApiResponse({ status: 200, description: 'Пользователь ферифицирован' })
    async verifyUser(@Param('id', ParseIntPipe) id: number, @CurrentUser() user: JwtPayload) {
        const company_id = user.company ? Number(user.company) : null;
        const changedUser = await this.userService.verifyUser(id, company_id);
        return new UserResponse(changedUser);
    }

    @Patch(':id/change-password')
    @ApiParam({ name: 'id', type: Number })
    @ApiBody({ type: ChangePasswordDto })
    @Permissions(SystemPermissions.SYSTEM_UPDATE_USER)
    @ApiOperation({ summary: 'Обновление пароля пользователя' })
    @ApiResponse({ status: 200, description: 'Обновление пароля пользователя' })
    async changePassword(
        @CurrentUser() currentUser: JwtPayload,
        @Param('id', ParseIntPipe) id: number,
        @Body() dto: ChangePasswordDto,
    ) {
        const company_id = currentUser.company ? Number(currentUser.company) : null;
        const changedUser = await this.userService.changePassword(id, dto, company_id);
        return new UserResponse(changedUser);
    }
}
