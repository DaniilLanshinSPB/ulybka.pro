import { generateVerifyCode } from '@common/helpers';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { HttpException, HttpStatus, Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Company, User } from '@prisma/postgres/client';
import { genSaltSync, hashSync } from 'bcrypt';
import { Cache } from 'cache-manager';
import { addMinutes, isAfter } from 'date-fns';

import { CabinetPermissions, SystemPermissions, UserInterface } from '@/auth/interfaces';
import { PrismaService } from '@/database/postgres-prisma.service';
import { MailService } from '@/mail/mail.service';
import { NoticeService } from '@/notice/notice.service';
import { ChangePasswordDto } from './dto/change-password.dto';

@Injectable()
export class UserService {
    protected readonly logger = new Logger(UserService.name);

    constructor(
        protected readonly prismaService: PrismaService,
        protected readonly mailService: MailService,
        protected readonly configService: ConfigService,
        protected readonly noticeService: NoticeService,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
    ) {}

    async create(user: Partial<User>, company_id: number | null = null) {
        const _user = await this.prismaService.user.findFirst({
            where: {
                email: user.email,
            },
        });

        if (_user) {
            throw new HttpException('Пользователь с таким email уже существует', HttpStatus.BAD_REQUEST);
        }

        const hashedPassword = user?.password ? this.hashPassword(user.password) : null;
        let verify_code = '';
        try {
            verify_code = await this.sendVerificationCode(user.email);
        } catch (e) {
            throw new HttpException('Не удалось отправить код подтверждения', 420);
        }
        try {
            const userCompanyId = company_id ? company_id : user?.company_id;
            const savedUser = await this.prismaService.user.create({
                data: {
                    is_active: user?.is_active || true,
                    email: user?.email,
                    first_name: user?.first_name || undefined,
                    last_name: user?.last_name || undefined,
                    middle_name: user?.middle_name || undefined,
                    email_verified: user?.email_verified || false,
                    avatar: user.avatar,
                    password: hashedPassword,

                    role_id: user?.role_id || 2,
                    company_id: userCompanyId,
                    verified_code: verify_code,

                    metadata: user?.metadata || {},
                },
                include: {
                    role: true,
                    company: {
                        include: {
                            type: true,
                        },
                    },
                    user_notice_type: {
                        include: {
                            notice_type: true,
                        },
                    },
                },
            });
            const role_notice_permissions = await this.noticeService.findAccessUserTypes(user.role_id);
            const user_notice_permissions = await this.noticeService.findUserNotificationPermissions(user.id);
            const user_notice_permissions_by_slug = user_notice_permissions.reduce((acc, item) => {
                acc[item.slug] = item.active;
                return acc;
            }, {});
            const notice_permissions = [];
            role_notice_permissions.forEach((el) => {
                notice_permissions.push({
                    slug: el.slug,
                    active: user_notice_permissions_by_slug[el.slug],
                });
            });
            return {
                ...savedUser,
                user_notice_permissions: notice_permissions,
            };
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    async update(id: number, user: Partial<User>, company_id: number | null = null) {
        const whereCondition: any = {
            id: Number(id),
        };
        if (company_id) {
            whereCondition.company_id = company_id;
        }
        const _user = await this.prismaService.user.findFirst({
            where: whereCondition,
        });

        if (!_user) {
            throw new HttpException('Пользователь не найден', HttpStatus.BAD_REQUEST);
        }

        const savedUser = await this.prismaService.user.update({
            where: {
                id: Number(id),
            },
            data: {
                is_active: user?.is_active || true,
                email: user?.email,
                first_name: user?.first_name || undefined,
                last_name: user?.last_name || undefined,
                middle_name: user?.middle_name || undefined,
                avatar: user.avatar,
                role_id: user?.role_id || 3,
                company_id: user?.company_id,
                email_verified: user?.email_verified || false,
                verified_code: user.verified_code || undefined,
                login_attempts: user.login_attempts || 0,
                last_failed_login: user.last_failed_login || null,
                metadata: user?.metadata || {},
                telegram_id: user?.telegram_id || null,
                telegram_key: user?.telegram_key || null,
                telegram_username: user?.telegram_username || null,
            },
            include: {
                role: true,
                company: {
                    include: {
                        type: true,
                    },
                },
                user_notice_type: {
                    include: {
                        notice_type: true,
                    },
                },
            },
        });
        const role_notice_permissions = await this.noticeService.findAccessUserTypes(user.role_id);
        const user_notice_permissions = await this.noticeService.findUserNotificationPermissions(user.id);
        const user_notice_permissions_by_slug = user_notice_permissions.reduce((acc, item) => {
            acc[item.slug] = item.active;
            return acc;
        }, {});
        const notice_permissions = [];
        role_notice_permissions.forEach((el) => {
            notice_permissions.push({
                slug: el.slug,
                active: user_notice_permissions_by_slug[el.slug],
            });
        });
        return {
            ...savedUser,
            user_notice_permissions: notice_permissions,
        };
    }

    async changePassword(id: number, dto: ChangePasswordDto, company_id: number | null = null) {
        const whereCondition: any = {
            id: Number(id),
        };
        if (company_id) {
            whereCondition.company_id = company_id;
        }
        const user = await this.prismaService.user.findUnique({
            where: whereCondition,
        });
        if (!user) {
            throw new HttpException('Пользователь не найден', HttpStatus.BAD_REQUEST);
        }
        if (dto.password !== dto.confirm_password) {
            throw new HttpException('Пароли не совпадают', HttpStatus.BAD_REQUEST);
        }
        const hashedPassword = user?.password ? this.hashPassword(dto.password) : null;
        try {
            return await this.prismaService.user.update({
                where: {
                    id: Number(id),
                },
                data: {
                    password: hashedPassword,
                },
            });
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    async verifyUser(id: number, company_id: number | null = null): Promise<UserInterface> {
        const _user = await this.findOne(id, company_id);

        if (!_user) {
            throw new HttpException('Пользователь не найден', 420);
        }
        if (_user.email_verified) {
            throw new HttpException('Пользователь уже верифицирован', 420);
        }
        const user = await this.prismaService.user.update({
            where: {
                id: Number(id),
            },
            data: {
                email_verified: true,
                verified_code: '',
            },
            include: {
                role: true,
                company: {
                    include: {
                        type: true,
                    },
                },
                user_notice_type: {
                    include: {
                        notice_type: true,
                    },
                },
            },
        });
        const role_notice_permissions = await this.noticeService.findAccessUserTypes(user.role_id);
        const user_notice_permissions = await this.noticeService.findUserNotificationPermissions(user.id);
        const user_notice_permissions_by_slug = user_notice_permissions.reduce((acc, item) => {
            acc[item.slug] = item.active;
            return acc;
        }, {});
        const notice_permissions = [];
        role_notice_permissions.forEach((el) => {
            notice_permissions.push({
                slug: el.slug,
                active: user_notice_permissions_by_slug[el.slug],
            });
        });
        return {
            ...user,
            user_notice_permissions: notice_permissions,
        };
    }

    async findOne(idOrEmail: string | number, company_id: number | null = null): Promise<UserInterface> {
        const whereCondition: any = {
            OR: [
                {
                    id: isNaN(Number(idOrEmail)) ? 0 : Number(idOrEmail),
                },
                {
                    email: idOrEmail.toString(),
                },
            ],
            deleted_at: null,
        };

        if (company_id) {
            whereCondition.company_id = Number(company_id);
        }

        const user = await this.prismaService.user.findFirst({
            where: whereCondition,
            include: {
                role: true,
                company: {
                    include: {
                        type: true,
                    },
                },
                user_notice_type: {
                    include: {
                        notice_type: true,
                    },
                },
            },
        });
        const role_notice_permissions = await this.noticeService.findAccessUserTypes(user.role_id);
        const user_notice_permissions = await this.noticeService.findUserNotificationPermissions(user.id);
        const user_notice_permissions_by_slug = user_notice_permissions.reduce((acc, item) => {
            acc[item.slug] = item.active;
            return acc;
        }, {});
        const notice_permissions = [];
        role_notice_permissions.forEach((el) => {
            notice_permissions.push({
                slug: el.slug,
                active: user_notice_permissions_by_slug[el.slug],
            });
        });

        return {
            ...user,
            user_notice_permissions: notice_permissions,
        };
    }
    async findByRoles(role_ids: number[]) {
        const user = await this.prismaService.user.findMany({
            where: {
                role_id: {
                    in: role_ids,
                },
            },
        });
        return user;
    }
    async findMany(
        company_id: number | number[] | null = null,
        filter?: {
            role_ids?: number[];
            permissions?: (SystemPermissions | CabinetPermissions)[];
            search?: string;
            is_active?: boolean;
            email_verified?: boolean;
        },
    ): Promise<User[]> {
        if (company_id && !Array.isArray(company_id)) {
            company_id = [company_id];
        }
        const whereCondition: Record<string, any> = {
            deleted_at: null,
        };

        if (company_id) {
            whereCondition.company_id = {
                in: company_id as number[],
            };
        }
        if (filter?.role_ids) {
            whereCondition.role_id = {
                in: filter?.role_ids,
            };
        }
        if (filter?.search) {
            whereCondition.OR = [
                {
                    first_name: {
                        contains: filter.search,
                    },
                },
                {
                    last_name: {
                        contains: filter.search,
                    },
                },
                {
                    middle_name: {
                        contains: filter.search,
                    },
                },
                {
                    email: {
                        contains: filter.search,
                    },
                },
            ];
        }
        if (filter?.is_active !== undefined) {
            whereCondition.is_active = filter.is_active;
        }
        if (filter?.email_verified !== undefined) {
            whereCondition.email_verified = filter.email_verified;
        }
        let permissions = {};
        if (filter?.permissions && filter?.permissions.length) {
            permissions = {
                role: {
                    role_permissions: {
                        some: {
                            permission: {
                                name: {
                                    in: filter.permissions,
                                },
                            },
                        },
                    },
                },
            };
        }
        return await this.prismaService.user.findMany({
            where: {
                role_id: {
                    in: filter?.role_ids,
                },
                ...whereCondition,
                ...permissions,
            },
            include: {
                role: true,
                company: {
                    include: {
                        type: true,
                    },
                },
            },
        });
    }
    async findByTelegramKey(key: string) {
        if (!key) return null;
        return this.prismaService.user.findFirst({
            where: {
                telegram_key: key,
            },
        });
    }

    async delete(id: number, company_id: number | null = null) {
        const whereCondition: any = {
            id: Number(id),
        };
        if (company_id) {
            whereCondition.company_id = Number(company_id);
        }
        const _user = await this.prismaService.user.findUnique({ where: whereCondition });
        if (!_user) {
            throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
        }

        try {
            return this.prismaService.user.update({
                where: whereCondition,
                data: {
                    deleted_at: new Date(),
                },
            });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private hashPassword(password: string) {
        return hashSync(password, genSaltSync(10));
    }
    async getCompanyByName(title: string): Promise<Company | null> {
        return this.prismaService.company.findFirst({
            where: {
                title,
            },
        });
    }
    async createCompany(title: string, type_id: number): Promise<Company> {
        return this.prismaService.company.create({
            data: {
                title,
                type_id: type_id,
            },
        });
    }

    async failedAuthorization(id: number) {
        try {
            await this.prismaService.user.update({
                where: { id },
                data: {
                    login_attempts: {
                        increment: 1,
                    },
                    last_failed_login: new Date(),
                },
            });
        } catch (error) {
            throw new Error(`Failed to update login attempts for user with id ${id}`);
        }
    }

    isUserBlockedDueToFailedAttempts(user: User): boolean {
        const currentDate = new Date();

        const unlockDate = user.last_failed_login ? addMinutes(user.last_failed_login, 30) : null;
        if (unlockDate) {
            if (user.login_attempts >= 5 && !isAfter(currentDate, unlockDate)) {
                return true;
            }
        }
        return false;
    }
    async resetFailedLoginData(user: User) {
        await this.prismaService.user.update({
            where: {
                id: user.id,
            },
            data: {
                login_attempts: 0,
                last_failed_login: null,
            },
        });
    }
    private async sendVerificationCode(email: string): Promise<string> {
        const code = generateVerifyCode();
        await this.mailService.sendVerificationCode(email, code);
        return code;
    }
}
