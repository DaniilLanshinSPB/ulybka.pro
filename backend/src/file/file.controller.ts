import { BadRequestException, Controller, Delete, InternalServerErrorException, Post, Query, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiExcludeController } from '@nestjs/swagger';
import * as fs from 'fs';
import { diskStorage } from 'multer';
import { extname } from 'path';

const storage = diskStorage({
    destination: './uploads',
    filename: (req, file, callback) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
        const ext = extname(file.originalname);
        callback(null, `${file.fieldname}-${uniqueSuffix}${ext}`);
    },
});

const uploadDir = './uploads';
if (!fs.existsSync(uploadDir)) {
    fs.mkdirSync(uploadDir);
}
@Controller('upload')
@ApiExcludeController()
export class FileController {
    @Post()
    @UseInterceptors(FileInterceptor('file', { storage }))
    uploadFile(@UploadedFile() file: File) {
        console.log(file);
        return {
            message: 'File uploaded successfully!',
            file: file,
        };
    }
    @Delete()
    async deleteFileByPath(@Query('filePath') filePath: string) {
        if (!filePath) {
            throw new BadRequestException('File path is required');
        }

        try {
            await fs.promises.unlink(filePath); // Удаляем файл асинхронно
            return {
                message: 'File deleted successfully',
            };
        } catch (err) {
            console.error(err);
            throw new InternalServerErrorException('Failed to delete file');
        }
    }
}
