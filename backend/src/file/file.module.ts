import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { ServeStaticModule } from '@nestjs/serve-static';
import * as multer from 'multer';
import { join } from 'path';
import { v4 } from 'uuid';
import { FileController } from './file.controller';

@Module({
    imports: [
        MulterModule.register({
            storage: multer.diskStorage({
                destination: './uploads',
                filename: (req, file, cb) => {
                    const fileExtension = file.originalname.split('.').pop();
                    cb(null, `${v4()}-${Date.now()}.${fileExtension}`);
                },
            }),
            fileFilter: (req, file, cb) => {
                cb(null, true);
            },
        }),
        ServeStaticModule.forRoot({
            rootPath: join(__dirname, '..', 'uploads'),
            serveRoot: '/uploads',
        }),
    ],
    controllers: [FileController],
})
export class FileModule {}
