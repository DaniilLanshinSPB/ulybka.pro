import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { ScheduleModule } from '@nestjs/schedule';
import { ApiKeyModule } from './api-keys/api-keys.module';
import { AuthModule } from './auth/auth.module';
import { JwtAuthGuard } from './auth/guards/jwt-auth.guard';
import { CabinetModule } from './cabinet/cabinet.module';
import { CompanyModule } from './companies/company.module';
import { DatabaseModule } from './database/database.module';
import { DirectoryModule } from './directories/directory.module';
import { API_PRIVATE_CONTROLLERS, API_PUBLIC_CONTROLLERS } from './docs';
import { EncryptionModule } from './encryption/encryption.module';
import { FieldModule } from './fields/field.module';
import { FileModule } from './file/file.module';
import { LogsModule } from './logs/logs.module';
import { MailModule } from './mail/mail.module';
import { NoticeModule } from './notice/notice.module';
import { OrderModule } from './orders/order.module';
import { PatientModule } from './patients/patient.module';
import { RoleModule } from './roles/role.module';
import { ServiceModule } from './services/service.module';
import { TelegramModule } from './telegram/telegram.module';
import { UserModule } from './users/user.module';
import { WebhookModule } from './webhooks/webhooks.module';

@Module({
    imports: [
        FileModule,
        CabinetModule,
        AuthModule,

        RoleModule,
        UserModule,
        CompanyModule,

        DirectoryModule,

        ServiceModule,
        FieldModule,
        PatientModule,
        OrderModule,

        MailModule,
        DatabaseModule,

        LogsModule,
        NoticeModule,

        TelegramModule,

        WebhookModule,
        ApiKeyModule,
        EncryptionModule,

        ScheduleModule.forRoot(),
        ConfigModule.forRoot({ isGlobal: true }),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                secret: configService.get<string>('JWT_SECRET'),
                signOptions: { expiresIn: '60m' },
            }),
            inject: [ConfigService],
        }),
    ],
    controllers: [...API_PRIVATE_CONTROLLERS, ...API_PUBLIC_CONTROLLERS],
    providers: [
        {
            provide: APP_GUARD,
            useClass: JwtAuthGuard,
        },
    ],
})
export class AppModule {}
