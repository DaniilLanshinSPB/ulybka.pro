import { CompanyService } from '@/companies/company.service';
import { PrismaService } from '@/database/postgres-prisma.service';
import { NoticeService } from '@/notice/notice.service';
import { CreateNotificationDto } from '@/notice/types/create-notification.dto';
import { OrderCommentService } from '@/orders/order-comment.service';
import { OrderService } from '@/orders/order.service';
import { PatientService } from '@/patients/patient.service';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EntityType, LogType } from '@prisma/postgres/client';
import { LogCreateDto } from './dto/log-create.dto';

@Injectable()
export class LogsService {
    protected readonly logger = new Logger(LogsService.name);

    constructor(
        protected readonly prismaService: PrismaService,
        protected readonly configService: ConfigService,
        protected readonly noticeService: NoticeService,
        protected readonly commentService: OrderCommentService,
        protected readonly orderService: OrderService,
        protected readonly companyService: CompanyService,
        protected readonly patientService: PatientService,
    ) {}

    async findAny(
        limit = 50,
        offset = 0,
        filter?: {
            search?: string;
            type?: LogType;
            user_id?: number;
            entity_type?: EntityType;
        },
    ) {
        const where: Record<string, any> = {};
        if (filter?.search) {
            where.search = filter.search;
        }
        if (filter?.type) {
            where.type = filter.type;
        }
        if (filter?.user_id) {
            where.user_id = filter.user_id;
        }
        if (filter?.entity_type) {
            where.entity_type = filter.entity_type;
        }
        return this.prismaService.logs.findMany({
            take: limit,
            skip: offset,
            where,
            orderBy: {
                created_at: 'desc',
            },
        });
    }
    async logAction(data: LogCreateDto) {
        await this.createNotifications(data);
        return this.prismaService.logs.create({ data });
    }
    async createNotifications(data: LogCreateDto) {
        let usersIds: number[] = [];

        const type = await this.noticeService.getTypeBySlug(data.type);
        if (type.slug === LogType.COMMENT) {
            const comment = await this.commentService.findOne(+data.entity_id);
            const order = await this.orderService.findById(comment.order_id);
            usersIds = [...new Set([order.creator_id, order.executor_id, order.customer_id, order.responsible_id])];
        } else if (type.slug === LogType.ORDER) {
            const order = await this.orderService.findById(+data.entity_id);
            usersIds = [...new Set([order.creator_id, order.executor_id, order.customer_id, order.responsible_id])];
        } else if (type.slug === LogType.PATIENT) {
            const patient = await this.patientService.findByUid(data.entity_id);
            const users = await this.companyService.findCompanyUsers(patient.company_id);
            usersIds = users.map((u) => u.id);
        } else {
            usersIds = await this.noticeService.findUserIdsByActiveNotificationType(data.type);
        }
        usersIds = usersIds.filter((el) => el);

        const dto: CreateNotificationDto[] = [];
        const telegramUserIds = await this.noticeService.findUserIdsByActiveNotificationType(LogType.TELEGRAM);

        const usersHasTelegramNotificationPermissionsIds = usersIds.filter((id) => telegramUserIds.includes(id));

        const usersWithTelegramNotifications = await this.prismaService.user.findMany({
            where: {
                id: {
                    in: usersHasTelegramNotificationPermissionsIds,
                },
                telegram_id: {
                    not: null,
                },
            },
            select: {
                id: true,
                telegram_id: true,
            },
        });

        const telegramIdByUserId = usersWithTelegramNotifications
            .filter((el) => el.telegram_id)
            .reduce((acc, el) => {
                acc[el.id] = el.telegram_id;
                return acc;
            }, {});

        usersIds.forEach((id) => {
            if (id !== data.user_id || LogType.ORDER) {
                const telegram_id = telegramIdByUserId[id];
                dto.push({
                    user_id: id,
                    type_id: type.id,
                    message: data.details,
                    metadata: data.metadata,
                    isSendTelegram: telegram_id ? true : false,
                    telegram_id,
                });
            }
        });

        await this.noticeService.create(dto);
    }
}
