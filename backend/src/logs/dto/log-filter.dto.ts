import { ApiProperty } from '@nestjs/swagger';
import { EntityType, LogType } from '@prisma/postgres/client';

export class LogFilterDto {
    @ApiProperty({ name: 'limit', type: Number })
    limit: number;
    @ApiProperty({ name: 'offset', type: Number })
    offset: number;
    @ApiProperty({ name: 'search', type: String })
    search: string;
    @ApiProperty({ name: 'type', enum: LogType })
    type: LogType;
    @ApiProperty({ name: 'user_id', type: Number })
    user_id: number;
    @ApiProperty({ name: 'entity_type', enum: EntityType })
    entity_type: EntityType;
}
