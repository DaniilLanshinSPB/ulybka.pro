import { ApiProperty } from '@nestjs/swagger';
import { EntityType, LogType } from '@prisma/postgres/client';

export class LogCreateDto {
    @ApiProperty({ name: 'user_id', type: String })
    user_id: number;
    @ApiProperty({ name: 'user_id', enum: EntityType })
    entity_type: EntityType;
    @ApiProperty({ name: 'user_id', enum: LogType })
    type: LogType;
    @ApiProperty({ name: 'user_id', type: String })
    entity_id: string;
    @ApiProperty({ name: 'user_id', type: String })
    details: string;
    @ApiProperty({ name: 'user_id', type: Object })
    metadata: any;
}
