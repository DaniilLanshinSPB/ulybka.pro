import { NotVerifiedGuard } from '@/auth/guards/not_verified.guard';
import { PermissionsGuard } from '@/auth/guards/permission.guard';
import { SystemPermissions } from '@/auth/interfaces';
import { Permissions } from '@common/decorators';
import { Body, ClassSerializerInterceptor, Controller, Post, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBody, ApiExcludeController, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { LogFilterDto } from './dto/log-filter.dto';
import { LogsService } from './logs.service';
import { LogResponse } from './response/log.response';

@ApiExcludeController()
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(PermissionsGuard)
@UseGuards(NotVerifiedGuard)
@Controller('logs')
export class LogsController {
    constructor(protected readonly logsService: LogsService) {}

    @Post('/all')
    @Permissions(SystemPermissions.SYSTEM_VIEW_LOGS)
    @ApiBody({ type: LogFilterDto })
    @ApiOperation({ summary: 'Получить все роли' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о ролях', type: [LogResponse] })
    async findAll(@Body() body: LogFilterDto) {
        const { limit, offset, ...filter } = body;
        const logs = await this.logsService.findAny(limit, offset, filter);
        return logs.map((log) => new LogResponse(log));
    }
}
