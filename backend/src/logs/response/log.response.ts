import { ApiProperty } from '@nestjs/swagger';
import { EntityType, Logs, LogType } from '@prisma/postgres/client';

export class LogResponse {
    @ApiProperty({ description: 'Идентификатор' })
    id: number;
    @ApiProperty({ description: 'Идентификатор пользователя' })
    user_id: number;
    @ApiProperty({ description: 'Тип сущности' })
    entity_type: EntityType;
    @ApiProperty({ description: 'Идентификатор сущности' })
    entity_id: string;
    @ApiProperty({ description: 'Тип события' })
    type: LogType;
    @ApiProperty({ description: 'Детали' })
    details: string;
    @ApiProperty({ description: 'Метадата' })
    metadata: string;
    @ApiProperty({ description: 'Дата' })
    created_at: Date;
    constructor(params: Logs) {
        Object.assign(this, params);
    }
}
