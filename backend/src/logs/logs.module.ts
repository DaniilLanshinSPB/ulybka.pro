import { CompanyService } from '@/companies/company.service';
import { EncryptionService } from '@/encryption/encryption.service';
import { MailService } from '@/mail/mail.service';
import { NoticeModule } from '@/notice/notice.module';
import { OrderCommentService } from '@/orders/order-comment.service';
import { OrderService } from '@/orders/order.service';
import { PatientService } from '@/patients/patient.service';
import { UserService } from '@/users/user.service';
import { WebhookService } from '@/webhooks/webhooks.service';
import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';
import { LogsController } from './logs.controller';
import { LogsService } from './logs.service';

@Module({
    controllers: [LogsController],
    providers: [
        LogsService,
        OrderService,
        OrderCommentService,
        CompanyService,
        PatientService,
        UserService,
        MailService,
        WebhookService,
        EncryptionService,
    ],
    exports: [LogsService],
    imports: [CacheModule.register(), NoticeModule],
})
export class LogsModule {}
