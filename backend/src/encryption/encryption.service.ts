import { Injectable } from '@nestjs/common';
import * as crypto from 'crypto';

@Injectable()
export class EncryptionService {
    private readonly algorithm = 'aes-256-cbc';

    private readonly iv = crypto.randomBytes(16); // Вектор инициализации (16 байт)

    encrypt(text: string, key: string): string {
        const cipher = crypto.createCipheriv(this.algorithm, key, this.iv);
        let encrypted = cipher.update(text, 'utf8', 'hex');
        encrypted += cipher.final('hex');
        return encrypted;
    }

    decrypt(encryptedText: string, key: string): string {
        const decipher = crypto.createDecipheriv(this.algorithm, key, this.iv);
        let decrypted = decipher.update(encryptedText, 'hex', 'utf8');
        decrypted += decipher.final('utf8');
        return decrypted;
    }

    getIv(): Buffer {
        return this.iv;
    }
}
