import { ApiProperty } from '@nestjs/swagger';

export class CreateApiKeyDto {
    @ApiProperty({ description: 'Название ключа', type: String })
    title: string;
}
