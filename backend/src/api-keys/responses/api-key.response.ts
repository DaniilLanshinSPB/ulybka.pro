import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';

export class ApiKeyResponse {
    @ApiProperty({ description: 'ID', type: Number })
    id: number;
    @ApiProperty({ description: 'Название ключа', type: String })
    title: string;

    @Exclude()
    key: string;

    @Exclude()
    company_id: number;

    @ApiProperty({ description: 'Отключен', type: Boolean })
    dispose: boolean;

    @ApiProperty({ description: 'Дата создания', type: Date })
    createdAt: Date;

    @ApiProperty({ description: 'Дата отключения', type: Date })
    updatedAt: Date;

    constructor(apiKey) {
        Object.assign(this, apiKey);
    }
}
