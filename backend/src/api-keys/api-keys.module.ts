import { Module } from '@nestjs/common';
import { ApiKeyController } from './api-keys.controller';
import { ApiKeyService } from './api-keys.service';
import { ApiKeyGuard } from './guards/api-key.guard';

@Module({
    controllers: [ApiKeyController],
    providers: [ApiKeyService, ApiKeyGuard],
})
export class ApiKeyModule {}
