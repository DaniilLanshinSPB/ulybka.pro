import { PrismaService } from '@/database/postgres-prisma.service';
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateApiKeyDto } from './dto';

@Injectable()
export class ApiKeyService {
    constructor(private readonly prisma: PrismaService) {}

    async findAll(company: number) {
        return this.prisma.apiKey.findMany({ where: { company_id: company, dispose: false } });
    }
    async createApiKey(company_id: number, createApiKeyDto: CreateApiKeyDto) {
        const apiKey = await this.prisma.apiKey.create({
            data: {
                title: createApiKeyDto.title,
                key: this.generateApiKey(),
                company_id,
            },
        });

        return apiKey;
    }

    async getApiKeyById(id: number) {
        const apiKey = await this.prisma.apiKey.findUnique({
            where: { id },
        });

        if (!apiKey) {
            throw new NotFoundException('API key not found');
        }

        return apiKey;
    }

    async disposeApiKey(company_id: number, id: number) {
        return this.prisma.apiKey.update({
            where: { id, company_id },
            data: { dispose: true },
        });
    }

    private generateApiKey(): string {
        return 'sk_' + crypto.randomUUID().toString();
    }
}
