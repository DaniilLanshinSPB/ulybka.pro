import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Post,
    UnauthorizedException,
    UseInterceptors,
} from '@nestjs/common';

import { JwtPayload } from '@/auth/interfaces';
import { CurrentUser } from '@common/decorators';
import { CurrentCompany } from '@common/decorators/current-company.decorator';
import { ExchangeApi } from '@common/decorators/exchange-api.decorator';
import { ApiBearerAuth, ApiBody, ApiHeader, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ApiKeyService } from './api-keys.service';
import { CreateApiKeyDto } from './dto';
import { ApiKeyResponse } from './responses/api-key.response';
import { CreateApiKeyResponse } from './responses/create-api-key.response';

@Controller('api-key')
@ApiTags('Апи ключи')
@ApiBearerAuth('authorization')
@UseInterceptors(ClassSerializerInterceptor)
export class ApiKeyController {
    constructor(private readonly apiKeyService: ApiKeyService) {}

    @Get('/:company_id')
    @ExchangeApi()
    @ApiHeader({ name: 'X-API-KEY', required: true, description: 'Ваш идентефикатор апи' })
    @ApiParam({ name: 'company_id', type: Number })
    @ApiOperation({ summary: 'Получить все ключи компании' })
    @ApiResponse({ status: 201, description: 'Информация успешно получена', type: [ApiKeyResponse] })
    @ApiResponse({ status: 400, description: 'Ошибка получения информации' })
    async getApiKeys(
        @CurrentUser() user: JwtPayload,
        @CurrentCompany() currentCompany: number,
        @Param('company_id', ParseIntPipe) company_id: number,
    ) {
        if (currentCompany && currentCompany !== company_id) {
            throw new UnauthorizedException();
        }
        const keys = await this.apiKeyService.findAll(company_id);
        return keys.map((key) => new ApiKeyResponse(key));
    }

    @Post('/:company_id')
    @ExchangeApi()
    @ApiHeader({ name: 'X-API-KEY', required: true, description: 'Ваш идентефикатор апи' })
    @ApiParam({ name: 'company_id', type: Number })
    @ApiBody({ type: CreateApiKeyDto })
    @ApiOperation({ summary: 'Создать новый ключ' })
    @ApiResponse({ status: 201, description: 'Ключ успешно создан', type: ApiKeyResponse })
    @ApiResponse({ status: 400, description: 'Ошибка создания' })
    async createApiKey(
        @CurrentUser() user: JwtPayload,
        @CurrentCompany() currentCompany: number,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Body() createApiKeyDto: CreateApiKeyDto,
    ): Promise<ApiKeyResponse> {
        if (currentCompany && currentCompany !== company_id) {
            throw new UnauthorizedException();
        }
        const newKey = await this.apiKeyService.createApiKey(company_id, createApiKeyDto);
        return new CreateApiKeyResponse(newKey);
    }

    @Delete('/:company_id/:id')
    @ExchangeApi()
    @ApiHeader({ name: 'X-API-KEY', required: true, description: 'Ваш идентефикатор апи' })
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'id', type: Number })
    @ApiOperation({ summary: 'Деактивировать ключ' })
    @ApiResponse({ status: 201, description: 'Ключ успешно деактивирован', type: Boolean })
    @ApiResponse({ status: 400, description: 'Ошибка деактивации' })
    async disposeApiKey(
        @CurrentCompany() currentCompany: number,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('id', ParseIntPipe) id: number,
    ) {
        if (currentCompany && currentCompany !== company_id) {
            throw new UnauthorizedException();
        }
        await this.apiKeyService.disposeApiKey(company_id, id);
        return true;
    }
}
