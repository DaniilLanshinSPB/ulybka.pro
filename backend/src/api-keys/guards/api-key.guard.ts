import { PrismaService } from '@/database/postgres-prisma.service';
import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';

@Injectable()
export class ApiKeyGuard implements CanActivate {
    constructor(private readonly reflector: Reflector, private readonly prismaService: PrismaService) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest<Request>();
        const apiKey = request.headers['x-api-key'] as string;

        if (!apiKey) {
            throw new UnauthorizedException('API key is missing');
        }

        const apiKeyRecord = await this.prismaService.apiKey.findUnique({
            where: { key: apiKey },
        });

        if (!apiKeyRecord || apiKeyRecord.dispose) {
            throw new UnauthorizedException('Invalid API key');
        }

        return !!apiKeyRecord.company_id;
    }
}

export const ApiKey = () => {
    return (target: any, key?: string, descriptor?: PropertyDescriptor) => {
        if (descriptor && descriptor.value) {
            Reflect.defineMetadata('apiKey', true, descriptor.value);
        }
        return descriptor;
    };
};
