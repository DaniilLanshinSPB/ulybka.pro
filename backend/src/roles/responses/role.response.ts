import { ApiProperty } from '@nestjs/swagger';
import { Role, RoleNoticeType, RolePermissions } from '@prisma/postgres/client';
import { Exclude } from 'class-transformer';

export class RoleResponse implements Role {
    @ApiProperty({ description: 'Идентификатор пользователя' })
    id: number;

    @ApiProperty({ description: 'Символьный код роли' })
    slug: string;

    @ApiProperty({ description: 'Название роли' })
    name: string;

    @ApiProperty({ description: 'Роль по умолчанию' })
    is_default: boolean;

    @ApiProperty({ description: 'Разрешения' })
    permissions?: Permissions[];

    @ApiProperty({ description: 'ID типа компании' })
    company_type_id: number;
    @Exclude()
    role_permissions: RolePermissions[];

    @Exclude()
    role_notice_type: RoleNoticeType[];

    @ApiProperty({ description: 'Метка рутовой роли' })
    is_root: boolean;
    constructor(role: Role) {
        Object.assign(this, role);
    }
}
