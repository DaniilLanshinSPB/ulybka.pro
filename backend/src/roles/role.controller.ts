import { NotVerifiedGuard } from '@/auth/guards/not_verified.guard';
import { PermissionsGuard } from '@/auth/guards/permission.guard';
import { JwtPayload, SystemPermissions } from '@/auth/interfaces';
import { NoticeTypesResponse } from '@/auth/responses/notice-type.response';
import { LogsService } from '@/logs/logs.service';
import { NoticeService } from '@/notice/notice.service';
import { CurrentUser, Permissions } from '@common/decorators';
import { getUserName } from '@common/helpers/user.helper';
import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiExcludeController, ApiOperation, ApiParam, ApiResponse } from '@nestjs/swagger';
import { EntityType, LogType } from '@prisma/postgres/client';
import { CreateRoleDto, RolePermissionsDto, UpdateRoleDto } from './dto';
import { RoleResponse } from './responses';
import { RoleService } from './role.service';

@ApiExcludeController()
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(PermissionsGuard)
@UseGuards(NotVerifiedGuard)
@Controller('role')
export class RoleController {
    constructor(
        protected readonly roleService: RoleService,
        protected readonly logsService: LogsService,
        protected readonly noticeService: NoticeService,
    ) {}

    @Get('all')
    @Permissions(SystemPermissions.SYSTEM_VIEW_ROLES)
    @ApiOperation({ summary: 'Получить все роли' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о ролях', type: [RoleResponse] })
    async findAll() {
        const roles = await this.roleService.findAll();
        return roles.map((role) => new RoleResponse(role));
    }

    @Get(':id')
    @ApiParam({ name: 'id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_VIEW_ROLES)
    @ApiOperation({ summary: 'Получить информацию о роли по ID' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о роли', type: RoleResponse })
    async findOne(@Param('id', ParseIntPipe) id: number) {
        const role = await this.roleService.findOne(id);
        return new RoleResponse(role);
    }
    @Get('notification-types/all')
    @Permissions(SystemPermissions.SYSTEM_VIEW_ROLES)
    @ApiOperation({ summary: 'Получить информацию о способах нотификации' })
    @ApiResponse({
        status: 200,
        description: 'Успешное получение информации о способах нотификации',
        type: [NoticeTypesResponse],
    })
    async findNotificationTypes() {
        const types = await this.noticeService.findAccessTypes();
        return types.map((t) => new NoticeTypesResponse(t));
    }

    @Post()
    @ApiBody({ type: CreateRoleDto })
    @Permissions(SystemPermissions.SYSTEM_CREATE_ROLES)
    @ApiOperation({ summary: 'Создание роли' })
    @ApiResponse({ status: 200, description: 'Роль успешно создана' })
    async createRole(@CurrentUser() currentUser: JwtPayload, @Body() dto: CreateRoleDto) {
        const role = await this.roleService.create(dto);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Role,
            entity_id: role.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} добавил новую роль ${role.name}`,
            metadata: role,
            type: LogType.SYSTEM,
        });
        return new RoleResponse(role);
    }

    @Patch(':id')
    @ApiParam({ name: 'id', type: Number })
    @ApiBody({ type: UpdateRoleDto })
    @Permissions(SystemPermissions.SYSTEM_UPDATE_ROLES)
    @ApiOperation({ summary: 'Обновление роли' })
    @ApiResponse({ status: 200, description: 'Роль успешно обновлена' })
    async updateRole(
        @CurrentUser() currentUser: JwtPayload,
        @Param('id', ParseIntPipe) id: number,
        @Body() dto: UpdateRoleDto,
    ) {
        const role = await this.roleService.update(id, dto);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Role,
            entity_id: role.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} изменил роль ${role.name}`,
            metadata: role,
            type: LogType.SYSTEM,
        });
        return new RoleResponse(role);
    }
    @Delete(':id')
    @ApiParam({ name: 'id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_DELETE_ROLES)
    @ApiOperation({ summary: 'Удалить роль по ID' })
    @ApiResponse({ status: 200, description: 'Пользователь успешно удален' })
    @ApiResponse({ status: 400, description: 'Пользователь не найден' })
    async deleteRole(@CurrentUser() currentUser: JwtPayload, @Param('id', ParseIntPipe) id: number) {
        const role = await this.roleService.findOne(id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Role,
            entity_id: role.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} удалил роль ${role.name}`,
            metadata: role,
            type: LogType.SYSTEM,
        });
        return this.roleService.delete(id);
    }

    @Get('permissions/all')
    @Permissions(SystemPermissions.SYSTEM_UPDATE_ROLES)
    @ApiOperation({ summary: 'Получить доступные разрешения' })
    @ApiResponse({ status: 200, description: 'Разрешения получены' })
    async fetchAllPermissions() {
        return this.roleService.fetchAllPermissions();
    }

    @Post('permissions/:role_id')
    @ApiParam({ name: 'role_id', type: Number })
    @ApiBody({ type: RolePermissionsDto })
    @Permissions(SystemPermissions.SYSTEM_UPDATE_ROLES)
    @ApiOperation({ summary: 'Задать разрешение для роли' })
    @ApiResponse({ status: 200, description: 'Разрешение задано' })
    async setPermissions(
        @CurrentUser() currentUser: JwtPayload,
        @Param('role_id', ParseIntPipe) role_id: number,
        @Body() dto: RolePermissionsDto,
    ) {
        const role = await this.roleService.findOne(role_id);
        const permissions = await this.roleService.fetchPermissionsByIds(dto.permissions);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Role,
            entity_id: role.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} добавил разрешения ${permissions
                .map((p) => p.name)
                .join(', ')} для роли ${role.name}`,
            type: LogType.SYSTEM,
            metadata: role,
        });
        return this.roleService.setPermissions(role_id, dto.permissions);
    }
}
