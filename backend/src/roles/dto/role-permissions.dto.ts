import { ApiProperty } from '@nestjs/swagger';
import { IsArray } from 'class-validator';

export class RolePermissionsDto {
    @IsArray()
    @ApiProperty({ description: 'Идентификаторы разрешений', type: [Number] })
    permissions: number[];
}
