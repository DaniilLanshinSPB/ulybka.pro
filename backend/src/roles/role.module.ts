import { CompanyService } from '@/companies/company.service';
import { LogsModule } from '@/logs/logs.module';
import { MailModule } from '@/mail/mail.module';
import { NoticeModule } from '@/notice/notice.module';
import { UserModule } from '@/users/user.module';
import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';
import { RoleController } from './role.controller';
import { RoleService } from './role.service';

@Module({
    providers: [RoleService, CompanyService],
    exports: [RoleService],
    controllers: [RoleController],
    imports: [CacheModule.register(), MailModule, LogsModule, UserModule, NoticeModule],
})
export class RoleModule {}
