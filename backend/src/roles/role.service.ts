import { CompanyService } from '@/companies/company.service';
import { PrismaService } from '@/database/postgres-prisma.service';
import { NoticeService } from '@/notice/notice.service';
import { transliterate } from '@common/helpers';
import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { LogType, Permissions, PermissionType, Role } from '@prisma/postgres/client';

@Injectable()
export class RoleService {
    protected readonly logger = new Logger(RoleService.name);

    constructor(
        protected readonly prismaService: PrismaService,
        protected readonly companyService: CompanyService,
        protected readonly configService: ConfigService,
        protected readonly noticeService: NoticeService,
    ) {}

    async findRolesByCompany(company_id: number | number[]): Promise<Role[]> {
        if (!Array.isArray(company_id)) {
            company_id = [company_id];
        }
        const companies = await this.companyService.findMany(company_id);
        if (!companies.length) {
            throw new HttpException('Компания не найдена', HttpStatus.NOT_FOUND);
        }
        return await this.prismaService.role.findMany({
            where: { is_default: false, company_type_id: { in: companies.map((company) => company.type_id) } },
        });
    }
    async findAll(): Promise<Role[]> {
        const roles = await this.prismaService.role.findMany({
            include: {
                role_permissions: {
                    include: {
                        permission: true,
                    },
                },
                role_notice_type: {
                    include: {
                        notice_type: true,
                    },
                },
            },
        });
        return roles.map((role) => {
            return {
                ...role,
                permissions: role.role_permissions.map((role_permission) => role_permission.permission),
                notification_permissions: role.role_notice_type.map((nt) => ({
                    slug: nt.notice_type.slug,
                    active: nt.notice_type.active,
                })),
            };
        });
    }
    async findOne(id: number): Promise<
        Role & {
            permissions: Permissions[];
            notification_permissions: { slug: LogType; active: boolean }[];
        }
    > {
        const role = await this.prismaService.role.findUnique({
            where: {
                id,
            },
            include: {
                role_permissions: {
                    include: {
                        permission: true,
                    },
                },
                role_notice_type: {
                    include: {
                        notice_type: true,
                    },
                },
            },
        });
        if (!role) {
            return null;
        }
        return {
            ...role,
            permissions: role.role_permissions.map((role_permission) => role_permission.permission),
            notification_permissions: role.role_notice_type.map((nt) => ({
                slug: nt.notice_type.slug as LogType,
                active: nt.notice_type.active,
            })),
        };
    }
    async findRoot() {
        return this.prismaService.role.findFirst({
            where: {
                is_root: true,
            },
        });
    }
    async create(
        role: Partial<
            Role & { permissions: Permissions[]; notification_permissions: { slug: LogType; active: boolean }[] }
        >,
    ) {
        console.log(role);
        const _role = await this.prismaService.role.findFirst({
            where: {
                name: role.name,
            },
        });

        if (_role) {
            throw new HttpException('Роль с таким именем уже существует', HttpStatus.BAD_REQUEST);
        }

        const slug = transliterate(role.name);

        try {
            const savedRole = await this.prismaService.role.create({
                data: {
                    slug,
                    name: role.name,
                    is_default: role.is_default,
                    company_type_id: role.company_type_id,
                },
            });
            if (role.permissions) {
                await this.prismaService.rolePermissions.createMany({
                    data: role.permissions.map((permission) => {
                        return {
                            role_id: savedRole.id,
                            permission_id: permission.id,
                        };
                    }),
                });
            }
            if (role.notification_permissions) {
                await this.noticeService.createRoleNotificationPermissions(savedRole.id, role.notification_permissions);
            }

            return {
                ...savedRole,
                permissions: role.permissions,
                notification_permissions: role.notification_permissions,
            };
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    async update(
        id: number,
        role: Partial<
            Role & { permissions: Permissions[]; notification_permissions: { slug: LogType; active: boolean }[] }
        >,
    ): Promise<Role & { permissions: Permissions[]; notification_permissions: { slug: LogType; active: boolean }[] }> {
        const _duplicate_role = await this.prismaService.role.findFirst({
            where: {
                name: role.name,
                id: {
                    not: id,
                },
            },
        });

        if (_duplicate_role) {
            throw new HttpException('Роль с таким именем уже существует', HttpStatus.BAD_REQUEST);
        }
        const _role = await this.prismaService.role.findFirst({
            where: {
                id: id,
                is_default: role.is_default,
                company_type_id: role.company_type_id,
            },
        });

        const slug = transliterate(role.name);

        if (!_role?.is_default) {
            await this.prismaService.role.update({
                where: {
                    id: id,
                },
                data: {
                    name: role.name,
                    is_default: role.is_default,
                    slug,
                    company_type_id: role.company_type_id,
                },
            });
        }
        await this.prismaService.rolePermissions.deleteMany({
            where: {
                role_id: id,
            },
        });
        if (role.permissions) {
            await this.prismaService.rolePermissions.createMany({
                data: role.permissions.map((permission) => {
                    return {
                        role_id: id,
                        permission_id: permission.id,
                    };
                }),
            });
        }
        if (role.notification_permissions) {
            await this.noticeService.updateRoleNotificationPermissions(id, role.notification_permissions);
        }

        return {
            ...(role as Role),
            permissions: role.permissions,
            notification_permissions: role.notification_permissions,
        };
    }

    async delete(id: number) {
        const _role = await this.prismaService.role.findUnique({
            where: { id: Number(id), is_default: false },
        });
        if (!_role || _role.is_default) {
            throw new HttpException('Невозможно удалить данную роль', HttpStatus.BAD_REQUEST);
        }

        try {
            return this.prismaService.role.delete({ where: { id: Number(id) }, select: { id: true } });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async fetchAllPermissions(type: PermissionType | null = null) {
        const where: any = {};
        if (type) {
            where.type = type;
        }
        return this.prismaService.permissions.findMany({
            where,
        });
    }

    async fetchPermissions(role_id: number) {
        return this.prismaService.rolePermissions.findMany({
            where: {
                role_id,
            },
        });
    }

    async setPermissions(role_id: number, permissions: number[]) {
        await this.prismaService.rolePermissions.deleteMany({
            where: {
                role_id,
            },
        });
        const data = permissions.map((permission_id) => {
            return {
                role_id,
                permission_id,
            };
        });
        return this.prismaService.rolePermissions.createMany({ data });
    }
    async fetchPermissionsByIds(permission_ids: number[]) {
        return this.prismaService.permissions.findMany({
            where: {
                id: {
                    in: permission_ids,
                },
            },
        });
    }
}
