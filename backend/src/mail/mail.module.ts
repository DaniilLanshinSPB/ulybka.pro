// mail.module.ts
import { MailerModule } from '@nestjs-modules/mailer';
import { Module } from '@nestjs/common';
import { MailService } from './mail.service';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { join } from 'path';

@Module({
    imports: [
        MailerModule.forRoot({
            transport: {
                host: 'mail.my.cmacom.ru',
                port: 587,
                auth: {
                    user: 'noreply@my.cmacom.ru',
                    pass: '!IWqi6r)$f7wr1wP',
                },
            },
            defaults: {
                from: '"No Reply" <noreply@my.cmacom.ru>',
            },
            template: {
                dir: join(__dirname, '..', 'templates/mail'),
                adapter: new HandlebarsAdapter(),
                options: {
                    strict: true,
                },
            },
        }),
    ],
    providers: [MailService],
    exports: [MailService],
})
export class MailModule {}
