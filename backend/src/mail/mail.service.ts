// mail.service.ts
import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MailService {
    constructor(protected readonly mailerService: MailerService) {}

    async sendVerificationCode(email: string, code: string) {
        await this.mailerService.sendMail({
            to: email,
            subject: 'Код подтверждения регистрации',
            template: './verification',
            context: {
                code,
            },
        });
    }
    async sendNewPassword(email: string, password: string) {
        await this.mailerService.sendMail({
            to: email,
            subject: 'Новый пароль',
            template: './new-password',
            context: {
                password,
            },
        });
    }
}
