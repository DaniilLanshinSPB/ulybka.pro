import { LogsModule } from '@/logs/logs.module';
import { UserModule } from '@/users/user.module';
import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';
import { FieldController } from './field.controller';
import { FieldService } from './field.service';

@Module({
    controllers: [FieldController],
    providers: [FieldService],
    exports: [FieldService],
    imports: [CacheModule.register(), UserModule, LogsModule],
})
export class FieldModule {}
