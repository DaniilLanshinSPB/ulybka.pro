import { ApiProperty } from '@nestjs/swagger';
import { $Enums, Directory, Field, FieldType, Prisma } from '@prisma/postgres/client';
import { Exclude } from 'class-transformer';

export class FieldResponse implements Field {
    @ApiProperty({ description: 'ID' })
    id: number;

    @ApiProperty({ description: 'Активация поля' })
    active: boolean;

    @ApiProperty({ description: 'Техническое имя' })
    technical_name: string;

    @ApiProperty({ description: 'Группа' })
    group: $Enums.FieldGroup;

    @ApiProperty({ description: 'Название' })
    name: string;

    @ApiProperty({ description: 'Плейсхолдер' })
    placeholder: string;

    @ApiProperty({ description: 'Множественной' })
    multiple: boolean;

    @ApiProperty({ description: 'Тип' })
    type: FieldType;

    @Exclude()
    directory_id: number;

    @ApiProperty({ description: 'Индекс сортировки' })
    sort: number;

    @ApiProperty({ description: 'Прикрепленный словарь' })
    directory: Directory;

    @ApiProperty({ description: 'ID родительского поля' })
    parent_id: number;

    @ApiProperty({ description: 'Родительское поле' })
    parent: Field;

    @ApiProperty({ description: 'Прикрепленные поля' })
    children: Field[];

    @ApiProperty({ description: 'Название компании' })
    title: string;

    @ApiProperty({ description: 'Количество пользователей' })
    users_count: number;

    @ApiProperty({ description: 'Условия отображения' })
    display_conditions: string;

    @ApiProperty({ description: 'Метадата объекта' })
    metadata: Prisma.JsonValue;

    @ApiProperty({ description: 'Обязательное поле' })
    required: boolean;

    constructor(field: Field) {
        Object.assign(this, field);
    }
}
