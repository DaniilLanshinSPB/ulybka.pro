import { ApiProperty } from '@nestjs/swagger';
import { FieldType, Prisma } from '@prisma/postgres/client';
import { IsBoolean, IsEmpty, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateFieldDto {
    @IsBoolean()
    @ApiProperty({ description: 'Флаг активности', type: Boolean })
    active: boolean;

    @IsString()
    @IsEmpty()
    @ApiProperty({ description: 'Техническое имя', type: String })
    technical_name: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'Название', type: String })
    name: string;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty({ description: 'Индекс сортировки', type: Number })
    sort: number;

    @IsBoolean()
    @ApiProperty({ description: 'Множественной', type: Boolean })
    multiple: boolean;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'Тип', enum: FieldType })
    type: FieldType;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty({ description: 'ID словаря', enum: Number })
    directory_id: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty({ description: 'ID родительского поля', type: Number })
    parent_id: number;

    @IsString()
    @IsEmpty()
    @ApiProperty({ description: 'Условия отображения', type: String })
    display_conditions: string;

    @ApiProperty({ description: 'Метадата поля', type: String })
    metadata: Prisma.JsonValue;

    @IsBoolean()
    @IsNotEmpty()
    @ApiProperty({ description: 'Обязательное поле', type: Boolean })
    required: boolean;
}
