import { ApiProperty } from '@nestjs/swagger';

export class FieldFilterDto {
    @ApiProperty({ description: 'Идентификаторы услуг', type: Number })
    service_id?: number[];
}
