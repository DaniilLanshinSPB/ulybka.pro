import { PrismaService } from '@/database/postgres-prisma.service';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { HttpException, HttpStatus, Inject, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Field } from '@prisma/postgres/client';
import { Cache } from 'cache-manager';
import { FieldFilterDto } from './dto/filter.dto';

export class FieldService {
    protected readonly logger = new Logger(FieldService.name);

    constructor(
        protected readonly prismaService: PrismaService,
        protected readonly configService: ConfigService,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
    ) {}

    async findAll(isTree = false, filter: FieldFilterDto = {}): Promise<Field[]> {
        const internalFilter: {
            service_fields?: {
                some?: {
                    service_id?: {
                        in: number[];
                    };
                };
            };
        } = {};
        if (filter?.service_id) {
            internalFilter.service_fields = {
                some: {
                    service_id: {
                        in: filter?.service_id.map((el) => Number(el)) || [],
                    },
                },
            };
        }

        const _fields = await this.prismaService.field.findMany({
            where: {
                ...internalFilter,
            },
            include: {
                directory: true,
            },
            orderBy: {
                sort: 'asc',
            },
        });

        _fields.map((field) => {
            if (field?.directory && field?.directory?.options) {
                field.directory.options = JSON.parse(field.directory.options as string);
            }
            return field;
        });
        if (!_fields || !_fields.length) {
            return [];
        }
        if (!isTree) return _fields;

        const fieldMap = new Map();

        _fields.forEach((field: any) => {
            field.children = [];
            fieldMap.set(field.id, field);
        });

        _fields.forEach((field) => {
            if (field.parent_id !== null) {
                const parent = fieldMap.get(field.parent_id);
                if (parent) {
                    parent.children.push(field);
                }
            }
        });

        const tree = _fields.filter((field) => field.parent_id === null);

        return tree;
    }

    async findOne(id: number): Promise<Field> {
        const field = await this.prismaService.field.findFirst({
            where: {
                id,
            },
            include: {
                child_fields: true,
            },
        });

        if (!field) {
            return null;
        }

        return field;
    }

    async create(field: Partial<Field>) {
        try {
            return this.prismaService.field.create({
                data: {
                    active: field.active,
                    technical_name: field.technical_name || field.name,
                    name: field.name,
                    multiple: field.multiple,
                    type: field.type,
                    directory_id: field?.directory_id || null,
                    parent_id: field?.parent_id || null,
                    sort: field?.sort || 0,
                    display_conditions: field?.display_conditions || null,
                    metadata: field?.metadata || null,
                    placeholder: field?.placeholder || null,
                    required: field?.required || false,
                },
            });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async update(id: number, field: Partial<Field>): Promise<Field> {
        try {
            return this.prismaService.field.update({
                where: {
                    id: id,
                },
                data: {
                    active: field.active,
                    technical_name: field.technical_name || field.name,
                    name: field.name,
                    multiple: field.multiple,
                    type: field.type,
                    directory_id: field?.directory_id || null,
                    parent_id: field?.parent_id || null,
                    sort: field?.sort || 0,
                    display_conditions: field?.display_conditions || null,
                    placeholder: field?.placeholder || null,
                    metadata: field?.metadata || null,
                    required: field?.required || false,
                },
            });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async deleteField(id: number) {
        const _field = await this.prismaService.field.findUnique({
            where: { id },
            include: { child_fields: true }, // Загрузить дочерние элементы
        });

        if (!_field) {
            throw new HttpException('Field not found', HttpStatus.NOT_FOUND);
        }

        try {
            await this.prismaService.$transaction(async (prisma) => {
                await prisma.serviceField.deleteMany({
                    where: {
                        field_id: id,
                    },
                });
                await prisma.field.deleteMany({
                    where: { parent_id: id },
                });

                await prisma.field.delete({
                    where: { id },
                });
            });

            return { message: 'Field and its child fields deleted successfully' };
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async copy(id: number): Promise<any> {
        const _field = await this.prismaService.field.findUnique({ where: { id } });

        if (!_field) {
            throw new HttpException('Field not found', HttpStatus.NOT_FOUND);
        }

        return this.prismaService.$transaction(async (prisma) => {
            const copyFieldRecursive = async (fieldId: number, newParentId: number | null = null): Promise<any> => {
                const field = await prisma.field.findUnique({
                    where: { id: fieldId },
                    include: { child_fields: true },
                });

                if (!field) {
                    throw new Error(`Field with id ${fieldId} not found.`);
                }

                const newField = await prisma.field.create({
                    data: {
                        active: field.active,
                        technical_name: field.technical_name,
                        name: field.name,
                        multiple: field.multiple,
                        type: field.type,
                        required: field.required,
                        directory_id: field.directory_id,
                        parent_id: newParentId,
                        sort: field.sort + 10,
                        display_conditions: field.display_conditions,
                        metadata: field.metadata,
                    },
                    include: { child_fields: true },
                });

                for (const childField of field.child_fields) {
                    await copyFieldRecursive(childField.id, newField.id);
                }

                return newField;
            };

            return copyFieldRecursive(_field.id, _field.parent_id);
        });
    }
}
