import { NotVerifiedGuard } from '@/auth/guards/not_verified.guard';
import { PermissionsGuard } from '@/auth/guards/permission.guard';
import { JwtPayload, SystemPermissions } from '@/auth/interfaces';
import { LogsService } from '@/logs/logs.service';
import { CurrentUser, Permissions } from '@common/decorators';
import { getUserName } from '@common/helpers/user.helper';
import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    Query,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiBody,
    ApiExcludeController,
    ApiOperation,
    ApiParam,
    ApiQuery,
    ApiResponse,
} from '@nestjs/swagger';
import { EntityType, LogType } from '@prisma/postgres/client';
import { CreateFieldDto, UpdateFieldDto } from './dto';
import { FieldFilterDto } from './dto/filter.dto';
import { FieldService } from './field.service';
import { FieldResponse } from './responses/field.response';

@ApiExcludeController()
@ApiBearerAuth('authorization')
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(NotVerifiedGuard)
@UseGuards(PermissionsGuard)
@Controller('fields')
export class FieldController {
    constructor(protected readonly fieldService: FieldService, protected readonly logsService: LogsService) {}

    @Get('all')
    @ApiQuery({ name: 'filter', type: FieldFilterDto })
    @Permissions(SystemPermissions.SYSTEM_VIEW_FIELD)
    @ApiOperation({ summary: 'Получить все поля' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о полях', type: [FieldResponse] })
    async findAll(@Query('filter') filter: FieldFilterDto = {}) {
        const fields = await this.fieldService.findAll(false, filter);

        return fields.map((field) => new FieldResponse(field));
    }
    @Get('tree')
    @Permissions(SystemPermissions.SYSTEM_VIEW_FIELD)
    @ApiQuery({ name: 'filter', type: FieldFilterDto })
    @ApiOperation({ summary: 'Получить все поля в виде дерева' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о полях', type: [FieldResponse] })
    async findTree(@Query('filter') filter: FieldFilterDto = {}) {
        const fields = await this.fieldService.findAll(true, filter);
        return fields.map((field) => new FieldResponse(field));
    }

    @Get(':id')
    @ApiParam({ name: 'id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_VIEW_FIELD)
    @ApiOperation({ summary: 'Получить информацию о поле по ID' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о поле', type: FieldResponse })
    async findOne(@Param('id', ParseIntPipe) id: number) {
        const field = await this.fieldService.findOne(id);
        return new FieldResponse(field);
    }

    @Post()
    @Permissions(SystemPermissions.SYSTEM_CREATE_FIELD)
    @ApiBody({ type: CreateFieldDto })
    @ApiOperation({ summary: 'Создать поле' })
    @ApiResponse({ status: 200, description: 'Поле успешно создано', type: FieldResponse })
    async create(@CurrentUser() currentUser: JwtPayload, @Body() dto: CreateFieldDto) {
        const field = await this.fieldService.create(dto);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Field,
            entity_id: field.id.toString(),
            metadata: field,
            details: `Пользователь ${getUserName(currentUser)} создал поле ${field.name}`,
            type: LogType.SYSTEM,
        });
        return new FieldResponse(field);
    }

    @Patch(':id')
    @ApiParam({ name: 'id', type: Number })
    @ApiBody({ type: UpdateFieldDto })
    @Permissions(SystemPermissions.SYSTEM_UPDATE_FIELD)
    @ApiOperation({ summary: 'Обновление поля' })
    @ApiResponse({ status: 200, description: 'Поле успешно обновлено', type: FieldResponse })
    async update(
        @CurrentUser() currentUser: JwtPayload,
        @Param('id', ParseIntPipe) id: number,
        @Body() dto: UpdateFieldDto,
    ) {
        const field = await this.fieldService.update(id, dto);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Field,
            entity_id: field.id.toString(),
            metadata: field,
            details: `Пользователь ${getUserName(currentUser)} обновил поле ${field.name}`,
            type: LogType.SYSTEM,
        });
        return new FieldResponse(field);
    }

    @Delete(':id')
    @Permissions(SystemPermissions.SYSTEM_DELETE_FIELD)
    @ApiParam({ name: 'id', type: Number })
    @ApiOperation({ summary: 'Удалить поле по ID' })
    @ApiResponse({ status: 200, description: 'Поле успешно удалено' })
    @ApiResponse({ status: 400, description: 'Поле не найдено' })
    async deleteField(@CurrentUser() currentUser: JwtPayload, @Param('id', ParseIntPipe) id: number) {
        const field = await this.fieldService.findOne(id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Field,
            entity_id: field.id.toString(),
            metadata: field,
            details: `Пользователь ${getUserName(currentUser)} удалил поле ${field.name}`,
            type: LogType.SYSTEM,
        });
        return await this.fieldService.deleteField(id);
    }

    @Patch(':field_id/copy')
    @Permissions(SystemPermissions.SYSTEM_CREATE_FIELD)
    @ApiParam({ name: 'field_id', type: Number })
    @ApiOperation({ summary: 'Скопировать поле' })
    @ApiResponse({ status: 200, description: 'Поле успешно скопировано', type: FieldResponse })
    async copy(@CurrentUser() currentUser: JwtPayload, @Param('field_id', ParseIntPipe) field_id: number) {
        const field = await this.fieldService.copy(field_id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Field,
            entity_id: currentUser.id.toString(),
            metadata: field,
            details: `Пользователь ${getUserName(currentUser)} скопировал поле ${field.name}`,
            type: LogType.SYSTEM,
        });
        return new FieldResponse(field);
    }
}
