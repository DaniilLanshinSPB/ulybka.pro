import { PrismaService } from '@/database/postgres-prisma.service';
import { WebhookService } from '@/webhooks/webhooks.service';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { HttpException, HttpStatus, Inject, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Patient, WebhookEvents } from '@prisma/postgres/client';
import { Cache } from 'cache-manager';
import { v4 } from 'uuid';
import { CreatePatientDto } from './dto/patient-create.dto';
import { UpdatePatientDto } from './dto/patient-update.dto';

export class PatientService {
    protected readonly logger = new Logger(PatientService.name);

    constructor(
        protected readonly prismaService: PrismaService,
        protected readonly configService: ConfigService,
        protected readonly webhookService: WebhookService,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
    ) {}
    async search(company_id: number, name: string): Promise<Patient[]> {
        const patients = await this.prismaService.patient.findMany({
            where: {
                company_id,
                OR: [
                    {
                        fio: {
                            contains: name,
                        },
                    },
                    {
                        fullinfo: {
                            contains: name,
                        },
                    },
                ],
            },
            include: {
                company: true,
            },
        });
        if (!patients || !patients.length) {
            return [];
        }

        return patients;
    }
    async findAll(company_id: number[]): Promise<Patient[]> {
        const patients = await this.prismaService.patient.findMany({
            where: {
                company_id: {
                    in: company_id,
                },
            },
            include: {
                company: true,
            },
        });
        if (!patients || !patients.length) {
            return [];
        }

        return patients;
    }
    async findByUid(uid: string) {
        const patient = await this.prismaService.patient.findFirst({
            where: {
                uid,
            },
        });
        return patient;
    }
    async findOne(company_id: number | number[], uid: string): Promise<Patient> {
        if (typeof company_id === 'number') {
            company_id = [company_id];
        }
        const patient = await this.prismaService.patient.findFirst({
            where: {
                uid,
                company_id: {
                    in: company_id,
                },
            },
        });

        if (!patient) {
            return null;
        }

        return patient;
    }
    async create(company_id: number, dto: CreatePatientDto): Promise<Patient> {
        const _patient = await this.prismaService.patient.findFirst({
            where: {
                fio: dto.fio,
                company_id,
            },
        });
        if (_patient) {
            throw new HttpException('Пациент с таким ФИО  уже существует', HttpStatus.BAD_REQUEST);
        }

        const patient = await this.prismaService.patient.create({
            data: {
                uid: v4(),
                fio: dto.fio,
                fullinfo: `${dto.fio}, ${dto.phone}`,
                phone: dto.phone,
                dob: dto.dob ?? null,
                company_id,
            },
            include: {
                company: true,
            },
        });
        const webhooks = await this.webhookService.getWebhookByType(company_id, WebhookEvents.NEW_PATIENT);
        await this.webhookService.sendData(webhooks, patient);
        return patient;
    }
    async update(company_id: number, uid: string, dto: UpdatePatientDto): Promise<Patient> {
        const _patient = await this.prismaService.patient.findFirst({
            where: {
                uid,
                company_id,
            },
        });
        if (!_patient) {
            throw new HttpException('Пациента с таким uid не существует', HttpStatus.BAD_REQUEST);
        }

        const patient = await this.prismaService.patient.update({
            where: {
                uid,
            },
            data: {
                fio: dto.fio,
                fullinfo: `${dto.fio}, ${dto.phone}`,
                phone: dto.phone,
                dob: dto.dob ?? null,
            },
        });

        return patient;
    }

    async delete(company_id: number, uid: string) {
        return await this.prismaService.patient.delete({
            where: {
                uid,
                company_id,
            },
        });
    }
}
