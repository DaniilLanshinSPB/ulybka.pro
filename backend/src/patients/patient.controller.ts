import { PermissionsGuard } from '@/auth/guards/permission.guard';
import { JwtPayload, SystemPermissions } from '@/auth/interfaces';
import { LogsService } from '@/logs/logs.service';
import { CurrentUser, Permissions } from '@common/decorators';
import { getUserName } from '@common/helpers/user.helper';
import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    Query,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiBody,
    ApiExcludeController,
    ApiOperation,
    ApiParam,
    ApiQuery,
    ApiResponse,
} from '@nestjs/swagger';
import { EntityType, LogType } from '@prisma/postgres/client';
import { CreatePatientDto } from './dto/patient-create.dto';
import { UpdatePatientDto } from './dto/patient-update.dto';
import { PatientService } from './patient.service';
import { PatientResponse } from './responses/patient.response';

@ApiExcludeController()
@ApiBearerAuth('authorization')
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(PermissionsGuard)
@Controller('patients')
export class PatientController {
    constructor(protected readonly patientService: PatientService, protected readonly logsService: LogsService) {}

    @Get(':company_id/search')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiQuery({ name: 'name', type: String })
    @Permissions(SystemPermissions.SYSTEM_VIEW_PATIENT)
    @ApiOperation({ summary: 'Поиск пациентов' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о пациентах' })
    async search(@Param('company_id', ParseIntPipe) company_id: number, @Query('name') name: string) {
        return this.patientService.search(company_id, name);
    }

    @Get(':company_id/all')
    @ApiParam({ name: 'company_id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_VIEW_PATIENT)
    @ApiOperation({ summary: 'Получить всех пациентов' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о пациентах', type: [PatientResponse] })
    async findAll(@Param('company_id', ParseIntPipe) company_id: number) {
        const patients = await this.patientService.findAll([company_id]);
        return patients.map((patient) => new PatientResponse(patient));
    }

    @Get(':company_id/:uid')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'uid', type: String })
    @Permissions(SystemPermissions.SYSTEM_VIEW_PATIENT)
    @ApiOperation({ summary: 'Получить информацию о пациенте по UID' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о пациенте', type: PatientResponse })
    async findOne(@Param('company_id', ParseIntPipe) company_id: number, @Param('uid') uid: string) {
        const patient = await this.patientService.findOne(company_id, uid);
        return new PatientResponse(patient);
    }

    @Post(':company_id')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiBody({ type: CreatePatientDto })
    @Permissions(SystemPermissions.SYSTEM_CREATE_PATIENT)
    @ApiOperation({ summary: 'Добавить пациента' })
    @ApiResponse({ status: 200, description: 'Успешное добавление пациента', type: PatientResponse })
    async create(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Body() dto: CreatePatientDto,
    ) {
        const patient = await this.patientService.create(company_id, dto);

        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Patient,
            entity_id: patient.uid,
            details: `Пользователь ${getUserName(currentUser)} добавил пациента ${patient.fio}`,
            type: LogType.SYSTEM,
            metadata: patient,
        });
        return new PatientResponse(patient);
    }

    @Patch(':company_id/:uid')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'uid', type: String })
    @ApiBody({ type: UpdatePatientDto })
    @Permissions(SystemPermissions.SYSTEM_UPDATE_PATIENT)
    @ApiOperation({ summary: 'Обновление пациента' })
    @ApiResponse({ status: 200, description: 'Пациент успешно обновлен', type: PatientResponse })
    async update(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('uid') uid: string,
        @Body() dto: UpdatePatientDto,
    ) {
        const patient = await this.patientService.update(company_id, uid, dto);

        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Patient,
            entity_id: patient.uid,
            details: `Пользователь ${getUserName(currentUser)} обновил пациента ${patient.fio}`,
            type: LogType.SYSTEM,
            metadata: patient,
        });
        return new PatientResponse(patient);
    }

    @Delete(':company_id/:uid')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'uid', type: String })
    @Permissions(SystemPermissions.SYSTEM_DELETE_PATIENT)
    @ApiOperation({ summary: 'Удаление пациента' })
    @ApiResponse({ status: 200, description: 'Пациент успешно удален', type: PatientResponse })
    async delete(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('uid') uid: string,
    ) {
        const patient = await this.patientService.findOne(company_id, uid);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Patient,
            entity_id: patient.uid,
            details: `Пользователь ${getUserName(currentUser)} удалил пациента ${patient.fio}`,
            type: LogType.SYSTEM,
            metadata: patient,
        });
        return await this.patientService.delete(company_id, uid);
    }
}
