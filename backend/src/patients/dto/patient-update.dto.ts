import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsEmpty, IsString } from 'class-validator';

export class UpdatePatientDto {
    @IsString()
    @ApiProperty({ description: 'UID' })
    uid: string;

    @IsString()
    @ApiProperty({ description: 'ФИО' })
    fio?: string;

    @IsString()
    @ApiProperty({ description: 'Телефон' })
    phone?: string;

    @IsDate()
    @IsEmpty()
    @ApiProperty({ description: 'Дата рождения' })
    dob?: Date;
}
