import { ApiProperty } from '@nestjs/swagger';
import { Patient } from '@prisma/postgres/client';
import { Exclude } from 'class-transformer';

export class PatientResponse implements Patient {
    @ApiProperty({ description: 'UID' })
    uid: string;

    @ApiProperty({ description: 'ФИО' })
    fio: string;

    @ApiProperty({ description: 'Представление' })
    fullinfo: string;

    @ApiProperty({ description: 'Телефон' })
    phone: string;

    @ApiProperty({ description: 'Дата рождения' })
    dob: Date;

    @Exclude()
    company_id: number;

    constructor(patient: Patient) {
        Object.assign(this, patient);
    }
}
