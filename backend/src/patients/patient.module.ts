import { EncryptionService } from '@/encryption/encryption.service';
import { LogsModule } from '@/logs/logs.module';
import { UserModule } from '@/users/user.module';
import { WebhookService } from '@/webhooks/webhooks.service';
import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';
import { PatientController } from './patient.controller';
import { PatientService } from './patient.service';
@Module({
    controllers: [PatientController],
    providers: [PatientService, WebhookService, EncryptionService],
    exports: [PatientService],
    imports: [CacheModule.register(), UserModule, LogsModule],
})
export class PatientModule {}
