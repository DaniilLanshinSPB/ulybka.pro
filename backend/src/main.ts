import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as compression from 'compression';
import * as cookieParser from 'cookie-parser';
import { AppModule } from './app.module';

async function bootstrap() {
    console.log('Starting the application');

    const app = await NestFactory.create(AppModule);
    app.use(cookieParser());
    app.setGlobalPrefix('api');
    app.useGlobalInterceptors();

    // Gzip configuration
    app.use(compression());

    // CORS configuration

    app.enableCors({
        origin: ['http://localhost:3000', 'http://localhost:4000'],
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        allowedHeaders: ['Content-Type', 'Origin', 'X-Requested-With', 'Accept', 'Authorization', 'X-API-KEY'],
        exposedHeaders: ['Authorization', 'X-API-KEY'],
        credentials: true,
    });

    //Swagger configuration
    const privateConfig = new DocumentBuilder()

        .setTitle('Документация для работы с API')
        .setDescription(
            'Сервис предназначен для удобной работы с заказами и клиентами для стоматологических лабораторий, ниже приведен доступный список методов для работы с сервисом по APi',
        )
        .setVersion(process.env.PROJECT_VERSION)
        .build();
    const privateDocument = SwaggerModule.createDocument(app, privateConfig, {});
    SwaggerModule.setup('docs', app, privateDocument);

    await app.listen(process.env.PORT);
}
bootstrap();
