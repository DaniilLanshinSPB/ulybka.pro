import { Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as dotenv from 'dotenv';

import { AuthService } from '@/auth/auth.service';
import TelegramBot from 'node-telegram-bot-api';
import { MENU, MESSAGES } from './data/messages';
import { TELEGRAM_BOT } from './telegram.provider';

dotenv.config();

@Injectable()
export class TelegramService {
    protected readonly logger = new Logger(TelegramService.name);

    constructor(
        protected readonly configService: ConfigService,
        protected readonly authService: AuthService,
        @Inject(TELEGRAM_BOT) private readonly bot: TelegramBot,
    ) {}

    onModuleInit() {
        this.bot.onText(/\/start(?: (.+))?/, async (msg: TelegramBot.Message, match: RegExpExecArray | null) => {
            const chatId = msg.chat.id;
            const username = msg.from.username;
            const param = match ? match[1] : null;

            if (param) {
                const user = await this.authService.connectUserTelegram(param, username, chatId);
                if (!user) {
                    this.bot.sendMessage(chatId, `Неизвестная комманда`, {
                        parse_mode: 'HTML',
                    });
                } else {
                    this.bot.sendMessage(chatId, MESSAGES.SUCCESS_CONNECT, {
                        parse_mode: 'HTML',
                    });
                }
            } else {
                this.bot.sendMessage(chatId, MESSAGES.START_MESSAGE, {
                    parse_mode: 'HTML',
                    reply_markup: MENU.START_MENU,
                });
            }
        });

        this.bot.onText(/\/autologin(.+)/, (msg: TelegramBot.Message, match: RegExpExecArray | null) => {
            const chatId = msg.chat.id;
            if (match) {
                const paramsString = match[1].trim();
                const params = new URLSearchParams(paramsString);

                const key = params.get('key');

                if (key) {
                    this.bot.sendMessage(chatId, `Ваш ключ: ${key}`);
                } else {
                    this.bot.sendMessage(chatId, 'Ключ не найден.');
                }
            } else {
                this.bot.sendMessage(chatId, 'Неверный формат команды.');
            }
        });
    }
}
