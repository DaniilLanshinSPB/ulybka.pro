import { HttpModule } from '@nestjs/axios';
import { CacheModule, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { AuthService } from '@/auth/auth.service';
import { CompanyModule } from '@/companies/company.module';
import { LogsModule } from '@/logs/logs.module';
import { MailModule } from '@/mail/mail.module';
import { NoticeService } from '@/notice/notice.service';
import { NotificationsGateway } from '@/notice/notification.gateway';
import { PatientModule } from '@/patients/patient.module';
import { UserModule } from '@/users/user.module';
import { TELEGRAM_BOT, TelegramBotProvider } from './telegram.provider';
import { TelegramService } from './telegram.service';

@Module({
    providers: [TelegramBotProvider, TelegramService, AuthService, NoticeService, NotificationsGateway],
    exports: [TelegramService, TELEGRAM_BOT],
    imports: [
        MailModule,
        PassportModule,
        CompanyModule,
        UserModule,
        JwtModule,
        PatientModule,
        HttpModule,
        LogsModule,
        CacheModule.register(),
    ],
})
export class TelegramModule {}
