import { Provider } from '@nestjs/common';
import * as dotenv from 'dotenv';
import * as TelegramBot from 'node-telegram-bot-api';
dotenv.config();

export const TELEGRAM_BOT = 'TELEGRAM_BOT';
const TELEGRAM_TOKEN = process.env.TELEGRAM_TOKEN;

export const TelegramBotProvider: Provider = {
    provide: TELEGRAM_BOT,
    useFactory: () => {
        const bot = new TelegramBot(TELEGRAM_TOKEN, { polling: true });
        return bot;
    },
};
