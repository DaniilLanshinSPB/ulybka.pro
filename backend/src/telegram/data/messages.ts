import { InlineKeyboardMarkup } from 'node-telegram-bot-api';

export enum COMMANDS {}

export const MESSAGES = {
    START_MESSAGE: 'Добро пожаловать в телеграм-бота сервиса Cmacom\nЧтобы начать работу воспользуйтесь меню',
    SUCCESS_CONNECT: 'Аккаунт успешно подключен',
};
export const MENU: Record<string, InlineKeyboardMarkup> = {
    START_MENU: {
        inline_keyboard: [[{ text: 'Подключить аккаунт', callback_data: 'login' }]],
    },
};
