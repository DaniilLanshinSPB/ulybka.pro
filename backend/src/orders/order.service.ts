import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EntityType, Order, WebhookEvents } from '@prisma/postgres/client';
import { setHours, setMinutes, setSeconds } from 'date-fns';

import { PrismaService } from '@/database/postgres-prisma.service';
import { WebhookService } from '@/webhooks/webhooks.service';
import { OrderCreateDto } from './dto';
import { OrdersFilterDto } from './dto/order-filter.dto';
import { DEFAULT_ORDER_STATUS_ID } from './interface';

@Injectable()
export class OrderService {
    protected readonly logger = new Logger(OrderService.name);

    constructor(
        protected readonly prismaService: PrismaService,
        protected readonly configService: ConfigService,
        protected readonly webhookService: WebhookService,
    ) {}

    async findAll(filter: OrdersFilterDto = {}, withDeleted = false, user_id: number | null = null): Promise<Order[]> {
        const whereCondition = {};
        if (filter.company_id) {
            whereCondition['company_id'] = { in: filter.company_id };
        }
        if (filter.customer_company_id) {
            whereCondition['customer_company_id'] = { in: filter.customer_company_id };
        }
        if (filter.status_id && filter.status_id.length) {
            whereCondition['status_id'] = {
                in: filter.status_id.map((status_id) => Number(status_id)),
            };
        }
        if (filter.patient_uid && filter.patient_uid.length) {
            whereCondition['patient_uid'] = {
                in: filter.patient_uid,
            };
        }
        if (user_id) {
            whereCondition['customer_id'] = user_id;
        }

        if (filter.customer_id && filter.customer_id.length) {
            whereCondition['customer_id'] = {
                in: filter.customer_id.map((customer_id) => Number(customer_id)),
            };
        }
        if (filter.responsible_id && filter.responsible_id.length) {
            whereCondition['responsible_id'] = {
                in: filter.responsible_id.map((responsible_id) => Number(responsible_id)),
            };
        }
        if (filter.executor_id && filter.executor_id.length) {
            whereCondition['executor_id'] = {
                in: filter.executor_id.map((executor_id) => Number(executor_id)),
            };
        }
        if (filter.created_at_range) {
            whereCondition['created_at'] = {
                gte: setSeconds(setMinutes(setHours(new Date(filter.created_at_range.from), 0), 0), 0),
                lte: setSeconds(setMinutes(setHours(new Date(filter.created_at_range.to), 23), 59), 59),
            };
        }
        if (filter.finished_at_range) {
            whereCondition['finish_at'] = {
                gte: setSeconds(setMinutes(setHours(new Date(filter.finished_at_range.from), 0), 0), 0),
                lte: setSeconds(setMinutes(setHours(new Date(filter.finished_at_range.to), 23), 59), 59),
            };
        }

        whereCondition['deleted_at'] = null;

        if (withDeleted) {
            whereCondition['deleted_at'] = {
                not: null,
            };
        }

        return await this.prismaService.order.findMany({
            where: whereCondition,
            orderBy: [
                {
                    id: 'desc',
                },
                {
                    status_id: 'asc',
                },
            ],
            include: {
                status: true,
                customer: {
                    include: {
                        company: true,
                    },
                },
                customer_company: true,
                patient: true,
                creator: true,
                responsible: true,
                executor: true,
                order_services: {
                    include: {
                        service: true,
                    },
                },
            },
        });
    }
    async findOne(company_id: number, id: number) {
        const whereCondition = {
            id,
        };
        if (company_id) {
            whereCondition['company_id'] = company_id;
        }
        const order = await this.prismaService.order.findUnique({
            where: whereCondition,
            include: {
                status: true,
                customer: {
                    include: {
                        company: true,
                    },
                },
                customer_company: true,
                patient: true,
                creator: true,
                responsible: true,
                executor: true,
                order_services: {
                    include: {
                        service: true,
                    },
                },
                status_history: {
                    include: {
                        status: true,
                    },
                },
            },
        });
        if (!order) {
            return null;
        }
        const logs = await this.prismaService.logs.findMany({
            where: {
                entity_type: EntityType.Order,
                entity_id: order.id.toString(),
            },
            orderBy: {
                id: 'desc',
            },
        });
        return { ...order, logs };
    }
    async findById(id: number) {
        return this.prismaService.order.findUnique({
            where: { id },
            include: { order_services: true },
        });
    }
    async create(company_id: number, creator_id: number, customer_id: number, order: OrderCreateDto) {
        const lastOrder = await this.prismaService.order.findFirst({
            orderBy: { id: 'desc' },
            select: { id: true },
        });
        const company = await this.prismaService.company.findFirst({
            where: { id: company_id },
            include: { childrens: true },
        });
        const clinicIds = company.childrens.map((child) => child.id);

        const lastOrderId = lastOrder?.id || 0;

        let patient = await this.prismaService.patient.findFirst({
            where: {
                uid: order?.patient.uid,
                company_id: { in: clinicIds },
            },
        });

        if (!patient) {
            patient = await this.prismaService.patient.create({
                data: {
                    uid: order?.patient.uid,
                    fio: order?.patient.fio,
                    fullinfo: order?.patient.fullinfo,
                    phone: order?.patient.phone,
                    dob: new Date(order?.patient.dob),
                    company_id: company_id,
                },
            });
        }

        try {
            const savedOrder = await this.prismaService.order.create({
                data: {
                    company_id: company_id,
                    title: `Заказ №${lastOrderId + 1}`,
                    description: order.description,
                    status_id: DEFAULT_ORDER_STATUS_ID,

                    customer_id: customer_id,
                    patient_uid: patient.uid,
                    creator_id: creator_id,
                    responsible_id: order.responsible_id || null,
                    executor_id: order.executor_id || null,
                    finish_at: order.finish_at || null,
                    prefer_date_at: order.prefer_date_at || null,
                    customer_company_id: order.customer_company_id,
                    order_services: {
                        create: order.services.map((service) => ({
                            title: service.title,
                            service_id: service.service_id,
                            metadata: service.metadata,
                        })),
                    },
                },
                include: {
                    status: true,
                    customer: true,
                    patient: true,
                    creator: true,
                    responsible: true,
                    executor: true,
                    customer_company: true,
                },
            });
            const webhooks = await this.webhookService.getWebhookByType(company_id, WebhookEvents.UPDATE_ORDER);
            await this.webhookService.sendData(webhooks, savedOrder);
            return savedOrder;
        } catch (err: any) {
            console.log(err);
            throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
        }
    }

    async update(id: number, order: Partial<Order>) {
        const _order = await this.prismaService.order.findFirst({
            where: {
                id: Number(id),
            },
        });

        if (!_order) {
            throw new HttpException('Заказ не найден', HttpStatus.BAD_REQUEST);
        }
        const savedOrder = await this.prismaService.order.update({
            where: {
                id: Number(id),
            },
            data: {
                description: order.description,
                status_id: order.status_id || DEFAULT_ORDER_STATUS_ID,
                responsible_id: order.responsible_id || null,
                executor_id: order.executor_id || null,
                finish_at: order.finish_at || null,
                prefer_date_at: order.prefer_date_at || null,
            },
        });
        if (_order.status_id !== order.status_id) {
            await this.prismaService.orderStatusHistory.create({
                data: {
                    order_id: _order.id,
                    status_id: order.status_id,
                },
            });
        }
        const webhooks = await this.webhookService.getWebhookByType(_order.company_id, WebhookEvents.UPDATE_ORDER);
        await this.webhookService.sendData(webhooks, savedOrder);
        return this.findOne(_order.company_id, _order.id);
    }

    async softDelete(id: number, reason: string) {
        const _order = await this.prismaService.order.findUnique({
            where: { id: Number(id) },
        });
        if (!_order) {
            throw new HttpException('Невозможно удалить данный заказ', HttpStatus.BAD_REQUEST);
        }

        try {
            return this.prismaService.order.update({
                where: { id: Number(id) },
                data: {
                    deleted_at: new Date(),
                    delete_reason: reason,
                },
            });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async delete(id: number) {
        const _order = await this.prismaService.order.findUnique({
            where: { id: Number(id) },
        });
        if (!_order) {
            throw new HttpException('Невозможно удалить данный заказ', HttpStatus.BAD_REQUEST);
        }

        try {
            return this.prismaService.order.delete({ where: { id: Number(id) }, select: { id: true } });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async restore(id: number) {
        const _order = await this.prismaService.order.findUnique({
            where: { id: Number(id) },
        });
        if (!_order) {
            throw new HttpException('Невозможно восстановить данный заказ', HttpStatus.BAD_REQUEST);
        }

        try {
            return this.prismaService.order.update({ where: { id: Number(id) }, data: { deleted_at: null } });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
