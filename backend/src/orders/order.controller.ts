import { NotVerifiedGuard } from '@/auth/guards/not_verified.guard';
import { PermissionsGuard } from '@/auth/guards/permission.guard';
import { JwtPayload, SystemPermissions } from '@/auth/interfaces';
import { CompanyService } from '@/companies/company.service';
import { LogsService } from '@/logs/logs.service';
import { UserService } from '@/users/user.service';
import { CurrentUser, Permissions } from '@common/decorators';
import { getUserName } from '@common/helpers/user.helper';
import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    NotFoundException,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    Query,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiOperation, ApiParam, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { EntityType, LogType } from '@prisma/postgres/client';
import { OrderCreateDto, OrderUpdateDto } from './dto';
import { OrderCommentCreateDto } from './dto/order-comment-create.dto';
import { OrdersFilterDto } from './dto/order-filter.dto';
import { OrderStatusCreateDto } from './dto/order-status-create.dto';
import { OrderStatusUpdateDto } from './dto/order-status-update.dto';
import { OrderCommentService } from './order-comment.service';
import { OrderStatusService } from './order-status.service';
import { OrderService } from './order.service';
import { OrderResponse } from './responses';
import { OrderCommentResponse } from './responses/order-comment.response';
import { OrderStatusResponse } from './responses/order-status.response';

// @ApiExcludeController()
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(NotVerifiedGuard)
@UseGuards(PermissionsGuard)
@Controller('orders')
export class OrderController {
    constructor(
        protected readonly orderService: OrderService,
        protected readonly companyService: CompanyService,
        protected readonly userService: UserService,
        protected readonly statusService: OrderStatusService,
        protected readonly commentService: OrderCommentService,
        protected readonly logsService: LogsService,
    ) {}

    // ORDERS
    @Get(':company_id/all')
    @Permissions(SystemPermissions.SYSTEM_VIEW_ORDER)
    @ApiParam({ name: 'company_id', type: Number })
    @ApiQuery({ name: 'filter', type: OrdersFilterDto })
    @ApiOperation({ summary: 'Получить все заказы' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о заказах', type: [OrderResponse] })
    async findAllOrders(
        @Param('company_id', ParseIntPipe) company_id: number,
        @Query('filter') filter: OrdersFilterDto = {},
    ) {
        const company = await this.companyService.findOne(Number(company_id));

        const where = {};
        if (company.childrens?.length) {
            where['company_id'] = [Number(company_id), ...company.childrens.map((child) => child.id)];
        } else {
            where['customer_company_id'] = [Number(company_id)];
        }

        const orders = await this.orderService.findAll(
            {
                ...where,
                ...filter,
            },
            false,
        );

        return orders.map((order) => new OrderResponse(order));
    }
    //ORDER STATUSE
    @Get('statuses')
    @ApiOperation({ summary: 'Получить все статусы' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о статусах', type: [OrderStatusResponse] })
    async findAllStatuses() {
        const statuses = await this.statusService.findAll();
        return statuses.map((status) => new OrderStatusResponse(status));
    }

    @Get(':company_id/:id')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_VIEW_ORDER)
    @ApiOperation({ summary: 'Получить информацию о заказе по ID' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о заказе', type: OrderResponse })
    async findOneOrder(@Param('id', ParseIntPipe) id: number, @Param('company_id', ParseIntPipe) company_id: number) {
        const order = await this.validateCompanyOrder(company_id, id);
        return new OrderResponse(order);
    }

    @Post(':company_id/create')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiBody({ type: OrderCreateDto })
    @Permissions(SystemPermissions.SYSTEM_CREATE_ORDER)
    @ApiOperation({ summary: 'Создание заказа' })
    @ApiResponse({ status: 200, description: 'Заказ успешно создана' })
    async createOrder(
        @CurrentUser() currentUser: JwtPayload,
        @Body() dto: OrderCreateDto,
        @Param('company_id', ParseIntPipe) company_id: number,
    ) {
        const order = await this.orderService.create(company_id, dto.customer_id, dto.customer_id, dto);

        const metadata = {
            id: order.id,
            title: order.title,
            description: order.description,
            status_id: order.status_id,
            customer_id: order.customer_id,
            patient_uid: order.patient_uid,
            creator_id: order.creator_id,
            responsible_id: order.responsible_id,
            executor_id: order.executor_id,
            finish_at: order.finish_at,
            prefer_date_at: order.prefer_date_at,
            customer_company_id: order.customer_company_id,
        };
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Order,
            entity_id: order.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} создал новый заказ ${order.title}`,
            metadata: metadata,
            type: LogType.SYSTEM,
        });
        return new OrderResponse(order);
    }

    @Patch(':company_id/:id')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'id', type: Number })
    @ApiBody({ type: OrderUpdateDto })
    @Permissions(SystemPermissions.SYSTEM_UPDATE_ORDER)
    @ApiOperation({ summary: 'Обновление заказа' })
    @ApiResponse({ status: 200, description: 'Заказ успешно обновлена' })
    async updateOrder(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('id', ParseIntPipe) id: number,
        @Body() dto: OrderUpdateDto,
    ) {
        await this.validateCompanyOrder(company_id, id);
        const order = await this.orderService.update(id, dto);
        const metadata = {
            id: order.id,
            title: order.title,
            description: order.description,
            status_id: order.status_id,
            customer_id: order.customer_id,
            patient_uid: order.patient_uid,
            creator_id: order.creator_id,
            responsible_id: order.responsible_id,
            executor_id: order.executor_id,
            finish_at: order.finish_at,
            prefer_date_at: order.prefer_date_at,
            customer_company_id: order.customer_company_id,
        };
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Order,
            entity_id: order.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} обновил заказ ${order.title}`,
            metadata,
            type: LogType.SYSTEM,
        });
        return new OrderResponse(order);
    }

    @Delete(':company_id/:id')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'id', type: Number })
    @ApiQuery({ name: 'reason', type: String })
    @Permissions(SystemPermissions.SYSTEM_DELETE_ORDER)
    @ApiOperation({ summary: 'Удалить заказ по ID' })
    @ApiResponse({ status: 200, description: 'Заказ успешно удален' })
    @ApiResponse({ status: 400, description: 'Заказ не найден' })
    async softDeleteOrder(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('id', ParseIntPipe) id: number,
        @Query('reason') reason: string,
    ) {
        const order = await this.validateCompanyOrder(company_id, id);
        const metadata = {
            id: order.id,
            title: order.title,
            description: order.description,
            status_id: order.status_id,
            customer_id: order.customer_id,
            patient_uid: order.patient_uid,
            creator_id: order.creator_id,
            responsible_id: order.responsible_id,
            executor_id: order.executor_id,
            finish_at: order.finish_at,
            prefer_date_at: order.prefer_date_at,
            customer_company_id: order.customer_company_id,
        };
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Order,
            entity_id: order.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} удалил заказ ${order.title}`,
            metadata: metadata,
            type: LogType.SYSTEM,
        });
        return this.orderService.softDelete(Number(id), reason);
    }

    @Delete(':company_id/:id/hard')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_DELETE_ORDER)
    @ApiOperation({ summary: 'Удалить заказ по ID' })
    @ApiResponse({ status: 200, description: 'Заказ успешно удален' })
    @ApiResponse({ status: 400, description: 'Заказ не найден' })
    async deleteOrder(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('id', ParseIntPipe) id: number,
    ) {
        const order = await this.validateCompanyOrder(company_id, id);
        const metadata = {
            id: order.id,
            title: order.title,
            description: order.description,
            status_id: order.status_id,
            customer_id: order.customer_id,
            patient_uid: order.patient_uid,
            creator_id: order.creator_id,
            responsible_id: order.responsible_id,
            executor_id: order.executor_id,
            finish_at: order.finish_at,
            prefer_date_at: order.prefer_date_at,
            customer_company_id: order.customer_company_id,
        };
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Order,
            entity_id: order.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} удалил заказ ${order.title}`,
            type: LogType.SYSTEM,
            metadata: metadata,
        });
        return this.orderService.delete(id);
    }

    @Get(':company_id/:id/restore')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_UPDATE_ORDER)
    @ApiOperation({ summary: 'Восстановить заказ по ID' })
    @ApiResponse({ status: 200, description: 'Заказ восстановлен' })
    @ApiResponse({ status: 400, description: 'Заказ не найден' })
    async restore(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('id', ParseIntPipe) id: number,
    ) {
        const order = await this.validateCompanyOrder(company_id, id);
        const metadata = {
            id: order.id,
            title: order.title,
            description: order.description,
            status_id: order.status_id,
            customer_id: order.customer_id,
            patient_uid: order.patient_uid,
            creator_id: order.creator_id,
            responsible_id: order.responsible_id,
            executor_id: order.executor_id,
            finish_at: order.finish_at,
            prefer_date_at: order.prefer_date_at,
            customer_company_id: order.customer_company_id,
        };
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Order,
            entity_id: order.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} восстановил заказ ${order.title}`,
            metadata,
            type: LogType.SYSTEM,
        });
        return this.orderService.restore(id);
    }

    @Post('statuses')
    @ApiOperation({ summary: 'Создание статуса' })
    @ApiResponse({ status: 200, description: 'Статус успешно создана' })
    async createStatus(@CurrentUser() currentUser: JwtPayload, @Body() dto: OrderStatusCreateDto) {
        const status = await this.statusService.create(dto);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Status,
            entity_id: status.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} добавил новый статус ${status.name}`,
            type: LogType.SYSTEM,
            metadata: status,
        });
        return new OrderStatusResponse(status);
    }

    @Patch('statuses/:id')
    @ApiParam({ name: 'id', type: Number })
    @ApiBody({ type: OrderStatusUpdateDto })
    @Permissions(SystemPermissions.SYSTEM_UPDATE_ORDER)
    @ApiOperation({ summary: 'Обновление заказа' })
    @ApiResponse({ status: 200, description: 'Заказ успешно обновлена' })
    async updateStatus(
        @CurrentUser() currentUser: JwtPayload,
        @Param('id', ParseIntPipe) id: number,
        @Body() dto: OrderStatusUpdateDto,
    ) {
        const status = await this.statusService.update(id, dto);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Status,
            entity_id: status.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} обновил статус ${status.name}`,
            type: LogType.SYSTEM,
            metadata: status,
        });
        return new OrderStatusResponse(status);
    }
    @Delete('statuses/:id')
    @ApiParam({ name: 'id', type: Number })
    @ApiOperation({ summary: 'Удалить статус по ID' })
    @ApiResponse({ status: 200, description: 'Статус успешно удален' })
    @ApiResponse({ status: 400, description: 'Статус не найден' })
    async deleteStatus(@CurrentUser() currentUser: JwtPayload, @Param('id', ParseIntPipe) id: number) {
        const status = await this.statusService.findOne(id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Status,
            entity_id: status.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} удалил статус ${status.name}`,
            type: LogType.SYSTEM,
            metadata: status,
        });
        return this.statusService.delete(id);
    }

    //ORDER COMMENTS
    @Get(':company_id/:order_id/comments/all')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'order_id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_VIEW_ORDER_COMMENT)
    @ApiOperation({ summary: 'Получить все комментарии' })
    @ApiResponse({
        status: 200,
        description: 'Успешное получение информации о комментариях',
        type: [OrderCommentResponse],
    })
    async findAllComments(
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('order_id', ParseIntPipe) order_id: number,
    ) {
        await this.validateCompanyOrder(company_id, order_id);
        const comments = await this.commentService.findAll(order_id);
        return comments.map((comment) => new OrderCommentResponse(comment));
    }

    @Post(':company_id/:order_id/comments')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'order_id', type: Number })
    @ApiBody({ type: OrderCommentCreateDto })
    @Permissions(SystemPermissions.SYSTEM_CREATE_ORDER_COMMENT)
    @ApiOperation({ summary: 'Создание комментария' })
    @ApiResponse({ status: 200, description: 'Комментарий успешно создана' })
    async createComment(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('order_id', ParseIntPipe) order_id: number,
        @Body() dto: OrderCommentCreateDto,
        @CurrentUser() user: JwtPayload,
    ) {
        const order = await this.validateCompanyOrder(company_id, order_id);
        const comment = await this.commentService.create(order_id, dto, user.id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.OrderComment,
            entity_id: comment.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} добавил новый комментарий "${
                comment.message
            }" к заказу ${order.title}`,
            type: LogType.SYSTEM,
            metadata: comment,
        });
        return new OrderCommentResponse(comment);
    }
    @Delete(':company_id/:order_id/comments/:id')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'order_id', type: Number })
    @ApiParam({ name: 'id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_DELETE_ORDER_COMMENT)
    @ApiOperation({ summary: 'Удалить комментарий по ID' })
    @ApiResponse({ status: 200, description: 'Комментарий успешно удален' })
    @ApiResponse({ status: 400, description: 'Комментарий не найден' })
    async deleteComment(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('order_id', ParseIntPipe) order_id: number,
        @Param('id', ParseIntPipe) id: number,
        @CurrentUser() user: JwtPayload,
    ) {
        const order = await this.validateCompanyOrder(company_id, order_id);
        const comment = await this.commentService.findOne(id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.OrderComment,
            entity_id: comment.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} удалил комментарий "${comment.message}" к заказу ${
                order.title
            }`,
            type: LogType.SYSTEM,
            metadata: comment,
        });
        return this.commentService.delete(id, user.id);
    }

    async validateCompanyOrder(company_id: number, order_id: number) {
        const company = await this.companyService.findOne(Number(company_id));
        if (!company) throw new NotFoundException('Компания не найдена');
        const _company_id = company?.parent ? company.parent.id : company.id;
        const order = await this.orderService.findOne(_company_id, Number(order_id));
        if (!order) throw new NotFoundException('Заказ не найден');
        return order;
    }
}
