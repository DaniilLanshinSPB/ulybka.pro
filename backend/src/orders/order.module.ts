import { CompanyModule } from '@/companies/company.module';
import { LogsModule } from '@/logs/logs.module';
import { UserModule } from '@/users/user.module';
import { WebhookService } from '@/webhooks/webhooks.service';
import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';
import { OrderCommentService } from './order-comment.service';
import { OrderStatusService } from './order-status.service';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';
import { EncryptionService } from '@/encryption/encryption.service';

@Module({
    providers: [OrderService, OrderStatusService, OrderCommentService, WebhookService, EncryptionService],
    exports: [OrderService, OrderStatusService, OrderCommentService],
    controllers: [OrderController],
    imports: [CacheModule.register(), LogsModule, UserModule, CompanyModule, LogsModule],
})
export class OrderModule {}
