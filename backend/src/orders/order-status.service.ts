import { PrismaService } from '@/database/postgres-prisma.service';
import { transliterate } from '@common/helpers';
import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { OrderStatus } from '@prisma/postgres/client';

@Injectable()
export class OrderStatusService {
    protected readonly logger = new Logger(OrderStatusService.name);

    constructor(protected readonly prismaService: PrismaService, protected readonly configService: ConfigService) {}

    async findAll(): Promise<OrderStatus[]> {
        return await this.prismaService.orderStatus.findMany({ orderBy: { sort: 'asc' } });
    }
    async findOne(id: number): Promise<OrderStatus> {
        const status = await this.prismaService.orderStatus.findUnique({
            where: {
                id,
            },
        });
        if (!status) {
            return null;
        }
        return status;
    }

    async create(status: Partial<OrderStatus>) {
        const _status = await this.prismaService.orderStatus.findFirst({
            where: {
                name: status.name,
            },
        });

        if (_status) {
            throw new HttpException('Статус с таким именем уже существует', HttpStatus.BAD_REQUEST);
        }

        const slug = transliterate(status.name);

        try {
            const savedStatus = await this.prismaService.orderStatus.create({
                data: {
                    slug,
                    name: status.name,
                },
            });

            return savedStatus;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    async update(id: number, status: Partial<OrderStatus>) {
        const _duplicate_status = await this.prismaService.orderStatus.findFirst({
            where: {
                name: status.name,
                id: {
                    not: Number(id),
                },
            },
        });

        if (_duplicate_status) {
            throw new HttpException('Статус с таким именем уже существует', HttpStatus.BAD_REQUEST);
        }

        if (id < 3) {
            throw new HttpException('Невозможно изменить данный статус', HttpStatus.BAD_REQUEST);
        }

        const slug = transliterate(status.name);

        const savedStatus = await this.prismaService.orderStatus.update({
            where: {
                id: Number(id),
            },
            data: {
                name: status.name,
                slug,
            },
        });

        return savedStatus;
    }

    async delete(id: number) {
        if (id < 3) {
            throw new HttpException('Невозможно удалить данный статус', HttpStatus.BAD_REQUEST);
        }

        try {
            return this.prismaService.orderStatus.delete({ where: { id: Number(id) }, select: { id: true } });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
