import { ApiProperty } from '@nestjs/swagger';
import { OrderStatus } from '@prisma/postgres/client';

export class OrderStatusResponse implements OrderStatus {
    @ApiProperty({ description: 'Идентификатор' })
    id: number;

    @ApiProperty({ description: 'Индекс сортировки' })
    sort: number;

    @ApiProperty({ description: 'Символьное название' })
    slug: string;

    @ApiProperty({ description: 'Название' })
    name: string;

    constructor(orderStatus: OrderStatus) {
        Object.assign(this, orderStatus);
    }
}
