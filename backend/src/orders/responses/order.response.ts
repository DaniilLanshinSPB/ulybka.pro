import { ProfileResponse } from '@/auth/responses';
import { ApiProperty } from '@nestjs/swagger';
import {
    Company,
    Logs,
    Order,
    OrderService,
    OrderStatus,
    OrderStatusHistory,
    Patient,
    Prisma,
} from '@prisma/postgres/client';
import { Exclude } from 'class-transformer';
import { OrderStatusResponse } from './order-status.response';

export class StatusHistoryResponse {
    @ApiProperty({ description: 'Идентификатор' })
    id: number;
    @ApiProperty({ description: 'Название' })
    name: string;
    @ApiProperty({ description: 'Дата' })
    created_at: Date;
}
export class OrderResponse implements Order {
    @ApiProperty({ description: 'Идентификатор пользователя' })
    id: number;

    @ApiProperty({ description: 'Название' })
    title: string;

    @ApiProperty({ description: 'Описание' })
    description: string;

    @Exclude()
    status_id: number;

    @ApiProperty({ description: 'Статус' })
    status: OrderStatusResponse;

    @ApiProperty({ description: 'Услуги' })
    services: OrderService[];

    @Exclude()
    customer_id: number;

    @ApiProperty({ description: 'Идентификатор компании' })
    company_id: number;

    @Exclude()
    customer_company_id: number;

    @Exclude()
    company: Company;

    @ApiProperty({ description: 'Компания заказчик' })
    customer_company: Company;

    @ApiProperty({ description: 'Заказчик' })
    customer: ProfileResponse;

    @Exclude()
    patient_uid: string;

    @ApiProperty({ description: 'Пациент' })
    patient: Patient;

    @Exclude()
    creator_id: number;

    @ApiProperty({ description: 'Создатель заказа' })
    creator: ProfileResponse;

    @Exclude()
    responsible_id: number;

    @ApiProperty({ description: 'Главный техник' })
    responsible: ProfileResponse;

    @Exclude()
    executor_id: number;

    @ApiProperty({ description: 'Техник' })
    executor: ProfileResponse;

    @ApiProperty({ description: 'Метадата заполненных полей' })
    metadata: Prisma.JsonValue;

    @ApiProperty({ description: 'Дата создания' })
    created_at: Date;

    @ApiProperty({ description: 'Дата завершения' })
    finish_at: Date | null;

    @ApiProperty({ description: 'Дата обновления' })
    updated_at: Date;

    @ApiProperty({ description: 'Желаемая дата' })
    prefer_date_at: Date;

    @ApiProperty({ description: 'Дата удаления' })
    deleted_at: Date;

    @ApiProperty({ description: 'Причина удаления' })
    delete_reason: string;

    @ApiProperty({ description: 'История статусов' })
    status_history: StatusHistoryResponse[];

    constructor(
        order: Order & {
            status_history?: (OrderStatusHistory & { status: OrderStatus })[];
            logs?: Logs[];
        },
    ) {
        const data: any = order;
        if (order.status_history) {
            data.status_history = order.status_history
                .sort((a, b) => a.created_at.getTime() - b.created_at.getTime())
                .map((el) => ({
                    id: el.id,
                    name: el.status.name,
                    created_at: el.created_at,
                }));
        } else {
            data.status_history = [];
        }
        if (order.logs) {
            data.logs = order?.logs.map((el) => ({ id: el.id, title: el.details, created_at: el.created_at })) || [];
        } else {
            data.logs = [];
        }
        Object.assign(this, data);
    }
}
