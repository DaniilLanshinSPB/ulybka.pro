import { ProfileResponse } from '@/auth/responses';
import { ApiProperty } from '@nestjs/swagger';
import { OrderComment } from '@prisma/postgres/client';
import { Exclude } from 'class-transformer';
import { IsDate, IsString } from 'class-validator';
import { OrderResponse } from './order.response';

export class OrderCommentResponse implements OrderComment {
    @ApiProperty({ description: 'Идентификатор пользователя' })
    id: number;

    @Exclude()
    order_id: number;

    @ApiProperty({ description: 'Заказ' })
    order: OrderResponse;

    @IsString()
    @ApiProperty({ description: 'Текст комментария' })
    message: string;

    @Exclude()
    user_id: number;

    @ApiProperty({ description: 'Автор' })
    user: ProfileResponse;

    @IsDate()
    @ApiProperty({ description: 'Дата создания' })
    created_at: Date;

    @IsDate()
    @ApiProperty({ description: 'Дата обновления' })
    updated_at: Date;

    constructor(orderComment: OrderComment) {
        Object.assign(this, orderComment);

        // if (orderComment?.user) {
        //     this.user = new ProfileResponse(orderComment.user);
        // }
    }
}
