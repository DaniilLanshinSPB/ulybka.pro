import { PrismaService } from '@/database/postgres-prisma.service';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { OrderComment } from '@prisma/postgres/client';

@Injectable()
export class OrderCommentService {
    constructor(protected readonly prismaService: PrismaService, protected readonly configService: ConfigService) {}

    async findAll(order_id: number): Promise<OrderComment[]> {
        return await this.prismaService.orderComment.findMany({
            where: { order_id: Number(order_id) },
            orderBy: { created_at: 'asc' },
            include: { user: true },
        });
    }
    async findOne(id: number): Promise<OrderComment> {
        const comment = await this.prismaService.orderComment.findUnique({
            where: {
                id: Number(id),
            },
            include: { user: true },
        });
        if (!comment) {
            return null;
        }
        return comment;
    }

    async create(order_id: number, comment: Partial<OrderComment>, author_id: number) {
        try {
            const savedComment = await this.prismaService.orderComment.create({
                data: {
                    order_id: Number(order_id),
                    message: comment.message,
                    user_id: Number(author_id),
                },
                include: { user: true },
            });

            return savedComment;
        } catch (err) {
            console.log(err);
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    async delete(id: number, author_id: number) {
        const _comment = await this.prismaService.orderComment.findUnique({
            where: { id: Number(id), user_id: Number(author_id) },
        });
        if (!_comment) {
            throw new HttpException('Невозможно удалить данный комментарий', HttpStatus.BAD_REQUEST);
        }

        try {
            return this.prismaService.orderComment.delete({ where: { id: Number(id) }, select: { id: true } });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
