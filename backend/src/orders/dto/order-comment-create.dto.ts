import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class OrderCommentCreateDto {
    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'Комментарий' })
    message: string;
}
