import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';

export class OrderStatusCreateDto {
    @IsNumber()
    @ApiProperty({ description: 'Индекс сортировки' })
    sort: number;

    @IsString()
    @ApiProperty({ description: 'Название' })
    name: string;
}
