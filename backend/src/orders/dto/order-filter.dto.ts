import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsEmpty } from 'class-validator';

export class DateRangeDto {
    @ApiProperty({ description: 'C', type: Date })
    from: Date;
    @ApiProperty({ description: 'По', type: Date })
    to: Date;
}
export class OrdersFilterDto {
    @IsArray()
    @IsEmpty()
    @ApiProperty({ description: 'Компании', type: [Number] })
    company_id?: number[];

    @IsArray()
    @IsEmpty()
    @ApiProperty({ description: 'Компания заказчик', type: [Number] })
    customer_company_id?: number[];

    @IsArray()
    @IsEmpty()
    @ApiProperty({ description: 'Статус', type: [Number] })
    status_id?: number[];

    @IsArray()
    @IsEmpty()
    @ApiProperty({ description: 'Идентификатор пациента', type: [Number] })
    patient_uid?: number[];

    @IsArray()
    @IsEmpty()
    @ApiProperty({ description: 'Идентификатор сервиса', type: [Number] })
    service_id?: number[];

    @IsArray()
    @IsEmpty()
    @ApiProperty({ description: 'Идентификатор заказчика', type: [Number] })
    customer_id?: number[];

    @IsArray()
    @IsEmpty()
    @ApiProperty({ description: 'Идентификатор главного техника', type: [Number] })
    responsible_id?: number[];

    @IsArray()
    @IsEmpty()
    @ApiProperty({ description: 'Идентификатор исполнителя', type: [Number] })
    executor_id?: number[];

    @IsEmpty()
    @ApiProperty({ description: 'Дата создания', type: DateRangeDto })
    created_at_range?: DateRangeDto;

    @IsEmpty()
    @ApiProperty({ description: 'Дата завершения', type: DateRangeDto })
    finished_at_range?: DateRangeDto;

    @IsEmpty()
    @ApiProperty({ description: 'Дата удаления', type: Boolean })
    deleted?: boolean;
}
