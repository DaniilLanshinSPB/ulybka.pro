import { ApiProperty } from '@nestjs/swagger';
import { Patient, Prisma } from '@prisma/postgres/client';
import { IsDate, IsEmpty, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class OrderCreateDto {
    @IsString()
    @IsEmpty()
    @ApiProperty({ description: 'Индекс сортировки', type: String })
    description: string;

    @IsNumber()
    @IsEmpty()
    @ApiProperty({ description: 'Статус', type: Number })
    status_id: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty({ description: 'Пациент' })
    patient: Patient;

    @IsNumber()
    @IsEmpty()
    @ApiProperty({ description: 'Идентификатор заказчика', type: Number })
    customer_id: number;

    @IsNumber()
    @IsEmpty()
    @ApiProperty({ description: 'Идентификатор главного техника', type: Number })
    responsible_id: number;

    @IsNumber()
    @IsEmpty()
    @ApiProperty({ description: 'Идентификатор исполнителя', type: Number })
    executor_id: number;

    @IsEmpty()
    @ApiProperty({ description: 'Метадата по полям', type: Object })
    metadata: Prisma.JsonValue;

    @IsDate()
    @IsEmpty()
    @ApiProperty({ description: 'Дата завершения', type: Date, nullable: true })
    finish_at: Date | null;

    @IsDate()
    @IsEmpty()
    @ApiProperty({ description: 'Желаемая дата', type: Date })
    prefer_date_at: Date;

    @IsNotEmpty()
    @ApiProperty({ description: 'Улсуга', type: Object })
    services: { title: string; service_id: number; metadata: Prisma.JsonValue }[];

    @IsNotEmpty()
    @ApiProperty({ description: 'Компания заказчик', type: Number })
    customer_company_id: number;
}
