import { ApiProperty } from '@nestjs/swagger';
import { Patient, Prisma } from '@prisma/postgres/client';
import { IsDate, IsEmpty, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class OrderUpdateDto {
    @IsString()
    @IsEmpty()
    @ApiProperty({ description: 'Индекс сортировки' })
    description: string;

    @IsNumber()
    @IsEmpty()
    @ApiProperty({ description: 'Статус' })
    status_id: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty({ description: 'Пациент' })
    patient: Patient;

    @IsNumber()
    @IsEmpty()
    @ApiProperty({ description: 'Идентификатор главного техника' })
    responsible_id: number;

    @IsNumber()
    @IsEmpty()
    @ApiProperty({ description: 'Идентификатор исполнителя' })
    executor_id: number;

    @IsEmpty()
    @ApiProperty({ description: 'Метадата по полям' })
    metadata: Prisma.JsonValue;

    @IsDate()
    @IsEmpty()
    @ApiProperty({ description: 'Дата завершения' })
    finish_at: Date | null;
}
