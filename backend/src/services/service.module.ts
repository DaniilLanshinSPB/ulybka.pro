import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';

import { LogsModule } from '@/logs/logs.module';
import { ServiceController } from './service.controller';
import { ServiceService } from './service.service';

@Module({
    providers: [ServiceService],
    exports: [ServiceService],
    controllers: [ServiceController],
    imports: [CacheModule.register(), LogsModule],
})
export class ServiceModule {}
