import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Service, ServiceCategory } from '@prisma/postgres/client';

import { PrismaService } from '@/database/postgres-prisma.service';
import { ServiceCreateDto } from './dto';

@Injectable()
export class ServiceService {
    protected readonly logger = new Logger(ServiceService.name);

    constructor(protected readonly prismaService: PrismaService, protected readonly configService: ConfigService) {}

    async findAll(): Promise<Service[]> {
        const services = await this.prismaService.service.findMany({
            include: {
                category: true,
                fields: {
                    select: {
                        field: true,
                    },
                },
            },
        });
        const response: any[] = services;
        response.map((service) => {
            service.fields = service.fields.map((field) => field.field).sort((a, b) => a.sort - b.sort);
        });
        return response;
    }

    async findDictionaries(id: number) {
        const response = await this.prismaService.serviceField.findMany({
            where: {
                service_id: id,
            },
            select: {
                field_id: true,
            },
        });
        const fieldIds = response.map((el) => el.field_id);

        const fields = await this.prismaService.field.findMany({
            where: {
                id: { in: fieldIds },
            },
        });

        const directoryIds = fields.map((el) => el.directory_id).filter((el) => el);

        return this.prismaService.directory.findMany({
            where: {
                id: { in: directoryIds },
            },
        });
    }
    async findOne(id: number): Promise<Service> {
        const service = await this.prismaService.service.findUnique({
            where: {
                id,
            },
            include: {
                category: true,
                fields: {
                    select: {
                        field: true,
                    },
                },
            },
        });

        if (!service) {
            return null;
        }
        const response: any = service;
        response.fields = service.fields.map((field) => field.field);

        return response;
    }

    async create(service: Partial<ServiceCreateDto>) {
        const _service = await this.prismaService.service.findFirst({
            where: {
                title: service.title,
            },
        });

        if (_service) {
            throw new HttpException('Сервис с таким именем уже существует', HttpStatus.BAD_REQUEST);
        }

        try {
            const savedService = await this.prismaService.service.create({
                data: {
                    title: service.title,
                    category_id: service.category_id,
                    is_default: service.is_default || false,
                    is_active: service.is_active || false,
                },
                include: {
                    category: true,
                    fields: {
                        select: {
                            field: true,
                        },
                    },
                },
            });

            const serviceFields: { service_id: number; field_id: number }[] = [];
            service.fields.forEach((field_id) => {
                serviceFields.push({
                    service_id: Number(savedService.id),
                    field_id: Number(field_id),
                });
            });

            await this.prismaService.serviceField.createMany({ data: serviceFields });

            const response: any = savedService;
            response.fields = savedService.fields.map((field) => field.field);

            return response;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    async update(id: number, service: Partial<ServiceCreateDto>) {
        const _duplicate_service = await this.prismaService.service.findFirst({
            where: {
                title: service.title,

                id: {
                    not: Number(id),
                },
            },
        });

        if (_duplicate_service) {
            throw new HttpException('Сервис с таким именем уже существует', HttpStatus.BAD_REQUEST);
        }

        const savedService = await this.prismaService.service.update({
            where: {
                id: Number(id),
            },
            data: {
                title: service.title,
                category_id: service.category_id,
                is_default: service.is_default,
                is_active: service.is_active,
            },
            include: {
                category: true,
                fields: {
                    select: {
                        field: true,
                    },
                },
            },
        });

        const serviceFields: { service_id: number; field_id: number }[] = [];
        service.fields.forEach((field_id) => {
            serviceFields.push({
                service_id: Number(savedService.id),
                field_id: Number(field_id),
            });
        });

        await this.prismaService.serviceField.deleteMany({ where: { service_id: Number(savedService.id) } });
        await this.prismaService.serviceField.createMany({ data: serviceFields });

        const response: any = this.findOne(Number(id));
        response.fields = savedService.fields.map((field) => field.field);

        return response;
    }

    async delete(id: number) {
        try {
            return this.prismaService.service.delete({
                where: { id: Number(id) },
                select: { id: true },
            });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async findAllCategories() {
        try {
            return await this.prismaService.serviceCategory.findMany({ include: { services: true } });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async findOneCategory(id: number) {
        try {
            const serviceCategory = await this.prismaService.serviceCategory.findUnique({
                where: {
                    id,
                },
                include: { services: true },
            });
            if (!serviceCategory) {
                return null;
            }
            return serviceCategory;
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async createCategory(category: Partial<ServiceCategory>) {
        try {
            const _category = await this.prismaService.serviceCategory.findFirst({
                where: {
                    title: category.title,
                },
            });

            if (_category) {
                throw new HttpException('Категория с таким именем уже существует', HttpStatus.BAD_REQUEST);
            }

            try {
                const savedCategory = await this.prismaService.serviceCategory.create({
                    data: {
                        title: category.title,
                    },
                    include: { services: true },
                });

                return savedCategory;
            } catch (err) {
                throw new HttpException(err, HttpStatus.BAD_REQUEST);
            }
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async updateCategory(id: number, category: Partial<ServiceCategory>) {
        const _duplicate = await this.prismaService.serviceCategory.findFirst({
            where: {
                title: category.title,
                id: {
                    not: Number(id),
                },
            },
            include: { services: true },
        });

        if (_duplicate) {
            throw new HttpException('Категория с таким именем уже существует', HttpStatus.BAD_REQUEST);
        }

        const savedCategory = await this.prismaService.serviceCategory.update({
            where: {
                id: Number(id),
            },
            data: {
                title: category.title,
            },
        });

        return savedCategory;
    }
    async deleteCategory(id: number) {
        try {
            return this.prismaService.serviceCategory.delete({ where: { id: Number(id) }, select: { id: true } });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async findCompanyServices(company_id: number | number[]) {
        if (!Array.isArray(company_id)) {
            company_id = [company_id];
        }
        const response = await this.prismaService.companyService.findMany({
            where: { company_id: { in: company_id } },
            include: {
                service: {
                    include: {
                        category: true,
                        fields: {
                            include: { field: true },
                        },
                    },
                },
            },
        });
        return response.map((company_service) => ({
            ...company_service.service,
            fields: company_service.service.fields.map((service_fields) => service_fields.field),
        }));
    }
}
