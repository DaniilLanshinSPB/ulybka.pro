import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CategoryUpdateDto {
    @IsString()
    @ApiProperty({ description: 'Название' })
    title: string;
}
