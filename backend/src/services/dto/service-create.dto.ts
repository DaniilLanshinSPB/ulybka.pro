import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsBoolean, IsNumber, IsString } from 'class-validator';

export class ServiceCreateDto {
    @IsString()
    @ApiProperty({ description: 'Название', type: String })
    title: string;

    @IsNumber()
    @ApiProperty({ description: 'Идентификатор категории', type: Number })
    category_id: number;

    @IsArray()
    @ApiProperty({ description: 'Идентификаторы полей', type: [Number] })
    fields: number[];

    @IsBoolean()
    @ApiProperty({ description: 'Статус услуги', type: Boolean })
    is_active: boolean;

    @IsBoolean()
    @ApiProperty({ description: 'По умолчанию', type: Boolean })
    is_default?: boolean;
}
