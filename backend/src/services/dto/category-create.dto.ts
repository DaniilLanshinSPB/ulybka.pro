import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CategoryCreateDto {
    @IsString()
    @ApiProperty({ description: 'Название' })
    title: string;
}
