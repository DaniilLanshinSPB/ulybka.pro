import { ApiProperty } from '@nestjs/swagger';
import { ServiceCategory } from '@prisma/postgres/client';
import { ServiceResponse } from './service.response';

export class ServiceCategoryResponse implements ServiceCategory {
    @ApiProperty({ description: 'Идентификатор' })
    id: number;

    @ApiProperty({ description: 'Название' })
    title: string;

    @ApiProperty({ description: 'Сервисы', type: [ServiceResponse] })
    services: ServiceResponse[] = [];

    constructor(category: ServiceCategory) {
        Object.assign(this, category);
    }
}
