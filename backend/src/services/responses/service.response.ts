import { ApiProperty } from '@nestjs/swagger';
import { Service } from '@prisma/postgres/client';
import { Exclude } from 'class-transformer';

export class ServiceResponse implements Service {
    @ApiProperty({ description: 'Идентификатор' })
    id: number;

    @ApiProperty({ description: 'Название' })
    title: string;

    @Exclude()
    category_id: number;

    @ApiProperty({ description: 'Категория' })
    category: string;

    @ApiProperty({ description: 'Поля' })
    fields: string[];

    @ApiProperty({ description: 'Статус' })
    is_active: boolean;

    is_default: boolean;
    constructor(service: Service) {
        Object.assign(this, service);
    }
}
