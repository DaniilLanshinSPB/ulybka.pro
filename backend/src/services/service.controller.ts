import { NotVerifiedGuard } from '@/auth/guards/not_verified.guard';
import { PermissionsGuard } from '@/auth/guards/permission.guard';
import { JwtPayload, SystemPermissions } from '@/auth/interfaces';
import { CurrentUser, Permissions } from '@common/decorators';
import { getUserName } from '@common/helpers/user.helper';
import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiExcludeController, ApiOperation, ApiParam, ApiResponse } from '@nestjs/swagger';
import { EntityType, LogType } from '@prisma/postgres/client';
import { LogsService } from 'src/logs/logs.service';
import { CategoryCreateDto, CategoryUpdateDto, ServiceCreateDto, ServiceUpdateDto } from './dto';
import { ServiceCategoryResponse, ServiceResponse } from './responses';
import { ServiceService } from './service.service';
import { DirectoryResponse } from '@/directories/responses';

@ApiExcludeController()
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(PermissionsGuard)
@UseGuards(NotVerifiedGuard)
@Controller('services')
export class ServiceController {
    constructor(protected readonly serviceService: ServiceService, protected readonly logsService: LogsService) {}

    @Get('all')
    @Permissions(SystemPermissions.SYSTEM_VIEW_SERVICE)
    @ApiOperation({ summary: 'Получить все сервисы' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о сервисе', type: [ServiceResponse] })
    async findAllServices() {
        const services = await this.serviceService.findAll();
        return services.map((service) => new ServiceResponse(service));
    }

    @Get(':id')
    @Permissions(SystemPermissions.SYSTEM_VIEW_SERVICE)
    @ApiParam({ name: 'id', type: Number })
    @ApiOperation({ summary: 'Получить информацию о сервисе по ID' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о сервисе', type: ServiceResponse })
    async findOneService(@Param('id', ParseIntPipe) id: number) {
        const service = await this.serviceService.findOne(id);
        return new ServiceResponse(service);
    }

    @Post()
    @Permissions(SystemPermissions.SYSTEM_CREATE_SERVICE)
    @ApiBody({ type: ServiceCreateDto })
    @ApiOperation({ summary: 'Создание сервиса' })
    @ApiResponse({ status: 200, description: 'Сервис успешно создан' })
    async createService(@CurrentUser() currentUser: JwtPayload, @Body() dto: ServiceCreateDto) {
        dto.is_default = true;
        const service = await this.serviceService.create(dto);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Service,
            entity_id: service.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} добавил новую услугу ${service.title}`,
            type: LogType.SYSTEM,
            metadata: service,
        });
        return new ServiceResponse(service);
    }

    @Patch(':id')
    @Permissions(SystemPermissions.SYSTEM_UPDATE_SERVICE)
    @ApiParam({ name: 'id', type: Number })
    @ApiBody({ type: ServiceUpdateDto })
    @ApiOperation({ summary: 'Обновление сервиса' })
    @ApiResponse({ status: 200, description: 'Сервис успешно обновлен' })
    async updateService(
        @CurrentUser() currentUser: JwtPayload,
        @Param('id', ParseIntPipe) id: number,
        @Body() dto: ServiceUpdateDto,
    ) {
        const service = await this.serviceService.update(id, dto);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Service,
            entity_id: service.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} изменил услугу ${service.title}`,
            type: LogType.SYSTEM,
            metadata: service,
        });
        return new ServiceResponse(service);
    }
    @Delete(':id')
    @Permissions(SystemPermissions.SYSTEM_DELETE_SERVICE)
    @ApiParam({ name: 'id', type: Number })
    @ApiOperation({ summary: 'Удалить сервис по ID' })
    @ApiResponse({ status: 200, description: 'Сервис успешно удален' })
    @ApiResponse({ status: 400, description: 'Сервис не найден' })
    async deleteService(@CurrentUser() currentUser: JwtPayload, @Param('id', ParseIntPipe) id: number) {
        const service = await this.serviceService.findOne(id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Service,
            entity_id: service.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} удалил услугу ${service.title}`,
            type: LogType.SYSTEM,
            metadata: service,
        });
        return this.serviceService.delete(id);
    }

    @Get(':id/dictionaries')
    @Permissions(SystemPermissions.SYSTEM_VIEW_SERVICE)
    @ApiParam({ name: 'id', type: Number })
    @ApiOperation({ summary: 'Получить информацию о словарях сервиса по ID' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о словарях сервиса' })
    @ApiResponse({ status: 400, description: 'Сервис не найден' })
    async findDictionaries(@Param('id', ParseIntPipe) id: number) {
        const service = await this.serviceService.findOne(id);
        const directories = await this.serviceService.findDictionaries(service.id);
        return directories.map((directory) => new DirectoryResponse(directory));
    }

    @Get('/categories/all')
    @Permissions(SystemPermissions.SYSTEM_VIEW_SERVICE_CATEGORY)
    @ApiOperation({ summary: 'Получить все категории' })
    @ApiResponse({
        status: 200,
        description: 'Успешное получение информации о категориях',
        type: [ServiceCategoryResponse],
    })
    async findAllServiceCategory() {
        const categories = await this.serviceService.findAllCategories();
        return categories.map((category) => new ServiceCategoryResponse(category));
    }

    @Get('/categories/:id')
    @Permissions(SystemPermissions.SYSTEM_VIEW_SERVICE_CATEGORY)
    @ApiParam({ name: 'id', type: Number })
    @ApiOperation({ summary: 'Получить категорию по ID' })
    @ApiResponse({
        status: 200,
        description: 'Успешное получение информации о категории',
        type: ServiceCategoryResponse,
    })
    async findOneServiceCategory(@Param('id', ParseIntPipe) id: number) {
        const category = await this.serviceService.findOneCategory(id);
        return new ServiceCategoryResponse(category);
    }

    @Post('/categories')
    @ApiBody({ type: CategoryCreateDto })
    @Permissions(SystemPermissions.SYSTEM_CREATE_SERVICE_CATEGORY)
    @ApiOperation({ summary: 'Создать категорию' })
    @ApiResponse({ status: 200, description: 'Категория успешно создана' })
    async createServiceCategory(@CurrentUser() currentUser: JwtPayload, @Body() dto: CategoryCreateDto) {
        const category = await this.serviceService.createCategory(dto);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Service,
            entity_id: category.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} добавил новую категорию услуг ${category.title}`,
            type: LogType.SYSTEM,
            metadata: category,
        });
        return new ServiceCategoryResponse(category);
    }

    @Patch('/categories/:id')
    @ApiParam({ name: 'id', type: Number })
    @ApiBody({ type: CategoryUpdateDto })
    @Permissions(SystemPermissions.SYSTEM_UPDATE_SERVICE_CATEGORY)
    @ApiOperation({ summary: 'Обновить категорию' })
    @ApiResponse({ status: 200, description: 'Категория успешно обновлена' })
    async updateServiceCategory(
        @CurrentUser() currentUser: JwtPayload,
        @Param('id', ParseIntPipe) id: number,
        @Body() dto: CategoryUpdateDto,
    ) {
        const category = await this.serviceService.updateCategory(id, dto);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Service,
            entity_id: category.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} изменил категорию услуг ${category.title}`,
            type: LogType.SYSTEM,
            metadata: category,
        });
        return new ServiceCategoryResponse(category);
    }

    @Delete('/categories/:id')
    @ApiParam({ name: 'id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_DELETE_SERVICE_CATEGORY)
    @ApiOperation({ summary: 'Удалить категорию' })
    @ApiResponse({ status: 200, description: 'Категория успешно удалена' })
    @ApiResponse({ status: 400, description: 'Категория не найдена' })
    async deleteServiceCategory(@CurrentUser() currentUser: JwtPayload, @Param('id', ParseIntPipe) id: number) {
        const category = await this.serviceService.findOneCategory(id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Service,
            entity_id: category.id.toString(),
            details: `Пользователь ${getUserName(currentUser)} удалил категорию услуг ${category.title}`,
            type: LogType.SYSTEM,
            metadata: category,
        });
        return this.serviceService.deleteCategory(id);
    }
}
