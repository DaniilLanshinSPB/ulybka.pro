import { JwtPayload } from '@/auth/interfaces';
import { PrismaService } from '@/database/postgres-prisma.service';
import { UserService } from '@/users/user.service';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { HttpException, HttpStatus, Inject, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Company, CompanyType, Service, User } from '@prisma/postgres/client';
import { Cache } from 'cache-manager';
import { CreateCompanyDto } from './dto';
import { COMPANY_TYPES } from './interfaces';

export class CompanyService {
    protected readonly logger = new Logger(CompanyService.name);

    constructor(
        protected readonly prismaService: PrismaService,
        protected readonly userService: UserService,
        protected readonly configService: ConfigService,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
    ) {}

    async findAll(): Promise<Company[]> {
        const companies = await this.prismaService.company.findMany({
            include: {
                type: true,
                parent: true,
                childrens: true,
                _count: {
                    select: { users: true },
                },
            },
        });
        if (!companies || !companies.length) {
            return [];
        }

        return companies;
    }
    async findMany(id: number[]): Promise<Company[]> {
        const companies = await this.prismaService.company.findMany({
            where: {
                id: {
                    in: id,
                },
            },
            include: {
                type: true,
                parent: true,
                childrens: true,
                _count: {
                    select: { users: true },
                },
            },
        });
        if (!companies || !companies.length) {
            return [];
        }

        return companies;
    }
    async findOne(id: number): Promise<Company & { type: CompanyType; parent: Company; childrens: Company[] }> {
        const company = await this.prismaService.company.findFirst({
            where: {
                id,
            },
            include: {
                type: true,
                parent: true,
                childrens: true,
                _count: {
                    select: { users: true },
                },
            },
        });

        if (!company) {
            return null;
        }
        await this.cacheManager.set(`company_${id}`, company);
        return company;
    }

    async create(company: Partial<Company>): Promise<Company> {
        const _company = await this.prismaService.company.findFirst({
            where: {
                title: company.title,
            },
        });
        if (_company) {
            throw new HttpException('Компания с таким названием уже существует', HttpStatus.BAD_REQUEST);
        }
        try {
            return this.prismaService.company.create({
                data: {
                    title: company.title,
                    type_id: company.type_id,
                    image: company.image ? company.image : null,
                    metadata: company.metadata,
                    is_verified: company.is_verified,
                    parent_id: company?.parent_id,
                },
                include: {
                    type: true,
                    parent: true,
                    _count: {
                        select: { users: true },
                    },
                },
            });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async update(id: number, company: Partial<Company>): Promise<Company> {
        const _company = await this.prismaService.company.findFirst({
            where: {
                title: company.title,
                id: { not: Number(id) },
            },
        });
        if (_company) {
            throw new HttpException('Компания с таким названием уже существует', HttpStatus.BAD_REQUEST);
        }
        try {
            return this.prismaService.company.update({
                where: {
                    id: id,
                },
                data: {
                    title: company.title,
                    type_id: company.type_id,
                    image: company.image ? company.image : null,
                    metadata: company.metadata,
                    is_verified: company.is_verified,
                    parent_id: company?.parent_id,
                },
                include: {
                    type: true,
                    parent: true,
                    _count: {
                        select: { users: true },
                    },
                },
            });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async deleteCompany(id: number) {
        const company = await this.prismaService.company.findUnique({ where: { id } });

        if (!company) {
            throw new HttpException('Company not found', HttpStatus.NOT_FOUND);
        }
        try {
            await this.prismaService.patient.deleteMany({ where: { company_id: id } });
            await this.prismaService.companyService.deleteMany({ where: { company_id: id } });
            await this.prismaService.order.deleteMany({ where: { company_id: id } });
            await this.prismaService.apiKey.deleteMany({ where: { company_id: id } });
            await this.prismaService.webhook.deleteMany({ where: { company_id: id } });
            await this.prismaService.user.deleteMany({ where: { company_id: id } });
            return this.prismaService.company.delete({ where: { id } });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async findCompanyUsers(company_id: number): Promise<User[]> {
        return await this.prismaService.user.findMany({
            where: {
                deleted_at: null,
                company_id: company_id,
            },
            include: {
                role: true,
                company: true,
            },
        });
    }

    async findOneUserByCompany(company_id: number, idOrEmail: string | number): Promise<User> {
        const user = await this.userService.findOne(idOrEmail);
        if (user.company.id !== company_id) {
            throw new HttpException('User not found', HttpStatus.NOT_FOUND);
        }
        return user;
    }
    async createUserByCompany(company_id: number, userDto: Partial<User>): Promise<User> {
        try {
            const user = await this.userService.create({
                ...userDto,
                company_id: company_id,
            });
            return user;
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async updateUserByCompany(company_id: number, id: number, userDto: Partial<User>): Promise<User> {
        const user = await this.userService.findOne(id);
        if (user.company.id !== company_id) {
            throw new HttpException('User not found', HttpStatus.NOT_FOUND);
        }
        return this.userService.update(id, userDto);
    }

    async deleteUserByCompany(company_id: number, user_id: number, currentUser: JwtPayload) {
        const _user = await this.userService.findOne(user_id);
        if (_user.company.id !== company_id) {
            throw new HttpException('User not found', HttpStatus.NOT_FOUND);
        }
        return this.userService.delete(user_id, currentUser.company);
    }

    async getTypes() {
        return this.prismaService.companyType.findMany();
    }

    async findAllClinics(company_id: number): Promise<Company[]> {
        return this.prismaService.company.findMany({
            where: {
                parent_id: company_id,
            },
            include: {
                _count: {
                    select: { users: true },
                },
            },
        });
    }
    async findOneClinic(company_id: number, clinic_id: number): Promise<Company> {
        return this.prismaService.company.findUnique({
            where: {
                id: clinic_id,
                parent_id: company_id,
            },
            include: {
                type: true,
                parent: true,
                _count: {
                    select: { users: true },
                },
            },
        });
    }
    async createClinic(company_id: number, item: Partial<CreateCompanyDto>): Promise<Company> {
        const company = await this.prismaService.company.findUnique({
            where: {
                id: company_id,
            },
        });
        if (!company) {
            throw new HttpException('Company not found', HttpStatus.NOT_FOUND);
        }
        return this.prismaService.company.create({
            data: {
                title: item.title,
                type_id: COMPANY_TYPES.PHARMACY,
                metadata: item.metadata,
                is_verified: true,
                parent_id: company_id,
            },
            include: {
                parent: true,
                _count: {
                    select: { users: true },
                },
            },
        });
    }

    async updateClinic(company_id: number, clinic_id: number, item: Partial<CreateCompanyDto>): Promise<Company> {
        const company = await this.prismaService.company.findUnique({
            where: {
                id: company_id,
            },
        });
        if (!company) {
            throw new HttpException('Company not found', HttpStatus.NOT_FOUND);
        }
        const clinic = await this.prismaService.company.findUnique({
            where: {
                id: clinic_id,
            },
        });
        if (!clinic) {
            throw new HttpException('Clinic not found', HttpStatus.NOT_FOUND);
        }
        return await this.prismaService.company.update({
            where: {
                id: clinic_id,
                parent_id: company_id,
            },
            data: {
                title: item.title,
                type_id: COMPANY_TYPES.PHARMACY,
                metadata: item.metadata,
                is_verified: true,
                parent_id: company_id,
            },
            include: {
                _count: {
                    select: { users: true },
                },
            },
        });
    }

    async deleteClinic(company_id: number, clinic_id: number) {
        const company = await this.prismaService.company.findUnique({
            where: {
                id: company_id,
            },
        });
        if (!company) {
            throw new HttpException('Company not found', HttpStatus.NOT_FOUND);
        }
        const clinic = await this.prismaService.company.findUnique({
            where: {
                id: clinic_id,
            },
        });
        if (!clinic) {
            throw new HttpException('Clinic not found', HttpStatus.NOT_FOUND);
        }
        const patients = await this.prismaService.patient.findMany({
            where: {
                company_id: clinic_id,
            },
        });

        if (patients.length > 0) {
            throw new HttpException('Clinic has patients', HttpStatus.BAD_REQUEST);
        }
        return await this.prismaService.company.delete({
            where: { id: clinic_id },
        });
    }
    async fetchServices(company_id: number): Promise<Service[]> {
        const _company = await this.prismaService.company.findUnique({
            where: {
                id: company_id,
            },
        });
        if (!_company) {
            throw new HttpException('Company not found', HttpStatus.NOT_FOUND);
        }
        const response = await this.prismaService.companyService.findMany({
            where: {
                company_id: company_id,
                service: {
                    is_active: true,
                },
            },
            include: {
                service: {
                    include: {
                        category: true,
                        fields: {
                            select: {
                                field: true,
                            },
                        },
                    },
                },
            },
        });

        return response.map((companyService) => ({
            ...companyService.service,
            fields: companyService.service.fields.map((service_fields) => service_fields.field),
        }));
    }
    async fetchOneService(service_id: number) {
        return this.prismaService.company.findUnique({
            where: {
                id: service_id,
            },
        });
    }
    async addService(company_id: number, service_id: number) {
        const _company = await this.prismaService.company.findUnique({
            where: {
                id: company_id,
            },
        });
        if (!_company) {
            throw new HttpException('Company not found', HttpStatus.NOT_FOUND);
        }
        const _service = await this.prismaService.service.findUnique({
            where: {
                id: service_id,
                is_active: true,
            },
        });
        if (!_service) {
            throw new HttpException('Service not found', HttpStatus.NOT_FOUND);
        }

        await this.prismaService.companyService.create({
            data: {
                company_id: company_id,
                service_id: service_id,
            },
        });
        return _service;
    }

    async removeService(company_id: number, service_id: number) {
        const _company = await this.prismaService.company.findUnique({
            where: {
                id: company_id,
            },
        });
        if (!_company) {
            throw new HttpException('Company not found', HttpStatus.NOT_FOUND);
        }
        return this.prismaService.companyService.delete({
            where: {
                service_id_company_id: {
                    service_id: service_id,
                    company_id: company_id,
                },
            },
        });
    }
}
