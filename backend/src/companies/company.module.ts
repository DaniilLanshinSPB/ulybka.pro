import { LogsModule } from '@/logs/logs.module';
import { RoleModule } from '@/roles/role.module';
import { UserModule } from '@/users/user.module';
import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';
import { CompanyController } from './company.controller';
import { CompanyService } from './company.service';

@Module({
    controllers: [CompanyController],
    providers: [CompanyService],
    exports: [CompanyService],
    imports: [CacheModule.register(), UserModule, RoleModule, LogsModule],
})
export class CompanyModule {}
