import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsEmail, IsEmpty, IsNotEmpty, IsNumber, IsString, MinLength } from 'class-validator';
import { Prisma } from '@prisma/postgres/client';

export class UpdateCompanyUserDto {
    @IsEmail()
    @IsNotEmpty()
    @ApiProperty({ description: 'Email пользователя' })
    email: string;

    @IsString()
    @MinLength(6)
    @IsNotEmpty()
    @ApiProperty({ description: 'Пароль пользователя' })
    password: string;

    @IsString()
    @IsEmpty()
    @ApiProperty({ description: 'Имя пользователя' })
    first_name?: string;

    @IsString()
    @IsEmpty()
    @ApiProperty({ description: 'Фамилия пользователя' })
    last_name?: string;

    @IsString()
    @IsEmpty()
    @ApiProperty({ description: 'Отчество' })
    middle_name?: string;

    @IsBoolean()
    @ApiProperty({ description: 'Верификация пользователя' })
    email_verified: boolean;

    @IsBoolean()
    @ApiProperty({ description: 'Статус пользователя' })
    is_active: boolean;

    @IsNumber()
    @ApiProperty({ description: 'Роль пользователя' })
    role_id: number;

    @IsEmpty()
    @ApiProperty({ description: 'Метадата по полям' })
    metadata?: Prisma.JsonValue;
}
