import { Prisma } from '@prisma/postgres/client';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmpty, IsNotEmpty, IsNumber, IsString, MinLength } from 'class-validator';

export class CreateCompanyDto {
    @IsString()
    @MinLength(6)
    @IsNotEmpty()
    @ApiProperty({ description: 'Название компании' })
    title: string;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty({ description: 'Идентификатор типа компании' })
    type_id: number;

    @IsEmpty()
    @ApiProperty({ description: 'Метадата по полям' })
    metadata: Prisma.JsonValue;
}
