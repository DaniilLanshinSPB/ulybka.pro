import { ApiProperty } from '@nestjs/swagger';
import { Company, CompanyType, Prisma } from '@prisma/postgres/client';
import { Exclude } from 'class-transformer';

interface ICompany extends Company {
    users_count?: number;
    _count?: any;
}

export class CompanyResponse implements Company {
    @ApiProperty({ description: 'ID компании' })
    id: number;

    @ApiProperty({ description: 'Название компании' })
    title: string;

    @ApiProperty({ description: 'Количество пользователей' })
    users_count: number;

    @Exclude()
    parent_id: number;

    @Exclude()
    type_id: number;

    @ApiProperty({ description: 'Логотип компании' })
    image: string | null;

    @ApiProperty({ description: 'Тип компании' })
    type: CompanyType;

    @ApiProperty({ description: 'Метадата объекта' })
    metadata: Prisma.JsonValue;

    @ApiProperty({ description: 'Флаг ферификациии компании' })
    is_verified: boolean;

    @Exclude()
    _count: any;

    constructor(company: ICompany) {
        Object.assign(this, company);

        if (company?._count) {
            this.users_count = company?._count?.users ?? 0;
        } else {
            this.users_count = 0;
        }
    }
}
