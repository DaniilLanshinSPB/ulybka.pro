import { NotVerifiedGuard } from '@/auth/guards/not_verified.guard';
import { PermissionsGuard } from '@/auth/guards/permission.guard';
import { JwtPayload, SystemPermissions } from '@/auth/interfaces';
import { LogsService } from '@/logs/logs.service';
import { RoleResponse } from '@/roles/responses';
import { RoleService } from '@/roles/role.service';
import { ServiceResponse } from '@/services/responses';
import { UserResponse } from '@/users/responses';
import { UserService } from '@/users/user.service';
import { CurrentUser, Permissions } from '@common/decorators';
import { getUserName } from '@common/helpers/user.helper';
import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiExcludeController, ApiOperation, ApiParam, ApiResponse } from '@nestjs/swagger';
import { EntityType, LogType } from '@prisma/postgres/client';
import { CompanyService } from './company.service';
import { CreateCompanyDto, CreateCompanyUserDto, UpdateCompanyDto, UpdateCompanyUserDto } from './dto';
import { CompanyResponse } from './responses/company.response';
@ApiExcludeController()
@ApiBearerAuth('authorization')
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(NotVerifiedGuard)
@UseGuards(PermissionsGuard)
@Controller('companies')
export class CompanyController {
    constructor(
        protected readonly companyService: CompanyService,
        protected readonly userService: UserService,
        protected readonly roleService: RoleService,
        protected readonly logsService: LogsService,
    ) {}

    @Get('/types')
    @Permissions(SystemPermissions.SYSTEM_VIEW_COMPANIES)
    @ApiOperation({ summary: 'Получить типы компаний' })
    @ApiResponse({ status: 200, description: 'Информация о типах компаний получена' })
    async getTypes() {
        return this.companyService.getTypes();
    }
    @Get('all')
    @Permissions(SystemPermissions.SYSTEM_VIEW_COMPANIES)
    @ApiOperation({ summary: 'Получить все компании' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о компаниях', type: [CompanyResponse] })
    async findAll() {
        const companies = await this.companyService.findAll();
        return companies.map((company) => new CompanyResponse(company));
    }

    @Get(':id')
    @ApiParam({ name: 'id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_VIEW_COMPANIES)
    @ApiOperation({ summary: 'Получить информацию о компании по ID' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о компании', type: CompanyResponse })
    async findOne(@Param('id', ParseIntPipe) id: number) {
        const company = await this.companyService.findOne(id);
        return new CompanyResponse(company);
    }

    @Post()
    @ApiBody({ type: CreateCompanyDto })
    @Permissions(SystemPermissions.SYSTEM_CREATE_COMPANIES)
    @ApiOperation({ summary: 'Создать компанию' })
    @ApiResponse({ status: 200, description: 'Компания успешно создана', type: CompanyResponse })
    async create(@CurrentUser() user: JwtPayload, @Body() dto: CreateCompanyDto) {
        const company = await this.companyService.create(dto);
        await this.logsService.logAction({
            user_id: user.id,
            entity_type: EntityType.Company,
            entity_id: company.id.toString(),
            metadata: company,
            details: `Пользователь ${getUserName(user)} создал компанию ${company.title}`,
            type: LogType.SYSTEM,
        });
        return new CompanyResponse(company);
    }

    @Patch(':id')
    @ApiParam({ name: 'id', type: Number })
    @ApiBody({ type: UpdateCompanyDto })
    @Permissions(SystemPermissions.SYSTEM_UPDATE_COMPANIES)
    @ApiOperation({ summary: 'Обновление компании' })
    @ApiResponse({ status: 200, description: 'Компания успешно обновлен', type: CompanyResponse })
    async update(
        @CurrentUser() user: JwtPayload,
        @Param('id', ParseIntPipe) id: number,
        @Body() dto: UpdateCompanyDto,
    ) {
        const company = await this.companyService.update(id, dto);
        await this.logsService.logAction({
            user_id: user.id,
            entity_type: EntityType.Company,
            entity_id: company.id.toString(),
            details: `Пользователь ${getUserName(user)} обновил компанию ${company.title}`,
            metadata: company,
            type: LogType.SYSTEM,
        });
        return new CompanyResponse(company);
    }

    @Delete(':id')
    @ApiParam({ name: 'id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_DELETE_COMPANIES)
    @ApiOperation({ summary: 'Удалить компанию по ID' })
    @ApiResponse({ status: 200, description: 'Компания успешно удалена' })
    @ApiResponse({ status: 400, description: 'Компания не найдена' })
    async deleteCompany(@CurrentUser() user: JwtPayload, @Param('id', ParseIntPipe) id: number) {
        const company = await this.companyService.findOne(id);
        await this.logsService.logAction({
            user_id: user.id,
            entity_type: EntityType.Company,
            entity_id: company.id.toString(),
            details: `Пользователь ${getUserName(user)} удалил компанию ${company.title}`,
            type: LogType.SYSTEM,
            metadata: company,
        });
        return this.companyService.deleteCompany(id);
    }

    //company roles
    @Get(':company_id/roles')
    @ApiParam({ name: 'company_id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_VIEW_USER)
    @ApiOperation({ summary: 'Получить всех ролей пользователей ' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о ролях пользователя' })
    async findRoles(@Param('company_id', ParseIntPipe) company_id: number) {
        const roles = await this.roleService.findRolesByCompany(company_id);
        return roles.map((role) => new RoleResponse(role));
    }

    //company users
    @Get(':company_id/users')
    @ApiParam({ name: 'company_id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_VIEW_USER)
    @ApiOperation({ summary: 'Получить всех пользователей компании' })
    @ApiResponse({
        status: 200,
        description: 'Успешное получение информации о пользователях',
        type: [UserResponse],
    })
    async findAllUsers(@Param('company_id', ParseIntPipe) company_id: number) {
        const users = await this.userService.findMany(company_id);
        return users.map((user) => new UserResponse(user));
    }

    @Get(':company_id/users/responsible')
    @ApiParam({ name: 'company_id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_VIEW_USER)
    @ApiOperation({ summary: 'Получить всех ответственных' })
    @ApiResponse({
        status: 200,
        description: 'Успешное получение информации о пользователях',
        type: [UserResponse],
    })
    async findAllResponsible(@Param('company_id', ParseIntPipe) company_id: number) {
        const company = await this.companyService.findOne(company_id);
        const filter_company_id = company.parent ? company.parent.id : company.id;

        const roles = await this.roleService.findRolesByCompany(filter_company_id);
        const role_ids = roles.map((el) => el.id);
        const users = await this.userService.findMany(filter_company_id, {
            role_ids,
        });
        return users.map((user) => new UserResponse(user));
    }

    @Get(':company_id/users/executors')
    @ApiParam({ name: 'company_id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_VIEW_USER)
    @ApiOperation({ summary: 'Получить всех исполнителей' })
    @ApiResponse({
        status: 200,
        description: 'Успешное получение информации о пользователях',
        type: [UserResponse],
    })
    async findAllExecutors(@Param('company_id', ParseIntPipe) company_id: number) {
        const company = await this.companyService.findOne(company_id);
        const filter_company_id = company.parent ? company.parent.id : company.id;

        const roles = await this.roleService.findRolesByCompany(filter_company_id);
        const role_ids = roles.map((el) => el.id);
        const users = await this.userService.findMany(filter_company_id, {
            role_ids,
        });

        return users.map((user) => new UserResponse(user));
    }

    @Get(':company_id/users/customers')
    @ApiParam({ name: 'company_id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_VIEW_USER)
    @ApiOperation({ summary: 'Получить всех заказчиков' })
    @ApiResponse({
        status: 200,
        description: 'Успешное получение информации о пользователях',
        type: [UserResponse],
    })
    async findAllCustomers(@Param('company_id', ParseIntPipe) company_id: number) {
        const company = await this.companyService.findOne(company_id);
        const filter_company_id: number | number[] = company.childrens.length
            ? company.childrens.map((el) => el.id)
            : company.id;

        const roles = await this.roleService.findRolesByCompany(filter_company_id);
        const role_ids = roles.map((el) => el.id);
        const users = await this.userService.findMany(filter_company_id, {
            role_ids,
        });
        return users.map((user) => new UserResponse(user));
    }

    @Get(':company_id/users/:user_id')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'user_id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_VIEW_USER)
    @ApiOperation({ summary: 'Получить пользователя компании' })
    @ApiResponse({
        status: 200,
        description: 'Успешное получение информации о пользователе',
        type: UserResponse,
    })
    async findOneUsers(
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('user_id', ParseIntPipe) user_id: number,
    ) {
        const user = await this.userService.findOne(user_id, company_id);
        return new UserResponse(user);
    }

    @Post(':company_id/users')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiBody({ type: CreateCompanyUserDto })
    @Permissions(SystemPermissions.SYSTEM_CREATE_USER)
    @ApiOperation({ summary: 'Создание пользователя компании' })
    @ApiResponse({ status: 200, description: 'Пользователь успешно создан' })
    async createUser(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Body() dto: CreateCompanyUserDto,
    ) {
        const user = await this.userService.create(dto, company_id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.User,
            entity_id: currentUser.id.toString(),
            details: `Пользователь ${getUserName(user)} создал нового пользоателя ${user.email} для компании ${
                user.company.title
            }`,
            type: LogType.SYSTEM,
            metadata: user,
        });
        return new UserResponse(user);
    }

    @Patch(':company_id/users/:id')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'id', type: Number })
    @ApiBody({ type: UpdateCompanyUserDto })
    @Permissions(SystemPermissions.SYSTEM_UPDATE_USER)
    @ApiOperation({ summary: 'Обновление пользователя компании' })
    @ApiResponse({ status: 200, description: 'Пользователь успешно обновлен' })
    async updateUser(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('id', ParseIntPipe) id: number,
        @Body() dto: UpdateCompanyUserDto,
    ) {
        const user = await this.userService.update(id, dto, company_id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.User,
            entity_id: currentUser.id.toString(),
            details: `Пользователь ${getUserName(user)} обновил пользоателя ${user.email} для компании ${
                user.company.title
            }`,
            type: LogType.SYSTEM,
            metadata: user,
        });
        return new UserResponse(user);
    }

    @Delete(':company_id/users/:id')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_DELETE_USER)
    @ApiOperation({ summary: 'Удалить пользователя компании по ID' })
    @ApiResponse({ status: 200, description: 'Пользователь успешно удален' })
    @ApiResponse({ status: 400, description: 'Пользователь не найден' })
    async deleteUser(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('id', ParseIntPipe) id: number,
    ) {
        const user = await this.userService.findOne(id, company_id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.User,
            entity_id: currentUser.id.toString(),
            metadata: user,
            details: `Пользователь ${getUserName(user)} удалил пользоателя ${user.email} для компании ${
                user.company.title
            }`,
            type: LogType.SYSTEM,
        });
        return this.userService.delete(id, company_id);
    }

    @Get(':company_id/clinics')
    @ApiParam({ name: 'company_id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_VIEW_CLINICS)
    @ApiOperation({ summary: 'Получить все клиники компании' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о клиниках', type: [CompanyResponse] })
    @ApiResponse({ status: 400, description: 'Компания не найдена' })
    async findAllClinics(@Param('company_id', ParseIntPipe) company_id: number) {
        const response = await this.companyService.findAllClinics(company_id);
        return response.map((clinic) => new CompanyResponse(clinic));
    }

    @Post(':company_id/clinics')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiBody({ type: CreateCompanyDto })
    @Permissions(SystemPermissions.SYSTEM_CREATE_CLINICS)
    @ApiOperation({ summary: 'Создание клиники компании' })
    @ApiResponse({ status: 200, description: 'Клиника успешно создана' })
    @ApiResponse({ status: 400, description: 'Компания не найдена' })
    async saveClinic(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Body() dto: CreateCompanyDto,
    ) {
        const response = await this.companyService.createClinic(company_id, dto);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Clinic,
            entity_id: response.id.toString(),
            metadata: response,
            details: `Пользователь ${getUserName(currentUser)} создал клинику ${response.title}`,
            type: LogType.SYSTEM,
        });
        return new CompanyResponse(response);
    }

    @Patch(':company_id/clinics/:clinic_id')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'clinic_id', type: Number })
    @ApiBody({ type: UpdateCompanyDto })
    @Permissions(SystemPermissions.SYSTEM_UPDATE_CLINICS)
    @ApiOperation({ summary: 'Обновление клиники компании' })
    @ApiResponse({ status: 200, description: 'Клиника успешно обновлен' })
    @ApiResponse({ status: 400, description: 'Клиника не найдена' })
    async updateClinic(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('clinic_id', ParseIntPipe) clinic_id: number,
        @Body() dto: UpdateCompanyDto,
    ) {
        const response = await this.companyService.updateClinic(company_id, clinic_id, dto);

        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Clinic,
            entity_id: response.id.toString(),
            metadata: response,
            details: `Пользователь ${getUserName(currentUser)} обновил клинику ${response.title}`,
            type: LogType.SYSTEM,
        });
        return new CompanyResponse(response);
    }

    @Delete(':company_id/clinics/:clinic_id')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'clinic_id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_DELETE_CLINICS)
    @ApiOperation({ summary: 'Удалить клинику компании по ID' })
    @ApiResponse({ status: 200, description: 'Клиника успешно удалена' })
    @ApiResponse({ status: 400, description: 'Клиника не найдена' })
    async deleteClinic(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('clinic_id', ParseIntPipe) clinic_id: number,
    ) {
        const clinic = await this.companyService.findOneClinic(company_id, clinic_id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Clinic,
            entity_id: clinic.id.toString(),
            metadata: clinic,
            details: `Пользователь ${getUserName(currentUser)} удалил клинику ${clinic.title}`,
            type: LogType.SYSTEM,
        });
        return this.companyService.deleteClinic(company_id, clinic_id);
    }

    @Get(':company_id/services')
    @ApiParam({ name: 'company_id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_VIEW_COMPANY_SERVICE)
    @ApiOperation({ summary: 'Получить все услуги компании' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о услугах', type: [ServiceResponse] })
    @ApiResponse({ status: 400, description: 'Компания не найдена' })
    async fetchServices(@Param('company_id', ParseIntPipe) company_id: number) {
        const response = await this.companyService.fetchServices(company_id);
        return response.map((service) => new ServiceResponse(service));
    }

    @Post(':company_id/services/:service_id')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'service_id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_CREATE_COMPANY_SERVICE)
    @ApiOperation({ summary: 'Создание услуги компании' })
    @ApiResponse({ status: 200, description: 'Услуга успешно создана' })
    @ApiResponse({ status: 400, description: 'Компания не найдена' })
    async addService(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('service_id', ParseIntPipe) service_id: number,
    ) {
        const service = await this.companyService.addService(company_id, service_id);

        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Service,
            entity_id: service.id.toString(),
            metadata: service,
            details: `Пользователь ${getUserName(currentUser)} добавил улсугу ${service.title}`,
            type: LogType.SYSTEM,
        });
        return service;
    }

    @Delete(':company_id/services/:service_id')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiParam({ name: 'service_id', type: Number })
    @Permissions(SystemPermissions.SYSTEM_DELETE_COMPANY_SERVICE)
    @ApiOperation({ summary: 'Удалить услугу компании по ID' })
    @ApiResponse({ status: 200, description: 'Услуга успешно удалена' })
    @ApiResponse({ status: 400, description: 'Услуга не найдена' })
    async removeService(
        @CurrentUser() currentUser: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Param('service_id', ParseIntPipe) service_id: number,
    ) {
        const service = await this.companyService.fetchOneService(service_id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Service,
            entity_id: service.id.toString(),
            metadata: service,
            details: `Пользователь ${getUserName(currentUser)} удалил улсугу ${service.title}`,
            type: LogType.SYSTEM,
        });
        return this.companyService.removeService(company_id, service_id);
    }
}
