import { ApiProperty } from '@nestjs/swagger';
import { WebhookEvents } from '@prisma/postgres/client';

export class CreateWebhookDto {
    @ApiProperty({ description: 'Название', type: String })
    title: string;
    @ApiProperty({ description: 'URL', type: String })
    url: string;
    @ApiProperty({ description: 'Тип', enum: WebhookEvents })
    type: WebhookEvents;
}
