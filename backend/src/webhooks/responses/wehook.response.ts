import { ApiProperty } from '@nestjs/swagger';
import { WebhookEvents } from '@prisma/postgres/client';
import { Exclude } from 'class-transformer';

export class WebhookResponse {
    @ApiProperty({ description: 'ID', type: Number })
    id: number;
    @ApiProperty({ description: 'URL', type: String })
    url: string;
    @ApiProperty({ description: 'Тип события', enum: WebhookEvents })
    event_type: WebhookEvents;
    @Exclude()
    crypto: string;
    @Exclude()
    company_id: number;
    @ApiProperty({ description: 'Дата создания', type: Date })
    createdAt: Date;
    @Exclude()
    updatedAt: Date;
    constructor(webhook) {
        Object.assign(this, webhook);
    }
}
