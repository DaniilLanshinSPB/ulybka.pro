import { ApiProperty } from '@nestjs/swagger';
import { WebhookEvents } from '@prisma/postgres/client';
import { Exclude } from 'class-transformer';

export class CreateWebhookResponse {
    @ApiProperty({ description: 'ID', type: Number })
    id: number;
    @ApiProperty({ description: 'URL', type: String })
    url: string;
    @ApiProperty({ description: 'Тип события', enum: WebhookEvents })
    event_type: WebhookEvents;
    @ApiProperty({ description: 'Ключ шифрования', type: String })
    crypto: string;
    @Exclude()
    company_id: number;
    @ApiProperty({ description: 'Дата создания', type: Date })
    createdAt: Date;
    @Exclude()
    updatedAt: Date;
    constructor(webhook) {
        Object.assign(this, webhook);
    }
}
