import { Injectable, NotFoundException } from '@nestjs/common';

import { EncryptionService } from '@/encryption/encryption.service';
import { Webhook, WebhookEvents } from '@prisma/postgres/client';
import * as crypto from 'crypto';
import { PrismaService } from 'src/database/postgres-prisma.service';
import { CreateWebhookDto } from './dto/create-webhook.dto';

@Injectable()
export class WebhookService {
    constructor(private readonly prisma: PrismaService, protected readonly encryptionService: EncryptionService) {}

    async createWebhook(company_id: number, createWebhookDto: CreateWebhookDto) {
        const { url, type, title } = createWebhookDto;

        const webhook = await this.prisma.webhook.create({
            data: {
                url,
                title,
                crypto: crypto.randomBytes(32).toString('hex'),
                company_id,
                event_type: type,
            },
        });

        return webhook;
    }

    async getWebhooksByCompanyId(company_id: number) {
        return this.prisma.webhook.findMany({
            where: { company_id },
        });
    }

    async getWebhookByType(company_id: number, event_type: WebhookEvents) {
        return this.prisma.webhook.findMany({
            where: {
                company_id,
                event_type,
            },
        });
    }
    async sendData(webhooks: Webhook[], data: any) {
        for (const webhook of webhooks) {
            await fetch(webhook.url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: this.encryptionService.encrypt(JSON.stringify(data), webhook.crypto),
            });
        }
    }
    async getWebhookById(id: number) {
        const webhook = await this.prisma.webhook.findUnique({
            where: { id },
        });

        if (!webhook) {
            throw new NotFoundException('Webhook not found');
        }

        return webhook;
    }

    async deleteWebhook(company_id: number, id: number) {
        return this.prisma.webhook.delete({
            where: { id, company_id },
        });
    }
}
