import { JwtPayload } from '@/auth/interfaces';
import { CurrentUser } from '@common/decorators';
import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateWebhookDto } from './dto/create-webhook.dto';
import { WebhookResponse } from './responses/wehook.response';
import { WebhookService } from './webhooks.service';

@Controller('webhook')
@ApiTags('Webhooks')
@ApiBearerAuth('authorization')
export class WebhookController {
    constructor(private readonly webhookService: WebhookService) {}

    @Post('/:company_id')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiBody({ type: [CreateWebhookDto] })
    @ApiOperation({ summary: 'Добавить вебхук' })
    @ApiResponse({ status: 201, description: 'Вебхук успешно добавлен', type: WebhookResponse })
    @ApiResponse({ status: 400, description: 'Ошибка деактивации' })
    async createWebhook(
        @CurrentUser() user: JwtPayload,
        @Param('company_id', ParseIntPipe) company_id: number,
        @Body() createWebhookDto: CreateWebhookDto,
    ): Promise<any> {
        const newWebhook = await this.webhookService.createWebhook(company_id, createWebhookDto);
        return new WebhookResponse(newWebhook);
    }

    @Get('/:company_id')
    @ApiParam({ name: 'company_id', type: Number })
    @ApiOperation({ summary: 'Получить доступные вебхуки' })
    @ApiResponse({ status: 201, description: 'Успешно', type: [WebhookResponse] })
    @ApiResponse({ status: 400, description: 'Ошибка' })
    async getWebhooks(@CurrentUser() user: JwtPayload, @Param('company_id', ParseIntPipe) company_id: number) {
        const webhooks = await this.webhookService.getWebhooksByCompanyId(company_id);
        return webhooks.map((w) => new WebhookResponse(w));
    }

    @Delete('/:company_id/:id')
    @ApiParam({ name: 'id', type: Number })
    @ApiParam({ name: 'company_id', type: Number })
    @ApiOperation({ summary: 'Удалить вебхук' })
    @ApiResponse({ status: 201, description: 'Вебхук успешно удален', type: Boolean })
    @ApiResponse({ status: 400, description: 'Ошибка удаления' })
    async deleteWebhook(@Param('id', ParseIntPipe) id: number, @Param('company_id', ParseIntPipe) company_id: number) {
        await this.webhookService.deleteWebhook(company_id, id);
        return true;
    }
}
