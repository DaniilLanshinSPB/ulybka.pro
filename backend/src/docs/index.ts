import { CompanyController } from '@/companies/company.controller';
import { DirectoryController } from '@/directories/directory.controller';
import { RoleController } from '@/roles/role.controller';
import { ServiceController } from '@/services/service.controller';
import { UserController } from '@/users/user.controller';
import { CabinetController } from 'src/cabinet/cabinet.controller';
import { FieldController } from 'src/fields/field.controller';
import { FileController } from 'src/file/file.controller';
import { LogsController } from 'src/logs/logs.controller';
import { NoticeController } from 'src/notice/notice.controller';
import { OrderController } from 'src/orders/order.controller';
import { PatientController } from 'src/patients/patient.controller';

export const API_PRIVATE_CONTROLLERS = [
    CabinetController,
    CompanyController,
    DirectoryController,
    FieldController,
    FileController,
    LogsController,
    NoticeController,
    OrderController,
    PatientController,
    RoleController,
    ServiceController,
    UserController,
];
export const API_PUBLIC_CONTROLLERS = [CabinetController];
