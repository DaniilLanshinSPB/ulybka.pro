import { CompanyModule } from '@/companies/company.module';
import { LogsModule } from '@/logs/logs.module';
import { OrderModule } from '@/orders/order.module';
import { PatientModule } from '@/patients/patient.module';
import { RoleModule } from '@/roles/role.module';
import { ServiceModule } from '@/services/service.module';
import { UserModule } from '@/users/user.module';
import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';
import { CabinetController } from './cabinet.controller';

@Module({
    controllers: [CabinetController],
    providers: [],
    exports: [],
    imports: [
        CacheModule.register(),
        UserModule,
        RoleModule,
        CompanyModule,
        ServiceModule,
        PatientModule,
        OrderModule,
        LogsModule,
    ],
})
export class CabinetModule {}
