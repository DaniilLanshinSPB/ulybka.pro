import { NotVerifiedGuard } from '@/auth/guards/not_verified.guard';
import { PermissionsGuard } from '@/auth/guards/permission.guard';
import { CabinetPermissions, JwtPayload } from '@/auth/interfaces';
import { CompanyService } from '@/companies/company.service';
import { CreateCompanyDto, UpdateCompanyDto } from '@/companies/dto';
import { COMPANY_TYPES } from '@/companies/interfaces';
import { CompanyResponse } from '@/companies/responses/company.response';
import { LogsService } from '@/logs/logs.service';
import { OrderCreateDto, OrderUpdateDto } from '@/orders/dto';
import { OrderCommentCreateDto } from '@/orders/dto/order-comment-create.dto';
import { OrdersFilterDto } from '@/orders/dto/order-filter.dto';
import { OrderCommentService } from '@/orders/order-comment.service';
import { OrderService } from '@/orders/order.service';
import { OrderResponse } from '@/orders/responses';
import { OrderCommentResponse } from '@/orders/responses/order-comment.response';
import { CreatePatientDto } from '@/patients/dto/patient-create.dto';
import { UpdatePatientDto } from '@/patients/dto/patient-update.dto';
import { PatientService } from '@/patients/patient.service';
import { PatientResponse } from '@/patients/responses/patient.response';
import { ServiceResponse } from '@/services/responses';
import { ServiceService } from '@/services/service.service';
import { CurrentUser, Permissions } from '@common/decorators';
import { CurrentCompany } from '@common/decorators/current-company.decorator';
import { ExchangeApi } from '@common/decorators/exchange-api.decorator';
import { getUserName } from '@common/helpers/user.helper';
import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    HttpException,
    HttpStatus,
    NotFoundException,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    Query,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiBody,
    ApiExcludeEndpoint,
    ApiHeader,
    ApiOperation,
    ApiParam,
    ApiQuery,
    ApiResponse,
    ApiTags,
} from '@nestjs/swagger';
import { EntityType, LogType } from '@prisma/postgres/client';

@ApiTags('Работа с кабинетом')
@ApiBearerAuth('authorization')
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(NotVerifiedGuard)
@UseGuards(PermissionsGuard)
@Controller('cabinet')
export class CabinetController {
    constructor(
        protected readonly companyService: CompanyService,
        protected readonly serviceService: ServiceService,
        protected readonly orderService: OrderService,
        protected readonly patientService: PatientService,
        protected readonly commentService: OrderCommentService,
        protected readonly logsService: LogsService,
    ) {}

    @Get('orders')
    @ApiExcludeEndpoint()
    @Permissions(CabinetPermissions.CABINET_VIEW_ORDER)
    @ApiOperation({ summary: 'Получить заказы компании' })
    @ApiQuery({ name: 'filter', type: OrdersFilterDto })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о заказах', type: [OrderResponse] })
    @ApiResponse({ status: 400, description: 'Заказ не найдена' })
    async findOrders(@CurrentUser() user: JwtPayload, @Query('filter') filter: OrdersFilterDto = {}) {
        const company = await this.companyService.findOne(user.company);

        const orderFilter: OrdersFilterDto = {
            ...filter,
        };
        if (!company.parent) {
            orderFilter.company_id = [company.id];
        } else {
            orderFilter.customer_company_id = [company.id];
        }

        const response = await this.orderService.findAll(orderFilter);

        return response.map((order) => new OrderResponse(order));
        return [];
    }
    @Get('orders/:order_id')
    @ApiExcludeEndpoint()
    @Permissions(CabinetPermissions.CABINET_VIEW_ORDER)
    @ApiParam({ name: 'order_id', type: Number })
    @ApiOperation({ summary: 'Получить заказ компании' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о заказе', type: OrderResponse })
    @ApiResponse({ status: 400, description: 'Заказ не найдена' })
    async findOneOrder(@CurrentUser() user: JwtPayload, @Param('order_id', ParseIntPipe) order_id: number) {
        const company = await this.companyService.findOne(user.company);
        await this.validateCompanyOrder(company.parent ? company.parent.id : company.id, order_id);
        const order = await this.orderService.findOne(company.parent ? company.parent.id : company.id, order_id);

        return new OrderResponse(order);
    }

    @Get('orders/:order_id/comments')
    @ApiExcludeEndpoint()
    @Permissions(CabinetPermissions.CABINET_VIEW_ORDER)
    @ApiParam({ name: 'order_id', type: Number })
    @ApiOperation({ summary: 'Получить заказ компании' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о заказе', type: [OrderCommentResponse] })
    @ApiResponse({ status: 400, description: 'Заказ не найдена' })
    async findOrderComments(@CurrentUser() user: JwtPayload, @Param('order_id', ParseIntPipe) order_id: number) {
        const company = await this.companyService.findOne(user.company);
        await this.validateCompanyOrder(company.parent ? company.parent.id : company.id, order_id);
        const comments = await this.commentService.findAll(order_id);
        return comments.map((comment) => new OrderCommentResponse(comment));
    }

    @Post('orders/:order_id/comments')
    @ApiExcludeEndpoint()
    @ApiParam({ name: 'order_id', type: Number })
    @ApiBody({ type: OrderCommentCreateDto })
    @Permissions(CabinetPermissions.CABINET_CREATE_ORDER_COMMENT)
    @ApiOperation({ summary: 'Создание комментария' })
    @ApiResponse({ status: 200, description: 'Комментарий успешно создана' })
    async createComment(
        @Param('order_id', ParseIntPipe) order_id: number,
        @Body() dto: OrderCommentCreateDto,
        @CurrentUser() user: JwtPayload,
    ) {
        const company = await this.companyService.findOne(user.company);
        await this.validateCompanyOrder(company.parent ? company.parent.id : company.id, order_id);
        const comment = await this.commentService.create(order_id, dto, user.id);
        await this.logsService.logAction({
            user_id: user.id,
            entity_type: EntityType.OrderComment,
            entity_id: comment.id.toString(),
            details: `Пользователь ${getUserName(user)} оставил комментарий к заказу №${order_id}`,
            type: LogType.COMMENT,
            metadata: comment,
        });
        return new OrderCommentResponse(comment);
    }
    @Delete('orders/:order_id/comments/:id')
    @ApiExcludeEndpoint()
    @ApiParam({ name: 'order_id', type: Number })
    @ApiParam({ name: 'id', type: Number })
    @Permissions(CabinetPermissions.CABINET_DELETE_ORDER_COMMENT)
    @ApiOperation({ summary: 'Удалить комментарий по ID' })
    @ApiResponse({ status: 200, description: 'Комментарий успешно удален' })
    @ApiResponse({ status: 400, description: 'Комментарий не найден' })
    async deleteComment(
        @Param('order_id', ParseIntPipe) order_id: number,
        @Param('id', ParseIntPipe) id: number,
        @CurrentUser() user: JwtPayload,
    ) {
        const company = await this.companyService.findOne(user.company);
        const order = await this.validateCompanyOrder(company.parent ? company.parent.id : company.id, order_id);
        const comment = await this.commentService.findOne(id);
        await this.logsService.logAction({
            user_id: user.id,
            entity_type: EntityType.OrderComment,
            entity_id: comment.id.toString(),
            details: `Пользователь ${getUserName(user)} удалил комментарий к заказу ${order.title}`,
            metadata: comment,
            type: LogType.SYSTEM,
        });
        return this.commentService.delete(id, user.id);
    }

    @Post('orders/create')
    @ApiExcludeEndpoint()
    @Permissions(CabinetPermissions.CABINET_CREATE_ORDER)
    @ApiBody({ type: OrderCreateDto })
    @ApiOperation({ summary: 'Создание заказа' })
    @ApiResponse({ status: 200, description: 'Заказ успешно создана' })
    async createOrder(@CurrentUser() user: JwtPayload, @Body() dto: OrderCreateDto) {
        const cabinet_company = await this.companyService.findOne(user.company);
        const company_id = cabinet_company.parent ? cabinet_company.parent.id : cabinet_company.id;
        dto.customer_company_id = cabinet_company.id;

        const order = await this.orderService.create(company_id, dto.customer_id, dto.customer_id, dto);
        const metadata = {
            id: order.id,
            title: order.title,
            description: order.description,
            status_id: order.status_id,
            customer_id: order.customer_id,
            patient_uid: order.patient_uid,
            creator_id: order.creator_id,
            responsible_id: order.responsible_id,
            executor_id: order.executor_id,
            finish_at: order.finish_at,
            prefer_date_at: order.prefer_date_at,
            customer_company_id: order.customer_company_id,
        };
        await this.logsService.logAction({
            user_id: user.id,
            entity_type: EntityType.Order,
            entity_id: order.id.toString(),
            details: `Добавлен новый заказ ${order.title}`,
            metadata,
            type: LogType.ORDER,
        });
        return new OrderResponse(order);
    }

    @Patch('orders/:id')
    @ApiExcludeEndpoint()
    @ApiParam({ name: 'id', type: Number })
    @ApiBody({ type: OrderUpdateDto })
    @Permissions(CabinetPermissions.CABINET_UPDATE_ORDER)
    @ApiOperation({ summary: 'Обновление заказа' })
    @ApiResponse({ status: 200, description: 'Заказ успешно обновлена' })
    async updateOrder(
        @CurrentUser() user: JwtPayload,
        @Param('id', ParseIntPipe) id: number,
        @Body() dto: OrderUpdateDto,
    ) {
        const company = await this.companyService.findOne(user.company);
        const oldOrder = await this.validateCompanyOrder(company.parent ? company.parent.id : company.id, id);
        const order = await this.orderService.update(id, dto);
        let message = null;

        if (oldOrder.status_id !== order.status_id) {
            message = `${order.title}\nИзменен статус на «${order.status.name}»`;
        }
        if (oldOrder.executor_id !== order.executor_id) {
            message = `${order.title}\nИзменен исполнитель на «${order.executor.last_name} ${order.executor.first_name}»`;
        }
        if (oldOrder.customer_id !== order.customer_id) {
            message = `${order.title}\nИзменен врач на «${order.customer.last_name} ${order.customer.first_name}»`;
        }
        if (oldOrder.patient_uid !== order.patient_uid) {
            message = `${order.title}\nИзменен пациент на «${order.patient.fio}»`;
        }
        if (oldOrder.responsible_id !== order.responsible_id) {
            message = `${order.title}\nИзменен ответственный на «${order.responsible.last_name} ${order.responsible.first_name}»`;
        }
        if (message) {
            const metadata = {
                id: order.id,
                title: order.title,
                description: order.description,
                status_id: order.status_id,
                customer_id: order.customer_id,
                patient_uid: order.patient_uid,
                creator_id: order.creator_id,
                responsible_id: order.responsible_id,
                executor_id: order.executor_id,
                finish_at: order.finish_at,
                prefer_date_at: order.prefer_date_at,
                customer_company_id: order.customer_company_id,
            };
            await this.logsService.logAction({
                user_id: user.id,
                entity_type: EntityType.Order,
                entity_id: order.id.toString(),
                metadata,
                details: message,
                type: LogType.ORDER,
            });
        }
        return new OrderResponse(order);
    }

    @Delete('orders/:id')
    @ApiExcludeEndpoint()
    @ApiParam({ name: 'id', type: Number })
    @ApiQuery({ name: 'reason', type: Number })
    @Permissions(CabinetPermissions.CABINET_DELETE_ORDER)
    @ApiOperation({ summary: 'Удалить заказ по ID' })
    @ApiResponse({ status: 200, description: 'Заказ успешно удален' })
    @ApiResponse({ status: 400, description: 'Заказ не найден' })
    async softDeleteOrder(
        @CurrentUser() user: JwtPayload,
        @Param('id', ParseIntPipe) id: number,
        @Query('reason') reason: string,
    ) {
        const company = await this.companyService.findOne(user.company);
        const order = await this.validateCompanyOrder(company.parent ? company.parent.id : company.id, id);
        const metadata = {
            id: order.id,
            title: order.title,
            description: order.description,
            status_id: order.status_id,
            customer_id: order.customer_id,
            patient_uid: order.patient_uid,
            creator_id: order.creator_id,
            responsible_id: order.responsible_id,
            executor_id: order.executor_id,
            finish_at: order.finish_at,
            prefer_date_at: order.prefer_date_at,
            customer_company_id: order.customer_company_id,
        };
        await this.logsService.logAction({
            user_id: user.id,
            entity_type: EntityType.Order,
            entity_id: order.id.toString(),
            metadata,
            details: `Заказ ${order.title} удален`,
            type: LogType.ORDER,
        });
        return this.orderService.softDelete(id, reason);
    }

    @Get('clinics')
    @ApiExcludeEndpoint()
    @Permissions(CabinetPermissions.CABINET_VIEW_CLINICS)
    @ApiOperation({ summary: 'Получить клиники компании' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о клиниках', type: [CompanyResponse] })
    @ApiResponse({ status: 400, description: 'Компания не найдена' })
    async findAllClinics(@CurrentUser() user: JwtPayload) {
        const response = await this.companyService.findAllClinics(user.company);
        return response.map((clinic) => new CompanyResponse(clinic));
    }

    @Get('clinics/:clinic_id')
    @ApiExcludeEndpoint()
    @ApiParam({ name: 'clinic_id', type: Number })
    @Permissions(CabinetPermissions.CABINET_VIEW_CLINICS)
    @ApiOperation({ summary: 'Получить клинику компании' })
    @ApiResponse({ status: 200, description: 'Клиника успешно обновлен' })
    @ApiResponse({ status: 400, description: 'Клиника не найдена' })
    async findOneClinic(@CurrentUser() user: JwtPayload, @Param('clinic_id', ParseIntPipe) clinic_id: number) {
        const response = await this.companyService.findOneClinic(user.company, clinic_id);
        return new CompanyResponse(response);
    }

    @Post('clinics')
    @ApiExcludeEndpoint()
    @Permissions(CabinetPermissions.CABINET_CREATE_CLINICS)
    @ApiBody({ type: CreateCompanyDto })
    @ApiOperation({ summary: 'Создание клиники компании' })
    @ApiResponse({ status: 200, description: 'Клиника успешно создана' })
    @ApiResponse({ status: 400, description: 'Компания не найдена' })
    async saveClinic(@CurrentUser() user: JwtPayload, @Body() dto: CreateCompanyDto) {
        const response = await this.companyService.createClinic(user.company, dto);
        await this.logsService.logAction({
            user_id: user.id,
            entity_type: EntityType.Clinic,
            entity_id: response.id.toString(),
            details: `Пользователь ${getUserName(user)} добавил клинику ${response.title}`,
            metadata: response,
            type: LogType.SYSTEM,
        });
        return new CompanyResponse(response);
    }

    @Patch('clinics/:clinic_id')
    @ApiExcludeEndpoint()
    @ApiParam({ name: 'clinic_id', type: Number })
    @ApiBody({ type: UpdateCompanyDto })
    @Permissions(CabinetPermissions.CABINET_UPDATE_CLINICS)
    @ApiOperation({ summary: 'Обновление клиники компании' })
    @ApiResponse({ status: 200, description: 'Клиника успешно обновлен' })
    @ApiResponse({ status: 400, description: 'Клиника не найдена' })
    async updateClinic(
        @CurrentUser() user: JwtPayload,
        @Param('clinic_id', ParseIntPipe) clinic_id: number,
        @Body() dto: UpdateCompanyDto,
    ) {
        const response = await this.companyService.updateClinic(user.company, clinic_id, dto);
        await this.logsService.logAction({
            user_id: user.id,
            entity_type: EntityType.Clinic,
            entity_id: response.id.toString(),
            details: `Пользователь ${getUserName(user)} обновил клинику ${response.title}`,
            type: LogType.SYSTEM,
            metadata: response,
        });
        return new CompanyResponse(response);
    }

    @Delete('clinics/:clinic_id')
    @ApiExcludeEndpoint()
    @ApiParam({ name: 'clinic_id', type: Number })
    @Permissions(CabinetPermissions.CABINET_DELETE_CLINICS)
    @ApiOperation({ summary: 'Удалить клинику компании по ID' })
    @ApiResponse({ status: 200, description: 'Клиника успешно удалена' })
    @ApiResponse({ status: 400, description: 'Клиника не найдена' })
    async deleteClinic(@CurrentUser() user: JwtPayload, @Param('clinic_id', ParseIntPipe) clinic_id: number) {
        const clinic = await this.companyService.findOneClinic(user.company, clinic_id);
        await this.logsService.logAction({
            user_id: user.id,
            entity_type: EntityType.Clinic,
            entity_id: clinic.id.toString(),
            details: `Пользователь ${getUserName(user)} удалил клинику ${clinic.title}`,
            type: LogType.SYSTEM,
            metadata: clinic,
        });
        return this.companyService.deleteClinic(user.company, clinic.id);
    }

    @Get('services')
    @ApiExcludeEndpoint()
    @Permissions(CabinetPermissions.CABINET_VIEW_SERVICE)
    @ApiOperation({ summary: 'Получить все сервисы' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о сервисе', type: [ServiceResponse] })
    async findAllServices(@CurrentUser() user: JwtPayload) {
        const company = await this.companyService.findOne(user.company);
        const company_id = company.parent ? company.parent.id : company.id;
        return this.serviceService.findCompanyServices(company_id);
    }
    @Get('services/:id')
    @ApiExcludeEndpoint()
    @ApiParam({ name: 'id', type: Number })
    @Permissions(CabinetPermissions.CABINET_VIEW_SERVICE)
    @ApiOperation({ summary: 'Получить информацию о сервисе по ID' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о сервисе', type: ServiceResponse })
    async findOneService(@CurrentUser() user: JwtPayload, @Param('id', ParseIntPipe) id: number) {
        const service = await this.serviceService.findOne(id);
        return new ServiceResponse(service);
    }

    @Get('patients/:clinic_id/search')
    @ApiParam({ name: 'clinic_id', type: Number })
    @Permissions(CabinetPermissions.CABINET_VIEW_PATIENT)
    @ApiOperation({ summary: 'Поиск пациентов' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о пациентах' })
    async search(@CurrentUser() user: JwtPayload, @Param('clinic_id', ParseIntPipe) clinic_id: number) {
        const company = await this.validateUserCompany(user.company, clinic_id);
        const company_ids: number[] = !company.parent
            ? [company.id]
            : [company.id, ...company.childrens.map((c) => c.id)];

        const patients = await this.patientService.findAll(company_ids);
        return patients.map((patient) => new PatientResponse(patient));
    }

    @Get('patients/:clinic_id/all')
    @ApiParam({ name: 'clinic_id', type: Number })
    @Permissions(CabinetPermissions.CABINET_VIEW_PATIENT)
    @ApiOperation({ summary: 'Получить всех пациентов' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о пациентах', type: [PatientResponse] })
    async findAll(@CurrentUser() user: JwtPayload, @Param('clinic_id', ParseIntPipe) clinic_id: number) {
        const company = await this.validateUserCompany(user.company, clinic_id);
        const company_ids: number[] = !company.parent
            ? [company.id, ...company.childrens.map((c) => c.id)]
            : [company.id];

        const patients = await this.patientService.findAll(company_ids);
        return patients.map((patient) => new PatientResponse(patient));
    }

    @Get('patients/:uid')
    @ExchangeApi()
    @ApiHeader({ name: 'X-API-KEY', required: true, description: 'Ваш идентефикатор апи' })
    @ApiParam({ name: 'uid', type: String })
    @Permissions(CabinetPermissions.CABINET_VIEW_PATIENT)
    @ApiOperation({ summary: 'Получить информацию о пациенте по UID' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о пациенте', type: PatientResponse })
    async findOne(
        @CurrentCompany() currentCompany: number,
        @CurrentUser() user: JwtPayload,
        @Param('uid') uid: string,
    ) {
        const company_id = currentCompany ? currentCompany : user.company;
        const company = await this.companyService.findOne(company_id);
        const company_ids: number[] = !company.parent
            ? [company.id, ...company.childrens.map((c) => c.id)]
            : [company.id];
        const patient = await this.patientService.findOne(company_ids, uid);
        return new PatientResponse(patient);
    }

    @Post('patients/:clinic_id')
    @ExchangeApi()
    @ApiHeader({ name: 'X-API-KEY', required: true, description: 'Ваш идентефикатор апи' })
    @ApiParam({ name: 'clinic_id', type: Number })
    @ApiBody({ type: CreatePatientDto })
    @Permissions(CabinetPermissions.CABINET_CREATE_PATIENT)
    @ApiOperation({ summary: 'Добавить пациента' })
    @ApiResponse({ status: 200, description: 'Успешное добавление пациента', type: PatientResponse })
    async create(
        @CurrentUser() user: JwtPayload,
        @CurrentCompany() currentCompany: number,
        @Param('clinic_id', ParseIntPipe) clinic_id: number,
        @Body() dto: CreatePatientDto,
    ) {
        const company_id = currentCompany ? currentCompany : user.company;

        await this.validateUserCompany(company_id, clinic_id);
        const patient = await this.patientService.create(clinic_id, dto);
        await this.logsService.logAction({
            user_id: user.id,
            entity_type: EntityType.Patient,
            entity_id: patient.uid,
            metadata: patient,
            type: LogType.PATIENT,
            details: `Пользователь ${getUserName(user)} добавил пациента ${patient.fio}`,
        });
        return new PatientResponse(patient);
    }

    @Patch('patients/:clinic_id/:uid')
    @ExchangeApi()
    @ApiHeader({ name: 'X-API-KEY', required: true, description: 'Ваш идентефикатор апи' })
    @ApiParam({ name: 'clinic_id', type: Number })
    @ApiParam({ name: 'uid', type: String })
    @ApiBody({ type: UpdatePatientDto })
    @Permissions(CabinetPermissions.CABINET_UPDATE_PATIENT)
    @ApiOperation({ summary: 'Обновление пациента' })
    @ApiResponse({ status: 200, description: 'Пациент успешно обновлен', type: PatientResponse })
    async update(
        @CurrentUser() user: JwtPayload,
        @CurrentCompany() currentCompany: number,
        @Param('clinic_id', ParseIntPipe) clinic_id: number,
        @Param('uid') uid: string,
        @Body() dto: UpdatePatientDto,
    ) {
        const company_id = currentCompany ? currentCompany : user.company;

        await this.validateUserCompany(company_id, clinic_id);
        const patient = await this.patientService.update(clinic_id, uid, dto);
        await this.logsService.logAction({
            user_id: user.id,
            entity_type: EntityType.Patient,
            entity_id: patient.uid,
            metadata: patient,
            details: `Пользователь ${getUserName(user)} обновил информацию о пациенте ${patient.fio}`,
            type: LogType.PATIENT,
        });
        return new PatientResponse(patient);
    }

    @Delete('patients/:clinic_id/:uid')
    @ExchangeApi()
    @ApiHeader({ name: 'X-API-KEY', required: true, description: 'Ваш идентефикатор апи' })
    @ApiParam({ name: 'clinic_id', type: Number })
    @ApiParam({ name: 'uid', type: String })
    @Permissions(CabinetPermissions.CABINET_DELETE_PATIENT)
    @ApiOperation({ summary: 'Удаление пациента' })
    @ApiResponse({ status: 200, description: 'Пациент успешно удален', type: PatientResponse })
    async delete(
        @CurrentUser() user: JwtPayload,
        @CurrentCompany() currentCompany: number,
        @Param('clinic_id', ParseIntPipe) clinic_id: number,
        @Param('uid') uid: string,
    ) {
        const company_id = currentCompany ? currentCompany : user.company;

        await this.validateUserCompany(company_id, clinic_id);
        const patient = await this.patientService.findOne(clinic_id, uid);
        await this.logsService.logAction({
            user_id: user.id,
            entity_type: EntityType.Patient,
            entity_id: patient.uid,
            details: `Пользователь ${getUserName(user)} удалил пациента ${patient.fio}`,
            metadata: patient,
            type: LogType.SYSTEM,
        });
        return await this.patientService.delete(clinic_id, uid);
    }

    async validateUserCompany(user_company_id: number, clinic_id: number) {
        const userCompany = await this.companyService.findOne(user_company_id);
        if (userCompany.type_id === COMPANY_TYPES.LABORATORY) {
            const clinic = this.companyService.findOneClinic(user_company_id, clinic_id);
            if (!clinic) throw new HttpException('Клиника не найдена', HttpStatus.BAD_REQUEST);
        }
        return userCompany;
    }

    async validateCompanyOrder(company_id: number, order_id: number) {
        const order = await this.orderService.findOne(company_id, order_id);
        if (!order) throw new NotFoundException('Заказ не найден');
        return order;
    }
}
