import { ApiProperty } from '@nestjs/swagger';
import { LogType } from '@prisma/postgres/client';

export class NoticeTypesResponse {
    @ApiProperty({ name: 'slug', enum: LogType })
    slug: LogType;
    @ApiProperty({ name: 'active', type: Boolean })
    active: boolean;

    constructor(data) {
        Object.assign(this, data);
    }
}
