import { ApiProperty } from '@nestjs/swagger';
import { Company, CompanyType, NoticeType, Prisma, User } from '@prisma/postgres/client';
import { Exclude, Transform } from 'class-transformer';

export class ProfileResponse implements User {
    @ApiProperty({ description: 'Идентификатор пользователя' })
    id: number;

    @ApiProperty({ description: 'Статус активации аккаунта пользователя' })
    is_active: boolean;

    @Exclude()
    role_id: number;

    @Exclude()
    role: string;

    @ApiProperty({ description: 'Email пользователя' })
    email: string;

    @Exclude()
    password: string;

    @ApiProperty({ description: 'Аватар пользователя' })
    avatar: string;

    @ApiProperty({ description: 'Имя пользователя' })
    first_name: string;

    @ApiProperty({ description: 'Фамилия пользователя' })
    last_name: string;

    @ApiProperty({ description: 'Отчество пользователя' })
    middle_name: string;

    @Exclude()
    email_verified: boolean;

    @Exclude()
    @Transform(({ value }) => value.toString(), { toPlainOnly: true })
    telegram_id: string | null;

    @ApiProperty({ description: 'Логин телеграм' })
    telegram_username: string | null;

    @Exclude()
    telegram_key: string | null;

    @Exclude()
    company_id: number;

    @ApiProperty({ description: 'Компания' })
    company: Company & { type: CompanyType };

    @Exclude()
    @ApiProperty({ description: 'Дата и время последнего входа' })
    last_login: Date;

    @Exclude()
    login_attempts: number;

    @Exclude()
    last_failed_login: Date;

    @Exclude()
    verified_code: string;

    @ApiProperty({ description: 'The metadata of the user' })
    metadata: Prisma.JsonValue;

    @Exclude()
    user_notice_type: NoticeType[];

    @ApiProperty({ description: 'Notice types' })
    user_notice_permissions: NoticeType[];

    @Exclude()
    created_at: Date;

    @Exclude()
    updated_at: Date;

    @Exclude()
    deleted_at: Date;

    constructor(user: User) {
        Object.assign(this, user);
    }
}
