import { Company, PermissionType, Role, Token, User } from '@prisma/postgres/client';

export interface Tokens {
    accessToken: string;
    refreshToken: Token;
}

export interface JwtPayload {
    id: number;
    email: string;
    first_name: string;
    last_name: string;
    middle_name: string;
    role: string;
    company: number;
    is_active: boolean;
    email_verified: boolean;
}

export enum SystemPermissions {
    SYSTEM_VIEW_LOGS = 'system_logs_view',

    SYSTEM_VIEW_USER = 'system_user_view',
    SYSTEM_CREATE_USER = 'system_user_create',
    SYSTEM_UPDATE_USER = 'system_user_update',
    SYSTEM_DELETE_USER = 'system_user_delete',

    SYSTEM_VIEW_COMPANIES = 'system_companies_view',
    SYSTEM_CREATE_COMPANIES = 'system_companies_create',
    SYSTEM_UPDATE_COMPANIES = 'system_companies_update',
    SYSTEM_DELETE_COMPANIES = 'system_companies_delete',

    SYSTEM_VIEW_COMPANY_SERVICE = 'system_company_service_view',
    SYSTEM_CREATE_COMPANY_SERVICE = 'system_company_service_create',
    SYSTEM_DELETE_COMPANY_SERVICE = 'system_company_service_delete',

    SYSTEM_VIEW_DIRECTORY = 'system_directory_view',
    SYSTEM_CREATE_DIRECTORY = 'system_directory_create',
    SYSTEM_UPDATE_DIRECTORY = 'system_directory_update',
    SYSTEM_DELETE_DIRECTORY = 'system_directory_delete',

    SYSTEM_VIEW_PATIENT = 'system_patient_view',
    SYSTEM_CREATE_PATIENT = 'system_patient_create',
    SYSTEM_UPDATE_PATIENT = 'system_patient_update',
    SYSTEM_DELETE_PATIENT = 'system_patient_delete',

    SYSTEM_VIEW_ORDER = 'system_order_view',
    SYSTEM_CREATE_ORDER = 'system_order_create',
    SYSTEM_UPDATE_ORDER = 'system_order_update',
    SYSTEM_DELETE_ORDER = 'system_order_delete',

    SYSTEM_VIEW_ORDER_STATUS = 'system_order_status_view',
    SYSTEM_CREATE_ORDER_STATUS = 'system_order_status_create',
    SYSTEM_UPDATE_ORDER_STATUS = 'system_order_status_update',
    SYSTEM_DELETE_ORDER_STATUS = 'system_order_status_delete',

    SYSTEM_VIEW_ORDER_COMMENT = 'system_order_comment_view',
    SYSTEM_CREATE_ORDER_COMMENT = 'system_order_comment_create',
    SYSTEM_UPDATE_ORDER_COMMENT = 'system_order_comment_update',
    SYSTEM_DELETE_ORDER_COMMENT = 'system_order_comment_delete',

    SYSTEM_VIEW_FIELD = 'system_field_view',
    SYSTEM_CREATE_FIELD = 'system_field_create',
    SYSTEM_UPDATE_FIELD = 'system_field_update',
    SYSTEM_DELETE_FIELD = 'system_field_delete',

    SYSTEM_VIEW_SERVICE = 'system_service_view',
    SYSTEM_CREATE_SERVICE = 'system_service_create',
    SYSTEM_UPDATE_SERVICE = 'system_service_update',
    SYSTEM_DELETE_SERVICE = 'system_service_delete',

    SYSTEM_VIEW_SERVICE_CATEGORY = 'system_service_category_view',
    SYSTEM_CREATE_SERVICE_CATEGORY = 'system_service_category_create',
    SYSTEM_UPDATE_SERVICE_CATEGORY = 'system_service_category_update',
    SYSTEM_DELETE_SERVICE_CATEGORY = 'system_service_category_delete',

    SYSTEM_VIEW_CLINICS = 'system_clinics_view',
    SYSTEM_CREATE_CLINICS = 'system_clinics_create',
    SYSTEM_UPDATE_CLINICS = 'system_clinics_update',
    SYSTEM_DELETE_CLINICS = 'system_clinics_delete',

    SYSTEM_VIEW_ROLES = 'system_roles_view',
    SYSTEM_CREATE_ROLES = 'system_roles_create',
    SYSTEM_UPDATE_ROLES = 'system_roles_update',
    SYSTEM_DELETE_ROLES = 'system_roles_delete',
}
export enum CabinetPermissions {
    CABINET_MY_COMPANY_EDIT = 'my_company_update',

    CABINET_VIEW_CLINICS = 'clinics_view',
    CABINET_CREATE_CLINICS = 'clinics_create',
    CABINET_UPDATE_CLINICS = 'clinics_update',
    CABINET_DELETE_CLINICS = 'clinics_delete',

    CABINET_VIEW_SERVICE = 'service_view',
    CABINET_TOGGLE_SERVICE = 'service_toggle',

    CABINET_VIEW_ORDER = 'order_view',
    CABINET_CREATE_ORDER = 'order_create',
    CABINET_UPDATE_ORDER = 'order_update',
    CABINET_DELETE_ORDER = 'order_delete',

    CABINET_UPDATE_ORDER_RESPONSIBLE = 'order_responsible_update',
    CABINET_UPDATE_ORDER_EXECUTOR = 'order_executor_update',
    CABINET_UPDATE_ORDER_STATUS = 'order_status_update',
    CABINET_UPDATE_ORDER_DESCRIPTION = 'order_description_update',
    CABINET_UPDATE_ORDER_FINISH_AT = 'order_finish_at_update',

    CABINET_VIEW_ORDER_STATUS = 'order_status_view',

    CABINET_VIEW_ORDER_COMMENT = 'order_comment_view',
    CABINET_CREATE_ORDER_COMMENT = 'order_comment_create',
    CABINET_UPDATE_ORDER_COMMENT = 'order_comment_update',
    CABINET_DELETE_ORDER_COMMENT = 'order_comment_delete',

    CABINET_VIEW_PATIENT = 'patient_view',
    CABINET_CREATE_PATIENT = 'patient_create',
    CABINET_UPDATE_PATIENT = 'patient_update',
    CABINET_DELETE_PATIENT = 'patient_delete',
}
export const permissionsDto = [
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_VIEW_LOGS,
        category: 'Логи',
        name: 'Просмотр логов',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_VIEW_USER,
        category: 'Пользователи',
        name: 'Просмотр пользователей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_CREATE_USER,
        category: 'Пользователи',
        name: 'Создание пользователей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_UPDATE_USER,
        category: 'Пользователи',
        name: 'Обновление пользователей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_DELETE_USER,
        category: 'Пользователи',
        name: 'Удаление пользователей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_VIEW_COMPANIES,
        category: 'Компании',
        name: 'Просмотр компании',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_CREATE_COMPANIES,
        category: 'Компании',
        name: 'Создание компании',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_UPDATE_COMPANIES,
        category: 'Компании',
        name: 'Обновление компании',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_DELETE_COMPANIES,
        category: 'Компании',
        name: 'Удаление компании',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_VIEW_COMPANY_SERVICE,
        category: 'Услуги компании',
        name: 'Просмотр услуг компании',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_CREATE_COMPANY_SERVICE,
        category: 'Услуги компании',
        name: 'Добавление услуг компании',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_DELETE_COMPANY_SERVICE,
        category: 'Услуги компании',
        name: 'Удаление услуг компании',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_VIEW_DIRECTORY,
        category: 'Словарь',
        name: 'Просмотр словарей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_CREATE_DIRECTORY,
        category: 'Словарь',
        name: 'Создание словарей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_UPDATE_DIRECTORY,
        category: 'Словарь',
        name: 'Обновление словарей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_DELETE_DIRECTORY,
        category: 'Словарь',
        name: 'Удаление словарей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_VIEW_PATIENT,
        category: 'Пациенты',
        name: 'Просмотр пациентов',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_CREATE_PATIENT,
        category: 'Пациенты',
        name: 'Создание пациентов',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_UPDATE_PATIENT,
        category: 'Пациенты',
        name: 'Обновление пациентов',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_DELETE_PATIENT,
        category: 'Пациенты',
        name: 'Удаление пациентов',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_VIEW_ORDER,
        category: 'Заказы',
        name: 'Просмотр заказов',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_CREATE_ORDER,
        category: 'Заказы',
        name: 'Создание заказов',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_UPDATE_ORDER,
        category: 'Заказы',
        name: 'Обновление заказов',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_DELETE_ORDER,
        category: 'Заказы',
        name: 'Удаление заказов',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_VIEW_ORDER_STATUS,
        category: 'Статусы заказов',
        name: 'Просмотр статусов заказа',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_CREATE_ORDER_STATUS,
        category: 'Статусы заказов',
        name: 'Создание статусов заказа',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_UPDATE_ORDER_STATUS,
        category: 'Статусы заказов',
        name: 'Обновление статусов заказа',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_DELETE_ORDER_STATUS,
        category: 'Статусы заказов',
        name: 'Удаление статусов заказа',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_VIEW_ORDER_COMMENT,
        category: 'Комментарии',
        name: 'Просмотр компантариев к заказу',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_CREATE_ORDER_COMMENT,
        category: 'Комментарии',
        name: 'Создание компантариев к заказу',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_UPDATE_ORDER_COMMENT,
        category: 'Комментарии',
        name: 'Обновление компантариев к заказу',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_DELETE_ORDER_COMMENT,
        category: 'Комментарии',
        name: 'Удаление компантариев к заказу',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_VIEW_FIELD,
        category: 'Поля',
        name: 'Просмотр полей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_CREATE_FIELD,
        category: 'Поля',
        name: 'Создание полей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_UPDATE_FIELD,
        category: 'Поля',
        name: 'Обновление полей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_DELETE_FIELD,
        category: 'Поля',
        name: 'Удаление полей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_VIEW_SERVICE,
        category: 'Услуги',
        name: 'Просмотр услуг',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_CREATE_SERVICE,
        category: 'Услуги',
        name: 'Создание услуг',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_UPDATE_SERVICE,
        category: 'Услуги',
        name: 'Обновление услуг',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_DELETE_SERVICE,
        category: 'Услуги',
        name: 'Удаление услуг',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_VIEW_SERVICE_CATEGORY,
        category: 'Категории услуг',
        name: 'Просмотр категорий услуг',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_CREATE_SERVICE_CATEGORY,
        category: 'Категории услуг',
        name: 'Создание категорий услуг',
    },
    {
        type: PermissionType.SYSTEM,

        slug: SystemPermissions.SYSTEM_UPDATE_SERVICE_CATEGORY,
        name: 'Категории услуг',
        category: 'Обновление категорий услуг',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_DELETE_SERVICE_CATEGORY,
        category: 'Категории услуг',
        name: 'Удаление атегорий услуг',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_VIEW_CLINICS,
        category: 'Клиники',
        name: 'Просмотр клиник',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_CREATE_CLINICS,
        category: 'Клиники',
        name: 'Создание клиник',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_UPDATE_CLINICS,
        category: 'Клиники',
        name: 'Обновление клиник',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_DELETE_CLINICS,
        category: 'Клиники',
        name: 'Удаление линик',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_VIEW_ROLES,
        category: 'Роли',
        name: 'Просмотр ролей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_CREATE_ROLES,
        category: 'Роли',
        name: 'Создание ролей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_UPDATE_ROLES,
        category: 'Роли',
        name: 'Обновление ролей',
    },
    {
        type: PermissionType.SYSTEM,
        slug: SystemPermissions.SYSTEM_DELETE_ROLES,
        category: 'Роли',
        name: 'Удаление ролей',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_VIEW_CLINICS,
        category: 'Клиники',
        name: 'Просмотр клиник',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_MY_COMPANY_EDIT,
        category: 'Управление своей компанией',
        name: 'Обновление данных клиники',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_CREATE_CLINICS,
        category: 'Клиники',
        name: 'Создание клиник',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_UPDATE_CLINICS,
        category: 'Клиники',
        name: 'Обновление линик',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_DELETE_CLINICS,
        category: 'Клиники',
        name: 'Удаление линик',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_VIEW_SERVICE,
        category: 'Услуги',
        name: 'Просмотр услуг',
    },

    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_VIEW_ORDER,
        category: 'Заказы',
        name: 'Просмотр заказов',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_CREATE_ORDER,
        category: 'Заказы',
        name: 'Создание заказов',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_UPDATE_ORDER,
        category: 'Заказы',
        name: 'Обновление заказов',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_DELETE_ORDER,
        category: 'Заказы',
        name: 'Удаление заказов',
    },

    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_VIEW_ORDER_STATUS,
        category: 'Статусы заказов',
        name: 'Просмотр статусов заказа',
    },

    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_VIEW_ORDER_COMMENT,
        category: 'Комментарии к заказу',
        name: 'Проссмотр комментариев',
    },

    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_CREATE_ORDER_COMMENT,
        category: 'Комментарии к заказу',
        name: 'Создание комментария',
    },

    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_UPDATE_ORDER_COMMENT,
        category: 'Комментарии к заказу',
        name: 'Изменение комментария',
    },

    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_DELETE_ORDER_COMMENT,
        category: 'Комментарии к заказу',
        name: 'Удаление комментария',
    },

    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_VIEW_PATIENT,
        category: 'Пациенты',
        name: 'Просмотр пациентов',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_CREATE_PATIENT,
        category: 'Пациенты',
        name: 'Добавление пациента',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_UPDATE_PATIENT,
        category: 'Пациенты',
        name: 'Редактирование пациента',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_DELETE_PATIENT,
        category: 'Пациенты',
        name: 'Удаление пациента',
    },

    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_UPDATE_ORDER_RESPONSIBLE,
        category: 'Управление заказом (ответственный)',
        name: 'Изменение ответственного',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_UPDATE_ORDER_EXECUTOR,
        category: 'Управление заказом (исполнитель)',
        name: 'Изменение исполнителя',
    },

    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_UPDATE_ORDER_STATUS,
        category: 'Управление заказом (статус)',
        name: 'Изменение статуса заказа',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_UPDATE_ORDER_DESCRIPTION,
        category: 'Управление заказом (описание)',
        name: 'Изменение описания',
    },
    {
        type: PermissionType.LABORATORY,
        slug: CabinetPermissions.CABINET_UPDATE_ORDER_FINISH_AT,
        category: 'Управление заказом (Дата окончиния)',
        name: 'Изменение дату окончания',
    },
];

export interface UserInterface extends User {
    role?: Role;
    company?: Company;
    user_notice_permissions?: { slug: string; active: boolean }[];
}
