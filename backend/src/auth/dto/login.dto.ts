import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class LoginDto {
    @IsEmail()
    @IsNotEmpty()
    @ApiProperty({ description: 'Email пользователя' })
    email: string;

    @IsString()
    @MinLength(6)
    @IsNotEmpty()
    @ApiProperty({ description: 'Пароль пользователя' })
    password: string;
}
