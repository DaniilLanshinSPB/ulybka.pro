import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class ConfirmAccountDto {
    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'Код подтверждения' })
    code: string;
}
