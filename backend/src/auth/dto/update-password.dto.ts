import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdatePasswordDto {
    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'Старый пароль' })
    old_password: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'Пароль' })
    password: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'Подтверждение пароля' })
    confirm_password: string;
}
