import { IsPasswordsMatchingConstraint } from '@common/decorators';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, MinLength, Validate } from 'class-validator';

export class RegisterDto {
    @IsEmail()
    @IsNotEmpty()
    @ApiProperty({ description: 'Email пользователя', type: String })
    email: string;

    @IsString()
    @MinLength(6)
    @IsNotEmpty()
    @ApiProperty({ description: 'Пароль пользователя', type: String })
    password: string;

    @IsString()
    @MinLength(6)
    @Validate(IsPasswordsMatchingConstraint)
    @IsNotEmpty()
    @ApiProperty({ description: 'Подтверждение пароля', type: String })
    confirm_password: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'Имя компании', type: String })
    company?: string;
}
