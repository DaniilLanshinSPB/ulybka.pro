import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class LoginByUserDto {
    @IsEmail()
    @IsNotEmpty()
    @ApiProperty({ description: 'Email пользователя' })
    email: string;
}
