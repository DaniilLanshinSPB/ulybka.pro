import { ApiProperty } from '@nestjs/swagger';
import { LogType, Prisma } from '@prisma/postgres/client';
import { IsArray, IsEmpty, IsNotEmpty, IsString } from 'class-validator';

export class UpdateProfileDto {
    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'Аватар', type: String })
    avatar: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'Имя', type: String })
    first_name: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'Фамилия', type: String })
    last_name: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'Отчество', type: String })
    middle_name: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'Email', type: String })
    email: string;

    @IsEmpty()
    @ApiProperty({ description: 'Метадата по полям', type: Object })
    metadata?: Prisma.JsonValue;

    @IsArray()
    @IsNotEmpty()
    @ApiProperty({ description: 'Уведомления', type: Array })
    user_notice_permissions: { slug: LogType; active: boolean }[];
}
