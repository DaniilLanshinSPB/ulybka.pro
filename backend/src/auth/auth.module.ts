import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { CompanyModule } from '@/companies/company.module';
import { LogsModule } from '@/logs/logs.module';
import { MailModule } from '@/mail/mail.module';
import { NoticeModule } from '@/notice/notice.module';
import { NoticeService } from '@/notice/notice.service';
import { NotificationsGateway } from '@/notice/notification.gateway';
import { PatientModule } from '@/patients/patient.module';
import { TelegramModule } from '@/telegram/telegram.module';
import { TelegramBotProvider } from '@/telegram/telegram.provider';
import { TelegramService } from '@/telegram/telegram.service';
import { UserModule } from '@/users/user.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { options } from './config';
import { GUARDS } from './guards';
import { STRTAGIES } from './strategies';

@Module({
    controllers: [AuthController],
    providers: [
        AuthService,
        NoticeService,
        NotificationsGateway,
        TelegramService,
        TelegramBotProvider,
        ...STRTAGIES,
        ...GUARDS,
    ],
    imports: [
        MailModule,
        PassportModule,
        CompanyModule,
        UserModule,
        JwtModule.registerAsync(options()),
        PatientModule,
        HttpModule,
        LogsModule,
        NoticeModule,
        TelegramModule,
    ],
})
export class AuthModule {}
