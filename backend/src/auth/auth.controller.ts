import { Cookie, CurrentUser, NotVerified, Permissions, Public, UserAgent } from '@common/decorators';

import { CompanyService } from '@/companies/company.service';
import { UpdateCompanyDto } from '@/companies/dto';
import { LogsService } from '@/logs/logs.service';
import { NoticeService } from '@/notice/notice.service';
import { UserResponse } from '@/users/responses';
import {
    BadRequestException,
    Body,
    ClassSerializerInterceptor,
    Controller,
    Get,
    HttpException,
    HttpStatus,
    Patch,
    Post,
    Query,
    Res,
    UnauthorizedException,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApiBody, ApiExcludeController, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { EntityType, LogType } from '@prisma/postgres/client';
import { Response } from 'express';
import { AuthService } from './auth.service';
import { ConfirmAccountDto, LoginDto, RegisterDto, ResetPasswordDto, UpdatePasswordDto, UpdateProfileDto } from './dto';
import { NotVerifiedGuard } from './guards/not_verified.guard';
import { CabinetPermissions, JwtPayload, Tokens } from './interfaces';
import { ProfileResponse, RegisterResponse } from './responses';
import { NoticeTypesResponse } from './responses/notice-type.response';

const REFRESH_TOKEN = 'refreshtoken';

@ApiExcludeController()
@Controller('auth')
@UseInterceptors(ClassSerializerInterceptor)
export class AuthController {
    constructor(
        protected readonly authService: AuthService,
        protected readonly companyService: CompanyService,
        protected readonly configService: ConfigService,
        protected readonly logsService: LogsService,
        protected readonly noticeService: NoticeService,
    ) {}

    @Patch('company')
    @ApiOperation({ summary: 'Получить информацию о компании пользователя' })
    @ApiResponse({ status: 201, description: 'Информация успешно получена', type: UserResponse })
    @ApiResponse({ status: 400, description: 'Ошибка получения информации' })
    async updateCompany(@CurrentUser() user: JwtPayload, @Body() dto: UpdateCompanyDto) {
        const company = await this.companyService.update(Number(user.company), dto);

        return company;
    }

    @Get('company')
    @Permissions(CabinetPermissions.CABINET_MY_COMPANY_EDIT)
    @ApiOperation({ summary: 'Получить информацию о компании пользователя' })
    @ApiResponse({ status: 201, description: 'Информация успешно получена', type: UserResponse })
    @ApiResponse({ status: 400, description: 'Ошибка получения информации' })
    async getCompany(@CurrentUser() user: JwtPayload) {
        const company = this.companyService.findOne(user.company);

        if (!company) {
            throw new BadRequestException(`Не удалось получить информацию о компании`);
        }

        return company;
    }

    @Public()
    @Post('register')
    @ApiOperation({ summary: 'Регистрация пользователя' })
    @ApiBody({ type: RegisterDto })
    @ApiResponse({ status: 201, description: 'Пользователь успешно зарегистрирован', type: UserResponse })
    @ApiResponse({ status: 400, description: 'Ошибка регистрации пользователя' })
    async register(@Body() dto: RegisterDto) {
        const user = await this.authService.register(dto);

        if (!user) {
            throw new BadRequestException(`Не удалось зарегистрировать пользователя с данными ${JSON.stringify(dto)}`);
        }
        await this.logsService.logAction({
            user_id: user.id,
            entity_type: EntityType.User,
            entity_id: user.id,
            details: `Зарегистрирован новый пользователь ${user.email}`,
            metadata: user,
            type: LogType.SYSTEM,
        });
        return new RegisterResponse(user);
    }

    @Public()
    @Post('login')
    @ApiOperation({ summary: 'Авторизация пользователя' })
    @ApiBody({ type: LoginDto })
    @ApiResponse({ status: 200, description: 'Пользователь успешно авторизован' })
    @ApiResponse({ status: 400, description: 'Не удалось выполнить вход с предоставленными данными' })
    async login(@Body() dto: LoginDto, @Res() res: Response, @UserAgent() agent: string) {
        const tokens = await this.authService.login(dto, agent);

        if (!tokens) {
            throw new BadRequestException(
                'Не удалось выполнить вход с предоставленными данными ' + JSON.stringify(dto),
            );
        }
        this.setRefreshTokenToCookies(tokens, res);
    }
    @Get('telegram-connect')
    @ApiOperation({ summary: 'Подключение телеграм бота' })
    @ApiResponse({ status: 200, description: 'Ссылка для подключения телеграм бота' })
    async connectTelegram(@CurrentUser() user: JwtPayload) {
        return this.authService.generateTelegramLink(user.id);
    }

    @UseGuards(NotVerifiedGuard)
    @NotVerified()
    @Get('logout')
    @ApiOperation({ summary: 'Выход из системы' })
    @ApiResponse({ status: 200, description: 'Вы вышли из системы' })
    async logout(@Cookie(REFRESH_TOKEN) refreshToken: string, @Res() res: Response) {
        if (!refreshToken) {
            res.sendStatus(HttpStatus.OK);
            return;
        }
        await this.authService.deleteRefreshToken(refreshToken);
        res.cookie(REFRESH_TOKEN, '', { httpOnly: true, secure: true, expires: new Date() });
        res.sendStatus(HttpStatus.OK);
    }

    @UseGuards(NotVerifiedGuard)
    @NotVerified()
    @Get('me')
    @ApiOperation({ summary: 'Получение информации об авторизованном пользователе' })
    @ApiResponse({ status: 200, description: 'Успешно получить информацию о текущем пользователе' })
    async me(@CurrentUser() user: JwtPayload) {
        const currentUser = await this.authService.getProfile(user.id);
        return new ProfileResponse(currentUser);
    }

    @UseGuards(NotVerifiedGuard)
    @NotVerified()
    @Public()
    @Get('refresh-tokens')
    @ApiOperation({ summary: 'Обновление токена' })
    @ApiResponse({ status: 200, description: 'Токен успешно обновлен' })
    @ApiResponse({ status: 401, description: 'Ошибка обновления токена' })
    async refreshTokens(@Cookie(REFRESH_TOKEN) refreshToken: string, @Res() res: Response, @UserAgent() agent: string) {
        if (!refreshToken) {
            throw new UnauthorizedException();
        }
        try {
            const tokens = await this.authService.refreshTokens(refreshToken, agent);

            if (!tokens) {
                throw new UnauthorizedException();
            }
            this.setRefreshTokenToCookies(tokens, res);
        } catch (e) {
            throw new BadRequestException(e.message);
        }
    }
    @Public()
    @Get('reset-password')
    @ApiOperation({ summary: 'Сброс пароля' })
    @ApiResponse({ status: 200, description: 'Новый пароль выслан на почту' })
    @ApiResponse({ status: 400, description: 'Пользователь не найден' })
    async resetPassword(@Query() dto: ResetPasswordDto) {
        try {
            await this.authService.resetPassword(dto.email);
            return true;
        } catch (e) {
            throw new BadRequestException(e.message);
        }
    }
    @Public()
    @Get('confirm-account')
    @ApiOperation({ summary: 'Подтверждение регистрации' })
    @ApiResponse({ status: 200, description: 'Регистрация подтверждена' })
    @ApiResponse({ status: 400, description: 'Пользователь ' })
    async confirmAccount(@Query() dto: ConfirmAccountDto) {
        try {
            return this.authService.verifyUser(dto.code);
        } catch (e) {
            throw new BadRequestException(e.message);
        }
    }
    @Public()
    @Get('send-code')
    @ApiOperation({ summary: 'Повторная отправка кода верификации' })
    async sendCode(@Query('email') email: string) {
        try {
            await this.authService.resendVerificationCode(email);
            return true;
        } catch (e) {
            throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
        }
    }

    @Patch('profile')
    @ApiOperation({ summary: 'Обновление профиля' })
    @ApiResponse({ status: 200, description: 'Профиль успешно обновлен', type: ProfileResponse })
    async updateProfile(@CurrentUser() user: JwtPayload, @Body() dto: UpdateProfileDto) {
        const profile = await this.authService.updateProfile(Number(user.id), dto);
        return new ProfileResponse(profile);
    }

    @Get('notification-types')
    @ApiOperation({ summary: 'Получить доступные типы нотификации' })
    @ApiResponse({ status: 200, description: 'Успешно получено', type: [NoticeTypesResponse] })
    async fetchNotificationTypes(@CurrentUser() user: JwtPayload) {
        const _user = await this.authService.getProfile(user.id);
        const noticeTypes = await this.noticeService.findAccessUserTypes(_user.role_id);
        return noticeTypes.map((nt) => new NoticeTypesResponse(nt));
    }

    @Patch('update-password')
    @ApiOperation({ summary: 'Изменение пароля' })
    @ApiResponse({ status: 200, description: 'Пароль успешно изменен' })
    async updatePassword(@CurrentUser() user: JwtPayload, @Body() dto: UpdatePasswordDto) {
        await this.authService.updatePassword(Number(user.id), dto);
        return true;
    }

    private setRefreshTokenToCookies(tokens: Tokens, res: Response) {
        if (!tokens) {
            throw new UnauthorizedException();
        }
        res.cookie(REFRESH_TOKEN, tokens.refreshToken.token, {
            httpOnly: true,
            sameSite: 'lax',
            expires: new Date(tokens.refreshToken.exp),
            secure: this.configService.get('NODE_ENV', 'development') === 'production',
            path: '/',
        });
        res.status(HttpStatus.CREATED).json({ accessToken: tokens.accessToken });
    }
}
