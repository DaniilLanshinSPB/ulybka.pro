import { JwtAuthGuard } from './jwt-auth.guard';
import { NotVerifiedGuard } from './not_verified.guard';
import { PermissionsGuard } from './permission.guard';

export const GUARDS = [JwtAuthGuard, PermissionsGuard, NotVerifiedGuard];
