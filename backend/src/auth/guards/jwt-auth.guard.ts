import { PrismaService } from '@/database/postgres-prisma.service';
import { isPublic } from '@common/decorators';
import { isExchange } from '@common/decorators/exchange-api.decorator';
import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') implements CanActivate {
    constructor(protected readonly reflector: Reflector, private readonly prismaService: PrismaService) {
        super();
    }

    async canActivate(ctx: ExecutionContext): Promise<boolean> {
        const _isPublic = isPublic(ctx, this.reflector);
        const _isExchangeApi = isExchange(ctx, this.reflector);

        // Если маршрут публичный, пропускаем проверку
        if (_isPublic) {
            return true;
        }

        // Если это запрос для Exchange API
        const request = ctx.switchToHttp().getRequest();
        const apiKey = request.headers['x-api-key'] as string;
        if (_isExchangeApi && apiKey) {
            if (!apiKey) {
                throw new UnauthorizedException('API key is missing');
            }

            // Ищем ключ в базе данных
            const apiKeyRecord = await this.prismaService.apiKey.findUnique({
                where: {
                    key: apiKey,
                },
            });

            if (apiKeyRecord) {
                // Пробрасываем company_id в объект запроса
                request.current_company = apiKeyRecord.company_id;
                return true;
            } else {
                // Если ключ не найден, вызываем родительский метод canActivate
                return super.canActivate(ctx) as Promise<boolean>;
            }
        }

        // Для всех остальных случаев вызываем родительский метод canActivate
        return super.canActivate(ctx) as Promise<boolean>;
    }
}
