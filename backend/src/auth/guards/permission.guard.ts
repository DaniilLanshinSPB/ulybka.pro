import { CabinetPermissions, SystemPermissions } from '@/auth/interfaces';
import { PERMISSION_KEY } from '@common/decorators';
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class PermissionsGuard implements CanActivate {
    constructor(private reflector: Reflector) {}

    canActivate(context: ExecutionContext): boolean {
        const requiredPermissions = this.reflector.getAllAndOverride<SystemPermissions[] | CabinetPermissions[]>(
            PERMISSION_KEY,
            [context.getHandler(), context.getClass()],
        );
        if (!requiredPermissions) {
            return true;
        }
        const { user } = context.switchToHttp().getRequest();

        return requiredPermissions.some((item) => user.permissions.includes(item));
    }
}
