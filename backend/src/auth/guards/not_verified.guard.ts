import { isNotVerified } from '@common/decorators';
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class NotVerifiedGuard implements CanActivate {
    constructor(private reflector: Reflector) {}

    canActivate(context: ExecutionContext): boolean {
        const _isNotVerified = isNotVerified(context, this.reflector);
        if (_isNotVerified) {
            return true;
        }
        const { user } = context.switchToHttp().getRequest();

        return user.email_verified;
    }
}
