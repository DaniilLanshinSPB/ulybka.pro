import { HttpException, HttpStatus, Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Company, Token, User } from '@prisma/postgres/client';

import { generateVerifyCode } from '@common/helpers';

import { compareSync } from 'bcrypt';
import { add, addMinutes, format } from 'date-fns';
import * as dotenv from 'dotenv';

import { PrismaService } from '@/database/postgres-prisma.service';
import { MailService } from '@/mail/mail.service';
import { NoticeService } from '@/notice/notice.service';
import { UserService } from '@/users/user.service';
import { v4 } from 'uuid';
import { LoginDto, RegisterDto, UpdatePasswordDto, UpdateProfileDto } from './dto';
import { LoginByUserDto } from './dto/login-by-user.dto';
import { Tokens, UserInterface } from './interfaces';
dotenv.config();
@Injectable()
export class AuthService {
    protected readonly logger = new Logger(AuthService.name);
    constructor(
        protected readonly userService: UserService,
        protected readonly jwtService: JwtService,
        protected readonly prismaService: PrismaService,
        protected readonly mailService: MailService,
        protected readonly noticeService: NoticeService,
    ) {}

    async refreshTokens(refreshToken: string, agent: string): Promise<Tokens> {
        const token = await this.prismaService.token.delete({ where: { token: refreshToken } });
        if (!token || new Date(token.exp) < new Date()) {
            throw new UnauthorizedException();
        }
        const user = await this.userService.findOne(token.user_id);
        return this.generateTokens(user, agent);
    }

    async register(dto: RegisterDto) {
        const user: User = await this.userService.findOne(dto.email);

        if (user) {
            throw new HttpException('Email уже используется', 420);
        }

        const company: Company = await this.userService.getCompanyByName(dto.company).catch((err) => {
            this.logger.error(err);
            console.log(err);
            return null;
        });

        if (company) {
            throw new HttpException('Компания с таким именем уже существует', 420);
        }
        let verify_code = '';
        try {
            verify_code = await this.sendVerificationCode(dto.email);
        } catch (e) {
            throw new HttpException('Не удалось отправить код подтверждения', 420);
        }
        const role = await this.prismaService.role.findFirst({
            where: {
                is_default: true,
            },
        });
        if (!role) {
            throw new HttpException('Не удалось зарегистрироваться, попробуйте позже', 420);
        }
        //1 - is id company type = лаборатория
        const newCompany = await this.userService.createCompany(dto.company, 1);

        const newUser = await this.userService
            .create({
                ...dto,
                verified_code: verify_code,
                company_id: newCompany.id,
                role_id: role.id,
            })
            .catch((err) => {
                this.logger.error(err);
                console.log(err);
                return null;
            });
        return newUser;
    }

    async login(dto: LoginDto, agent: string): Promise<Tokens> {
        const user: UserInterface = await this.userService.findOne(dto.email);
        if (!user) {
            throw new HttpException('Такого аккаунта не существует', 420);
        }
        if (!user.is_active) {
            throw new HttpException('Пользователь заблокирован', 420);
        }

        if (this.userService.isUserBlockedDueToFailedAttempts(user)) {
            throw new HttpException(
                `Вы заблокированы из-за множественных ошибок авторизации на 30 мин c ${format(
                    addMinutes(user.last_failed_login, 30),
                    'dd.MM.yyyy HH:mm:ss',
                )}`,
                HttpStatus.BAD_REQUEST,
            );
        }

        if (!user || !compareSync(dto.password, user.password)) {
            if (user) {
                this.userService.failedAuthorization(user.id);
            }
            throw new HttpException('Неверный email или пароль', 420);
        }
        await this.userService.update(user.id, {
            ...user,
            login_attempts: 0,
            last_failed_login: null,
        });
        return this.generateTokens(user, agent);
    }

    async loginByUser(dto: LoginByUserDto, agent: string): Promise<Tokens> {
        const user: UserInterface = await this.userService.findOne(dto.email);
        if (!user) {
            throw new HttpException('Такого аккаунта не существует', 420);
        }
        if (!user.email_verified) {
            throw new HttpException('Подтвердите почту', 420);
        }
        return this.generateTokens(user, agent);
    }

    async getProfile(id: number): Promise<UserInterface> {
        return this.userService.findOne(id);
    }

    async resetPassword(email: string) {
        const user: UserInterface = await this.userService.findOne(email);
        if (!user) {
            throw new HttpException('Такого аккаунта не существует', 420);
        }
        const newPassword = this.generateRandomPassword(12);
        await this.userService.changePassword(user.id, {
            password: newPassword,
            confirm_password: newPassword,
        });
        await this.mailService.sendNewPassword(user.email, newPassword);
        return true;
    }

    async verifyUser(code: string) {
        const user = await this.prismaService.user.findFirst({
            where: {
                verified_code: code,
            },
        });
        if (!user) {
            throw new HttpException('Не удалось верифицировать пользователя', HttpStatus.BAD_REQUEST);
        }

        if (code !== user.verified_code) {
            throw new HttpException('Неверный код', HttpStatus.BAD_REQUEST);
        }
        try {
            await this.prismaService.user.update({
                where: {
                    id: user.id,
                },
                data: {
                    verified_code: '',
                    email_verified: true,
                },
            });
            return true;
        } catch (error) {
            throw new HttpException(`Не удалось подтвердить почту, попробуйте позже`, HttpStatus.BAD_REQUEST);
        }
    }

    async resendVerificationCode(email: string) {
        try {
            const user = await this.userService.findOne(email);

            if (!user) {
                throw new HttpException('Пользователь не найден', HttpStatus.BAD_REQUEST);
            }

            const code = await this.sendVerificationCode(user.email);

            await this.userService.update(user.id, { ...user, verified_code: code });
            return code;
        } catch (e) {
            console.log(e.message);
            throw new HttpException('Не удалось отправить код подтверждения', 420);
        }
    }

    async updateProfile(id: number, dto: UpdateProfileDto) {
        try {
            const _user = await this.userService.findOne(id);

            if (!_user) {
                throw new HttpException('Пользователь не найден', HttpStatus.BAD_REQUEST);
            }

            if (dto?.user_notice_permissions) {
                await this.noticeService.updateUserNotificationPermissions(id, dto.user_notice_permissions);
            }

            const user = await this.userService.update(_user.id, {
                ..._user,
                first_name: dto.first_name,
                last_name: dto.last_name,
                middle_name: dto.middle_name,
                avatar: dto.avatar,
                email: dto.email,
                metadata: dto.metadata,
            });

            return user;
        } catch (e) {
            console.log(e.message);
            throw new HttpException('Не удалось обновить профиль', 420);
        }
    }
    async updatePassword(id: number, dto: UpdatePasswordDto) {
        try {
            const user = await this.userService.findOne(id);

            if (!user) {
                throw new HttpException('Пользователь не найден', HttpStatus.BAD_REQUEST);
            }

            if (!compareSync(dto.old_password, user.password)) {
                throw new HttpException('Неверный старый пароль', HttpStatus.BAD_REQUEST);
            }
            return await this.userService.changePassword(user.id, dto);
        } catch (e) {
            console.log(e.message);
            throw new HttpException('Не удалось обновить пароль', 420);
        }
    }

    async generateTelegramLink(id: number) {
        const user = await this.userService.findOne(id);
        if (!user) throw new HttpException('Пользователь не найден', HttpStatus.BAD_REQUEST);
        const res = await this.prismaService.user.update({
            where: {
                id: user.id,
            },
            data: {
                telegram_key: crypto.randomUUID(),
            },
        });
        return `${process.env.TELEGRAM_URL}/?start=${res.telegram_key}`;
    }
    async connectUserTelegram(key: string, telegram_username: string, telegram_id: number) {
        console.log(key);
        const user = await this.userService.findByTelegramKey(key);
        if (!user) return null;

        return this.userService.update(user.id, {
            ...user,
            telegram_id: telegram_id.toString(),
            telegram_username,
            telegram_key: '',
        });
    }

    private async generateTokens(user: UserInterface, agent: string): Promise<Tokens> {
        const userPermissions = await this.prismaService.rolePermissions.findMany({
            where: {
                role_id: user.role_id,
            },
            include: {
                permission: true,
            },
        });
        const permissions = userPermissions.map((item) => item.permission.slug);

        const accessToken =
            'Bearer ' +
            this.jwtService.sign({
                id: user.id,
                email: user.email,
                telegram_username: user.telegram_username,
                first_name: user.first_name,
                last_name: user.last_name,
                middle_name: user.middle_name,
                role: user.role.name,
                permissions: permissions,
                is_active: user.is_active,
                email_verified: user.email_verified,
                company: user.company?.id,
            });

        const refreshToken = await this.getRefreshToken(user.id, agent);
        return { accessToken, refreshToken };
    }

    private async sendVerificationCode(email: string): Promise<string> {
        const code = generateVerifyCode();
        await this.mailService.sendVerificationCode(email, code);
        return code;
    }

    private async getRefreshToken(user_id: number, agent: string): Promise<Token> {
        const _token = await this.prismaService.token.findFirst({
            where: {
                user_id: user_id,
                userAgent: agent,
            },
        });
        const token = _token?.token ?? '';
        return await this.prismaService.token.upsert({
            where: {
                token,
            },
            update: {
                token: v4(),
                exp: add(new Date(), { months: 1 }),
            },
            create: {
                token: v4(),
                exp: add(new Date(), { months: 1 }),
                user_id,
                userAgent: agent,
            },
        });
    }

    deleteRefreshToken(token: string) {
        return this.prismaService.token.delete({ where: { token } });
    }

    generateRandomPassword(length = 12) {
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+[]{}|;:,.<>?';
        let password = '';
        for (let i = 0; i < length; i++) {
            const randomIndex = Math.floor(Math.random() * characters.length);
            password += characters[randomIndex];
        }
        return password;
    }
}
