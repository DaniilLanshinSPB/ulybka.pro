import { NotVerifiedGuard } from '@/auth/guards/not_verified.guard';
import { PermissionsGuard } from '@/auth/guards/permission.guard';
import { JwtPayload, SystemPermissions } from '@/auth/interfaces';
import { LogsService } from '@/logs/logs.service';
import { CurrentUser, Permissions } from '@common/decorators';
import { getUserName } from '@common/helpers/user.helper';
import {
    BadRequestException,
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiExcludeController, ApiOperation, ApiParam, ApiResponse } from '@nestjs/swagger';
import { EntityType, LogType } from '@prisma/postgres/client';
import { DirectoryService } from './directory.service';
import { CreateDirectoryDto, UpdateDirectoryDto } from './dto';
import { DirectoryResponse } from './responses';

@ApiExcludeController()
@UseGuards(NotVerifiedGuard)
@UseGuards(PermissionsGuard)
@UseInterceptors(ClassSerializerInterceptor)
@Controller('directory')
export class DirectoryController {
    constructor(protected readonly directoryService: DirectoryService, protected readonly logsService: LogsService) {}

    @Get('all')
    @ApiOperation({ summary: 'Получить все словари' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о словорях', type: [DirectoryResponse] })
    async findAll() {
        const directories = await this.directoryService.findAll();
        return directories.map((directory) => new DirectoryResponse(directory));
    }

    @Get(':id')
    @Permissions(SystemPermissions.SYSTEM_VIEW_DIRECTORY)
    @ApiParam({ name: 'id', type: Number })
    @ApiOperation({ summary: 'Получить информацию о словаре по ID' })
    @ApiResponse({ status: 200, description: 'Успешное получение информации о словаре', type: DirectoryResponse })
    async findOne(@Param('id', ParseIntPipe) id: number) {
        const directory = await this.directoryService.findOne(id);
        if (!directory) {
            throw new BadRequestException('Словарь не найден');
        }
        return new DirectoryResponse(directory);
    }

    @Post()
    @ApiBody({ type: CreateDirectoryDto })
    @Permissions(SystemPermissions.SYSTEM_CREATE_DIRECTORY)
    @ApiOperation({ summary: 'Создание словаря' })
    @ApiResponse({ status: 200, description: 'Словарь успешно создана' })
    async createDirectory(@CurrentUser() currentUser: JwtPayload, @Body() dto: CreateDirectoryDto) {
        const directory = await this.directoryService.create(dto);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Directory,
            entity_id: directory.id.toString(),
            metadata: directory,
            details: `Пользователь ${getUserName(currentUser)} добавил новый словарь ${directory.title}`,
            type: LogType.SYSTEM,
        });
        return new DirectoryResponse(directory);
    }

    @Patch(':id')
    @Permissions(SystemPermissions.SYSTEM_UPDATE_DIRECTORY)
    @ApiParam({ name: 'id', type: Number })
    @ApiBody({ type: UpdateDirectoryDto })
    @ApiOperation({ summary: 'Обновление словаря' })
    @ApiResponse({ status: 200, description: 'Словарь успешно обновлен' })
    async updateDirectory(
        @CurrentUser() currentUser: JwtPayload,
        @Param('id', ParseIntPipe) id: number,
        @Body() dto: UpdateDirectoryDto,
    ) {
        const directory = await this.directoryService.update(id, dto);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Directory,
            entity_id: directory.id.toString(),
            metadata: directory,
            details: `Пользователь ${getUserName(currentUser)} изменил словарь ${directory.title}`,
            type: LogType.SYSTEM,
        });
        return new DirectoryResponse(directory);
    }

    @Delete(':id')
    @Permissions(SystemPermissions.SYSTEM_DELETE_DIRECTORY)
    @ApiParam({ name: 'id', type: Number })
    @ApiOperation({ summary: 'Удалить словарь по ID' })
    @ApiResponse({ status: 200, description: 'Словарь успешно удален' })
    @ApiResponse({ status: 400, description: 'Словарь не найден' })
    async deleteDirectory(@CurrentUser() currentUser: JwtPayload, @Param('id', ParseIntPipe) id: number) {
        const directory = await this.directoryService.findOne(id);
        await this.logsService.logAction({
            user_id: currentUser.id,
            entity_type: EntityType.Directory,
            entity_id: directory.id.toString(),
            metadata: directory,
            details: `Пользователь ${getUserName(currentUser)} удалил словарь ${directory.title}`,
            type: LogType.SYSTEM,
        });
        return this.directoryService.delete(id);
    }
}
