import { ApiProperty } from '@nestjs/swagger';
import { Directory } from '@prisma/postgres/client';
import { Transform } from 'class-transformer';

export class DirectoryResponse implements Directory {
    @ApiProperty({ description: 'Идентификатор' })
    id: number;

    @ApiProperty({ description: 'Название' })
    title: string;

    @ApiProperty({ description: 'Значения' })
    @Transform(({ value }) => JSON.parse(value))
    options: string[];

    constructor(directory: Directory) {
        Object.assign(this, directory);
    }
}
