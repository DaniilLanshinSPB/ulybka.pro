import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';

import { LogsModule } from '@/logs/logs.module';
import { DirectoryController } from './directory.controller';
import { DirectoryService } from './directory.service';

@Module({
    providers: [DirectoryService],
    exports: [DirectoryService],
    controllers: [DirectoryController],
    imports: [CacheModule.register(), LogsModule],
})
export class DirectoryModule {}
