import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Directory } from '@prisma/postgres/client';
import { PrismaService } from '@/database/postgres-prisma.service';
import { CreateDirectoryDto } from './dto';

@Injectable()
export class DirectoryService {
    protected readonly logger = new Logger(DirectoryService.name);

    constructor(protected readonly prismaService: PrismaService, protected readonly configService: ConfigService) {}

    async findAll(): Promise<Directory[]> {
        return await this.prismaService.directory.findMany();
    }
    async findOne(id: number): Promise<Directory> {
        const role = await this.prismaService.directory.findUnique({
            where: {
                id,
            },
        });
        if (!role) {
            return null;
        }
        return role;
    }

    async create(directory: CreateDirectoryDto) {
        const _directory = await this.prismaService.directory.findFirst({
            where: {
                title: directory.title,
            },
        });

        if (_directory) {
            throw new HttpException('Словарь с таким именем уже существует', HttpStatus.BAD_REQUEST);
        }

        try {
            const savedDirectory = await this.prismaService.directory.create({
                data: {
                    title: directory.title,
                    options: JSON.stringify(directory.options),
                },
            });

            return savedDirectory;
        } catch (err) {
            console.log(err);
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    async update(id: number, directory: Partial<Directory>) {
        const _duplicate_directory = await this.prismaService.directory.findFirst({
            where: {
                title: directory.title,
                id: {
                    not: Number(id),
                },
            },
        });

        if (_duplicate_directory) {
            throw new HttpException('Словарь с таким именем уже существует', HttpStatus.BAD_REQUEST);
        }

        const savedDirectory = await this.prismaService.directory.update({
            where: {
                id: Number(id),
            },
            data: {
                title: directory.title,
                options: JSON.stringify(directory.options),
            },
        });

        return savedDirectory;
    }

    async delete(id: number) {
        const _directory = await this.prismaService.directory.findUnique({
            where: {
                id: Number(id),
            },
        });
        if (!_directory) {
            throw new HttpException('Словарь не найден', HttpStatus.BAD_REQUEST);
        }
        try {
            return this.prismaService.directory.delete({ where: { id: Number(id) }, select: { id: true } });
        } catch (e) {
            throw new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
