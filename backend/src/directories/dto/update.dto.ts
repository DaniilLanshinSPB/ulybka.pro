import { ApiProperty } from '@nestjs/swagger';
import { IsJSON, IsString } from 'class-validator';

export class UpdateDirectoryDto {
    @IsString()
    @ApiProperty({ description: 'Название' })
    title: string;

    @IsJSON()
    @ApiProperty({ description: 'Значения' })
    options: string[];
}
