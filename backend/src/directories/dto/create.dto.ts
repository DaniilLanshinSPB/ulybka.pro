import { ApiProperty } from '@nestjs/swagger';
import { IsJSON, IsString } from 'class-validator';

export class CreateDirectoryDto {
    @IsString()
    @ApiProperty({ description: 'Название', type: String })
    title: string;

    @IsJSON()
    @ApiProperty({ description: 'Значения', type: [String] })
    options: string[];
}
