import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { NoticeController } from './notice.controller';
import { NoticeService } from './notice.service';
import { NotificationsGateway } from './notification.gateway';
import { TelegramBotProvider } from '@/telegram/telegram.provider';

@Module({
    controllers: [NoticeController],
    providers: [NoticeService, NotificationsGateway, JwtService, TelegramBotProvider],
    exports: [NoticeService],
    imports: [CacheModule.register()],
})
export class NoticeModule {}
