import { ApiProperty } from '@nestjs/swagger';
import { Notice } from '@prisma/postgres/client';
import { Exclude } from 'class-transformer';

export class NotificationResponse {
    @ApiProperty({ type: Number })
    id: number;
    @Exclude()
    user_id: number;
    @Exclude()
    type_id: number;
    @ApiProperty({ type: Boolean })
    is_read: boolean;
    @ApiProperty({ type: String })
    message: string;
    @ApiProperty({ type: Object })
    metadata: any;
    @ApiProperty({ type: Date })
    created_at: Date;

    constructor(params: Notice) {
        Object.assign(this, params);
    }
}
