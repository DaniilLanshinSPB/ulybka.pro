import { Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService, JwtVerifyOptions } from '@nestjs/jwt';
import {
    OnGatewayConnection,
    OnGatewayDisconnect,
    OnGatewayInit,
    WebSocketGateway,
    WebSocketServer,
} from '@nestjs/websockets';
import { Notice } from '@prisma/postgres/client';
import { Server, Socket } from 'socket.io';

@WebSocketGateway({ cors: { origin: '*' } })
export class NotificationsGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    private readonly logger = new Logger(NotificationsGateway.name);
    constructor(private readonly jwtService: JwtService, protected readonly configService: ConfigService) {}

    @WebSocketServer() io: Server;

    afterInit() {
        this.logger.log('[SOCKET] Initialized');
    }

    handleConnection(client: Socket, ...args: any[]) {
        const { sockets } = this.io.sockets;
        const token = client.handshake?.auth?.token.split(' ')[1];

        const option: JwtVerifyOptions = {
            secret: this.configService.get('JWT_SECRET'),
        };
        if (!token) {
            client.disconnect(true);
        }
        try {
            const payload = this.jwtService.verify(token, option);
            client.data.userId = payload.id;
            this.logger.debug(`Client ${client.id} connected with user ID ${payload.id}`);
        } catch (error) {
            console.log('disconnected');
            client.disconnect(true);
        }
        this.logger.debug(`Number of connected clients: ${sockets.size}`);
    }

    handleDisconnect(client: any) {
        this.logger.log(`Cliend id:${client.id} disconnected`);
    }

    sendNotificationToClient(userId: number, message: Partial<Notice>) {
        this.io.sockets.sockets.forEach((client) => {
            if (Number(client.data.userId) === Number(userId)) {
                client.emit('notifications', { message });
            }
        });
    }
}
