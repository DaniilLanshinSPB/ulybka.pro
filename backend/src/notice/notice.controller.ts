import { NotVerifiedGuard } from '@/auth/guards/not_verified.guard';
import { JwtPayload } from '@/auth/interfaces';
import { CurrentUser } from '@common/decorators';
import {
    ClassSerializerInterceptor,
    Controller,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Query,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { ApiExcludeController, ApiOperation, ApiParam, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { NoticeService } from './notice.service';
import { NotificationResponse } from './responses/notification.response';

@ApiExcludeController()
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(NotVerifiedGuard)
@Controller('notifications')
export class NoticeController {
    constructor(protected readonly noticeService: NoticeService) {}

    @Get('')
    @ApiOperation({ summary: 'Получить все уведомления' })
    @ApiQuery({ name: 'limit', type: Number })
    @ApiQuery({ name: 'offset', type: Number })
    @ApiResponse({ status: 200, description: 'Успешное получение уведомлений', type: [NotificationResponse] })
    async findAllNotifications(
        @CurrentUser() user: JwtPayload,
        @Query('limit', ParseIntPipe) limit: number,
        @Query('offset', ParseIntPipe) offset: number,
    ) {
        const notifications = await this.noticeService.findAll(user.id, limit, offset);
        return notifications.map((n) => new NotificationResponse(n));
    }
    @Patch('/:id')
    @ApiOperation({ summary: 'Пометить как прочитаное' })
    @ApiParam({ name: 'id', type: Number })
    @ApiResponse({ status: 200, description: 'Уведомление прочитано' })
    async readNotification(@CurrentUser() user: JwtPayload, @Param('id', ParseIntPipe) id: number) {
        await this.noticeService.read(id);
        return true;
    }
}
