import { Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { LogType, Notice, RoleNoticeType, UserNoticeType } from '@prisma/postgres/client';
import TelegramBot from 'node-telegram-bot-api';

import { PrismaService } from '@/database/postgres-prisma.service';
import { TELEGRAM_BOT } from '@/telegram/telegram.provider';
import * as dotenv from 'dotenv';
import { NotificationsGateway } from './notification.gateway';
import { CreateNotificationDto } from './types/create-notification.dto';
dotenv.config();
@Injectable()
export class NoticeService {
    protected readonly logger = new Logger(NoticeService.name);

    constructor(
        protected readonly prismaService: PrismaService,
        protected readonly configService: ConfigService,
        protected readonly notificationsGateway: NotificationsGateway,
        @Inject(TELEGRAM_BOT) private readonly bot: TelegramBot,
    ) {}

    getTypeBySlug(type: LogType) {
        return this.prismaService.noticeType.findFirst({ where: { slug: type } });
    }
    async findAccessTypes(): Promise<LogType[]> {
        const permissions = await this.prismaService.noticeType.findMany();
        return permissions.map((t) => t.slug as LogType);
    }
    async findAccessUserTypes(role_id: number): Promise<{ slug: LogType; active: boolean }[]> {
        const permissions = await this.findRoleNotificationPermissions(role_id);
        return permissions.filter((el) => el.active);
    }
    async createRoleNotificationPermissions(
        role_id: number,
        notification_permissions: { slug: LogType; active: boolean }[],
    ): Promise<
        {
            slug: LogType;
            active: boolean;
        }[]
    > {
        const allSlugs = notification_permissions.map((el) => el.slug);
        const activeSlugs = notification_permissions.filter((el) => el.active).map((el) => el.slug);

        const types = await this.prismaService.noticeType.findMany({ where: { slug: { in: allSlugs } } });

        const dtos: RoleNoticeType[] = [];
        types.forEach((type) => {
            dtos.push({
                role_id,
                notice_type_id: type.id,
                active: activeSlugs.includes(type.slug as LogType),
            });
        });
        await this.prismaService.roleNoticeType.createMany({ data: dtos });

        const users = await this.prismaService.user.findMany({ where: { role_id } });
        const userIds = users.map((el) => el.id);
        await this.prismaService.userNoticeType.deleteMany({ where: { user_id: { in: userIds } } });
        const userDtos: UserNoticeType[] = [];
        users.forEach((user) => {
            types.forEach((type) => {
                userDtos.push({
                    user_id: user.id,
                    notice_type_id: type.id,
                    active: activeSlugs.includes(type.slug as LogType),
                });
            });
        });
        await this.prismaService.userNoticeType.createMany({ data: userDtos });
        return this.findRoleNotificationPermissions(role_id);
    }
    async updateRoleNotificationPermissions(
        role_id: number,
        notification_permissions: { slug: LogType; active: boolean }[],
    ): Promise<{ slug: LogType; active: boolean }[]> {
        const activeSlugs = notification_permissions.filter((el) => el.active).map((el) => el.slug);
        const permissionsEntities = await this.prismaService.noticeType.findMany({
            where: { slug: { in: activeSlugs } },
        });
        const activePermissionsIds = permissionsEntities
            .filter((el) => activeSlugs.includes(el.slug as LogType))
            .map((el) => el.id);
        const deactivePermissionsIds = permissionsEntities
            .filter((el) => !activeSlugs.includes(el.slug as LogType))
            .map((el) => el.id);
        const roleNoticePermissions = await this.prismaService.roleNoticeType.findMany({ where: { role_id } });

        for (const np of roleNoticePermissions) {
            await this.prismaService.roleNoticeType.update({
                where: {
                    role_id_notice_type_id: { role_id: np.role_id, notice_type_id: np.notice_type_id },
                },
                data: { active: activePermissionsIds.includes(np.notice_type_id) },
            });
        }

        await this.prismaService.userNoticeType.deleteMany({
            where: {
                notice_type_id: { in: deactivePermissionsIds },
            },
        });
        const users = await this.prismaService.user.findMany({ where: { role_id } });

        for (const user of users) {
            for (const type_id of activePermissionsIds) {
                await this.prismaService.userNoticeType.upsert({
                    where: {
                        user_id_notice_type_id: {
                            user_id: user.id,
                            notice_type_id: type_id,
                        },
                    },
                    create: {
                        user_id: user.id,
                        notice_type_id: type_id,
                        active: false,
                    },
                    update: {
                        user_id: user.id,
                        notice_type_id: type_id,
                    },
                });
            }
        }

        return this.findRoleNotificationPermissions(role_id);
    }
    async findRoleNotificationPermissions(role_id: number): Promise<{ slug: LogType; active: boolean }[]> {
        const permissions = await this.prismaService.roleNoticeType.findMany({
            where: { role_id },
            include: { notice_type: true },
        });
        return permissions.map((el) => ({ slug: el.notice_type.slug as LogType, active: el.active }));
    }
    async updateUserNotificationPermissions(
        user_id: number,
        notification_permissions: { slug: LogType; active: boolean }[],
    ) {
        const activeSlugs = notification_permissions.filter((el) => el.active).map((el) => el.slug);

        const permissionsEntities = await this.prismaService.noticeType.findMany({
            where: { slug: { in: activeSlugs } },
        });
        const activePermissionsIds = permissionsEntities
            .filter((el) => activeSlugs.includes(el.slug as LogType))
            .map((el) => el.id);

        const userNotificationPermissions = await this.prismaService.userNoticeType.findMany({ where: { user_id } });

        for (const np of userNotificationPermissions) {
            await this.prismaService.userNoticeType.update({
                where: {
                    user_id_notice_type_id: { user_id: np.user_id, notice_type_id: np.notice_type_id },
                },
                data: { active: activePermissionsIds.includes(np.notice_type_id) },
            });
        }
    }
    async findUserNotificationPermissions(user_id: number): Promise<{ slug: LogType; active: boolean }[]> {
        const permissions = await this.prismaService.userNoticeType.findMany({
            where: { user_id },
            include: { notice_type: true },
        });
        return permissions.map((el) => ({
            slug: el.notice_type.slug as LogType,
            active: el.active,
        }));
    }
    async findUserIdsByActiveNotificationType(type: LogType): Promise<number[]> {
        const typeEntity = await this.prismaService.noticeType.findFirst({ where: { slug: type } });
        const permissions = await this.prismaService.userNoticeType.findMany({
            where: { notice_type_id: typeEntity.id, active: true },
        });
        return permissions.map((el) => el.user_id);
    }
    async findAll(user_id: number, limit?: number, offset?: number): Promise<Notice[]> {
        return this.prismaService.notice.findMany({
            where: { is_read: false, user_id },
            orderBy: { created_at: 'desc' },
            take: limit || 50,
            skip: offset || 0,
        });
    }
    async read(id: number): Promise<Notice> {
        return this.prismaService.notice.update({
            where: { id },
            data: { is_read: true },
        });
    }
    async create(data: CreateNotificationDto[]) {
        for (const dto of data) {
            const { isSendTelegram, telegram_id, ...item } = dto;
            const notification = await this.prismaService.notice.create({ data: item });
            await this.send(notification.user_id, notification);
            const CABINET_URL = process.env.CABINET_URL;

            const message = `ℹ️ ${notification.message}\n\nПерейдите в кабинет чтобы посмотреть\n🔗${CABINET_URL}`;
            await this.sendToTelegram(telegram_id, message);
        }
    }
    async send(userId: number, data: Notice) {
        this.notificationsGateway.sendNotificationToClient(userId, {
            id: data.id,
            is_read: data.is_read,
            message: data.message,
            metadata: data.metadata,
            created_at: data.created_at,
        });
    }
    async sendToTelegram(telegram_id: number, message: string) {
        this.bot.sendMessage(telegram_id, message, {
            parse_mode: 'HTML',
        });
    }
}
