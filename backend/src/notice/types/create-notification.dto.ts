export class CreateNotificationDto {
    user_id: number;
    type_id: number;
    message: string;
    metadata: any;
    isSendTelegram: boolean;
    telegram_id?: number;
}
