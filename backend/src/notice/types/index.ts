import { LogType } from '@prisma/postgres/client';

export const NOTICE_TYPES: Record<LogType, string> = {
    [LogType.COMMENT]: 'Комментарии',
    [LogType.ORDER]: 'Заказы',
    [LogType.PATIENT]: 'Пациенты',
    [LogType.SYSTEM]: 'Системные',
    [LogType.TELEGRAM]: 'Телеграм',
};
