/*!999999\- enable the sandbox mode */ 
-- MariaDB dump 10.19  Distrib 10.11.8-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: cmacom_lk
-- ------------------------------------------------------
-- Server version	10.11.8-MariaDB-ubu2204

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) NOT NULL,
  `type_id` int(11) NOT NULL,
  `metadata` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`metadata`)),
  PRIMARY KEY (`id`),
  KEY `companies_type_id_idx` (`type_id`),
  CONSTRAINT `companies_type_id_fkey` FOREIGN KEY (`type_id`) REFERENCES `company_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES
(1,'cmacom',1,'{}');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_types`
--

DROP TABLE IF EXISTS `company_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_types`
--

LOCK TABLES `company_types` WRITE;
/*!40000 ALTER TABLE `company_types` DISABLE KEYS */;
INSERT INTO `company_types` VALUES
(1,'Лаборатория'),
(2,'Клиника');
/*!40000 ALTER TABLE `company_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directories`
--

DROP TABLE IF EXISTS `directories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) NOT NULL,
  `options` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`options`)),
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Directory_company_id_fkey` (`company_id`),
  CONSTRAINT `Directory_company_id_unique` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directories`
--

LOCK TABLES `directories` WRITE;
/*!40000 ALTER TABLE `directories` DISABLE KEYS */;
INSERT INTO `directories` VALUES
(1,'Продукт для изготовления','\"[\\\"Винир\\\",\\\"Коронка\\\",\\\"Коронка на имплантате\\\",\\\"Промежуточная часть\\\",\\\"Восстановительная вкладка\\\",\\\"Абатмент\\\"]\"',1),
(2,'Виды прозрачности виниров','\"[\\\"HT\\\",\\\"LT\\\",\\\"MO\\\",\\\"MT\\\"]\"',1),
(3,'Цвет с BL','\"[\\\"BL1\\\",\\\"BL2\\\",\\\"BL3\\\",\\\"BL4\\\",\\\"A1\\\",\\\"A2\\\",\\\"A3\\\",\\\"A4\\\",\\\"B1\\\",\\\"B2\\\",\\\"B3\\\",\\\"B4\\\",\\\"C1\\\",\\\"C2\\\",\\\"C3\\\",\\\"C4\\\"]\"',1),
(4,'Цвет без BL','\"[\\\"A1\\\",\\\"A2\\\",\\\"A3\\\",\\\"A3.5\\\",\\\"A4\\\",\\\"B1\\\",\\\"B2\\\",\\\"B3\\\",\\\"B4\\\",\\\"C1\\\",\\\"C2\\\",\\\"C3\\\",\\\"C4\\\",\\\"D2\\\",\\\"D3\\\",\\\"D4\\\"]\"',1),
(5,'Метериалы вениров','\"[\\\"e-max\\\"]\"',1),
(6,'Материалы коронок','\"[\\\"ZrO2\\\",\\\"PMMA\\\",\\\"Graphy\\\"]\"',1),
(7,'Прозрачность коронок','\"[\\\"Mono\\\",\\\"Multi\\\"]\"',1),
(8,'Системы коронок на имплантанте','\"[\\\"Astra (OsseoSpeed TX)\\\",\\\"Dentium (Superline)\\\",\\\"Dentium (Implantium)\\\",\\\"MIS (7 (Seven)/M4)\\\",\\\"MIS (C1)\\\",\\\"Neodent (Helix GM)\\\",\\\"Neodent (Drive GM)\\\",\\\"Neodent (Titamax GM)\\\",\\\"Neodent (CM)\\\",\\\"Nobel (Active)\\\",\\\"Nobel (Parallel CC)\\\",\\\"Nobel (Replace)\\\",\\\"Osstem - SS (Regular)\\\",\\\"Osstem - SS (Wide)\\\",\\\"Osstem - TS-система (Mini)\\\",\\\"Osstem - TS-система (Regular)\\\",\\\"Osstem - TS-система (MS-система)\\\",\\\"Osstem (MS-система)\\\",\\\"Straumann (BLT)\\\",\\\"Straumann (BL)\\\",\\\"Straumann (TL)\\\",\\\"INNO\\\",\\\"Xive\\\",\\\"Alpha Bio\\\",\\\"Ankylos\\\",\\\"Neo Biotech\\\",\\\"Commet\\\",\\\"Другая система\\\"]\"',1),
(9,'Виды фиксации коронки','\"[\\\"Винтовая\\\",\\\"Цементная\\\"]\"',1),
(10,'Диаметры Astra (OsseoSpeed TX)','\"[\\\"3.5\\\",\\\"4.3\\\",\\\"5.0\\\"]\"',1),
(11,'Диаметры Dentium (Superline)','\"[\\\"3.6\\\",\\\"4.0\\\",\\\"4.5\\\",\\\"6.0\\\",\\\"7.0\\\"]\"',1),
(12,'Диаметры Dentium (Implantium)','\"[\\\"3.4\\\",\\\"3.8\\\",\\\"4.3\\\",\\\"4.8\\\"]\"',1),
(13,'Диаметры MIS (7 (Seven)/M4)','\"[\\\"3.75/4.2\\\",\\\"5.0/6.0\\\"]\"',1),
(14,'Диаметры MIS (C1)','\"[\\\"3.3\\\",\\\"3.75/4.2\\\",\\\"5.0\\\"]\"',1),
(15,'Диаметры Neodent (Helix GM)','\"[\\\"3.5\\\",\\\"3.75\\\",\\\"4.0\\\",\\\"4.3\\\",\\\"5.0\\\",\\\"5.0/6.0\\\",\\\"6.0\\\",\\\"7.0\\\"]\"',1),
(16,'Диаметры Neodent (Drive GM)','\"[\\\"3.5\\\",\\\"4.3\\\",\\\"5.0\\\"]\"',1),
(17,'Диаметры Neodent (Titamax GM)','\"[\\\"3.5\\\",\\\"3.75\\\",\\\"4.0\\\",\\\"5.0\\\"]\"',1),
(18,'Диаметры Nobel (Active)','\"[\\\"3.5\\\",\\\"4.3\\\",\\\"5.0\\\"]\"',1),
(19,'Диаметры Nobel (Parallel CC)','\"[\\\"3.75\\\",\\\"4.3\\\",\\\"5.0\\\"]\"',1),
(20,'Диаметры Nobel (Replace)','\"[\\\"3.5\\\",\\\"4.3\\\",\\\"5.0\\\"]\"',1),
(21,'Диаметры Osstem - SS (Regular)','\"[\\\"3.5\\\",\\\"4.0\\\",\\\"4.5\\\"]\"',1),
(22,'Диаметры Osstem - SS (Wide)','\"[\\\"5.0\\\",\\\"6.0\\\",\\\"7.0\\\"]\"',1),
(23,'Диаметры Osstem - TS-система (Mini)','\"[\\\"3.0\\\",\\\"3.5\\\"]\"',1),
(24,'Диаметры Osstem - TS-система (Regular)','\"[\\\"4.0\\\",\\\"4.5\\\",\\\"5.0\\\",\\\"5.5\\\",\\\"6.0\\\",\\\"7.0\\\"]\"',1),
(25,'Диаметры Osstem - TS-система (MS-система)','\"[\\\"1.8\\\",\\\"2.0\\\",\\\"2.5\\\",\\\"3.0\\\"]\"',1),
(26,'Диаметры Straumann (BLT)','\"[\\\"2.9 (SC)\\\",\\\"3.3 (NC)\\\",\\\"4.1 (RC)\\\",\\\"4.8 (RC)\\\"]\"',1),
(27,'Диаметры Straumann (BL)','\"[\\\"3.3\\\",\\\"4.1\\\",\\\"4.8\\\"]\"',1),
(28,'Диаметры Straumann (TL)','\"[\\\"3.3\\\",\\\"4.1\\\",\\\"4.8 (RN (4.8))\\\",\\\"4.8 (WN (6.5))\\\"]\"',1),
(29,'Диаметры INNO','\"[\\\"3.5\\\",\\\"4.0\\\",\\\"4.5\\\",\\\"5.0\\\",\\\"6.0\\\"]\"',1),
(30,'Диаметры Xive','\"[\\\"3.5\\\",\\\"4.0\\\",\\\"4.5\\\",\\\"5.0\\\",\\\"6.0\\\"]\"',1),
(31,'Материал промежуточной части','\"[\\\"ZrO2\\\",\\\"PMMA\\\",\\\"WAX\\\",\\\"Graphy\\\"]\"',1),
(32,'Материал восстановительной вкладки','\"[\\\"ZrO2\\\",\\\"e-max\\\",\\\"Graphy\\\"]\"',1),
(33,'Прозрачность (MONO/MULTI)','\"[\\\"Mono\\\",\\\"Multi\\\"]\"',1),
(34,'Прозрачность (HT, LT, MO, MT)','\"[\\\"HT\\\",\\\"LT\\\",\\\"MO\\\",\\\"MT\\\"]\"',1),
(35,'Материал абатмента','\"[\\\"ZrO2\\\",\\\"PEEK\\\"]\"',1),
(36,'Виды моделирования','\"[\\\"Mock-up\\\",\\\"Сплинт\\\"]\"',1),
(37,'Продукт для фрезеровки','\"[\\\"ZrO2\\\",\\\"PMMA\\\",\\\"WAX\\\"]\"',1),
(38,'Фрезеровка, материал восстановительной вкладки','\"[\\\"ZrO2\\\",\\\"e-max\\\"]\"',1),
(39,'Фрезеровка, продукт для изготовления','\"[\\\"Коронка\\\",\\\"Восстановительная вкладка\\\",\\\"Абатмент\\\",\\\"Винир\\\",\\\"Промежуточная часть\\\"]\"',1),
(40,'Виды жалоб','\"[\\\"Неровное положение зубов\\\",\\\"Скрежетание зубами\\\",\\\"Боли со стороны ВНЧС\\\",\\\"Подготовка к протезированию\\\"]\"',1),
(41,'Да/нет','\"[\\\"Да\\\",\\\"Нет\\\"]\"',1),
(42,'Способы расширения','\"[\\\"Корпусное перемещение\\\",\\\"Торк\\\"]\"',1),
(43,'Менять/Не менять','\"[\\\"Менять\\\",\\\"Не менять\\\"]\"',1),
(44,'Способы изменения плоскости','\"[\\\"интрузии без использования минивинта\\\",\\\"интрузии с использованием минивинта\\\",\\\"экструзии\\\"]\"',1),
(45,'Верхняя/Нижняя челюсть','\"[\\\"На верхней челюсти\\\",\\\"На нижней челюсти\\\"]\"',1),
(46,'По режущему/десновому краю','\"[\\\"По режущему краю\\\",\\\"По десневому краю\\\"]\"',1),
(47,'За счет чего менять соотношение','\"[\\\"верхних зубов\\\",\\\"нижних зубов\\\",\\\"верхних и нижних зубов\\\"]\"',1),
(48,'Классы','\"[\\\"1 класс\\\",\\\"2 класс\\\",\\\"3 класс\\\"]\"',1),
(49,'Способы замены соотношения зубных рядов','\"[\\\"дистализации\\\",\\\"мезиализации\\\",\\\"встречной дистализации и мезиализации\\\",\\\"изменения положения нижней челюсти (Подготовительный этап с использованием диагностического сплинта)\\\"]\"',1),
(50,'Виды зубных рядов','\"[\\\"верхнего зубного ряда\\\",\\\"нижнего зубного ряда\\\"]\"',1),
(51,'Виды минивинтов','\"[\\\"Межкорневых\\\",\\\"BSH / IZC\\\",\\\"На усмотрение лаборатории\\\"]\"',1);
/*!40000 ALTER TABLE `directories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fields`
--

DROP TABLE IF EXISTS `fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `multiple` tinyint(1) NOT NULL,
  `type` enum('INPUT','IMAGE','TOOTH','FILE','COLOR','OPTION','TITLE','DELIMITER','TAB') NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `required_for_own` tinyint(1) NOT NULL DEFAULT 0,
  `directory_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `display_conditions` varchar(191) DEFAULT NULL,
  `metadata` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`metadata`)),
  `company_id` int(11) NOT NULL,
  `technical_name` varchar(191) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Field_company_id_fkey` (`company_id`),
  KEY `fields_directory_id_idx` (`directory_id`),
  KEY `fields_parent_id_idx` (`parent_id`),
  CONSTRAINT `Field_company_id_unique` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fields_directory_id_fkey` FOREIGN KEY (`directory_id`) REFERENCES `directories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fields_parent_id_fkey` FOREIGN KEY (`parent_id`) REFERENCES `fields` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fields`
--

LOCK TABLES `fields` WRITE;
/*!40000 ALTER TABLE `fields` DISABLE KEYS */;
INSERT INTO `fields` VALUES
(1,'Выберите зубы',1,'TOOTH',0,0,NULL,NULL,10,NULL,'{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Выберите зубы'),
(2,'Что нужно изготовить',0,'OPTION',0,0,1,1,1001,NULL,'{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Что нужно изготовить'),
(3,'Вид моделирования',0,'OPTION',0,0,36,NULL,8000,NULL,'{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Моделирование) Вид моделирования'),
(4,'Выберите зубы для моделироания',1,'TOOTH',0,0,NULL,NULL,9000,NULL,'{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Моделирование) Выберите зубы для моделироания'),
(5,'Файл скан для фрезировки',0,'FILE',0,0,NULL,NULL,7000,NULL,'{\"extensions\":\".stl\",\"class\":\"col-12\"}',1,'(Фрезеровка) Файл скан для фрезировки'),
(6,'Выберите материал',0,'OPTION',0,0,5,2,1002,'Винир','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Выберите материал'),
(7,'Прозрачность виниров',0,'OPTION',0,0,34,2,1003,'Винир','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Прозрачность виниров'),
(8,'Цвет виниров',0,'COLOR',0,0,3,2,1004,'Винир','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Цвет виниров'),
(9,'Материал коронки',0,'OPTION',0,0,6,2,2000,'Коронка','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Материал коронки'),
(10,'Цвет ZrO2',0,'COLOR',0,0,3,9,2002,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Цвет ZrO2'),
(11,'Прозрачность ZrO2',0,'OPTION',0,0,33,9,2001,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Прозрачность ZrO2'),
(12,'Прозрачность PMMA',0,'OPTION',0,0,33,9,2003,'PMMA','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Прозрачность PMMA'),
(13,'Цвет PMMA',0,'COLOR',0,0,4,9,2004,'PMMA','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Цвет PMMA'),
(14,'Цвет Graphy',0,'COLOR',0,0,4,9,2005,'Graphy','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Цвет Graphy'),
(15,'Система коронки на имплантанте',0,'OPTION',0,0,8,2,3000,'Коронка на имплантате','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Система коронки на имплантанте'),
(16,'Диаметр Astra (OsseoSpeed TX)',0,'OPTION',0,0,10,15,3001,'Astra (OsseoSpeed TX)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Astra (OsseoSpeed TX)'),
(17,'Виды фиксации',0,'OPTION',0,0,9,2,3001,'Коронка на имплантате','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Виды фиксации'),
(18,'Диаметр Dentium (Superline)',0,'OPTION',0,0,11,15,3002,'Dentium (Superline)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Dentium (Superline)'),
(19,'Диаметр Dentium (Implantium)',0,'OPTION',0,0,12,15,3003,'Dentium (Implantium)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Dentium (Implantium)'),
(20,'Диаметр MIS (7 (Seven)/M4)',0,'OPTION',0,0,13,15,3004,'MIS (7 (Seven)/M4)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр MIS (7 (Seven)/M4)'),
(21,'Диаметр MIS (C1)',0,'OPTION',0,0,14,15,3005,'MIS (C1)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр MIS (C1)'),
(22,'Диаметр Neodent (Helix GM)',0,'OPTION',0,0,15,15,3006,'Neodent (Helix GM)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Neodent (Helix GM)'),
(23,'Диаметр Neodent (Drive GM)',0,'OPTION',0,0,16,15,3007,'Neodent (Drive GM)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Neodent (Drive GM)'),
(24,'Диаметр Neodent (Titamax GM)',0,'OPTION',0,0,17,15,3008,'Neodent (Titamax GM)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Neodent (Titamax GM)'),
(25,'Диаметр Nobel (Active)',0,'OPTION',0,0,18,15,3009,'Nobel (Active)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Nobel (Active)'),
(26,'Диаметр Neodent(CM)',0,'INPUT',0,0,NULL,15,3010,'Neodent (CM)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Neodent(CM)'),
(27,'Диаметр Nobel (Parallel CC)',0,'OPTION',0,0,19,15,3012,'Nobel (Parallel CC)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Nobel (Parallel CC)'),
(28,'Диаметр Nobel (Replace)',0,'OPTION',0,0,20,15,3013,'Nobel (Replace)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Nobel (Replace)'),
(29,'Диаметр Osstem - SS (Regular)',0,'OPTION',0,0,21,15,3014,'Osstem - SS (Regular)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Osstem - SS (Regular)'),
(30,'Диаметр Osstem - SS (Wide)',0,'OPTION',0,0,22,15,3015,'Osstem - SS (Wide)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Osstem - SS (Wide)'),
(31,'Диаметр Osstem - TS-система (Mini)',0,'OPTION',0,0,23,15,3016,'Osstem - TS-система (Mini)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Osstem - TS-система (Mini)'),
(32,'Диаметр Osstem - TS-система (Regular)',0,'OPTION',0,0,24,15,3017,'Osstem - SS (Regular)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Osstem - TS-система (Regular)'),
(33,'Диаметр Osstem - TS-система (MS-система)',0,'OPTION',0,0,25,15,3018,'Osstem - TS-система (MS-система)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Osstem - TS-система (MS-система)'),
(34,'Диаметр Osstem (MS-система)',0,'OPTION',0,0,25,15,3019,'Osstem (MS-система)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Osstem (MS-система)'),
(35,'Диаметр Straumann (BLT)',0,'OPTION',0,0,26,15,3020,'Straumann (BLT)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Straumann (BLT)'),
(36,'Диаметр Straumann (BL)',0,'OPTION',0,0,27,15,3021,'Straumann (BL)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Straumann (BL)'),
(37,'Диаметр Straumann (TL)',0,'OPTION',0,0,28,15,3022,'Straumann (TL)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Straumann (TL)'),
(38,'Диаметр INNO',0,'OPTION',0,0,29,15,3023,'INNO','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр INNO'),
(39,'Диаметр Xive',0,'OPTION',0,0,30,15,3024,'Xive','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Xive'),
(40,'Диаметр Alpha Bio',0,'INPUT',0,0,NULL,15,3025,'Alpha Bio','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Alpha Bio'),
(41,'Диаметр Ankylos',0,'INPUT',0,0,NULL,15,3026,'Ankylos','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Ankylos'),
(42,'Диаметр Neo Biotech',0,'INPUT',0,0,NULL,15,3027,'Neo Biotech','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Neo Biotech'),
(43,'Диаметр Commet',0,'INPUT',0,0,NULL,15,3028,'Commet','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Commet'),
(44,'Диаметр системы',0,'INPUT',0,0,NULL,15,3029,'Другая система','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр системы'),
(45,'Материал промежуточной части',0,'OPTION',0,0,31,2,4000,'Промежуточная часть','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Материал промежуточной части'),
(46,'Прозрачность материала ZrO2',0,'OPTION',0,0,33,45,4001,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Прозрачность материала ZrO2'),
(47,'Цвет материала ZrO2',0,'COLOR',0,0,3,45,4002,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'Цвет материала ZrO2'),
(48,'Прозрачность материала PMMA',0,'OPTION',0,0,33,45,4003,'PMMA','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Прозрачность материала PMMA'),
(49,'Цвет материала PMMA',0,'COLOR',0,0,4,45,4004,'PMMA','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Цвет материала PMMA'),
(50,'Цвет материала Graphy',0,'COLOR',0,0,4,45,4005,'Graphy','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Цвет материала Graphy'),
(51,'Материал восстановительной вкладки',0,'OPTION',0,0,32,2,5000,'Восстановительная вкладка','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Материал восстановительной вкладки'),
(52,'Прозрачность вкладки ZrO2',0,'OPTION',0,0,33,51,5001,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Прозрачность вкладки ZrO2'),
(53,'Цвет вкладки ZrO2',0,'COLOR',0,0,3,51,5002,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Цвет вкладки ZrO2'),
(54,'Прозрачность вкладки e-max',0,'OPTION',0,0,34,51,5003,'e-max','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Прозрачность вкладки e-max'),
(55,'Цвет вкладки e-max',0,'COLOR',0,0,3,51,5004,'e-max','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Цвет вкладки e-max'),
(56,'Цвет вкладки Graphy',0,'COLOR',0,0,3,51,5005,'Graphy','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Цвет вкладки Graphy'),
(57,'Материал абатмента',0,'OPTION',0,0,35,2,6000,'Абатмент','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Материал абатмента'),
(58,'Прозрачность материала абатмента ZrO2',0,'OPTION',0,0,33,59,6002,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Прозрачность материала абатмента ZrO2'),
(59,'Система абатмента',0,'OPTION',0,0,8,2,6001,'Абатмент','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Система абатмента'),
(60,'Анфас',0,'IMAGE',1,1,NULL,NULL,9002,NULL,'{\"extensions\":\".jpg, .png, .jpeg\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Анфас'),
(61,'Анфас с улыбкой',0,'IMAGE',0,0,NULL,NULL,9003,NULL,'{\"extensions\":\".jpg, .png, .jpeg\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Анфас с улыбкой'),
(62,'Анфас с «Эмма»',0,'IMAGE',0,0,NULL,NULL,9004,NULL,'{\"extensions\":\".jpg, .png, .jpeg\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Анфас с «Эмма»'),
(63,'Анфас с ретрактором',0,'IMAGE',0,0,NULL,NULL,9005,NULL,'{\"extensions\":\".jpg, .png, .jpeg\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Анфас с ретрактором'),
(64,'Профиль (90°)',0,'IMAGE',0,0,NULL,NULL,9006,NULL,'{\"extensions\":\".jpg, .png, .jpeg\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Профиль (90°)'),
(65,'Профиль (90°) с улыбкой',0,'IMAGE',0,0,NULL,NULL,9007,NULL,'{\"extensions\":\".jpg, .png, .jpeg\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Профиль (90°) с улыбкой'),
(66,'Профиль (45°)',0,'IMAGE',0,0,NULL,NULL,9008,NULL,'{\"extensions\":\".jpg, .png, .jpeg\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Профиль (45°)'),
(67,'Профиль (45°) с улыбкой',0,'IMAGE',0,0,NULL,NULL,9009,NULL,'{\"extensions\":\".jpg, .png, .jpeg\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Профиль (45°) с улыбкой'),
(68,'Фронтальный вид (зубные ряды, окклюзия)',0,'IMAGE',0,0,NULL,NULL,9010,NULL,'{\"extensions\":\".jpg, .png, .jpeg\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Фронтальный вид (зубные ряды, окклюзия)'),
(69,'Фронтальный вид с приоткрыты ртом',0,'IMAGE',0,0,NULL,NULL,9011,NULL,'{\"extensions\":\".jpg, .png, .jpeg\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Фронтальный вид с приоткрыты ртом'),
(70,'Вид слева (зубные ряды, окклюзия)',0,'IMAGE',0,0,NULL,NULL,9012,NULL,'{\"extensions\":\".jpg, .png, .jpeg\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Вид слева (зубные ряды, окклюзия)'),
(71,'Вид справа (зубные ряды, окклюзия)',0,'IMAGE',0,0,NULL,NULL,9013,NULL,'{\"extensions\":\".jpg, .png, .jpeg\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Вид справа (зубные ряды, окклюзия)'),
(72,'Файл STL «Верхняя челюсть»',0,'FILE',0,0,NULL,NULL,9015,NULL,'{\"extensions\":\".stl\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Файл STL «Верхняя челюсть»'),
(73,'Файл STL «Нижняя челюсть»',0,'FILE',0,0,NULL,NULL,9016,NULL,'{\"extensions\":\".stl\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Файл STL «Нижняя челюсть»'),
(74,'Файл STL «Окклюзия справа»',0,'FILE',0,0,NULL,NULL,9017,NULL,'{\"extensions\":\".stl\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Файл STL «Окклюзия справа»'),
(75,'Файл STL «Окклюзия слева»',0,'FILE',0,0,NULL,NULL,9018,NULL,'{\"extensions\":\".stl\",\"class\":\"col-12 md:col-4\"}',1,'(Ортодонтия) Файл STL «Окклюзия слева»'),
(76,'Файл томограммы',0,'FILE',0,0,NULL,NULL,9020,NULL,'{\"extensions\":\".jpg, .png, .jpeg, .stl\",\"class\":\"col-12\"}',1,'(Ортодонтия) Файл томограммы'),
(77,'Фото',0,'TITLE',0,0,NULL,NULL,9001,NULL,'{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Ортодонтия) Фото'),
(78,'Файлы сканирования',0,'TITLE',0,0,NULL,NULL,9014,NULL,'{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Ортодонтия) Файлы сканирования'),
(79,'Компьютерная томограмма',0,'TITLE',0,0,NULL,NULL,9019,NULL,'{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Ортодонтия) Компьютерная томограмма'),
(80,'Диаметр Astra (OsseoSpeed TX)',0,'OPTION',0,0,10,59,6002,'Astra (OsseoSpeed TX)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'Диаметр Astra (OsseoSpeed TX)'),
(81,'(м+ф) Виды фиксации',0,'OPTION',0,0,9,2,1002,'Коронка на имплантате','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'Виды фиксации'),
(82,'Диаметр Dentium (Superline)',0,'OPTION',0,0,11,59,6002,'Dentium (Superline)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Dentium (Superline)'),
(83,'Диаметр Dentium (Implantium)',0,'OPTION',0,0,12,59,6002,'Dentium (Implantium)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Dentium (Implantium)'),
(84,'Диаметр MIS (7 (Seven)/M4)',0,'OPTION',0,0,13,59,6002,'MIS (7 (Seven)/M4)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр MIS (7 (Seven)/M4)'),
(85,'Диаметр MIS (C1)',0,'OPTION',0,0,14,59,6002,'MIS (C1)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр MIS (C1)'),
(86,'Диаметр Neodent (Helix GM)',0,'OPTION',0,0,15,59,6002,'Neodent (Helix GM)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Neodent (Helix GM)'),
(87,'Диаметр Neodent (Drive GM)',0,'OPTION',0,0,16,59,6002,'Neodent (Drive GM)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Neodent (Drive GM)'),
(88,'Диаметр Neodent (Titamax GM)',0,'OPTION',0,0,17,59,6002,'Neodent (Titamax GM)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Neodent (Titamax GM)'),
(89,'Диаметр Nobel (Active)',0,'OPTION',0,0,18,59,6002,'Nobel (Active)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Nobel (Active)'),
(90,'Диаметр Neodent(CM)',0,'INPUT',0,0,NULL,15,3011,'Neodent (CM)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Neodent(CM)'),
(91,'Диаметр Nobel (Parallel CC)',0,'OPTION',0,0,19,59,6002,'Nobel (Parallel CC)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Nobel (Parallel CC)'),
(92,'Диаметр Nobel (Replace)',0,'OPTION',0,0,20,59,6002,'Nobel (Replace)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Nobel (Replace)'),
(93,'Диаметр Osstem - SS (Regular)',0,'OPTION',0,0,21,59,6002,'Osstem - SS (Regular)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Osstem - SS (Regular)'),
(94,'Диаметр Osstem - SS (Wide)',0,'OPTION',0,0,22,59,6002,'Osstem - SS (Wide)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Osstem - SS (Wide)'),
(95,'Диаметр Osstem - TS-система (Mini)',0,'OPTION',0,0,23,59,6002,'Osstem - TS-система (Mini)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Osstem - TS-система (Mini)'),
(96,'Диаметр Osstem - TS-система (Regular)',0,'OPTION',0,0,24,59,6002,'Osstem - SS (Regular)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Osstem - TS-система (Regular)'),
(97,'Диаметр Osstem - TS-система (MS-система)',0,'OPTION',0,0,25,59,6002,'Osstem - TS-система (MS-система)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Osstem - TS-система (MS-система)'),
(98,'Диаметр Osstem (MS-система)',0,'OPTION',0,0,25,59,6002,'Osstem (MS-система)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Osstem (MS-система)'),
(99,'Диаметр Straumann (BLT)',0,'OPTION',0,0,26,59,6002,'Straumann (BLT)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Straumann (BLT)'),
(100,'Диаметр Straumann (BL)',0,'OPTION',0,0,27,59,6002,'Straumann (BL)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Straumann (BL)'),
(101,'Диаметр Straumann (TL)',0,'OPTION',0,0,28,59,6002,'Straumann (TL)','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Straumann (TL)'),
(102,'Диаметр INNO',0,'OPTION',0,0,29,59,6002,'INNO','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр INNO'),
(103,'Диаметр Xive',0,'OPTION',0,0,30,59,6002,'Xive','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Xive'),
(104,'Диаметр Alpha Bio',0,'INPUT',0,0,NULL,59,6002,'Alpha Bio','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Alpha Bio'),
(105,'Диаметр Ankylos',0,'INPUT',0,0,NULL,59,6002,'Ankylos','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Ankylos'),
(106,'Диаметр Neo Biotech',0,'INPUT',0,0,NULL,59,6002,'Neo Biotech','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Neo Biotech'),
(107,'Диаметр Commet',0,'INPUT',0,0,NULL,59,6002,'Commet','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр Commet'),
(108,'Диаметр системы',0,'INPUT',0,0,NULL,59,6002,'Другая система','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(м+ф) Диаметр системы'),
(109,'Файл верхней челюсти',0,'FILE',0,0,NULL,NULL,10000,NULL,'{\"extensions\":\".stl\",\"class\":\"col-12 md:col-4\"}',1,'Файл верхней челюсти'),
(110,'Файл нижней челюсти',0,'FILE',0,0,NULL,NULL,10001,NULL,'{\"extensions\":\".stl\",\"class\":\"col-12 md:col-4\"}',1,'Файл нижней челюсти'),
(111,'Файлы прикуса',0,'FILE',0,0,NULL,NULL,10003,NULL,'{\"extensions\":\".stl\",\"class\":\"col-12 md:col-4\"}',1,'Файлы прикуса'),
(112,'Иной файл',0,'FILE',0,0,NULL,NULL,10005,NULL,'{\"extensions\":\".stl\",\"class\":\"col-12\"}',1,'Иной файл'),
(122,'(Фрезировка) Что нужно изготовить',0,'OPTION',0,0,39,NULL,7010,NULL,'{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Фрезировка) Что нужно изготовить'),
(123,'Выберите материал',0,'OPTION',0,0,37,122,7020,'Коронка','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Коронка) Выберите материал'),
(124,'Выберите прозрачность',0,'OPTION',0,0,33,123,7030,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'Прозрачность ZrO2'),
(125,'Выберите прозрачность',0,'OPTION',0,0,33,123,7050,'PMMA','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'Прозрачность PMMA'),
(126,'Выберите цвет',0,'OPTION',0,0,3,123,7040,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'Цвет ZrO2'),
(127,'Выберите цвет',0,'OPTION',0,0,4,123,7060,'PMMA','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'Цвет PMMA'),
(128,'Выберите прозрачность',0,'OPTION',0,0,34,122,7100,'Винир','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Виниры) Прозрачность виниров'),
(129,'Выберите цвет',0,'OPTION',0,0,3,122,7110,'Винир','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Виниры) Цвет виниров'),
(130,'Выберите материал',0,'OPTION',0,0,38,122,7200,'Восстановительная вкладка','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Восстановительная вкладка) Выберите материал'),
(131,'Выберите прозрачность',0,'OPTION',0,0,33,130,7210,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Фрезеровка) Выберите прозрачность'),
(132,'Выберите цвет',0,'OPTION',0,0,3,130,7220,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Фрезеровка) Выберите цвет'),
(133,'Выберите прозрачность',0,'OPTION',0,0,34,130,7230,'e-max','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Фрезеровка) Выберите прозрачность'),
(134,'Выберите цвет',0,'OPTION',0,0,3,130,7240,'e-max','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Фрезеровка) Выберите цвет'),
(135,'Выберите материал',0,'OPTION',0,0,35,122,7300,'Абатмент','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Абатмент) Выберите материал'),
(136,'Выберите прозрачность',0,'OPTION',0,0,33,135,7310,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'Прозрачность ZrO2'),
(137,'Выберите цвет',0,'OPTION',0,0,3,135,7320,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'Цвет ZrO2'),
(138,'Выберите материал',0,'OPTION',0,0,31,122,7400,'Промежуточная часть','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Промежуточная часть) Выберите материал'),
(139,'Выберите прозрачность',0,'OPTION',0,0,33,138,7410,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'Прозрачность ZrO2'),
(140,'Выберите цвет',0,'OPTION',0,0,3,138,7420,'ZrO2','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'Цвет ZrO2'),
(141,'Выберите прозрачность',0,'OPTION',0,0,33,138,7430,'PMMA','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'Прозрачность PMMA'),
(142,'Выберите цвет',0,'OPTION',0,0,4,138,7440,'PMMA','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'Цвет PMMA'),
(143,'Выберите цвет',0,'OPTION',0,0,4,138,7450,'Graphy','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'Цвет Graphy'),
(144,'Заполнить опросник',0,'OPTION',0,0,41,NULL,10100,NULL,'{\"extensions\":\"\",\"class\":\"\"}',1,'Заполнить опросник'),
(145,'Жалобы пациента',1,'OPTION',0,0,40,144,10110,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Жалобы'),
(146,'Зубная формула',1,'TOOTH',0,0,NULL,144,10120,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Зубная формула'),
(147,'Планируется удаление зубов?',1,'TOOTH',0,0,NULL,144,10130,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Удаляемые зубы'),
(148,'Планируемое протезирование',1,'TOOTH',0,0,NULL,144,10140,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Планируемое протезирование'),
(149,'Опросник',0,'TITLE',0,0,NULL,144,10105,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Заголовок) Опросник'),
(150,'План лечения',0,'TITLE',0,0,NULL,144,10200,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Заголовок) План лечения'),
(151,'Трасверзальная плоскость',0,'TITLE',0,0,NULL,144,10210,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Заголовок) Трасверзальная плоскость'),
(152,'Задачи на верхней челюсти',0,'TITLE',0,0,NULL,144,10220,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Заголовок) Задачи на верхней челюсти'),
(153,'Задачи на нижней челюсти',0,'TITLE',0,0,NULL,144,10260,'Да','{\"extensions\":\"\",\"class\":\"col-12 \"}',1,'(Заголовок) Задачи на нижней челюсти'),
(154,'Требуется расширение',0,'OPTION',0,0,41,144,10230,'Да','{\"extensions\":\"\",\"class\":\"col-12 md:col-4\"}',1,'(Опросник) Требуется расширение верхней челюсти'),
(155,'За счёт чего?',1,'OPTION',0,0,42,154,10240,'Да','{\"extensions\":\"\",\"class\":\"col-12 md:col-6\"}',1,'(Опросник) Способ расширения верхней челюсти'),
(156,'Требуется расширение',0,'OPTION',0,0,41,144,10270,'Да','{\"extensions\":\"\",\"class\":\"col-12 md:col-4\"}',1,'(Опросник) Требуется расширение нижней челюсти'),
(157,'За счет чего?',1,'OPTION',0,0,42,156,10280,'Да','{\"extensions\":\"\",\"class\":\"col-12 md:col-6\"}',1,'(Опросник) Способ расширения нижней челюсти'),
(158,'Вертикальная плоскость',0,'TITLE',0,0,NULL,144,10300,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Заголовок) Вертикальная плоскость'),
(159,'Соотношение резцов',0,'OPTION',0,0,43,144,10310,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Соотношение резцов'),
(160,'За счет чего?',0,'OPTION',0,0,47,159,10320,'Менять','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) За счет чего меняется соотношение зубов'),
(161,'Метод замены',0,'OPTION',0,0,44,160,10330,'верхних зубов','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Метод замены соотношения верхних резцов'),
(162,'Метод замены',0,'OPTION',0,0,44,160,10340,'нижних зубов','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Метод замены соотношения нижних резцов'),
(163,'Выравнивание резцов',0,'OPTION',0,0,45,144,10350,'Да','{\"extensions\":\"\",\"class\":\"col-4 md:col-12\"}',1,'(Опросник) Выравнивание резцов'),
(164,'Выберите край',0,'OPTION',0,0,46,144,10360,'Да','{\"extensions\":\"\",\"class\":\"col-4 md:col-12\"}',1,'(Опросник) Край выравнивания резцов'),
(165,'Сагиттальная плоскость',0,'TITLE',0,0,NULL,144,10370,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Заголовок) Сагиттальная плоскость'),
(166,'Соотношение зубных рядов в конце лечения:',0,'TITLE',0,0,NULL,144,10380,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Заголовок) Соотношение зубных рядов в конце лечения:'),
(167,'Справа',0,'OPTION',0,0,48,144,10390,'Да','{\"extensions\":\"\",\"class\":\"col-12 md:col-6\"}',1,'(Опросник) Соотношение зубных рядов справа'),
(168,'Слева',0,'OPTION',0,0,48,144,10400,'Да','{\"extensions\":\"\",\"class\":\"col-12 md:col-6\"}',1,'(Опросник) Соотношение зубных рядов слева'),
(169,'Соотношение зубных рядов',0,'OPTION',0,0,43,144,10410,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Соотношение зубных рядов'),
(170,'За счет',0,'OPTION',0,0,49,169,10420,'Менять','{\"extensions\":\"\",\"class\":\"\"}',1,'(Опросник) Способы замены соотношения зубных рядов'),
(171,'Выберите ряд',0,'OPTION',0,0,50,170,10430,'дистализации','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Выбор ряда для дистализации'),
(172,'За счет чего?',0,'OPTION',0,0,50,170,10440,'мезиализации','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Выбор ряда для менизализации'),
(173,'Допускается использование минивинтов?',0,'OPTION',0,0,41,169,10460,'Менять','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Допускается использование минивинтов?'),
(174,'Выберите вид минивинтов',0,'OPTION',0,0,51,173,10470,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Вид минивинтов'),
(175,'Выравнивание окклюзионной плоскости',0,'OPTION',0,0,45,144,10500,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Выравнивание окклюзионной плоскости'),
(176,'Соотношение межрезцовых линий',0,'TITLE',0,0,NULL,144,10510,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Соотношение межрезцовых линий'),
(177,'Межрезцовая линия верхнего зубного ряда совпадает с межрезцовой линией нижнего зубного ряда?',0,'OPTION',0,0,41,144,10600,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Межрезцовая линия верхнего зубного ряда совпадает с межрезцовой линией нижнего зубного ряда?'),
(178,'Межрезцовая линия верхнего зубного ряда совпадает с центром лица?',0,'OPTION',0,0,41,144,10620,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Межрезцовая линия верхнего зубного ряда совпадает с центром лица?'),
(179,'Межрезцовая линия нижнего зубного ряда совпадает с центром лица?',0,'OPTION',0,0,41,144,10680,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Межрезцовая линия нижнего зубного ряда совпадает с центром лица?'),
(180,'Выравнивание центральной линии',0,'TITLE',0,0,NULL,144,10800,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Заголовок) Выравнивание центральной линии'),
(181,'Верхняя центральная линия',0,'OPTION',0,0,43,144,10810,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Выравнивание верхней центральной линии'),
(182,'Нижняя центральная линия',0,'OPTION',0,0,43,144,10820,'Да','{\"extensions\":\"\",\"class\":\"col-12\"}',1,'(Опросник) Выравнивание нижней центральной линии'),
(183,'На (мм)',0,'INPUT',0,0,NULL,177,10610,'Нет','{\"extensions\":\"\",\"class\":\"col-12 md:col-4\"}',1,'Отклонение (мм)'),
(184,'Отклонение влево на (мм)',0,'INPUT',0,0,NULL,178,10630,'Нет','{\"extensions\":\"\",\"class\":\"col-12 md:col-4\"}',1,'Отклонение влево на (мм)'),
(185,'Отклонение вправо на (мм)',0,'INPUT',0,0,NULL,178,10640,'Нет','{\"extensions\":\"\",\"class\":\"col-12 md:col-4\"}',1,'Отклонение вправо на (мм)'),
(186,'Отклонение влево на (мм)',0,'INPUT',0,0,NULL,179,10690,'Нет','{\"extensions\":\"\",\"class\":\"col-12 md:col-4\"}',1,'Отклонение влево на (мм)'),
(187,'Отклонение вправо на (мм)',0,'INPUT',0,0,NULL,179,10700,'Нет','{\"extensions\":\"\",\"class\":\"col-12 md:col-4\"}',1,'Отклонение вправо на (мм)');
/*!40000 ALTER TABLE `fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_comments`
--

DROP TABLE IF EXISTS `order_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime(3) NOT NULL DEFAULT current_timestamp(3),
  `updated_at` datetime(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_comments_order_id_idx` (`order_id`),
  KEY `order_comments_user_id_idx` (`user_id`),
  CONSTRAINT `order_comments_order_id_fkey` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `order_comments_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_comments`
--

LOCK TABLES `order_comments` WRITE;
/*!40000 ALTER TABLE `order_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `title` varchar(191) NOT NULL,
  `description` text NOT NULL DEFAULT '',
  `status_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `patient_uid` varchar(191) NOT NULL,
  `service_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `responsible_id` int(11) DEFAULT NULL,
  `executor_id` int(11) DEFAULT NULL,
  `metadata` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`metadata`)),
  `created_at` datetime(3) NOT NULL DEFAULT current_timestamp(3),
  `finish_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) NOT NULL,
  `deleted_at` datetime(3) DEFAULT NULL,
  `delete_reason` text DEFAULT NULL,
  `prefer_date_at` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_company_id_fkey` (`company_id`),
  KEY `orders_status_id_fkey` (`status_id`),
  KEY `orders_customer_id_fkey` (`customer_id`),
  KEY `orders_patient_uid_fkey` (`patient_uid`),
  KEY `orders_service_id_fkey` (`service_id`),
  KEY `orders_creator_id_fkey` (`creator_id`),
  KEY `orders_responsible_id_fkey` (`responsible_id`),
  KEY `orders_executor_id_fkey` (`executor_id`),
  CONSTRAINT `orders_company_id_fkey` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `orders_creator_id_fkey` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `orders_customer_id_fkey` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `orders_executor_id_fkey` FOREIGN KEY (`executor_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `orders_responsible_id_fkey` FOREIGN KEY (`responsible_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `orders_patient_uid_fkey` FOREIGN KEY (`patient_uid`) REFERENCES `patients` (`uid`) ON UPDATE CASCADE,
  CONSTRAINT `orders_service_id_fkey` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `orders_status_id_fkey` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patients`
--

DROP TABLE IF EXISTS `patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patients` (
  `uid` varchar(191) NOT NULL,
  `fio` varchar(191) DEFAULT NULL,
  `fullinfo` varchar(191) DEFAULT NULL,
  `phone` varchar(191) DEFAULT NULL,
  `dob` datetime(3) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `patients_uid_key` (`uid`),
  KEY `Patient_company_id_fkey` (`company_id`),
  CONSTRAINT `Customer_company_id_unique` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patients`
--

LOCK TABLES `patients` WRITE;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` VALUES
('5ae3c59f-9d3e-4d6d-96ef-22c60adc84b9','Сафонов Георгий Михайлович','Сафонов Георгий Михайлович, +7 (926) 6764495, № 483','+7 (926) 6764495','0001-01-01 00:00:00.000',1);
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_key` (`slug`),
  UNIQUE KEY `roles_name_key` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES
(1,'SuperAdmin','Root',0),
(2,'Admin','Администратор',0),
(3,'MainTechnic','Главный техник',0),
(4,'Technic','Техник',0),
(5,'Doctor','Врач',1);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_categories`
--

DROP TABLE IF EXISTS `service_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_categories`
--

LOCK TABLES `service_categories` WRITE;
/*!40000 ALTER TABLE `service_categories` DISABLE KEYS */;
INSERT INTO `service_categories` VALUES
(1,'Ортопедия'),
(2,'Ортодонтия');
/*!40000 ALTER TABLE `service_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_fields`
--

DROP TABLE IF EXISTS `service_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_fields` (
  `service_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  PRIMARY KEY (`service_id`,`field_id`),
  KEY `service_fields_field_id_fkey` (`field_id`),
  CONSTRAINT `service_fields_field_id_fkey` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `service_fields_service_id_fkey` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_fields`
--

LOCK TABLES `service_fields` WRITE;
/*!40000 ALTER TABLE `service_fields` DISABLE KEYS */;
INSERT INTO `service_fields` VALUES
(1,1),
(1,2),
(1,6),
(1,7),
(1,8),
(1,9),
(1,10),
(1,11),
(1,12),
(1,13),
(1,14),
(1,15),
(1,16),
(1,17),
(1,18),
(1,19),
(1,20),
(1,21),
(1,22),
(1,23),
(1,24),
(1,25),
(1,26),
(1,27),
(1,28),
(1,29),
(1,30),
(1,31),
(1,32),
(1,33),
(1,34),
(1,35),
(1,36),
(1,37),
(1,38),
(1,39),
(1,40),
(1,41),
(1,42),
(1,43),
(1,44),
(1,45),
(1,46),
(1,47),
(1,48),
(1,49),
(1,50),
(1,51),
(1,52),
(1,53),
(1,54),
(1,55),
(1,56),
(1,57),
(1,58),
(1,59),
(1,80),
(1,81),
(1,82),
(1,83),
(1,84),
(1,85),
(1,86),
(1,87),
(1,88),
(1,89),
(1,90),
(1,91),
(1,92),
(1,93),
(1,94),
(1,95),
(1,96),
(1,97),
(1,98),
(1,99),
(1,100),
(1,101),
(1,102),
(1,103),
(1,104),
(1,105),
(1,106),
(1,107),
(1,108),
(1,109),
(1,110),
(1,111),
(1,112),
(2,5),
(2,122),
(2,123),
(2,124),
(2,125),
(2,126),
(2,127),
(2,128),
(2,129),
(2,130),
(2,131),
(2,132),
(2,133),
(2,134),
(2,135),
(2,136),
(2,137),
(2,138),
(2,139),
(2,140),
(2,141),
(2,142),
(2,143),
(3,3),
(3,4),
(4,60),
(4,61),
(4,62),
(4,63),
(4,64),
(4,65),
(4,66),
(4,67),
(4,68),
(4,69),
(4,70),
(4,71),
(4,72),
(4,73),
(4,74),
(4,75),
(4,76),
(4,77),
(4,78),
(4,79),
(4,144),
(4,145),
(4,146),
(4,147),
(4,148),
(4,149),
(4,150),
(4,151),
(4,152),
(4,153),
(4,154),
(4,155),
(4,156),
(4,157),
(4,158),
(4,159),
(4,160),
(4,161),
(4,162),
(4,163),
(4,164),
(4,165),
(4,166),
(4,167),
(4,168),
(4,169),
(4,170),
(4,171),
(4,172),
(4,173),
(4,174),
(4,175),
(4,176),
(4,177),
(4,178),
(4,179),
(4,180),
(4,181),
(4,182),
(4,183),
(4,184),
(4,185),
(4,186),
(4,187);
/*!40000 ALTER TABLE `service_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) NOT NULL,
  `category_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Service_company_id_fkey` (`company_id`),
  KEY `services_category_id_fkey` (`category_id`),
  CONSTRAINT `Service_company_id_unique` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `services_category_id_fkey` FOREIGN KEY (`category_id`) REFERENCES `service_categories` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES
(1,'Моделирование + фрезеровка (CAD + CAM)',1,1),
(2,'Фрезеровка (CAM)',1,1),
(3,'Моделирование + 3D печать',1,1),
(4,'Ортодонтия',2,1);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statuses`
--

DROP TABLE IF EXISTS `statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort` int(11) NOT NULL DEFAULT 0,
  `slug` varchar(191) NOT NULL,
  `name` varchar(191) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `OrderStatus_slug_idx` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statuses`
--

LOCK TABLES `statuses` WRITE;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` VALUES
(1,0,'new','Новый заказ'),
(2,10000,'finished','Завершен'),
(3,1,'na_modelirovanii','На моделировании'),
(4,3,'frezirovka','Фрезеровка'),
(5,3,'utverzhdenie','На утверждении'),
(6,4,'proverka','Проверка'),
(7,5,'vipolnen','Выполнен'),
(8,6,'dostavka','Доставка');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `token` varchar(191) NOT NULL,
  `exp` datetime(3) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_agent` varchar(191) NOT NULL,
  UNIQUE KEY `tokens_token_key` (`token`),
  KEY `Token_user_id_fkey` (`user_id`),
  CONSTRAINT `tokens_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
INSERT INTO `tokens` VALUES
('11173632-95c6-406a-9b35-ea1b1a418de1','2024-08-28 08:58:53.672',1,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36'),
('769be64c-6cd0-420d-890d-444ea6f69f3d','2024-08-29 00:00:24.124',1,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36'),
('9e6f6e56-b532-4eba-8765-8ab942898178','2024-08-30 11:26:24.418',1,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36'),
('cf47cdd2-a332-43ac-beb3-40f1af237e80','2024-08-29 09:07:46.652',1,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36');
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `email` varchar(191) DEFAULT NULL,
  `password` varchar(191) DEFAULT NULL,
  `avatar` varchar(1000) DEFAULT NULL,
  `first_name` varchar(191) DEFAULT NULL,
  `middle_name` varchar(191) DEFAULT NULL,
  `last_name` varchar(191) DEFAULT NULL,
  `email_verified` tinyint(1) NOT NULL DEFAULT 0,
  `verified_code` varchar(191) NOT NULL DEFAULT '',
  `metadata` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`metadata`)),
  `role_id` int(11) NOT NULL DEFAULT 4,
  `company_id` int(11) NOT NULL,
  `last_login` datetime(3) DEFAULT NULL,
  `login_attempts` int(11) NOT NULL DEFAULT 0,
  `last_failed_login` datetime(3) DEFAULT NULL,
  `created_at` datetime(3) NOT NULL DEFAULT current_timestamp(3),
  `deleted_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `is_own` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `User_role_id_fkey` (`role_id`),
  KEY `User_company_id_fkey` (`company_id`),
  CONSTRAINT `User_company_id_unique` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `User_role_id_unique` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES
(1,1,'admin@cmacom.lab','$2b$10$Aov0EvYQNdT562q40ONP3uu.Ppb2bjqqgl1VCqII.KFTKJSqIwl0C','uploads/1335ae61-4dd6-453d-94a6-395f3b99cf4b-1721685567631.png','Иван','Иванович','Иванов',1,'','{}',1,1,NULL,1,'2024-07-18 16:39:16.095','2024-07-17 20:21:41.233',NULL,'2024-07-22 21:59:29.888',1),
(2,1,'feedback@cmacom.ru','$2b$10$Q43hX2y5t79UPkHqdIlHUuH5yQgHeikCbtHOALoxcgMUDw.umFXba','','Евгения','Петровна','Техник',1,'','{}',4,1,NULL,0,NULL,'2024-07-19 20:14:35.664',NULL,'2024-07-20 16:12:14.563',1),
(3,1,'info@cmacom.ru','$2b$10$MOC7eAxkxPNKlECziRCzEOUw0TpT371c/tEKxi.hwUIHkpneX95je','','Сергей','Петров','Доктор',1,'','{}',5,1,NULL,0,NULL,'2024-07-19 20:15:21.713',NULL,'2024-07-20 15:52:34.426',1),
(4,1,'g_safonov@dhcentre.ru','$2b$10$OERfhmmZgjH2cT3JaB56S.RtWIsSUfJRwNUD3Bz2UDt5c0UjvT82.','','Анастасия','Ивановна','Ресепшен',1,'','{}',2,1,NULL,1,'2024-07-19 20:27:08.120','2024-07-19 20:16:22.476',NULL,'2024-07-20 16:05:24.982',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-07-30 17:18:07
