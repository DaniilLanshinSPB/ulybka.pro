import type { INotification } from '~/types/notification.type';

const useNotifications = () => {
  const fetchUnread = async (limit: number = 50, offset: number = 0) => {
    const { $api } = useApi();

    const response = await $api<INotification[]>(`/notifications/`, {
      params: {
        limit,
        offset
      }
    });
    return response.data;
  };
  const read = async (id: number) => {
    const { $api } = useApi();
    await $api.patch<INotification[]>(`/notifications/${id}`);
    return true;
  };
  return {
    fetchUnread,
    read
  };
};
export default useNotifications;
