const useOrders = () => {
  const fetchAll = async (company_id: number, filter: IOrderFilterDto = {}): Promise<IOrder[] | unknown> => {
    const { $api } = useApi();

    const response = await $api.get<IOrder[]>(`/orders/${company_id}/all`, {
      params: {
        filter
      }
    });
    return response.data;
  };

  const fetchItem = async (company_id: number, order_id: number): Promise<IOrder> => {
    const { $api } = useApi();

    const response = await $api<IOrder>(`/orders/${company_id}/${order_id}`);
    return response.data;
  };

  const update = async (company_id: number, order_id: number, order: ICreateOrderDto): Promise<IOrder> => {
    const { $api } = useApi();

    const response = await $api.patch<IOrder>(`/orders/${company_id}/${order_id}`, {
      description: order.description,
      responsible_id: order.responsible_id || null,
      executor_id: order.executor_id || null,
      status_id: order.status_id || 1,
      finish_at: order.finish_at || null
    });
    return response.data;
  };

  const create = async (company_id: number, order: ICreateOrderDto): Promise<IOrder> => {
    const { $api } = useApi();

    const response = await $api.post<IOrder>(`/orders/${company_id}/create`, {
      description: order.description,
      patient: order.patient,
      customer_id: order.customer_id || null,
      responsible_id: order.responsible_id || null,
      executor_id: order.executor_id || null,
      status_id: order.status_id || 1,
      finish_at: order.finish_at || null,
      services: order.services || [],
      customer_company_id: order.customer_company_id
    });
    return response.data;
  };
  const softDeleteOrder = async (company_id: number, order_id: number, deleteReason: string) => {
    const { $api } = useApi();

    const response = await $api(`/orders/${company_id}/${order_id}`, {
      method: 'DELETE',
      params: {
        reason: deleteReason
      }
    });
    return response;
  };
  const deleteOrder = async (company_id: number, order_id: number) => {
    const { $api } = useApi();

    const response = await $api.delete(`/orders/${company_id}/${order_id}/hard`);
    return response;
  };
  const restore = async (company_id: number, order_id: number) => {
    const { $api } = useApi();

    const response = await $api.get(`/orders/${company_id}/${order_id}/restore`);
    return response;
  };

  const fetchStatuses = async (): Promise<IOrderStatus[]> => {
    const { $api } = useApi();

    const response = await $api.get<IOrderStatus[]>('/orders/statuses');
    return response.data;
  };
  const createStatus = async (dto): Promise<IOrderStatus[]> => {
    const { $api } = useApi();

    const response = await $api.post<IOrderStatus[]>('/orders/statuses', dto);
    return response.data;
  };
  const updateStatus = async (id: number, dto): Promise<IOrderStatus[]> => {
    const { $api } = useApi();

    const response = await $api.patch<IOrderStatus[]>(`/orders/statuses/${id}`, dto);
    return response.data;
  };
  const deleteStatus = async (): Promise<IOrderStatus[]> => {
    const { $api } = useApi();

    const response = await $api.delete(`/orders/statuses/${id}`);
    return response.data;
  };

  const fetchOrderComments = async (company_id: number, order_id: number): Promise<IOrderComment[]> => {
    const { $api } = useApi();

    const response = await $api.get<IOrderComment[]>(`/orders/${company_id}/${order_id}/comments/all`);
    return response.data;
  };

  const createOrderComment = async (company_id: number, order_id: number, dto: IOrderCommentCreateDto): Promise<IOrderComment> => {
    const { $api } = useApi();

    const response = await $api.post<IOrderComment>(`/orders/${company_id}/${order_id}/comments/`, {
      message: dto.message
    });
    return response.data;
  };
  const deleteOrderComment = async (company_id: number, order_id: number, comment_id: number) => {
    const { $api } = useApi();

    const response = await $api.delete(`/orders/${company_id}/${order_id}/comments/${comment_id}`);
    return response;
  };
  return {
    fetchAll,
    fetchItem,
    update,
    create,
    softDeleteOrder,
    deleteOrder,
    restore,
    fetchStatuses,
    createStatus,
    updateStatus,
    deleteStatus,
    fetchOrderComments,
    createOrderComment,
    deleteOrderComment
  };
};
export default useOrders;
