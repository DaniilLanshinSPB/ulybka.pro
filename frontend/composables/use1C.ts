interface IApiResponse {
  Parameters: any;
  Error: any;
  ErrorMessage: any;
}

const use1C = () => {
  const getCustomers = async (name: string = ''): Promise<IPatient[]> => {
    const { $api } = useApi();

    if (!name.length || name.length < 3) return [];

    try {
      const response = await $api.get<any>(`/patients/search`, {
        params: {
          name
        }
      });
      
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  return {
    getCustomers
  };
};
export default use1C;
