const useCompanies = () => {
  const fetchAll = async (): Promise<ICompany[] | unknown> => {
    const { $api } = useApi();

    try {
      const response = await $api.get<ICompany[]>('/companies/all');
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const fetchItem = async (company_id: number): Promise<ICompany> => {
    const { $api } = useApi();

    try {
      const response = await $api.get<ICompany>(`/companies/${company_id}`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const updateCompany = async (company_id: number, dto: ICreateCompanyDto): Promise<ICompany> => {
    const { $api } = useApi();

    try {
      const response = await $api.patch<ICompany>(`/companies/${company_id}`, {
        ...dto
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const createCompany = async (dto: ICreateCompanyDto): Promise<ICompany> => {
    const { $api } = useApi();

    try {
      const response = await $api.post<ICompany>('/companies', {
        ...dto
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const deleteCompany = async (company_id: number) => {
    const { $api } = useApi();

    try {
      const response = await $api.delete(`/companies/${company_id}`);
      return response;
    } catch (e) {
      throw e;
    }
  };

  const fetchTypes = async (): Promise<ICompanyType[] | unknown> => {
    const { $api } = useApi();

    try {
      const response = await $api.get<ICompanyType[]>(`/companies/types`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const fetchRoles = async (company_id: number): Promise<IRole[]> => {
    const { $api } = useApi();

    try {
      const response = await $api.get<IRole[]>(`/companies/${company_id}/roles`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const fetchCompanyUsers = async (company_id: number, filter: IUserFilterDto = {}): Promise<IUser[]> => {
    const { $api } = useApi();

    try {
      const response = await $api.get<IUser[]>(`/companies/${company_id}/users`, { params: { filter } });
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const fetchCompanyUser = async (company_id: number, user_id: number): Promise<IUser> => {
    const { $api } = useApi();

    try {
      const response = await $api.get<IUser>(`/companies/${company_id}/users/${user_id}`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const fetchCompanyResponsibleUsers = async (company_id: number): Promise<IUser[]> => {
    const { $api } = useApi();

    try {
      const response = await $api.get<IUser[]>(`/companies/${company_id}/users/responsible`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const fetchCompanyExecutorsUsers = async (company_id: number): Promise<IUser[]> => {
    const { $api } = useApi();

    try {
      const response = await $api.get<IUser[]>(`/companies/${company_id}/users/executors`);

      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const fetchCompanyCustomersUsers = async (company_id: number): Promise<IUser[]> => {
    const { $api } = useApi();

    try {
      const response = await $api.get<IUser[]>(`/companies/${company_id}/users/customers`);

      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const createCompanyUser = async (company_id: number, dto: ICreateCompanyUserDto) => {
    const { $api } = useApi();

    try {
      const response = await $api.post<IUser>(`/companies/${company_id}/users`, {
        email: dto.email,
        first_name: dto.first_name,
        last_name: dto.last_name,
        middle_name: dto.middle_name,
        is_active: dto.is_active,
        email_verified: dto.email_verified,
        password: dto.password,
        role_id: dto.role_id
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const updateCompanyUser = async (company_id: number, user_id: number, dto: ICreateCompanyUserDto) => {
    const { $api } = useApi();

    try {
      const response = await $api.patch<IUser>(`/companies/${company_id}/users/${user_id}`, {
        email: dto.email,
        first_name: dto.first_name,
        last_name: dto.last_name,
        middle_name: dto.middle_name,
        email_verified: dto.email_verified,
        is_active: dto.is_active,
        role_id: dto.role_id
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const deleteCompanyUser = async (company_id: number, user_id: number) => {
    const { $api } = useApi();

    try {
      const response = await $api.delete<IUser>(`/companies/${company_id}/users/${user_id}`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const fetchCompanyClinics = async (company_id: number) => {
    const { $api } = useApi();

    try {
      const response = await $api.get<ICompany[]>(`/companies/${company_id}/clinics`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const createCompanyClinic = async (company_id: number, dto: Partial<ICreateCompanyDto>): Promise<ICompany> => {
    const { $api } = useApi();

    try {
      const response = await $api.post<ICompany>(`/companies/${company_id}/clinics`, dto);
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const updateCompanyClinic = async (company_id: number, clinic_id: number, dto: Partial<ICreateCompanyDto>): Promise<ICompany> => {
    const { $api } = useApi();

    try {
      const response = await $api.patch<ICompany>(`/companies/${company_id}/clinics/${clinic_id}`, dto);
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const deleteCompanyClinic = async (company_id: number, clinic_id: number) => {
    const { $api } = useApi();

    try {
      const response = await $api.delete<ICompany>(`/companies/${company_id}/clinics/${clinic_id}`);
      return response;
    } catch (e) {
      throw e;
    }
  };
  const fetchServices = async (company_id: number) => {
    const { $api } = useApi();

    try {
      const response = await $api.get<IService[]>(`/companies/${company_id}/services`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const addService = async (company_id: number, service_id: number) => {
    const { $api } = useApi();

    try {
      const response = await $api.post(`/companies/${company_id}/services/${service_id}`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const removeService = async (company_id: number, service_id: number) => {
    const { $api } = useApi();

    try {
      const response = await $api.delete(`/companies/${company_id}/services/${service_id}`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const fetchApiKeys = async (company_id: number): Promise<IApiKey[]> => {
    const { $api } = useApi();
    const response = await $api.get<IApiKey[]>(`/api-key/${company_id}`);
    return response.data;
  };

  const createApiKey = async (company_id: number, dto: ICreateApiKey): Promise<IApiKey> => {
    const { $api } = useApi();
    const response = await $api.post<IApiKey>(`/api-key/${company_id}`, dto);
    return response.data;
  };
  const removeApiKey = async (company_id: number, id: number): Promise<boolean> => {
    const { $api } = useApi();
    const response = await $api.delete<boolean>(`/api-key/${company_id}/${id}`);
    return response.data;
  };
  const fetchHooks = async (company_id: number): Promise<IWebhook[]> => {
    const { $api } = useApi();
    const response = await $api.get<IWebhook[]>(`/webhook/${company_id}`);
    return response.data;
  };

  const createHook = async (company_id: number, dto: ICreateWebhook): Promise<IWebhook> => {
    const { $api } = useApi();
    const response = await $api.post<IWebhook>(`/webhook/${company_id}`, dto);
    return response.data;
  };
  const removeHook = async (company_id: number, id: number): Promise<boolean> => {
    const { $api } = useApi();
    const response = await $api.delete<boolean>(`/webhook/${company_id}/${id}`);
    return response.data;
  };
  return {
    fetchAll,
    fetchItem,
    updateCompany,
    createCompany,
    deleteCompany,
    fetchTypes,
    fetchRoles,
    fetchCompanyUsers,
    fetchCompanyUser,
    createCompanyUser,
    updateCompanyUser,
    deleteCompanyUser,
    fetchCompanyClinics,
    createCompanyClinic,
    updateCompanyClinic,
    deleteCompanyClinic,
    fetchServices,
    addService,
    removeService,
    fetchCompanyResponsibleUsers,
    fetchCompanyExecutorsUsers,
    fetchCompanyCustomersUsers,
    fetchApiKeys,
    createApiKey,
    removeApiKey,
    fetchHooks,
    createHook,
    removeHook
  };
};
export default useCompanies;
