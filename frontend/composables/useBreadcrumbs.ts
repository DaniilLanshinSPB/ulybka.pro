export const useBreadcrumbs = () => {
  const router = useRouter();
  const route = useRoute();
  const routes = router.getRoutes();

  const HOMEPAGE = { label: 'Главная', path: '/' };

  const getBreadcrumbs = async (currRoute: string): Promise<any[]> => {
    if (currRoute === '') return [HOMEPAGE];

    const paths = getBreadcrumbs(currRoute.slice(0, currRoute.lastIndexOf('/')));

    const founds = routes.filter((r) => isMathPatternPath(r.path, currRoute));

    const matchRoute = founds.length > 1 ? founds.find((r): any | undefined => r.path === currRoute) : founds[0];

    if (matchRoute?.meta.breadcrumb === false) {
      return [...(await paths)];
    } else {
      return [
        ...(await paths),
        {
          path: currRoute,
          name: matchRoute?.name,
          label:
            (typeof matchRoute?.meta.breadcrumb === 'function' ? await matchRoute?.meta.breadcrumb() : matchRoute?.meta.breadcrumb) ||
            matchRoute?.meta.title ||
            matchRoute?.path ||
            currRoute
        }
      ];
    }
  };

  const isMathPatternPath = (pathA: string, pathB: string) => {
    const partsA = pathA.split('/');
    const partsB = pathB.split('/');

    if (partsA.length !== partsB.length) return false;

    const isMatch = partsA.every((part: string, i: number) => {
      return part === partsB[i] || part.startsWith(':');
    });

    return isMatch;
  };

  return {
    getBreadcrumbs
  };
};
