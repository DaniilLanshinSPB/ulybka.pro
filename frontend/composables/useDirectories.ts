const useDirectories = () => {
  const fetchAll = async (): Promise<IDirectory[] | unknown> => {
    const { $api } = useApi();

    try {
      const response = await $api.get<IDirectory[]>('/directory/all');
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const fetchItem = async (directory_id: number): Promise<IDirectory> => {
    const { $api } = useApi();

    try {
      const response = await $api<IDirectory>(`/directory/${directory_id}`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const update = async (directory_id: number, directory: IDirectoryCreateDto): Promise<IDirectory> => {
    const { $api } = useApi();

    try {
      const response = await $api.patch<IDirectory>(`/directory/${directory_id}`, {
        title: directory.title,
        options: directory.options
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const create = async (storeData: IDirectoryCreateDto): Promise<IDirectory> => {
    const { $api } = useApi();

    try {
      const response = await $api.post<IDirectory>('/directory', {
        title: storeData.title,
        options: storeData.options
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const deleteDirectory = async (directory_id: number) => {
    const { $api } = useApi();

    try {
      const response = await $api.delete(`/directory/${directory_id}`);
      return response;
    } catch (e) {
      throw e;
    }
  };

  return {
    fetchAll,
    fetchItem,
    update,
    create,
    deleteDirectory
  };
};
export default useDirectories;
