import type { IFilterFieldDto } from '~/types/field.type';

const useFields = () => {
  const fetchTree = async (filter?: IFilterFieldDto): Promise<IField[] | unknown> => {
    const { $api } = useApi();

    try {
      const response = await $api.get<IField[]>('/fields/tree', { params: { filter } });
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const fetchAll = async (filter?: IFilterFieldDto): Promise<IField[] | unknown> => {
    const { $api } = useApi();

    try {
      const response = await $api.get<IField[]>('/fields/all', { params: { filter } });
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const fetchItem = async (field_id: number): Promise<IField> => {
    const { $api } = useApi();

    try {
      const response = await $api<IField>(`/fields/${field_id}`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const copyField = async (field_id: number) => {
    const { $api } = useApi();
    try {
      const response = await $api.patch(`/fields/${field_id}/copy`);
      return response;
    } catch (e) {
      throw e;
    }
  };

  const update = async (field_id: number, dto: ICreateFieldDto): Promise<IField> => {
    const { $api } = useApi();

    try {
      const response = await $api.patch<IField>(`/fields/${field_id}`, {
        active: dto.active,
        technical_name: dto.technical_name || dto.name,
        name: dto.name,
        group: dto.group,
        placeholder: dto.placeholder,
        multiple: dto.multiple || false,
        type: dto.type || FieldType.INPUT,
        directory_id: dto.directory_id || null,
        parent_id: dto.parent_id || null,
        sort: dto.sort || 0,
        display_conditions: dto.display_conditions || null,
        metadata: dto.metadata,
        required: dto.required || false
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const create = async (dto: ICreateFieldDto): Promise<IField> => {
    const { $api } = useApi();

    try {
      const response = await $api.post<IField>('/fields', {
        active: dto.active,
        technical_name: dto.technical_name || dto.name,
        name: dto.name,
        group: dto.group,
        placeholder: dto.placeholder,
        multiple: dto.multiple || false,
        type: dto.type || FieldType.INPUT,
        directory_id: dto.directory_id || null,
        parent_id: dto.parent_id || null,
        sort: dto.sort || 0,
        display_conditions: dto.display_conditions || null,
        metadata: dto.metadata,
        required: dto.required || false
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const deleteField = async (field_id: number) => {
    const { $api } = useApi();

    try {
      const response = await $api.delete(`/fields/${field_id}`);
      return response;
    } catch (e) {
      throw e;
    }
  };

  return {
    fetchAll,
    fetchTree,
    fetchItem,
    copyField,
    update,
    create,
    deleteField
  };
};
export default useFields;
