import axios from 'axios';

const useApi = () => {
  const apiPath = useRuntimeConfig().public.apiPath;

  const $api = axios.create({
    withCredentials: true,
    baseURL: `${apiPath}`
  });

  if (process.client) {
    const token = window.localStorage.getItem('access_token');
    if (token) {
      $api.defaults.headers.common['Authorization'] = token;
    }
  }

  $api.interceptors.response.use(
    (config) => {
      return config;
    },
    async (error) => {
      const originalRequest = error.config;

      if (error.response?.status === 401 && error.config && !error.config._isRetry) {
        originalRequest._isRetry = true;
        try {
          const response = await $default_api.get(`${apiPath}/auth/refresh-tokens`, { withCredentials: true });
          localStorage.setItem('access_token', response.data.accessToken);
          return $api.request(originalRequest);
        } catch (e) {
          throw e;
        }
      }

      throw error.response?.data;
    }
  );

  const $default_api = axios.create({
    withCredentials: true,
    baseURL: `${apiPath}`
  });

  $default_api.interceptors.response.use(
    (config) => {
      return config;
    },
    async (error) => {
      if (error.response?.status === 500) {
        console.log('Произошла ошибка на сервере, попробуте позже');
      }
      return error.response;
    }
  );
  return {
    $api,
    $default_api
  };
};
export default useApi;
