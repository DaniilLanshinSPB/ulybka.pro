import type { ILogFilter } from '~/types/logs.type';

const useLogs = () => {
  const fetchAll = async (
    filter: ILogFilter = {
      limit: 20,
      offset: 0
    }
  ) => {
    const { $api } = useApi();
    const response = await $api.post<ILog[]>(`/logs/all`, filter);
    return response.data;
  };

  return {
    fetchAll
  };
};
export default useLogs;
