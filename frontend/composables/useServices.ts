const useServices = () => {
  const fetchCategories = async (): Promise<IServiceCategory[]> => {
    const { $api } = useApi();
    const response = await $api.get<IServiceCategory[]>('/services/categories/all');
    return response.data;
  };

  const fetchAll = async (): Promise<IService[] | unknown> => {
    const { $api } = useApi();
    const response = await $api.get<IService[]>('/services/all');
    return response.data;
  };

  const fetchItem = async (service_id: number): Promise<IService> => {
    const { $api } = useApi();
    const response = await $api<IService>(`/services/${service_id}`);
    return response.data;
  };

  const update = async (service_id: number, service: IServiceCreateDto): Promise<IService> => {
    const { $api } = useApi();
    const response = await $api.patch<IService>(`/services/${service_id}`, {
      title: service.title,
      category_id: service.category_id,
      fields: service.fields,
      is_active: service.is_active,
      is_default: service.is_default
    });
    return response.data;
  };

  const create = async (storeData: IServiceCreateDto): Promise<IService> => {
    const { $api } = useApi();
    const response = await $api.post<IService>('/services', {
      title: storeData.title,
      category_id: storeData.category_id,
      fields: storeData.fields,
      is_active: storeData.is_active,
      is_default: storeData.is_default
    });
    return response.data;
  };
  const deleteService = async (service_id: number) => {
    const { $api } = useApi();
    const response = await $api.delete(`/services/${service_id}`);
    return response;
  };
  const addCategory = async (dto: IServiceCategoryCreateDto) => {
    const { $api } = useApi();
    const response = await $api.post(`/services/categories`, dto);
    return response.data;
  };
  const updateCategory = async (dto: IServiceCategoryCreateDto) => {
    const { $api } = useApi();
    const response = await $api.patch(`/services/categories/${dto.id}`, dto);
    return response.data;
  };
  const deleteCategory = async (id: number) => {
    const { $api } = useApi();
    const response = await $api.delete(`/services/categories/${id}`);
    return response;
  };
  const fetchServiceDirectories = async (id: number) => {
    const { $api } = useApi();
    const response = await $api.get(`/services/${id}/dictionaries`);
    return response.data;
  };

  return {
    fetchCategories,
    fetchAll,
    fetchItem,
    update,
    create,
    deleteService,
    addCategory,
    updateCategory,
    deleteCategory,
    fetchServiceDirectories
  };
};
export default useServices;
