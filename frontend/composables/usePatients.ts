const usePatients = () => {

  const search = async (company_id:number, name: string = ''): Promise<IPatient[]> => {
    const { $api } = useApi();

    if (!name.length || name.length < 3) return [];

    try {
      const response = await $api.get<any>(`/patients/${company_id}/search`, {
        params: {
          name
        }
      });
      
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const fetchAll = async (company_id: number): Promise<IPatient[]> => {
    const { $api } = useApi();

    try {
      const response = await $api.get<IPatient[]>(`/patients/${company_id}/all`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const fetchItem = async (company_id: number, patient_uid: string): Promise<IPatient> => {
    const { $api } = useApi();

    try {
      const response = await $api<IPatient>(`/patients/${company_id}/${patient_uid}`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const create = async (company_id: number, patient: IPatient): Promise<IPatient> => {
    const { $api } = useApi();
    try {
      const response = await $api.post<IPatient>(`/patients/${company_id}`, patient);
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const update = async (company_id: number, patient: IPatient): Promise<IPatient> => {
    const { $api } = useApi();
    try {
      const response = await $api.patch<IPatient>(`/patients/${company_id}/${patient.uid}`, patient);
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const deletePatient = async (company_id: number, patient_uid: string) => {
    const { $api } = useApi();
    try {
      const response = await $api.delete<IPatient>(`/patients/${company_id}/${patient_uid}`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  return {
    search,
    fetchAll,
    fetchItem,
    create,
    update,
    deletePatient
  };
};
export default usePatients;
