import type { IChangeAvatarDto, IChangeProfileDto, IChangeProfilePasswordDto } from '~/types/user.type';

const useAuth = () => {
  const { $api, $default_api } = useApi();
  const toast = useToast();
  const authStore = useAuthStore();
  const login = async (email: string, password: string) => {
    try {
      const response = await $default_api.post('/auth/login', { email, password });

      const token = response?.data?.accessToken;
      if (token) {
        localStorage.setItem('access_token', token);

        const tokenInfo = jwt_decode(token);
        authStore.setVerified(tokenInfo.email_verified);
        authStore.setLoggedIn(true);
        authStore.setRole(tokenInfo.role);
        authStore.setPermissions(tokenInfo.permissions);
        checkAuth();

        toast.add({ severity: 'success', summary: 'Успешно', detail: 'Вы успешно авторизованы', life: 3000 });

        return true;
      }
      toast.add({ severity: 'error', summary: 'Ошибка', detail: response?.data?.message, life: 3000 });
      return false;
    } catch (e) {
      throw e;
    }
  };
  const register = async (email: string, password: string, confirm_password: string, company: string) => {
    try {
      return await $default_api.post('/auth/register', { email, password, confirm_password, company });
    } catch (e) {
      throw e;
    }
  };
  const logout = async () => {
    try {
      await $api.get('/auth/logout');
      localStorage.removeItem('access_token');
      authStore.resetState();
    } catch (e) {
      toast.add({ severity: 'error', summary: 'Ошибка', detail: e?.message, life: 3000 });
      throw e;
    }
  };
  const checkAuth = async () => {
    try {
      const response = await $api.get('/auth/me', {
        headers: {
          Authorization: `${localStorage.getItem('access_token')}`
        }
      });

      authStore.setUser(response.data);
      const tokenInfo = jwt_decode(localStorage.getItem('access_token'));

      if (tokenInfo?.role) {
        authStore.setRole(tokenInfo?.role);
      }
      authStore.setVerified(tokenInfo?.email_verified);
      authStore.setLoggedIn(true);
      authStore.setPermissions(tokenInfo?.permissions);
    } catch (e) {
      authStore.resetState();
      toast.add({ severity: 'error', summary: 'Ошибка', detail: e?.message, life: 3000 });
      throw e;
    }
  };

  const confirmAccount = async (code: string) => {
    try {
      await $api.get('/auth/confirm-account', { params: { code } });
      await refreshToken();
      authStore.setVerified(true);
      return true;
    } catch (e) {
      throw e;
    }
  };

  const refreshToken = async () => {
    try {
      const response = await $api.get('/auth/refresh-tokens', {
        headers: {
          Authorization: `${localStorage.getItem('access_token')}`
        }
      });
      localStorage.setItem('access_token', response.data?.accessToken);
    } catch (e) {
      throw e;
    }
  };

  const resendCode = async (email: string) => {
    try {
      await $api.get(`/auth/send-code/`, { params: { email } });
    } catch (e) {
      throw e;
    }
  };

  const resetPassword = async (email: string) => {
    try {
      await $api.get(`/auth/reset-password/`, { params: { email } });
    } catch (e) {
      throw e;
    }
  };

  const changeAvatar = async (changeProfileDto: IChangeAvatarDto) => {
    try {
      const response = await $api.patch(`/auth/profile/`, {
        avatar: changeProfileDto.avatar
      });
      authStore.setUser(response.data);
    } catch (e) {
      throw e;
    }
  };
  const changeProfile = async (changeProfileDto: IChangeProfileDto) => {
    try {
      const response = await $api.patch(`/auth/profile/`, {
        first_name: changeProfileDto.first_name,
        last_name: changeProfileDto.last_name,
        middle_name: changeProfileDto.middle_name,
        avatar: changeProfileDto.avatar,
        email: changeProfileDto.email,
        metadata: {},
        user_notice_permissions: changeProfileDto.user_notice_permissions
      });
      authStore.setUser(response.data);
    } catch (e) {
      throw e;
    }
  };
  const fetchNotificationTypes = async (): Promise<{ slug: LogType; active: boolean }[]> => {
    try {
      const response = await $api.get<{ slug: LogType; active: boolean }[]>(`/auth/notification-types/`, {
        headers: {
          Authorization: `${localStorage.getItem('access_token')}`
        }
      });
      authStore.setNotificationTypes(response.data);
    } catch (e) {
      throw e;
    }
  };
  const changePassword = async (changePasswordDto: IChangeProfilePasswordDto) => {
    try {
      await $api.patch(`/auth/update-password/`, {
        old_password: changePasswordDto.old_password,
        password: changePasswordDto.password,
        confirm_password: changePasswordDto.confirm_password
      });
    } catch (e) {
      throw e;
    }
  };

  const connectTelegram = async () => {
    try {
      const response = await $api.get('/auth/telegram-connect', {
        headers: {
          Authorization: `${localStorage.getItem('access_token')}`
        }
      });
      const url = response?.data;

      window.open(url, '_blank', 'noopener,noreferrer');
    } catch (e: any) {
      toast.add({ severity: 'error', summary: 'Ошибка', detail: e?.message, life: 3000 });
      throw e;
    }
  };

  return {
    login,
    register,
    logout,
    checkAuth,
    confirmAccount,
    resendCode,
    changeAvatar,
    changeProfile,
    changePassword,
    resetPassword,
    connectTelegram,
    fetchNotificationTypes
  };
};

export default useAuth;
