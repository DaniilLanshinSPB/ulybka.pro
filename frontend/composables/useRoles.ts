const useRoles = () => {
  const fetchRoles = async (): Promise<IRole[]> => {
    const { $api } = useApi();
    try {
      const response = await $api.get<IRole[]>('/role/all');
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const fetchOne = async (role_id: number): Promise<IRole> => {
    const { $api } = useApi();
    try {
      const response = await $api.get<IRole>(`/role/${role_id}`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const create = async (storeData: Partial<ICreateRoleDto>): Promise<IRole> => {
    const { $api } = useApi();
    try {
      const response = await $api.post<IRole>('/role', {
        name: storeData.name,
        permissions: storeData.permissions,
        company_type_id: storeData.company_type_id,
        notification_permissions: storeData.notification_permissions,
        is_default: storeData.is_default
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const update = async (role_id: number, storeData: Partial<ICreateRoleDto>): Promise<IRole> => {
    const { $api } = useApi();
    try {
      const response = await $api.patch<IRole>(`/role/${role_id}`, {
        id: storeData.id,
        name: storeData.name,
        permissions: storeData.permissions,
        notification_permissions: storeData.notification_permissions,
        company_type_id: storeData.company_type_id,
        is_default: storeData.is_default
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const deleteRole = async (role_id: number): Promise<IRole> => {
    const { $api } = useApi();
    try {
      const response = await $api.delete<IRole>(`/role/${role_id}`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const fetchPermissions = async (): Promise<IPermission[]> => {
    const { $api } = useApi();
    try {
      const response = await $api.get<IPermission[]>(`/role/permissions/all`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const fetchNotificationTypes = async (): Promise<INotificationType[]> => {
    const { $api } = useApi();
    try {
      const response = await $api.get<INotificationType[]>(`/role/notification-types/all`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const setPermissions = async (role_id: number, permissions: number[]): Promise<IRole> => {
    const { $api } = useApi();
    try {
      const response = await $api.post(`/role/permissions/${role_id}`, {
        permissions
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  return {
    fetchRoles,
    fetchPermissions,
    setPermissions,
    fetchNotificationTypes,
    fetchOne,
    create,
    update,
    deleteRole
  };
};
export default useRoles;
