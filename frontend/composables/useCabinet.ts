const useCabinet = () => {
  const { $api } = useApi();

  const fetch = async <T>(url: string, params?: Record<string, unknown>) => {
    const response = await $api[params?.method ? params.method : 'get']<T>(
      url,
      params?.method && ['post', 'patch'].includes(params.method) ? params?.data : { params }
    );
    return response.data;
  };

  const fetchCompany = () => fetch<ICompany>('/auth/company');
  const updateCompany = (dto: ICreateCompanyDto) => fetch<ICompany>(`/auth/company`, { method: 'patch', data: dto });

  const fetchOrders = (filter: IOrderFilterDto) => fetch<IOrder[]>(`/cabinet/orders`, { filter });
  const fetchOrder = (order_id: number) => fetch<IOrder>(`/cabinet/orders/${order_id}`);
  const updateOrder = (order_id: number, order: ICreateOrderDto) =>
    fetch<IOrder>(`/cabinet/orders/${order_id}`, {
      method: 'patch',
      data: {
        description: order.description,
        responsible_id: order.responsible_id || null,
        executor_id: order.executor_id || null,
        status_id: order.status_id || 1,
        finish_at: order.finish_at || null
      }
    });

  const createOrder = (order: ICreateOrderDto) =>
    fetch<IOrder>(`/cabinet/orders/create`, {
      method: 'post',
      data: {
        description: order.description,
        patient: order.patient,
        customer_id: order.customer_id || null,
        responsible_id: order.responsible_id || null,
        executor_id: order.executor_id || null,
        status_id: order.status_id || 1,
        finish_at: order.finish_at || null,
        services: order.services || [],
        customer_company_id: order.customer_company_id
      }
    });
  const softDeleteOrder = (order_id: number, deleteReason: string) =>
    fetch<IOrder>(`/cabinet/orders/${order_id}`, {
      method: 'delete',
      params: {
        reason: deleteReason
      }
    });

  const fetchOrderComments = (order_id: number) => fetch<IOrderComment[]>(`/cabinet/orders/${order_id}/comments`);

  const createOrderComment = async (order_id: number, dto: IOrderCommentCreateDto): Promise<IOrderComment> =>
    fetch<IOrderComment>(`/cabinet/orders/${order_id}/comments`, {
      method: 'post',
      data: {
        message: dto.message
      }
    });

  const deleteOrderComment = async (order_id: number, comment_id: number) =>
    fetch<IOrder>(`/cabinet/orders/${order_id}/comments/${comment_id}`, { method: 'delete' });

  const fetchClinics = () => fetch<ICompany[]>(`/cabinet/clinics`);
  const fetchItemClinic = (clinic_id: number) => fetch<ICompany>(`/cabinet/clinics/${clinic_id}`);
  const createClinic = (dto: Partial<ICreateCompanyDto>) => fetch<ICompany>(`/cabinet/clinics`, { method: 'post', data: dto });
  const updateClinic = (clinic_id: number, dto: Partial<ICreateCompanyDto>) =>
    fetch<ICompany>(`/cabinet/clinics/${clinic_id}`, { method: 'patch', data: dto });
  const deleteClinic = (clinic_id: number) => fetch<ICompany>(`/cabinet/clinics/${clinic_id}`, { method: 'delete' });

  const fetchServices = () => fetch<IService[]>(`/cabinet/services`);
  const fetchItemService = (service_id: number) => fetch<IService>(`/cabinet/services/${service_id}`);

  const fetchAllPatients = (clinic_id: number) => fetch<IPatient[]>(`/cabinet/patients/${clinic_id}/all`);
  const fetchPatient = (patient_id: string) => fetch<IPatient[]>(`/cabinet/patients/${patient_id}`);
  const searchPatients = (clinic_id: number, query: string) => fetch<IPatient[]>(`/cabinet/patients/${clinic_id}/search?query=${query}`);
  const fetchItemPatient = (clinic_id: number, patient_uid: string) => fetch<IPatient>(`/cabinet/patients/${clinic_id}/${patient_uid}`);
  const createPatient = (clinic_id: number, patient: IPatient) =>
    fetch<IPatient>(`/cabinet/patients/${clinic_id}`, { method: 'post', data: patient });
  const updatePatient = (clinic_id: number, patient: IPatient) =>
    fetch<IPatient>(`/cabinet/patients/${clinic_id}/${patient.uid}`, { method: 'patch', data: patient });
  const deletePatient = (clinic_id: number, patient_uid: string) =>
    fetch<IPatient>(`/cabinet/patients/${clinic_id}/${patient_uid}`, { method: 'delete' });

  return {
    fetchCompany,
    updateCompany,
    fetchOrders,
    fetchOrder,
    updateOrder,
    createOrder,
    softDeleteOrder,
    fetchOrderComments,
    createOrderComment,
    deleteOrderComment,
    fetchClinics,
    fetchItemClinic,
    createClinic,
    updateClinic,
    deleteClinic,
    fetchServices,
    fetchItemService,
    fetchAllPatients,
    searchPatients,
    fetchPatient,
    fetchItemPatient,
    createPatient,
    updatePatient,
    deletePatient
  };
};
export default useCabinet;
