const useWebhooks = () => {
  const fetchAll = async (company_id: number): Promise<IWebhook[] | unknown> => {
    const { $api } = useApi();
    const response = await $api.get<IWebhook[]>(`/webhook/${company_id}/all`);
    return response.data;
  };

  const create = async (company_id: number): Promise<IWebhook> => {
    const { $api } = useApi();
    const response = await $api.post<IWebhook>(`/webhook/${company_id}`);
    return response.data;
  };
  const remove = async (company_id: number, id: number): Promise<boolean> => {
    const { $api } = useApi();
    const response = await $api.delete<boolean>(`/webhook/${company_id}/${id}`);
    return response.data;
  };
  return {
    fetchAll,
    create,
    remove
  };
};
