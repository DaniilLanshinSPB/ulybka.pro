const useFile = () => {
  const uploadFile = async (file: File) => {
    const { $api } = useApi();

    const formData = new FormData();
    formData.append('file', file);
    try {
      const response = await $api.post('/upload', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
      return response.data?.file.path;
    } catch (e) {
      throw e;
    }
  };
  const deleteFile = async (filePath: string) => {
    const { $api } = useApi();

    try {
      const response = await $api.delete('/upload', {
        params: {
          filePath
        }
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  return {
    uploadFile,
    deleteFile
  };
};
export default useFile;
