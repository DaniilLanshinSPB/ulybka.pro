const useUsers = () => {
  const fetchAll = async (filter?: IUserFilter): Promise<IUser[] | unknown> => {
    const { $api } = useApi();

    try {
      const response = await $api.post<IUser[]>('/user/all', filter);
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const fetchItem = async (user_id: number): Promise<IUser> => {
    const { $api } = useApi();

    try {
      const response = await $api<IUser>(`/user/${user_id}`);
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const updateUser = async (user: ICreateUserDto): Promise<IUser> => {
    const { $api } = useApi();

    try {
      const response = await $api.patch<IUser>(`/user/${user.id}`, {
        is_active: user.is_active,
        email: user.email,
        first_name: user.first_name,
        email_verified: user.email_verified,
        last_name: user.last_name,
        middle_name: user.middle_name,
        role_id: user.role_id,
        avatar: user.avatar,
        company_id: user.company_id
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  const createUser = async (storeData: ICreateUserDto): Promise<IUser> => {
    const { $api } = useApi();

    try {
      const response = await $api.post<IUser>('/user', {
        email: storeData.email,
        avatar: storeData.avatar || '',
        last_name: storeData.last_name || '',
        first_name: storeData.first_name || '',
        middle_name: storeData.middle_name || '',
        email_verified: storeData.email_verified,
        is_active: storeData.is_active || false,
        role_id: storeData.role_id,
        company_id: storeData.company_id,
        password: storeData.password,

        metadata: storeData.metadata
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };
  const deleteUser = async (user_id: number) => {
    const { $api } = useApi();

    try {
      const response = await $api.delete(`/user/${user_id}`);
      return response;
    } catch (e) {
      throw e;
    }
  };
  const changeUserPassword = async (user_id: number, changeData: IChangePasswordDto) => {
    const { $api } = useApi();

    try {
      const response = await $api.patch<IUser>(`/user/${user_id}/change-password`, {
        password: changeData.password,
        confirm_password: changeData.confirm_password
      });
      return response.data;
    } catch (e) {
      throw e;
    }
  };

  return {
    fetchAll,
    fetchItem,
    updateUser,
    createUser,
    deleteUser,
    changeUserPassword
  };
};
export default useUsers;
