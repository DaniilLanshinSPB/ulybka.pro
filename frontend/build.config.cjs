module.exports = {
  apps: [
    {
      name: 'cabinet_frontend',
      script: './.output/server/index.mjs',
      env: {
        APP_NODE_ENV: 'development',
        PORT: 5000
      },
      env_production: {
        APP_NODE_ENV: 'production',
        PORT: 5000
      }
    }
  ]
};
