import type { LogType } from '#imports';
import { defineStore } from 'pinia';

interface IAuthStore {
  user: IAuthUser | undefined;
  role: string | null;
  permissions: string[];
  isLoggedIn: boolean;
  is_verified: boolean;
}

export const useAuthStore = defineStore({
  id: 'auth',
  state: (): IAuthStore => ({
    user: {
      id: 0,
      is_active: false,
      email: '',
      avatar: '',
      first_name: '',
      middle_name: '',
      last_name: '',
      email_verified: false,
      telegram_username: null,
      company: {
        id: 0,
        title: '',
        image: null,
        is_verified: false
      },
      user_notice_permissions: []
    },
    permissions: [],
    role: null,
    isLoggedIn: false,
    is_verified: false
  }),
  getters: {
    getTokenInfo(): any | null {
      const token = localStorage.getItem('access_token') || null;
      let tokenInfo = null;
      if (token) {
        tokenInfo = jwt_decode(token);
      }
      return tokenInfo;
    },
    getUser(state): IAuthUser | undefined {
      return state.user;
    },
    getAccessToken(): string | null {
      return localStorage.getItem('access_token') || null;
    }
  },
  actions: {
    setUser(user: IAuthUser) {
      this.user = user;
    },
    setNotificationTypes(notificationTypes: { slug: LogType; active: boolean }[]) {
      this.notification_permissions = notificationTypes;
    },
    updateUserCompanyData(companyData: { title: string; image: string | null }) {
      if (this.user && this.user.company) {
        this.user.company.title = companyData.title;
        this.user.company.image = companyData.image;
      }
    },
    setRole(role: string) {
      this.role = role;
    },
    setPermissions(permissions: string[]) {
      this.permissions = permissions;
    },
    setVerified(is_verified: boolean) {
      this.is_verified = is_verified;
    },
    setLoggedIn(is_auth: boolean) {
      this.isLoggedIn = is_auth;
    },
    logout() {
      this.resetState();
      localStorage.removeItem('access_token');
    },
    hasAccess(SYSTEM_PERMISSION: string): boolean {
      return this.permissions.includes(SYSTEM_PERMISSION);
    },
    resetState() {
      this.user = {
        id: 0,
        is_active: false,
        email: '',
        avatar: '',
        first_name: '',
        middle_name: '',
        last_name: '',
        email_verified: false,
        company: {
          id: 0,
          title: '',
          is_verified: false
        },
        user_notice_permissions: [],
        telegram_username: ''
      };
      this.isLoggedIn = false;
      this.role = null;
      this.is_verified = false;
      this.permissions = [];
    }
  }
});
