import { defineStore } from 'pinia';
import type { ICompany, ICompanyType } from '~/types/company.type';

interface ICompaniesStore {
  types: ICompanyType[];
  list: ICompany[];
  item: ICompany | null;
  users: IUser[];
  user: IUser | undefined;
  roles: IRole[];
  clinics: ICompany[];
  services: IService[];
  apiKeys: IApiKey[];
  hooks: IWebhook[];
}

const companiesService = useCompanies();

export const useCompanyStore = defineStore({
  id: 'company',
  state: (): ICompaniesStore => ({
    types: [],
    list: [],
    item: null,
    users: [],
    user: undefined,
    roles: [],
    clinics: [],
    services: [],
    apiKeys: [],
    hooks: []
  }),
  getters: {},
  actions: {
    async getItemTitle(company_id: number) {
      if (!this.item || this.item?.id !== company_id) {
        const response = await companiesService.fetchItem(company_id);
        this.item = response;
      }

      return this.item?.title;
    },
    async fetchAll() {
      const response = await companiesService.fetchAll();
      this.list = response as [];
    },

    async fetchTypes() {
      const response = await companiesService.fetchTypes();
      this.types = response as ICompanyType[];
    },

    async fetchItem(id: number) {
      const response = await companiesService.fetchItem(id);
      this.item = response;
    },
    async save(item: ICreateCompanyDto) {
      if (item.id) {
        return await this.update(item);
      } else {
        return await this.create(item);
      }
    },
    async create(item: ICreateCompanyDto) {
      const response = await companiesService.createCompany({
        title: item.title,
        type_id: item.type_id,
        metadata: item.metadata,
        is_verified: item.is_verified,
        image: item.image,
        parent_id: item?.parent_id
      });
      this.list = [...this.list, response];
      return response;
    },
    async update(item: ICreateCompanyDto) {
      if (!item.id) return;

      const response = await companiesService.updateCompany(item.id, {
        title: item.title,
        type_id: item.type_id,
        metadata: item.metadata,
        is_verified: item.is_verified,
        image: item.image,
        parent_id: item?.parent_id
      });
      this.list = this.list.map((company) => (company.id === response.id ? response : company));
      return response;
    },

    async delete(company_id: number) {
      await companiesService.deleteCompany(company_id);
      this.list = this.list.filter((item) => item.id !== company_id);
    },

    async fetchCompanyRoles(company_id: number) {
      const response = await companiesService.fetchRoles(company_id);
      this.roles = response;
    },
    async fetchCompanyUsers(company_id: number, filter: IUserFilterDto = {}) {
      const response = await companiesService.fetchCompanyUsers(company_id, filter);
      this.users = response;
    },
    async fetchCompanyUser(company_id: number, user_id: number) {
      const response = await companiesService.fetchCompanyUser(company_id, user_id);
      this.user = response;
    },

    async createCompanyUser(company_id: number, dto: ICreateCompanyUserDto) {
      const response = await companiesService.createCompanyUser(company_id, dto);
      this.users = [...this.users, response];
    },

    async updateCompanyUser(company_id: number, user_id: number, dto: ICreateCompanyUserDto) {
      const response = await companiesService.updateCompanyUser(company_id, user_id, dto);
      this.users = this.users.map((user) => (user.id === response.id ? response : user));
    },

    async deleteCompanyUser(company_id: number, user_id: number) {
      const response = await companiesService.deleteCompanyUser(company_id, user_id);
      this.users = this.users.filter((item) => item.id !== response.id);
    },

    async fetchCompanyClinics(company_id: number) {
      const response = await companiesService.fetchCompanyClinics(company_id);
      this.clinics = response;
    },
    async saveClinic(company_id: number, item: Partial<ICreateCompanyDto>) {
      if (item.id) {
        await this.updateClinic(company_id, item);
      } else {
        await this.createClinic(company_id, item);
      }
    },
    async createClinic(company_id: number, item: Partial<ICreateCompanyDto>) {
      const response = await companiesService.createCompanyClinic(company_id, {
        title: item.title,
        metadata: item.metadata
      });
      this.clinics = [...this.clinics, response];
    },
    async updateClinic(company_id: number, item: Partial<ICreateCompanyDto>) {
      if (!item.id) return;

      const response = await companiesService.updateCompanyClinic(company_id, item.id, {
        title: item.title,
        metadata: item.metadata
      });
      this.clinics = this.clinics.map((clinic) => (clinic.id === response.id ? response : clinic));
    },
    async deleteClinic(company_id: number, clinic_id: number) {
      await companiesService.deleteCompanyClinic(company_id, clinic_id);
      this.clinics = this.clinics.filter((item) => item.id !== clinic_id);
    },

    async fetchServices(company_id: number) {
      const response = await companiesService.fetchServices(company_id);
      this.services = response;
    },
    async addService(company_id: number, service_id: number) {
      const response = await companiesService.addService(company_id, service_id);
      this.services = [...this.services, response];
    },
    async removeService(company_id: number, service_id: number) {
      await companiesService.removeService(company_id, service_id);
      this.services = this.services.filter((item) => item.id !== service_id);
    },

    async fetchApiKeys(company: number) {
      const response = await companiesService.fetchApiKeys(company);
      this.apiKeys = response;
    },
    async saveApiKey(company: number, dto: ICreateApiKey) {
      const response = await companiesService.createApiKey(company, dto);
      this.apiKeys = [...this.apiKeys, response];
      return response;
    },
    async removeApiKey(company: number, id: number) {
      await companiesService.removeApiKey(company, id);
      this.apiKeys = this.apiKeys.filter((item) => item.id !== id);
    },

    async fetchWebHooks(company: number) {
      const response = await companiesService.fetchHooks(company);
      this.hooks = response;
    },
    async saveWebHook(company: number, dto: ICreateWebhook) {
      const response = await companiesService.createHook(company, dto);
      this.hooks.push(response);
      return response;
    },
    async removeWebHook(company: number, id: number) {
      await companiesService.removeHook(company, id);
      this.hooks = this.hooks.filter((item) => item.id !== id);
    }
  }
});
