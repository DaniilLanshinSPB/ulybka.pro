import { defineStore } from 'pinia';

interface IUsersStore {
  list: IUser[];
  item: IUser | undefined;
  roles: IRole[];
}

const usersService = useUsers();

export const useUsersStore = defineStore({
  id: 'user',
  state: (): IUsersStore => ({
    list: [],
    item: undefined,

    roles: []
  }),
  getters: {},
  actions: {
    async getItemTitle(company_id: number) {
      if (!this.item || this.item?.id !== company_id) {
        const response = await usersService.fetchItem(company_id);
        this.item = response;
      }

      return this.item?.first_name + ' ' + this.item?.last_name + ' ' + this.item?.middle_name;
    },
    async fetchAll(filter?: IUserFilter) {
      const response = await usersService.fetchAll(filter);
      this.list = response as [];
    },

    async fetchItem(id: number) {
      const response = await usersService.fetchItem(id);
      this.item = response;
    },
    async save(item: ICreateUserDto) {
      if (item?.id) {
        await this.update(item);
      } else {
        await this.create(item);
      }
    },
    async create(item: ICreateUserDto) {
      if (!item.company_id || !item.role_id) return;
      const response = await usersService.createUser({
        ...item,
        company_id: item.company_id,
        role_id: item.role_id
      });
      this.list = [...this.list, response];
    },
    async update(item: ICreateUserDto) {
      const response = await usersService.updateUser(item);
      this.list = this.list.map((user) => (user.id === response.id ? response : user));
    },

    async delete(user_id: number) {
      await usersService.deleteUser(user_id);
      this.list = this.list.filter((item) => item.id !== user_id);
    },

    async changePassword(changePasswordData: IChangePasswordDto) {
      await usersService.changeUserPassword(changePasswordData.user_id, changePasswordData);
    }
  }
});
