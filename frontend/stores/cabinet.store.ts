import { defineStore } from 'pinia';
import type { ICompany } from '~/types/company.type';

interface ICabinetStore {
  company: ICompany | undefined;
  clinics: ICompany[];
  clinic: ICompany | undefined;
  services: IService[];
  patients: IPatient[];
  patient: IPatient | undefined;
  orders: IOrder[];
  order: IOrder | undefined;
}

const cabinetService = useCabinet();

export const useCabinetStore = defineStore({
  id: 'cabinet',
  state: (): ICabinetStore => ({
    company: undefined,
    clinics: [],
    clinic: undefined,
    services: [],
    patients: [],
    patient: undefined,
    orders: [],
    order: undefined
  }),
  getters: {},
  actions: {
    async fetchCompany() {
      this.company = await cabinetService.fetchCompany();
    },
    async updateCompany(dto: ICreateCompanyDto) {
      this.company = await cabinetService.updateCompany(dto);
      const authStore = useAuthStore();
      if (this.company) {
        authStore.updateUserCompanyData({
          title: this.company.title,
          image: this.company.image
        });
      }
    },
    async fetchOrders(filter: IOrderFilterDto = {}) {
      this.orders = await cabinetService.fetchOrders(filter);
    },
    async fetchOrder(order_id: number) {
      this.order = await cabinetService.fetchOrder(order_id);
    },
    async save(item: ICreateOrderDto | IUpdateOrderDto) {
      if (item.id) {
        await this.updateOrder(item);
      } else {
        await this.createOrder(item);
      }
    },
    async createOrder(item: ICreateOrderDto) {
      const response = await cabinetService.createOrder({
        description: item.description || '',
        status_id: item.status_id || 1,
        responsible_id: item.responsible_id,
        executor_id: item.executor_id,
        finish_at: item.finish_at,
        patient: item.patient,
        customer_id: item.customer_id,
        services: item.services,
        customer_company_id: item.customer_company_id
      });
      this.orders = [...this.orders, response];
      return response;
    },
    async updateOrder(item: IUpdateOrderDto) {
      if (!item.id) return;
      const response = await cabinetService.updateOrder(item.id, {
        description: item.description,
        status_id: item.status_id,
        responsible_id: item.responsible_id,
        executor_id: item.executor_id,
        patient: item.patient,
        customer_id: item.customer_id,
        finish_at: item.finish_at,
        services: item.services
      });
      this.orders = this.orders.map((order) => (order.id === response.id ? response : order));
    },

    async deleteOrder(order_id: number, deleteReason: string) {
      await cabinetService.softDeleteOrder(order_id, deleteReason);
      this.orders = this.orders.filter((item) => item.id !== order_id);
    },

    async fetchOrderComments(order_id: number) {
      if (this.order) {
        const response = await cabinetService.fetchOrderComments(order_id);
        this.order.comments = response as IOrderComment[];
      }
    },
    async createOrderComment(order_id: number, comment: IOrderCommentCreateDto) {
      if (!this.order) return;
      const response = await cabinetService.createOrderComment(order_id, comment);
      this.order.comments?.push(response);
    },

    async deleteOrderComment(order_id: number, comment_id: number) {
      if (this.order && this.order.comments) {
        await cabinetService.deleteOrderComment(order_id, comment_id);
        this.order.comments = this.order.comments.filter((order) => order.id !== comment_id);
      }
    },

    async fetchClinics() {
      this.clinics = await cabinetService.fetchClinics();
    },
    async fetchItemClinic(clinic_id: number) {
      this.clinic = await cabinetService.fetchItemClinic(clinic_id);
    },
    async saveClinic(item: Partial<ICreateCompanyDto>) {
      if (item.id) {
        await this.updateClinic(item);
      } else {
        await this.createClinic(item);
      }
    },
    async createClinic(item: Partial<ICreateCompanyDto>) {
      const response = await cabinetService.createClinic({
        title: item.title,
        metadata: item.metadata
      });
      this.clinics = [...this.clinics, response];
    },
    async updateClinic(item: Partial<ICreateCompanyDto>) {
      if (!item.id) return;

      const response = await cabinetService.updateClinic(item.id, {
        title: item.title,
        metadata: item.metadata
      });
      this.clinics = this.clinics.map((clinic) => (clinic.id === response.id ? response : clinic));
    },
    async deleteClinic(clinic_id: number) {
      await cabinetService.deleteClinic(clinic_id);
      this.clinics = this.clinics.filter((item) => item.id !== clinic_id);
    },
    async fetchServices() {
      this.services = await cabinetService.fetchServices();
    },
    async fetchPatients(clinic_id: number) {
      this.patients = await cabinetService.fetchAllPatients(clinic_id);
    },

    async fetchPatient(patient_uid: string) {
      this.patient = await cabinetService.fetchPatient(patient_uid);
    },
    async savePatient(company_id: number, patient: IPatient): Promise<IPatient> {
      if (patient.uid) {
        return await this.updatePatient(company_id, patient);
      } else {
        return await this.createPatient(company_id, patient);
      }
    },
    async createPatient(company_id: number, patient: IPatient): Promise<IPatient> {
      const response = await cabinetService.createPatient(company_id, patient);
      this.patients = [...this.patients, response];
      return response;
    },
    async updatePatient(company_id: number, patient: IPatient): Promise<IPatient> {
      const response = await cabinetService.updatePatient(company_id, patient);
      this.patients = this.patients.map((item) => (item.uid === patient.uid ? patient : item));
      return response;
    },
    async deletePatient(company_id: number, patient_uid: string) {
      await cabinetService.deletePatient(company_id, patient_uid);
      this.patients = this.patients.filter((item) => item.uid !== patient_uid);
    }
  }
});
