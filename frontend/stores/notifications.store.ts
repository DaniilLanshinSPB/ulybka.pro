import useNotifications from '~/composables/useNotifications';
import type { INotification } from '~/types/notification.type';

interface INotificationsStore {
  notifications: INotification[];
}
const notificationsService = useNotifications();

export const useNotificationsStore = defineStore({
  id: 'notifications',
  state: (): INotificationsStore => ({
    notifications: []
  }),
  getters: {},
  actions: {
    addNotification(message: any) {
      this.notifications.push(message);
    },
    async fetchUnread() {
      this.notifications = await notificationsService.fetchUnread();
    },
    async readNotification(id: number) {
      await notificationsService.read(id);
      this.notifications = this.notifications.filter((n) => n.id !== id);
    }
  }
});
