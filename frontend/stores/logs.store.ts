interface INotificationsStore {
  list: ILog[];
}
const logsService = useLogs();

export const useLogsStore = defineStore({
  id: 'logs',
  state: (): INotificationsStore => ({
    list: []
  }),
  getters: {},
  actions: {
    async fetchAll(filters?: ILogFilter) {
      this.list = await logsService.fetchAll(filters);
    }
  }
});
