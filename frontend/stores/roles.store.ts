import { defineStore } from 'pinia';

interface IRolesStore {
  list: IRole[];
  item: IRole | null;
  permissions: IPermission[];
  notificationTypes: INotificationType[];
}

const rolesService = useRoles();

export const useRolesStore = defineStore({
  id: 'role',
  state: (): IRolesStore => ({
    list: [],
    item: null,
    permissions: [],
    notificationTypes: []
  }),
  getters: {},
  actions: {
    async fetchAll() {
      const response = await rolesService.fetchRoles();
      this.list = response as [];
    },

    async fetchAllPermissions() {
      this.permissions = await rolesService.fetchPermissions();
    },
    async setPermissions(role_id: number, permissions: number[]) {
      this.item = await rolesService.setPermissions(role_id, permissions);
    },
    async fetchNotificationTypes() {
      this.notificationTypes = await rolesService.fetchNotificationTypes();
    },
    async create(storeData: Partial<ICreateRoleDto>) {
      const response = await rolesService.create(storeData);
      this.list = [...this.list, response];
    },
    async update(role_id: number, storeData: Partial<ICreateRoleDto>) {
      const response = await rolesService.update(role_id, storeData);
      this.list = this.list.map((role) => (role.id === response.id ? response : role));
    },
    async save(storeData: Partial<ICreateRoleDto>) {
      if (storeData.id) {
        await this.update(storeData.id, storeData);
      } else {
        await this.create(storeData);
      }
    },
    async delete(role_id: number) {
      await rolesService.deleteRole(role_id);
      this.list = this.list.filter((role) => role.id !== role_id);
    }
  }
});
