import { defineStore } from 'pinia';
import type { IFilterFieldDto } from '~/types/field.type';

interface IFieldsStore {
  list: IField[];
  tree: IField[];
  item: IField | null;
}

const fieldService = useFields();

export const useFieldStore = defineStore({
  id: 'field',
  state: (): IFieldsStore => ({
    list: [],
    tree: [],
    item: null
  }),
  getters: {},
  actions: {
    parentField(parent_id: number) {
      return this.list.find((field: IField) => field.id === parent_id);
    },
    async getItemTitle(field_id: number) {
      if (!this.item || this.item?.id !== field_id) {
        const response = await fieldService.fetchItem(field_id);
        this.item = response;
      }

      return this.item?.name;
    },

    async fetchTree(filter?: IFilterFieldDto) {
      const response = await fieldService.fetchTree(filter);
      this.tree = response as [];
    },
    async fetchAll(filter?: IFilterFieldDto) {
      const response = await fieldService.fetchAll(filter);
      this.list = response as [];
    },

    async fetchItem(id: number) {
      const response = await fieldService.fetchItem(id);
      this.item = response;
    },
    async save(item: ICreateFieldDto) {
      if (item.id) {
        await this.update(item);
      } else {
        await this.create(item);
      }
    },
    async create(item: ICreateFieldDto) {
      const response = await fieldService.create({
        technical_name: item.technical_name || item.name,
        active: item.active,
        name: item.name,
        group: item.group,
        multiple: item.multiple,
        type: item.type,
        directory_id: item.directory_id,
        placeholder: item.placeholder,
        parent_id: item.parent_id,
        sort: item.sort || 0,
        display_conditions: item.display_conditions?.toString() || null,
        metadata: item.metadata,
        required: item.required
      });
      this.list = [...this.list, response];
    },
    async update(item: ICreateFieldDto) {
      if (!item.id) return;
      const response = await fieldService.update(item.id, {
        active: item.active,

        technical_name: item.technical_name || item.name,
        name: item.name,
        group: item.group,
        multiple: item.multiple,
        type: item.type,
        placeholder: item.placeholder,
        directory_id: item.directory_id,
        parent_id: item.parent_id,
        sort: item.sort || 0,
        display_conditions: item.display_conditions?.toString() || null,
        metadata: item.metadata,
        required: item.required
      });
      this.list = this.list.map((company) => (company.id === response.id ? response : company));
    },

    async delete(field_id: number) {
      await fieldService.deleteField(field_id);
      this.list = this.list.filter((item) => item.id !== field_id);
    },

    async copy(field_id: number) {
      const response = await fieldService.copyField(field_id);
      this.fetchTree();
    }
  }
});
