import { defineStore } from 'pinia';

interface IAppStore {
  theme: {
    darkMode: boolean;
  };
}

export const useAppStore = defineStore({
  id: 'app',
  state: (): IAppStore => ({
    theme: {
      darkMode: false
    }
  }),
  getters: {
    themeMode(state) {
      localStorage.getItem('theme') === 'dark' ? (state.theme.darkMode = true) : (state.theme.darkMode = false);
      return state.theme.darkMode;
    }
  },
  actions: {
    setDarkMode(darkMode: boolean) {
      this.theme.darkMode = darkMode;
      localStorage.setItem('theme', darkMode ? 'dark' : 'light');
    }
  }
});
