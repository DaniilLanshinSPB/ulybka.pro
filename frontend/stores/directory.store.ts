import { defineStore } from 'pinia';

interface IDirectoryStore {
  list: IDirectory[];
  item: IDirectory | null;
}

const directoryService = useDirectories();

export const useDirectoryStore = defineStore({
  id: 'directory',
  state: (): IDirectoryStore => ({
    list: [],
    item: null
  }),
  getters: {},
  actions: {
    async getItemTitle(directory_id: number) {
      if (!this.item || this.item?.id !== directory_id) {
        const response = await directoryService.fetchItem(directory_id);
        this.item = response;
      }

      return this.item?.title;
    },
    async fetchAll() {
      const response = await directoryService.fetchAll();
      this.list = response as [];
    },

    async fetchItem(id: number) {
      const response = await directoryService.fetchItem(id);
      this.item = response;
    },
    async save(item: IDirectoryCreateDto) {
      if (item.id) {
        await this.update(item);
      } else {
        await this.create(item);
      }
    },
    async create(item: IDirectoryCreateDto) {
      const response = await directoryService.create({
        title: item.title,
        options: this._prepareOptions(item.options) || []
      });
      this.list = [...this.list, response];
    },
    async update(item: IDirectoryCreateDto) {
      if (!item.id) return;
      const response = await directoryService.update(item.id, {
        title: item.title,
        options: this._prepareOptions(item.options) || []
      });
      this.list = this.list.map((company) => (company.id === response.id ? response : company));
    },

    async delete(directory_id: number) {
      await directoryService.deleteDirectory(directory_id);
      this.list = this.list.filter((item) => item.id !== directory_id);
    },

    _prepareOptions(options: string[]): string[] {
      return options.filter((option) => option.length > 0);
    }
  }
});
