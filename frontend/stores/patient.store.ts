import { defineStore } from 'pinia';
import usePatients from '~/composables/usePatients';

interface IPatientStore {
  list: IPatient[];
  item: IPatient | undefined;
}

const patientService = usePatients();

export const usePatientStore = defineStore({
  id: 'patient',
  state: (): IPatientStore => ({
    list: [],
    item: undefined
  }),
  getters: {},
  actions: {
    async getItemTitle(company_id: number, patient_uid: string) {
      if (!this.item || this.item?.uid !== patient_uid) {
        const response = await patientService.fetchItem(company_id, patient_uid);
        this.item = response;
      }

      return this.item?.fio;
    },
    async fetchAll(company_id: number) {
      const response = await patientService.fetchAll(company_id);
      this.list = response as [];
    },

    async fetchItem(company_id: number, uid: string) {
      const response = await patientService.fetchItem(company_id, uid);
      this.item = response;
    },
    async save(company_id: number, patient: IPatient): Promise<IPatient> {
      if (patient.uid) {
        return await this.update(company_id, patient);
      } else {
        return await this.create(company_id, patient);
      }
    },
    async create(company_id: number, patient: IPatient): Promise<IPatient> {
      const response = await patientService.create(company_id, patient);
      this.list = [...this.list, response];
      return response;
    },
    async update(company_id: number, patient: IPatient): Promise<IPatient> {
      const response = await patientService.update(company_id, patient);
      this.list = this.list.map((item) => (item.uid === patient.uid ? patient : item));
      return response;
    },
    async delete(company_id: number, patient_uid: string) {
      await patientService.deletePatient(company_id, patient_uid);
      this.list = this.list.filter((item) => item.uid !== patient_uid);
    },
    _reset() {
      this.list = [];
      this.item = undefined;
    }
  }
});
