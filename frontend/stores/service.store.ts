import { defineStore } from 'pinia';

interface IServicesStore {
  categories: IServiceCategory[];
  list: IService[];
  item: IService | null;
  directories: IDirectory[];
}

const servicesService = useServices();

export const useServiceStore = defineStore({
  id: 'service',
  state: (): IServicesStore => ({
    categories: [],
    list: [],
    item: null,
    directories: []
  }),
  getters: {},
  actions: {
    async getItemTitle(service_id: number) {
      if (!this.item || this.item?.id !== service_id) {
        const response = await servicesService.fetchItem(service_id);
        this.item = response;
      }

      return this.item?.title;
    },
    async fetchAll() {
      const response = await servicesService.fetchAll();
      this.list = response as [];
    },

    async fetchCategories() {
      const response = await servicesService.fetchCategories();
      this.categories = response as IServiceCategory[];
    },

    async fetchItem(id: number) {
      const response = await servicesService.fetchItem(id);
      this.item = response;
    },
    async save(item: IServiceCreateDto) {
      if (item.id) {
        await this.update(item);
      } else {
        await this.create(item);
      }
    },
    async create(item: IServiceCreateDto) {
      const response = await servicesService.create({
        title: item.title,
        category_id: item.category_id,
        is_default: item.is_default || false,
        is_active: item.is_active,
        fields: item.fields
      });
      this.list = [...this.list, response];
    },
    async update(item: IServiceCreateDto) {
      if (!item.id) return;
      const response = await servicesService.update(item.id, {
        title: item.title,
        category_id: item.category_id,
        is_default: item.is_default,
        is_active: item.is_active,
        fields: item.fields
      });

      this.list = this.list.map((service) => (service.id === response.id ? response : service));
    },

    async delete(service_id: number) {
      await servicesService.deleteService(service_id);
      this.list = this.list.filter((item) => item.id !== service_id);
    },

    async saveCategory(dto: IServiceCategoryCreateDto) {
      if (dto.id) {
        await this.updateCategory(dto);
      } else {
        await this.addCategory(dto);
      }
    },
    async addCategory(dto: IServiceCategoryCreateDto) {
      const response = await servicesService.addCategory(dto);
      this.categories = [...this.categories, response];
    },
    async updateCategory(dto: IServiceCategoryCreateDto) {
      const response = await servicesService.updateCategory(dto);
      this.categories = this.categories.map((category) => (category.id === response.id ? response : category));
    },
    async deleteCategory(id: number) {
      await servicesService.deleteCategory(id);
      this.categories = this.categories.filter((item) => item.id !== id);
    },
    async fetchServiceDirectories(id: number) {
      const directories = await servicesService.fetchServiceDirectories(id);
      this.directories = directories;
    }
  }
});
