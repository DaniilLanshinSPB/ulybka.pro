import { defineStore } from 'pinia';
import type { ICreateOrderDto, IOrderCommentCreateDto, IOrderStatus, IUpdateOrderDto } from '~/types/order.type';

interface IOrderStore {
  statuses: IOrderStatus[];
  list: IOrder[];
  item: IOrder | null;
}

const orderService = useOrders();

export const useOrderStore = defineStore({
  id: 'order',
  state: (): IOrderStore => ({
    statuses: [],
    list: [],
    item: null
  }),
  getters: {},
  actions: {
    async getItemTitle(company_id: number, order_id: number) {
      if (!this.item || this.item?.id !== order_id) {
        const response = await orderService.fetchItem(company_id, order_id);
        this.item = response;
      }

      return this.item?.title;
    },

    async fetchAll(company_id: number, filter: IOrderFilterDto = {}) {
      const response = await orderService.fetchAll(company_id, filter);
      this.list = response as [];
    },

    async fetchStatuses() {
      const response = await orderService.fetchStatuses();
      this.statuses = response as IOrderStatus[];
    },

    async fetchItem(company_id: number, id: number) {
      const response = await orderService.fetchItem(company_id, id);

      this.item = response;
    },
    async save(company_id: number, item: ICreateOrderDto | IUpdateOrderDto) {
      if (item.id) {
        await this.update(company_id, item);
      } else {
        await this.create(company_id, item);
      }
    },
    async create(company_id: number, item: ICreateOrderDto) {
      const response = await orderService.create(company_id, {
        description: item.description || '',
        status_id: item.status_id || 1,
        responsible_id: item.responsible_id,
        executor_id: item.executor_id,
        finish_at: item.finish_at,
        patient: item.patient,
        customer_id: item.customer_id,
        services: item.services,
        customer_company_id: item.customer_company_id
      });
      this.list = [...this.list, response];
      return response;
    },
    async update(company_id: number, item: IUpdateOrderDto) {
      if (!item.id) return;
      const response = await orderService.update(company_id, item.id, {
        description: item.description,
        status_id: item.status_id,
        responsible_id: item.responsible_id,
        executor_id: item.executor_id,
        patient: item.patient,
        customer_id: item.customer_id,
        finish_at: item.finish_at,
        services: item.services
      });
      this.list = this.list.map((order) => (order.id === response.id ? response : order));
    },

    async delete(company_id: number, order_id: number, deleteReason: string) {
      await orderService.softDeleteOrder(company_id, order_id, deleteReason);
      this.list = this.list.filter((item) => item.id !== order_id);
    },
    async hardDelete(company_id: number, order_id: number) {
      await orderService.deleteOrder(company_id, order_id);
      this.list = this.list.filter((item) => item.id !== order_id);
    },
    async restore(company_id: number, order_id: number) {
      await orderService.restore(company_id, order_id);
      this.list = this.list.filter((item) => item.id !== order_id);
    },

    async fetchComments(company_id: number, order_id: number) {
      if (this.item) {
        const response = await orderService.fetchOrderComments(company_id, order_id);
        this.item.comments = response as IOrderComment[];
      }
    },
    async createComment(company_id: number, order_id: number, comment: IOrderCommentCreateDto) {
      if (!this.item) return;
      const response = await orderService.createOrderComment(company_id, order_id, comment);
      this.item.comments?.push(response);
    },

    async deleteComment(company_id: number, order_id: number, comment_id: number) {
      if (this.item && this.item.comments) {
        await orderService.deleteOrderComment(company_id, order_id, comment_id);
        this.item.comments = this.item.comments.filter((item) => item.id !== comment_id);
      }
    }
  }
});
