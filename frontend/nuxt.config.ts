// https://nuxt.com/docs/api/configuration/nuxt-config

const isProduction = process.env.NODE_ENV === 'production';
const plugins = ['@/plugins/draggable', '~/plugins/primevue-confirmation-service.ts', '~/plugins/app-page.ts', '~/plugins/socket.io.ts'];
const modules = ['nuxt-primevue', '@pinia/nuxt', 'vue-yandex-maps/nuxt'];
const css = ['primeicons/primeicons.css', 'primeflex/primeflex.scss', 'primevue/resources/primevue.min.css', '@/assets/styles.scss'];

export default defineNuxtConfig({
  devtools: { enabled: true },
  ssr: false,
  generate: {
    fallback: true,
    routes: ['/']
  },
  runtimeConfig: {
    public: {
      appUrl: process.env.APP_URL,
      apiPath: process.env.APP_API_URL,
      api1CPath: process.env.APP_1C_URL,
      api1CKey: process.env.APP_1C_API_KEY,
      filePath: process.env.FILE_PATH,
      siteUrl: process.env.APP_PUBLIC_URL,
      siteName: process.env.APP_PUBLIC_SITE_NAME,
      siteDescription: process.env.APP_PUBLIC_SITE_DESCRIPTION,
      socketUrl: process.env.SOCKET_URL,
      titleSeparator: '|'
    }
  },

  head: {
    meta: [
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1.0, user-scalable=no'
      }
    ]
  },
  app: {
    head: {
      title: process.env.APP_PUBLIC_SITE_NAME,
      link: [
        {
          id: 'theme-css',
          rel: 'stylesheet',
          type: 'text/css',
          href: '/themes/aura-light-blue/theme.css'
        }
      ]
    }
  },
  modules: modules,
  plugins: plugins,
  yandexMaps: {
    apikey: 'ab9b6625-4052-491b-bd60-a06fa0e90098'
  },

  css: css,
  primevue: {
    options: { ripple: true }
  },
  // pwa: {
  //   icon: {
  //     source: '/public/favicon/icon.png',
  //   }
  // },
  pinia: {
    autoImports: ['defineStore', 'acceptHMRUpdate']
  },
  imports: {
    dirs: [
      // Scan top-level modules
      'composables',
      // ... or scan modules nested one level deep with a specific name and file extension
      'composables/*/index.{ts,js,mjs,mts}',
      // ... or scan all modules within given directory
      'composables/**',
      'stores',
      'types',
      'types/**',
      'utils',
      'utils/**'
    ]
  }
});
