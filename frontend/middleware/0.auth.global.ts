export const AUTH_PAGES = ['/auth/register', '/auth/forgot-password', '/auth/confirm-account'];
export const GUEST_PAGES = [...AUTH_PAGES, '/'];
export default defineNuxtRouteMiddleware(async (to: { path: string }, from: any) => {
  const config = useRuntimeConfig();
  const authStore = useAuthStore();
  const AuthService = useAuth();

  try {
    if (localStorage.getItem('access_token') && !authStore.isLoggedIn) {
      await AuthService.checkAuth();
    }
  } catch (err) {
    localStorage.removeItem('access_token');
    return navigateTo('/error');
  }

  if (authStore.isLoggedIn && '/' === to.path) {
    return navigateTo('/profile');
  }

  if (to.path === '/auth/confirm-account' && authStore.isLoggedIn && !authStore.is_verified) {
    return true;
  } else if (!AUTH_PAGES.includes(to.path) && authStore.isLoggedIn && !authStore.is_verified) {
    return navigateTo('/auth/confirm-account');
  }

  if (AUTH_PAGES.includes(to.path) && authStore.isLoggedIn) {
    return navigateTo('/cabinet');
  }
  if (!GUEST_PAGES.includes(to.path) && !authStore.isLoggedIn) {
    return navigateTo('/');
  }

  if (
    authStore.user.company !== null &&
    authStore.user.company?.id !== 0 &&
    !authStore.user.company?.is_verified &&
    to.path !== '/verify_company' &&
    authStore.isLoggedIn
  ) {
    return navigateTo('/verify_company');
  } else if (authStore.user.company?.is_verified && to.path === '/verify_company' && authStore.isLoggedIn) {
    return navigateTo('/profile');
  }

  return true;
});
