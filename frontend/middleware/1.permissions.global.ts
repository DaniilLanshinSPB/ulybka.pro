export default defineNuxtRouteMiddleware(async (to: { path: string; meta: any }, from: any) => {
  const config = useRuntimeConfig();
  const authStore = useAuthStore();

  const userPermissions = authStore.permissions || [];
  const pagePermissions = to.meta?.permissions || [];

  if (pagePermissions.length === 0) {
    return true;
  }
  
  if (pagePermissions.some((item: string) => userPermissions.includes(item))) {
    return true;
  }

  return navigateTo('/access');
});
