import { usePrimeVue } from 'primevue/config';
import { useLayout } from '~/layouts/composables/layout';

export default defineNuxtRouteMiddleware(async (to: { path: string }, from: any) => {
  const appStore = useAppStore();
  const { layoutConfig, onMenuToggle } = useLayout();

  const isDarkTheme = computed(() => appStore.themeMode);
  const $primevue = usePrimeVue();

  if (layoutConfig.darkTheme.value !== isDarkTheme.value) {
    layoutConfig.darkTheme.value = isDarkTheme.value;

    const theme = layoutConfig.darkTheme.value
      ? layoutConfig.theme.value.replace('light', 'dark')
      : layoutConfig.theme.value.replace('dark', 'light');

    $primevue.changeTheme(layoutConfig.theme.value, theme, 'theme-css', () => {
      layoutConfig.theme.value = theme;
      layoutConfig.darkTheme.value = layoutConfig.darkTheme.value;
    });
  }

  return true;
});
