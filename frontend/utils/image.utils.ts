export const FILE_URL = `${useRuntimeConfig().public.filePath}`;
export const PLACEHOLDER_IMAGE = '/images/common/image-placeholder.svg';
export const PLACEHOLDER_LOGO = '/images/common/logo-placeholder.png';

export async function fileToBase64(file: File): Promise<string | ArrayBuffer | null> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = () => {
      resolve(reader.result);
    };

    reader.onerror = (error) => {
      reject(error);
    };

    reader.readAsDataURL(file);
  });
}

export function isBase64(str: string): boolean {
  if (str === '' || str.trim() === '') return false;
  try {
    return str.startsWith('data:image/');
  } catch (err) {
    return false;
  }
}

export function getExtension(file: string): string {
  if (file.includes('.')) {
    const parts = file.split('.');

    return parts[parts.length - 1];
  }

  return '';
}
export function fileUrl(filePath: string): string {
  return `${FILE_URL}/${filePath}`;
}
