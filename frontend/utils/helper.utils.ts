//Обрезаем текст для создания небольшого описания
export function trimText(text: string, length: number = 120) {
  if (text.length > length) {
    return text.substring(0, length) + '...';
  }
  return text;
}
export function truncateText(text: string, startLength: number, endLength: number) {
  if (text.length <= startLength + endLength) {
    return text;
  }

  const start = text.slice(0, startLength);
  const end = text.slice(-endLength);
  return `${start}...${end}`;
}

//Конвертирование строки в число + удаление лишних символов
export function convertToNumber(value: string | number): number {
  if (!value) return 0;
  if (typeof value === 'number') return value;
  const cleanedStr = value.replace(/[^\d.-]/g, '');
  const number = parseFloat(cleanedStr);
  return number;
}

export function jwt_decode(token: any) {
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  const jsonPayload = decodeURIComponent(
    atob(base64)
      .split('')
      .map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join('')
  );
  return JSON.parse(jsonPayload);
}
export function getTagByFirstLetter(word: string) {
  const code = word.charCodeAt(0);
  const colors = ['secondary', 'success', 'info', 'warning', 'danger', 'contrast'];

  return colors[code % (colors.length - 1)];
}

export function localeFilter() {
  const primevue = usePrimeVue();
  primevue.config.locale = {
    accept: 'Да',
    addRule: 'Добавить правило',
    am: 'AM',
    apply: 'Применить',
    cancel: 'Отменить',
    choose: 'Выбрать',
    chooseDate: 'Выбрать дату',
    chooseMonth: 'Выбрать месяц',
    chooseYear: 'Выбрать год',
    clear: 'Очистить',
    completed: 'Завершено',
    contains: 'Содержит',
    dateAfter: 'Дата после',
    dateBefore: 'Дата до',
    dateFormat: 'dd.mm.yy',
    dateIs: 'Дата равна',
    dateIsNot: 'Дата не равна',
    dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
    dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    dayNamesShort: ['Вос', 'Пон', 'Вто', 'Сре', 'Чет', 'Пят', 'Суб'],
    emptyFilterMessage: 'Результаты не найдены',
    emptyMessage: 'Доступных опций нет',
    emptySearchMessage: 'Результаты не найдены',
    emptySelectionMessage: 'Не выбран элемент',
    endsWith: 'Заканчивается на',
    equals: 'Равно',
    fileSizeTypes: ['Б', 'КБ', 'МБ', 'ГБ', 'ТБ', 'ПБ', 'ЭБ', 'ЗБ', 'ЙБ'],
    firstDayOfWeek: 1,
    gt: 'Больше чем',
    gte: 'Больше или равно',
    lt: 'Меньше чем',
    lte: 'Меньше или равно',
    matchAll: 'Соответствует всем',
    matchAny: 'Соответствует любому',
    medium: 'Средний',
    monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
    nextDecade: 'Следующее десятилетие',
    nextHour: 'Следующий час',
    nextMinute: 'Следующая минута',
    nextMonth: 'Следующий месяц',
    nextSecond: 'Следующая секунда',
    nextYear: 'Следующий год',
    noFilter: 'Без фильтра',
    notContains: 'Не содержит',
    notEquals: 'Не равно',
    passwordPrompt: 'Введите пароль',
    pending: 'В ожидании',
    pm: 'PM',
    prevDecade: 'Предыдущее десятилетие',
    prevHour: 'Предыдущий час',
    prevMinute: 'Предыдущая минута',
    prevMonth: 'Предыдущий месяц',
    prevSecond: 'Предыдущая секунда',
    prevYear: 'Предыдущий год',
    reject: 'Нет',
    removeRule: 'Удалить правило',
    searchMessage: '{0} результатов доступно',
    selectionMessage: '{0} элементов выбрано',
    showMonthAfterYear: false,
    startsWith: 'Начинается с',
    strong: 'Сильный',
    today: 'Сегодня',
    upload: 'Загрузить',
    weak: 'Слабый',
    weekHeader: 'Нед'
  };
}

export function fullName(user: IAuthUser | IOrderUser): string {
  return user.first_name ? `${user.first_name}${user.last_name ? ' ' + user.last_name[0] + '.' : ''}${user.middle_name ? user.middle_name[0] + '.' : ''}`: user.email;
}

export function hasPermission(permission: CABINET_PERMISSIONS | CABINET_PERMISSIONS[]) {
  if (!Array.isArray(permission)) {
    permission = [permission];
  }
  const authStore = useAuthStore();
  return useAuthStore().permissions?.some((p) => permission.includes(p)) ?? false;
}

export function getLogoLink(): string {
  const authStore = useAuthStore();
  if (!authStore.isLoggedIn) {
    return '/';
  }
  const userPermissions = authStore.permissions || [];

  if (userPermissions.includes(CABINET_PERMISSIONS.CABINET_VIEW_ORDER)) {
    return '/cabinet';
  }
  if (userPermissions.includes(CABINET_PERMISSIONS.CABINET_VIEW_PATIENT)) {
    return '/cabinet/patients';
  }
  if (userPermissions.includes(CABINET_PERMISSIONS.CABINET_VIEW_CLINICS)) {
    return '/cabinet/clinics';
  }
  if (userPermissions.includes(CABINET_PERMISSIONS.CABINET_MY_COMPANY_EDIT)) {
    return '/cabinet/company';
  }

  return '/profile';
}
export function getUserCompanyLogo(): string {
  const FILE_URL = `${useRuntimeConfig().public.filePath}`;

  const authStore = useAuthStore();
  if (authStore.user?.company?.image) {
    return FILE_URL + '/' + authStore.user?.company?.image;
  }
  return '/images/common/logo-placeholder.png';
}
export function delay(time: number) {
  return new Promise((resolve) => setTimeout(resolve, time));
}
