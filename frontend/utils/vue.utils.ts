import type { ToastServiceMethods } from 'primevue/toastservice';

const toastError = (toast: ToastServiceMethods) => (message: string, summary?: string) =>
  toast.add({ severity: 'error', summary: summary ?? 'Ошибка', detail: message, life: 3000 });
const toastWarning = (toast: ToastServiceMethods) => (message: string, summary?: string) =>
  toast.add({ severity: 'warn', summary: summary ?? 'Внимание', detail: message, life: 3000 });
const toastSuccess = (toast: ToastServiceMethods) => (message: string, summary?: string) =>
  toast.add({ severity: 'success', summary: summary ?? 'Успешно', detail: message, life: 3000 });
const toastInfo = (toast: ToastServiceMethods) => (message: string, summary?: string) =>
  toast.add({ severity: 'info', summary: summary ?? 'Успешно', detail: message, life: 3000 });

export function getToasts(toast: ToastServiceMethods) {
  return {
    error: toastError(toast),
    warning: toastWarning(toast),
    success: toastSuccess(toast),
    info: toastInfo(toast)
  };
}
