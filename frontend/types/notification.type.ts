import type { LogType } from '#imports';

export interface INotificationType {
  slug: LogType;
  active: boolean;
}
export interface INotification {
  id: number;
  is_read: boolean;
  message: string;
  metadata: any;
  created_at: Date;
}
