export interface IService {
  id: number;
  title: string;
  category: IServiceCategory;
  fields?: IField[];
  is_default: boolean;
  is_active: boolean;
}

export interface IServiceCategory {
  id: number;
  title: string;
  services?: IService[];
}
export interface IServiceCategoryCreateDto {
  id?: number;
  title: string;
}

export interface IServiceCreateDto {
  id?: number;
  title: string;
  category_id: number;
  is_default?: boolean;
  is_active: boolean;
  fields?: number[];
}
