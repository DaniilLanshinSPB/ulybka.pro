export interface IUserFilter {
  search: string | undefined;
  is_active: boolean | undefined;
  email_verified: boolean | undefined;
  role_ids: number[] | undefined;
  company_id: number | undefined;
}
export interface ICommonUser {
  id: number;
  is_active: boolean;
  email: string;
  avatar: string;
  first_name: string;
  middle_name: string;
  last_name: string;
  email_verified: boolean;
  telegram_username: string | null;
  company?: IUserCompany;
}
export interface IUserCompany {
  id: number;
  title: string;
  is_verified: boolean;
  image?: undefined | null | string;
}

export enum LogType {
  COMMENT = 'COMMENT',
  ORDER = 'ORDER',
  PATIENT = 'PATIENT',
  SYSTEM = 'SYSTEM',
  TELEGRAM = 'TELEGRAM'
}
export interface IAuthUser extends ICommonUser {
  user_notice_permissions: { slug: LogType; active: boolean }[];
}
export interface IOrderUser extends ICommonUser {}

export interface IUser extends ICommonUser {
  role?: IRole;
}

export interface IRole {
  id: number;
  slug?: string;
  name: string;
  is_default?: boolean;
  permissions?: IPermission[];
  company_type_id?: number;
  notification_permissions?: INoticeType[];
}
export interface ICreateRoleDto {
  id?: number;
  slug?: string;
  name: string;
  is_default: boolean;
  permissions?: IPermission[];
  notification_permissions?: INoticeType[];
  company_type_id: number;
}

export enum Role {
  SuperAdmin = 'SuperAdmin',
  Admin = 'Admin',
  Manager = 'Manager',
  MainTechnic = 'MainTechnic',
  Technic = 'Technic',
  Doctor = 'Doctor'
}

export const ROLES = {
  [Role.SuperAdmin]: 1,
  [Role.Admin]: 2,
  [Role.MainTechnic]: 3,
  [Role.Technic]: 4,
  [Role.Doctor]: 5,
  [Role.Manager]: 6
};

export const DEFAULT_ROLE: IRole = {
  id: 5,
  name: Role.Doctor
};

export interface ILoginByUserDto {
  email: string;
}

export interface IResendCodeDto {
  email: string;
}

export interface IChangePasswordDto {
  user_id: number;
  email?: string;
  password: string;
  confirm_password: string;
}

export interface ICreateUserDto {
  id?: number;
  email: string;
  password?: string;
  confirm_password?: string;
  avatar?: string;
  first_name: string;
  middle_name: string;
  last_name: string;
  is_active: true;
  role_id: number;
  company_id: number;
  email_verified: boolean;
}
export interface ICreateCompanyUserDto {
  id?: number;
  password?: string;
  confirm_password?: string;
  email: string;
  first_name: string;
  middle_name: string;
  last_name: string;
  is_active: boolean;
  email_verified: boolean;
  role_id: number;
  role?: IRole;
}
export interface IUpdateUserDto {
  email: string;
  avatar: string;
  first_name: string;
  middle_name: string;
  last_name: string;
  is_active: true;
  role_id: number;
  company_id: number;
  email_verified: boolean;
}

export interface IUserFilterDto {
  role_ids?: number;
}
export interface IChangeAvatarDto {
  avatar: string;
}
export interface IChangeProfileDto {
  first_name: string;
  last_name: string;
  middle_name: string;
  email: string;
  avatar: string;
  user_notice_permissions: INoticeType[];
}
export interface IChangeProfilePasswordDto {
  old_password: string;
  password: string;
  confirm_password: string;
}

enum EBillingType {
  bill = 'bill',
  online = 'online',
  other = 'other'
}

export const BillingTypes: Record<EBillingType, string> = {
  [EBillingType.bill]: 'Выставить счет',
  [EBillingType.online]: 'Оплата онлайн',
  [EBillingType.other]: 'Другой способ'
};
export interface IPermission {
  id: number;
  name: string;
  slug: string;
  category: string;
  is_system: boolean;
  type: PermissionType;
}

export enum PermissionType {
  SYSTEM = 'SYSTEM',
  LABORATORY = 'LABORATORY',
  PHARMACY = 'PHARMACY'
}

export enum SYSTEM_PERMISSIONS {
  SYSTEM_VIEW_LOGS = 'system_logs_view',

  SYSTEM_VIEW_USER = 'system_user_view',
  SYSTEM_CREATE_USER = 'system_user_create',
  SYSTEM_UPDATE_USER = 'system_user_update',
  SYSTEM_DELETE_USER = 'system_user_delete',

  SYSTEM_VIEW_COMPANIES = 'system_companies_view',
  SYSTEM_CREATE_COMPANIES = 'system_companies_create',
  SYSTEM_UPDATE_COMPANIES = 'system_companies_update',
  SYSTEM_DELETE_COMPANIES = 'system_companies_delete',

  SYSTEM_VIEW_COMPANY_SERVICE = 'system_company_service_view',
  SYSTEM_CREATE_COMPANY_SERVICE = 'system_company_service_create',
  SYSTEM_DELETE_COMPANY_SERVICE = 'system_company_service_delete',

  SYSTEM_VIEW_DIRECTORY = 'system_directory_view',
  SYSTEM_CREATE_DIRECTORY = 'system_directory_create',
  SYSTEM_UPDATE_DIRECTORY = 'system_directory_update',
  SYSTEM_DELETE_DIRECTORY = 'system_directory_delete',

  SYSTEM_VIEW_PATIENT = 'system_patient_view',
  SYSTEM_CREATE_PATIENT = 'system_patient_create',
  SYSTEM_UPDATE_PATIENT = 'system_patient_update',
  SYSTEM_DELETE_PATIENT = 'system_patient_delete',

  SYSTEM_VIEW_ORDER = 'system_order_view',
  SYSTEM_CREATE_ORDER = 'system_order_create',
  SYSTEM_UPDATE_ORDER = 'system_order_update',
  SYSTEM_DELETE_ORDER = 'system_order_delete',

  SYSTEM_VIEW_ORDER_STATUS = 'system_order_status_view',
  SYSTEM_CREATE_ORDER_STATUS = 'system_order_status_create',
  SYSTEM_UPDATE_ORDER_STATUS = 'system_order_status_update',
  SYSTEM_DELETE_ORDER_STATUS = 'system_order_status_delete',

  SYSTEM_VIEW_ORDER_COMMENT = 'system_order_comment_view',
  SYSTEM_CREATE_ORDER_COMMENT = 'system_order_comment_create',
  SYSTEM_UPDATE_ORDER_COMMENT = 'system_order_comment_update',
  SYSTEM_DELETE_ORDER_COMMENT = 'system_order_comment_delete',

  SYSTEM_VIEW_FIELD = 'system_field_view',
  SYSTEM_CREATE_FIELD = 'system_field_create',
  SYSTEM_UPDATE_FIELD = 'system_field_update',
  SYSTEM_DELETE_FIELD = 'system_field_delete',

  SYSTEM_VIEW_SERVICE = 'system_service_view',
  SYSTEM_CREATE_SERVICE = 'system_service_create',
  SYSTEM_UPDATE_SERVICE = 'system_service_update',
  SYSTEM_DELETE_SERVICE = 'system_service_delete',

  SYSTEM_VIEW_SERVICE_CATEGORY = 'system_service_category_view',
  SYSTEM_CREATE_SERVICE_CATEGORY = 'system_service_category_create',
  SYSTEM_UPDATE_SERVICE_CATEGORY = 'system_service_category_update',
  SYSTEM_DELETE_SERVICE_CATEGORY = 'system_service_category_delete',

  SYSTEM_VIEW_CLINICS = 'system_clinics_view',
  SYSTEM_CREATE_CLINICS = 'system_clinics_create',
  SYSTEM_UPDATE_CLINICS = 'system_clinics_update',
  SYSTEM_DELETE_CLINICS = 'system_clinics_delete',

  SYSTEM_VIEW_ROLES = 'system_roles_view',
  SYSTEM_CREATE_ROLES = 'system_roles_create',
  SYSTEM_UPDATE_ROLES = 'system_roles_update',
  SYSTEM_DELETE_ROLES = 'system_roles_delete'
}

export enum CABINET_PERMISSIONS {
  CABINET_MY_COMPANY_EDIT = 'my_company_update',

  CABINET_VIEW_CLINICS = 'clinics_view',
  CABINET_CREATE_CLINICS = 'clinics_create',
  CABINET_UPDATE_CLINICS = 'clinics_update',
  CABINET_DELETE_CLINICS = 'clinics_delete',

  CABINET_VIEW_SERVICE = 'service_view',
  CABINET_TOGGLE_SERVICE = 'service_toggle',

  CABINET_VIEW_ORDER = 'order_view',
  CABINET_CREATE_ORDER = 'order_create',
  CABINET_UPDATE_ORDER = 'order_update',
  CABINET_DELETE_ORDER = 'order_delete',

  CABINET_UPDATE_ORDER_RESPONSIBLE = 'order_responsible_update',
  CABINET_UPDATE_ORDER_EXECUTOR = 'order_executor_update',
  CABINET_UPDATE_ORDER_STATUS = 'order_status_update',
  CABINET_UPDATE_ORDER_DESCRIPTION = 'order_description_update',
  CABINET_UPDATE_ORDER_FINISH_AT = 'order_finish_at_update',

  CABINET_VIEW_ORDER_STATUS = 'order_status_view',

  CABINET_VIEW_ORDER_COMMENT = 'order_comment_view',
  CABINET_CREATE_ORDER_COMMENT = 'order_comment_create',
  CABINET_UPDATE_ORDER_COMMENT = 'order_comment_update',
  CABINET_DELETE_ORDER_COMMENT = 'order_comment_delete',

  CABINET_VIEW_PATIENT = 'patient_view',
  CABINET_CREATE_PATIENT = 'patient_create',
  CABINET_UPDATE_PATIENT = 'patient_update',
  CABINET_DELETE_PATIENT = 'patient_delete'
}
