export interface ICompany {
  id: number;
  title: string;
  type: ICompanyType;
  is_verified: boolean;
  image: string;
  metadata: ICompanyMetadata;

  children?: ICompany[];
  parent?: ICompany;
}

export interface ICompanyType {
  id: number;
  title: string;
}
export interface ICreateCompanyDto {
  id?: number;
  title: string;
  type_id: number;
  is_verified: boolean;
  metadata: ICompanyMetadata;
  image?: string;
  parent_id?: number | null;
}
export interface ICompanyMetadata {
  address?: '';
  phone?: '';
  email?: '';
}
export const COMPANY_TYPES = {
  LABORATORY: 1,
  PHARMACY: 2
};
