export interface IDirectory {
  id: number;
  title: string;
  options: string[];
}

export interface IDirectoryCreateDto {
  id?: number;
  title: string;
  options: string[];
}
