export interface IWebhook {
  id: number;
  event_type: WebhookEvents;
  url: string;
  crypto?: string;
  title: string;
  createdAt: Date;
}
export interface ICreateWebhook {
  type: WebhookEvents;
  url: string;
  title: string;
}
export enum WebhookEvents {
  NEW_PATIENT = 'NEW_PATIENT',
  UPDATE_ORDER = 'UPDATE_ORDER'
}
export const WebhookEventsList = [
  { label: 'Новый пациент', value: WebhookEvents.NEW_PATIENT },
  { label: 'Обновление заказа', value: WebhookEvents.UPDATE_ORDER }
];
export const WebhookEventsLabels: Record<WebhookEvents, string> = {
  [WebhookEvents.NEW_PATIENT]: 'Новый пациент',
  [WebhookEvents.UPDATE_ORDER]: 'Обновление заказа'
};
