export interface IPatient {
  uid?: string;
  fio: string;
  fullinfo?: string;
  phone: string;
  dob: Date | string;
}
