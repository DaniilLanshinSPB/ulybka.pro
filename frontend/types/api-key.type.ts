export interface IApiKey {
  id: number;
  title: string;
  dispose: boolean;
  key?: string;
  createdAt: Date;
  updatedAt: Date;
}
export interface ICreateApiKey {
  title: string;
}
