export interface IField {
  id: number;
  active: boolean;
  group: FieldGroup;
  technical_name: string;
  name: string;
  multiple: boolean;
  sort: number;
  type: FieldType;
  placeholder: string;
  parent_id: number | null;
  directory_id?: number | null;
  directory?: IDirectory;
  children?: IField[];
  value?: string | string[];
  metadata?: IFieldMetadata;
  display_conditions?: string | null;
  required: boolean;
}

export interface IFieldMetadata {
  extensions?: string;
  class?: string;
}
export interface ICreateFieldDto {
  id?: number;
  active: boolean;
  group: FieldGroup;
  technical_name: string;
  name: string;
  multiple: boolean;
  placeholder: string;
  type: FieldType;
  directory_id?: number | null;
  parent_id?: number | null;
  sort: number;
  metadata?: IFieldMetadata;
  display_conditions?: string | null;
  required: boolean;
}

export interface IFilterFieldDto {
  service_id?: number[];
}

export enum FieldType {
  INPUT = 'INPUT',
  BOOLEAN = 'BOOLEAN',
  DATE = 'DATE',
  OPTION = 'OPTION',
  IMAGE = 'IMAGE',
  TOOTH = 'TOOTH',
  TOOTH_DETAILS = 'TOOTH_DETAILS',
  COLOR = 'COLOR',
  FILE = 'FILE',
  TITLE = 'TITLE',
  DELIMITER = 'DELIMITER',
  TAB = 'TAB'
}
export const FIELD_TYPE_TITLES: Record<any, string> = {
  [FieldType.INPUT]: 'Текстовое поле',
  [FieldType.OPTION]: 'Список',
  [FieldType.IMAGE]: 'Изображение',
  [FieldType.TOOTH]: 'Зуб',
  [FieldType.TOOTH_DETAILS]: 'Детали зуба',
  [FieldType.COLOR]: 'Цвет',
  [FieldType.FILE]: 'Файл',
  [FieldType.TITLE]: 'Заголовок',
  [FieldType.DELIMITER]: 'Разделитель',
  [FieldType.TAB]: 'Вкладка',
  [FieldType.DATE]: 'Дата',
  [FieldType.BOOLEAN]: 'Логическое'
};

export const FIELD_COLORS: any[] = {
  BL1: '#F8F7F4',
  BL2: '#F1F0EA',
  BL3: '#E8E6DE',
  BL4: '#E0DED5',
  A1: '#F4E7DA',
  A2: '#F0E2D3',
  A3: '#EBDFC8',
  'A3.5': '#E6D8C0',
  A4: '#E1D3B8',
  B1: '#F6F0E5',
  B2: '#F1EBD9',
  B3: '#EBE4C8',
  B4: '#E5DDBA',
  C1: '#E5E1D6',
  C2: '#D9D4C7',
  C3: '#D3CBBF',
  C4: '#C8C1B2',
  D1: '#f1efec',
  D2: '#E8DFD1',
  D3: '#DDCEBA',
  D4: '#D3C1A9'
};

export enum FieldGroup {
  GENERAL = 'GENERAL',
  EXTENDED = 'EXTENDED'
}
