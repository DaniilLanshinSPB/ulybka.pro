export enum LogType {
  SYSTEM = 'SYSTEM',
  COMMENT = 'COMMENT',
  ORDER = 'ORDER',
  PATIENT = 'PATIENT',
  TELEGRAM = 'TELEGRAM'
}
export const LOG_TYPE_LABELS: Record<LogType, { label: string; severity: string }> = {
  [LogType.SYSTEM]: { label: 'Система', severity: 'warn' },
  [LogType.COMMENT]: { label: 'Комментарии', severity: 'success' },
  [LogType.ORDER]: { label: 'Заказ', severity: 'info' },
  [LogType.PATIENT]: { label: 'Пациент', severity: 'danger' },
  [LogType.TELEGRAM]: { label: 'Телеграм', severity: 'primary' }
};

export const NOTICE_TYPE_LABELS: Record<LogType, { label: string; severity: string }> = {
  [LogType.SYSTEM]: { label: 'Получать системные уведомления', severity: 'warn' },
  [LogType.COMMENT]: { label: 'Получать уведомления о комментариях', severity: 'success' },
  [LogType.ORDER]: { label: 'Получать уведомления о заказах', severity: 'info' },
  [LogType.PATIENT]: { label: 'Получать уведомления о пациентах', severity: 'danger' },
  [LogType.TELEGRAM]: { label: 'Получать уведомления в телеграм', severity: 'primary' }
};

export enum EntityType {
  User = 'User',
  Status = 'Status',
  Permissions = 'Permissions',
  RolePermissions = 'RolePermissions',
  Role = 'Role',
  Company = 'Company',
  Clinic = 'Clinic',
  CompanyType = 'CompanyType',
  Directory = 'Directory',
  Patient = 'Patient',
  Order = 'Order',
  OrderComment = 'OrderComment',
  Field = 'Field',
  Service = 'Service',
  CompanyService = 'CompanyService',
  ServiceField = 'ServiceField'
}
export const ENTITY_TYPE_LABELS: Record<EntityType, { label: string; severity: string }> = {
  [EntityType.User]: { label: 'Пользователь', severity: 'warn' },
  [EntityType.Status]: { label: 'Статус', severity: 'success' },
  [EntityType.Permissions]: { label: 'Разрешения', severity: 'info' },
  [EntityType.RolePermissions]: { label: 'Ролевые разрешения', severity: 'danger' },
  [EntityType.Role]: { label: 'Роль', severity: 'primary' },
  [EntityType.Company]: { label: 'Компания', severity: 'secondary' },
  [EntityType.Clinic]: { label: 'Клиника', severity: 'contrast' },
  [EntityType.CompanyType]: { label: 'Тип компании', severity: 'warn' },
  [EntityType.Directory]: { label: 'Директория', severity: 'success' },
  [EntityType.Patient]: { label: 'Пациент', severity: 'info' },
  [EntityType.Order]: { label: 'Заказ', severity: 'danger' },
  [EntityType.OrderComment]: { label: 'Комментарии к заказу', severity: 'primary' },
  [EntityType.Field]: { label: 'Поле', severity: 'secondary' },
  [EntityType.Service]: { label: 'Услуга', severity: 'contrast' },
  [EntityType.CompanyService]: { label: 'Услуги компании', severity: 'warn' },
  [EntityType.ServiceField]: { label: 'Поле услуги', severity: 'success' }
};
export interface ILog {
  id: number;
  user_id: number;
  entity_type: EntityType;
  entity_id: string;
  type: LogType;
  details: string;
  metadata: string;
  created_at: Date;
}
export interface ILogFilter {
  limit: number;
  offset: number;
  search?: string;
  type?: LogType;
  user_id?: number;
  entity_type?: EntityType;
}
export interface INoticeType {
  slug: LogType;
  active: boolean;
}
export function getLinkByEntityType(type: EntityType, id: number | string, metadata: any) {
  const appUrl = useRuntimeConfig().public.appUrl;
  switch (type) {
    case EntityType.User:
      return appUrl + `/admin/users/${metadata.id}`;
    case EntityType.Status:
      return null;
    case EntityType.Permissions:
      return null;
    case EntityType.RolePermissions:
      return null;
    case EntityType.Role:
      return appUrl + `/admin/roles?id=${metadata.id}`;
    case EntityType.Company:
      return appUrl + `/admin/companies/${metadata.id}`;
    case EntityType.Clinic:
      return appUrl + `/admin/companies/${metadata.id}`;
    case EntityType.CompanyType:
      return null;
    case EntityType.Directory:
      return appUrl + `/admin/directories?id=${metadata.id}`;
    case EntityType.Patient:
      return null;
    case EntityType.Order:
      return null;
    case EntityType.OrderComment:
      return null;
    case EntityType.Field:
      return appUrl + `/admin/fields/${metadata.id}`;
    case EntityType.Service:
      return appUrl + `/admin/services/${metadata.id}`;
    case EntityType.CompanyService:
      return null;
    case EntityType.ServiceField:
      return null;
  }
}
