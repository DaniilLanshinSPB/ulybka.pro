export interface IOrder {
  id: number;
  title: string;
  description: string;
  created_at: string;
  finish_at: Date | null;
  updated_at: Date;
  deleted_at: Date;
  delete_reason: string;
  status: IOrderStatus;
  patient: IPatient;
  company_id?: number;
  customer: IUser;
  creator: IUser;
  responsible: IUser | null;
  executor: IUser | null;
  comments?: IOrderComment[];
  order_services?: IOrderService[];
  status_history: {
    id: number;
    name: string;
    created_at: Date;
  }[];
  logs: {
    id: number;
    title: string;
    created_at: Date;
  }[];
}

export interface IOrderStatus {
  id: number;
  sort: number;
  slug: string;
  name: string;
}
export interface IOrderMetadata {
  id: number;
  name: string;
  value: any;
  type: typeof FieldType;
}

export interface IOrderCreate {
  id?: number;
  description: string;
  finish_at: Date | undefined;
  patient: IPatient | undefined;
  customer: IUser | undefined;
  responsible: IUser | undefined;
  executor: IUser | undefined;
}
export interface IUpdateOrderDto {
  id?: number;
  description: string;
  patient: IPatient;
  customer_company_id?: number;
  customer_id?: number | null;
  responsible_id?: number | null;
  executor_id?: number | null;
  status_id?: number;
  finish_at?: Date;
  services?: {
    title: string;
    service_id: number;
    metadata: any;
  }[];
}

export interface ICreateOrderDto extends IUpdateOrderDto {}

export interface IOrderFilter {
  created_at_range?: [Date, Date] | [];
  finished_at_range?: [Date, Date] | [];
  statuses?: IOrderStatus[];
  services?: IService[];
  patients?: IPatient[];
  customers?: any[];
  responsibles?: any[];
  executors?: any[];
  creators?: any[];
}

export interface IOrderFilterDto {
  created_at_range?: {
    from?: Date;
    to?: Date;
  };
  finished_at_range?: {
    from?: Date;
    to?: Date;
  };
  status_id?: number[];
  service_id?: number[];
  customer_id?: number[];
  responsible_id?: number[];
  executor_id?: number[];
  creator_id?: number[];
  patient_uid?: string[];
  deleted?: boolean;
}

export interface IOrderComment {
  id: number;
  order: IOrder;
  message: string;
  user: IOrderUser;
  created_at: Date;
  updated_at: Date;
}

export interface IOrderCommentCreateDto {
  message: string;
}
export interface IOrderService {
  id?: number | string;
  title?: string;
  order?: IOrder | null;
  service: IService | null;
  metadata: any;
}

export const FINISHED_STATUS = 'finished';
