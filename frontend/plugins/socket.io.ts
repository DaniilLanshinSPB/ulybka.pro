import { io } from 'socket.io-client';
import { useNotificationsStore } from '~/stores/notifications.store';
export default defineNuxtPlugin(async (nuxtApp) => {
  const socketUrl = useRuntimeConfig().public.socketUrl as string;
  const token = localStorage.getItem('access_token');

  if (token) {
    const socket = io(socketUrl, {
      autoConnect: true,
      auth: {
        token
      },
      reconnection: true,
      reconnectionAttempts: 5,
      reconnectionDelay: 1000,
      reconnectionDelayMax: 5000,
      randomizationFactor: 0.5
    });
    socket.connect();
    const notificationsStore = useNotificationsStore();
    await notificationsStore.fetchUnread();

    socket.on('notifications', ({ message }) => {
      notificationsStore.addNotification(message);
    });

    socket.on('connect_error', (error) => {
      console.info('WebSocket connection error');
    });

    socket.on('connect', () => {
      console.log('WebSocket connected');
    });

    socket.on('disconnect', (reason) => {
      console.warn('WebSocket disconnected:', reason);
    });

    socket.on('reconnect_failed', () => {
      console.info('WebSocket reconnection failed');
    });
  }
});
