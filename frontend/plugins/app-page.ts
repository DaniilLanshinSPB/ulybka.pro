import { defineNuxtPlugin } from '#app';

import AppPage from '~/layouts/AppPage.vue';

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(AppPage);
});
